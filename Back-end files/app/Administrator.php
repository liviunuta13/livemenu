<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property ProfilePromote[] $profilePromotes
 */
class Administrator extends Authenticatable
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profilePromotes()
    {
        return $this->hasMany('App\ProfilePromote');
    }
}
