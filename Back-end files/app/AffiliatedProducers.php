<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $producer_profile_id
 * @property ProducerProfile $producerProfile
 * @property AffiliatedProductsAd[] $affiliatedProductsAds
 */
class AffiliatedProducer extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['confirmed', 'affiliated_product_ad_id', 'producer_profile_from_id', 'producer_profile_for_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileFrom()
    {
        return $this->belongsTo('App\ProducerProfile', 'producer_profile_from_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileFor()
    {
        return $this->belongsTo('App\ProducerProfile', 'producer_profile_for_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function affiliatedProductAd()
    {
        return $this->hasMany('App\AffiliatedProductsAd', 'affiliated_product_ad_id');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function affiliatedProductsAds()
    // {
    //     return $this->hasMany('App\AffiliatedProductsAd', 'affiliated_producers_id');
    // }
}
