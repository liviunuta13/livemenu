<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $affiliated_producer_id
 * @property int $product_id
 * @property int $final_amount_unit_id
 * @property int $current_amount_unit_id
 * @property int $currency_id
 * @property int $location_id
 * @property string $title
 * @property string $description
 * @property int $final_amount
 * @property int $current_amount
 * @property int $final_price
 * @property AffiliatedProducer $affiliatedProducer
 * @property Currency $currency
 * @property Location $location
 * @property Product $product
 * @property Unit $unit
 * @property Unit $unit
 */
class AffiliatedProductsAd extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['product_name', 'producer_id', 'product_id', 'final_amount_unit_id', 'current_amount_unit_id', 'currency_id', 'location_id', 'title', 'description', 'final_amount', 'current_amount', 'final_price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producer()
    {
        return $this->belongsTo('App\ProducerProfile', 'producer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitFinal()
    {
        return $this->belongsTo('App\Unit', 'final_amount_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitCurrent()
    {
        return $this->belongsTo('App\Unit', 'current_amount_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliatedProducersRequests()
    {
        return $this->hasMany('App\AffiliatedProducer', 'affiliated_product_ad_id');
    }
}
