<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $attraction_id
 * @property string $picture
 * @property TouristAttraction $touristAttraction
 */
class AttractionPicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['attraction_id', 'picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function touristAttraction()
    {
        return $this->belongsTo('App\TouristAttraction', 'attraction_id');
    }
}
