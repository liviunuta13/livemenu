<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $from_id
 * @property int $to_id
 * @property int $currency_id
 * @property int $amount
 * @property string $description
 * @property string $type
 * @property Currency $currency
 * @property User $user
 * @property User $user
 */
class Balance extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['from_id', 'to_id', 'currency_id', 'amount', 'description', 'type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userFrom()
    {
        return $this->belongsTo('App\User', 'from_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userTo()
    {
        return $this->belongsTo('App\User', 'to_id');
    }
}
