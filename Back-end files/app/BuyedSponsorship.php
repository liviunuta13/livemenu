<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_id
 * @property int $producer_profile_id
 * @property int $for_restaurant_location_id
 * @property int $for_producer_profile_id
 * @property int $for_profesionist_profile_id
 * @property int $restaurant_profile_id
 * @property int $for_restaurant_profile_id
 * @property ProducerProfile $producerProfile
 * @property ProducerProfile $producerProfile
 * @property ProducerProfile $producerProfile
 * @property ProfesionistProfile $profesionistProfile
 * @property RestaurantLocation $restaurantLocation
 * @property RestaurantLocation $restaurantLocation
 */
class BuyedSponsorship extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['restaurant_location_id', 'producer_profile_id', 'for_restaurant_location_id', 'for_producer_profile_id', 'for_profesionist_profile_id', 'restaurant_profile_id', 'for_restaurant_profile_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileFor()
    {
        return $this->belongsTo('App\ProducerProfile', 'for_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfile()
    {
        return $this->belongsTo('App\ProducerProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    // public function producerProfile()
    // {
    //     return $this->belongsTo('App\ProducerProfile', 'for_producer_profile_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile', 'for_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationFor()
    {
        return $this->belongsTo('App\RestaurantLocation', 'for_restaurant_location_id');
    }
}
