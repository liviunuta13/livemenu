<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $county_id
 * @property string $name
 * @property County $county
 * @property ProducerProfileLocation[] $producerProfileLocations
 * @property ProfesionistProfile[] $profesionistProfiles
 * @property ProfesstionalHistoric[] $professtionalHistorics
 * @property RestaurantLocation[] $restaurantLocations
 */
class City extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['county_id', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function county()
    {
        return $this->belongsTo('App\County');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producerProfileLocations()
    {
        return $this->hasMany('App\ProducerProfileLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profesionistProfiles()
    {
        return $this->hasMany('App\ProfesionistProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function professtionalHistorics()
    {
        return $this->hasMany('App\ProfesstionalHistoric');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocations()
    {
        return $this->hasMany('App\RestaurantLocation');
    }
}
