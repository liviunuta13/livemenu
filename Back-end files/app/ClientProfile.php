 <?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $profile_picture
 * @property boolean $suspended
 * @property ClientProfileHasMusicType[] $clientProfileHasMusicTypes
 * @property Post[] $posts
 * @property Post[] $posts
 * @property RestaurantStaff[] $restaurantStaffs
 * @property User[] $users
 */
class ClientProfile extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['profile_picture', 'suspended'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientProfileHasMusicTypes()
    {
        return $this->hasMany('App\ClientProfileHasMusicType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Post', 'from_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsTo()
    {
        return $this->hasMany('App\Post', 'to_client_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantStaffs()
    {
        return $this->hasMany('App\RestaurantStaff');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany('App\User');
    }
}
