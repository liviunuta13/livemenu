<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $client_profile_id
 * @property int $music_type_id
 * @property ClientProfile $clientProfile
 * @property MusicType $musicType
 */
class ClientProfileHasMusicType extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'client_profile_has_music_type';

    /**
     * @var array
     */
    protected $fillable = ['client_profile_id', 'music_type_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientProfile()
    {
        return $this->belongsTo('App\ClientProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function musicType()
    {
        return $this->belongsTo('App\MusicType');
    }
}
