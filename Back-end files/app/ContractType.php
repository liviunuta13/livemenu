<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property JobOffer[] $jobOffers
 * @property RestaurantStaff[] $restaurantStaffs
 * @property WorkRequest[] $workRequests
 */
class ContractType extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobOffers()
    {
        return $this->hasMany('App\JobOffer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantStaffs()
    {
        return $this->hasMany('App\RestaurantStaff');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workRequests()
    {
        return $this->hasMany('App\WorkRequest');
    }
}
