<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property County[] $counties
 * @property ProducerProfileLocation[] $producerProfileLocations
 * @property RestaurantLocation[] $restaurantLocations
 */
class Country extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function counties()
    {
        return $this->hasMany('App\County');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producerProfileLocations()
    {
        return $this->hasMany('App\ProducerProfileLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocations()
    {
        return $this->hasMany('App\RestaurantLocation');
    }
}
