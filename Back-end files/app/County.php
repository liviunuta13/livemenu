<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property Country $country
 * @property City[] $cities
 * @property ProducerProfileLocation[] $producerProfileLocations
 * @property RestaurantLocation[] $restaurantLocations
 */
class County extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['country_id', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producerProfileLocations()
    {
        return $this->hasMany('App\ProducerProfileLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocations()
    {
        return $this->hasMany('App\RestaurantLocation');
    }
}
