<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property AffiliatedProductsAd[] $affiliatedProductsAds
 * @property Balance[] $balances
 * @property Ingredient[] $ingredients
 * @property JobOffer[] $jobOffers
 * @property Menu[] $menuses
 * @property Product[] $products
 * @property RestaurantStaff[] $restaurantStaffs
 * @property TipsType[] $tipsTypes
 */
class Currency extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliatedProductsAds()
    {
        return $this->hasMany('App\AffiliatedProductsAd');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balances()
    {
        return $this->hasMany('App\Balance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ingredients()
    {
        return $this->hasMany('App\Ingredient');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobOffers()
    {
        return $this->hasMany('App\JobOffer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menuses()
    {
        return $this->hasMany('App\Menu', 'restaurant_currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantStaffs()
    {
        return $this->hasMany('App\RestaurantStaff', 'salary_currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tipsTypes()
    {
        return $this->hasMany('App\TipsType');
    }
}
