<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_id
 * @property mixed $conditions
 * @property int $menu_id
 * @property RestaurantLocation $restaurantLocation
 */
class DailyMenuAvailability extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name', 'picture', 'begin_date', 'end_date', 'days_of_week', 'breakfast_begin_time', 'breakfast_end_time', 'lunch_begin_time', 'lunch_end_time', 'dinner_begin_time', 'dinner_end_time', 'breakfast_menus_id', 'lunch_menus_id', 'dinner_menus_id', 'restaurant_location_id', 'price', 'currency_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }
}
