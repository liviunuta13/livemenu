<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationAndTraining extends Model
{
    use SoftDeletes;

    protected $fillable = ['begin_date', 'end_date', 'title_gain', 'institution_name',
        'city_id', 'country_id','description','profesionist_profile_id'];

    public function professionistProfile(){
        $this->belongsTo('App\ProfesionistProfile','profesionist_profile_id');
    }

    public function cities(){
        $this->hasOne('App\City','city_id');
    }
    public function countries(){
        $this->hasOne('App\Country','country_id');
    }

}
