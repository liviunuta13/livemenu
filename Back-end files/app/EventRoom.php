<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_id
 * @property int $surface_unit_id
 * @property int $restaurant_location_profile_id
 * @property boolean $owns_event_room
 * @property string $descripton
 * @property int $rooms_number
 * @property int $tables_number
 * @property int $persons_number
 * @property int $surface
 * @property int $disponible_rooms_number
 * @property RestaurantLocation $restaurantLocation
 * @property RestaurantProfile $restaurantProfile
 * @property Unit $unit
 * @property EventRoomHasFacility[] $eventRoomHasFacilities
 * @property EventRoomsHasMenu[] $eventRoomsHasMenus
 */
class EventRoom extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['restaurant_location_id', 'surface_unit_id', 'restaurant_location_profile_id', 'owns_event_room', 'descripton', 'rooms_number', 'tables_number', 'persons_number', 'surface', 'disponible_rooms_number'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfile()
    {
        return $this->belongsTo('App\RestaurantProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo('App\Unit', 'surface_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventRoomHasFacilities()
    {
        return $this->hasMany('App\EventRoomHasFacility');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventRoomsHasMenus()
    {
        return $this->hasMany('App\EventRoomsHasMenu');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomPictures()
    {
        return $this->hasMany('App\EventRoomPicture');
    }
}
