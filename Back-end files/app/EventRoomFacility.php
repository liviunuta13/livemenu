<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property EventRoomHasFacility[] $eventRoomHasFacilities
 */
class EventRoomFacility extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name', 'icon'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventRoomHasFacilities()
    {
        return $this->hasMany('App\EventRoomHasFacility', 'facility_id');
    }
}
