<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $event_room_id
 * @property int $facility_id
 * @property EventRoomFacility $eventRoomFacility
 * @property EventRoom $eventRoom
 */
class EventRoomHasFacility extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_room_has_facility';

    /**
     * @var array
     */
    protected $fillable = ['event_room_id', 'facility_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventRoomFacility()
    {
        return $this->belongsTo('App\EventRoomFacility', 'facility_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventRoom()
    {
        return $this->belongsTo('App\EventRoom');
    }
}
