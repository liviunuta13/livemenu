<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventRoomPicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['event_room_id', 'picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventRoom()
    {
        return $this->belongsTo('App\EventRoom');
    }
}
