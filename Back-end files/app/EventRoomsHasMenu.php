<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $event_room_id
 * @property int $menu_id
 * @property EventRoom $eventRoom
 * @property Menu $menu
 */
class EventRoomsHasMenu extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_rooms_has_menu';

    /**
     * @var array
     */
    protected $fillable = ['event_room_id', 'menu_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventRoom()
    {
        return $this->belongsTo('App\EventRoom');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }
}
