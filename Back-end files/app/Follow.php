<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $from_user_id
 * @property int $from_profesionist_profile_id
 * @property int $to_restaurant_profile_id
 * @property int $to_profesionist_profile_id
 * @property int $to_producer_profile_id
 * @property ProducerProfile $producerProfile
 * @property ProfesionistProfile $profesionistProfile
 * @property ProfesionistProfile $profesionistProfile
 * @property RestaurantProfile $restaurantProfile
 * @property User $user
 */
class Follow extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'from_user_id',
        'to_profesionist_profile_id',
        'to_restaurant_location_id',
        'to_producer_location_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerLocation()
    {
        return $this->belongsTo('App\ProducerProfileLocation', 'to_producer_location_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile', 'to_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation', 'to_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'from_user_id');
    }
}
