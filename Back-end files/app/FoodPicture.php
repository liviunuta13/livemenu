<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $menu_id
 * @property string $picture
 * @property Menu $menu
 */
class FoodPicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['menu_id', 'picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }
}
