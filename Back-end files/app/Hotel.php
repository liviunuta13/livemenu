<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $stars
 * @property int $restaurant_location_id
 * @property string $email
 * @property string $phone_number
 * @property int $capacity
 * @property HotelFacility[] $hotelFacilities
 * @property HotelPicture[] $hotelPictures
 * @property Room[] $rooms
 */
class Hotel extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'stars', 'email', 'phone_number', 'capacity', 'restaurant_location_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hotelHasFacilities()
    {
        return $this->hasMany('App\HotelHasFacility');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hotelPictures()
    {
        return $this->hasMany('App\HotelPicture');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany('App\Room');
    }
}
