<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $hotel_id
 * @property string $name
 * @property Hotel $hotel
 */
class HotelFacility extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name', 'icon'];

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  */
    // public function hotel()
    // {
    //     return $this->belongsTo('App\Hotel');
    // }
}
