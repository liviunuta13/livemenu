<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $hotel_id;
 * @property int $facility_id;
 * @property int $facilities_id;
 * @property HotelFacility $hotelFacility
 * @property Hotel $hotel
 */


class HotelHasFacility extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hotel_has_facility';

    /**
     * @var array
     */
    protected $fillable = ['hotel_id', 'facility_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotelFacility()
    {
        return $this->belongsTo('App\HotelFacility');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }
}
