<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $hotel_id
 * @property string $picture
 * @property Hotel $hotel
 */
class HotelPicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['hotel_id', 'picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }
}
