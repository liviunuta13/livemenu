<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\Administrator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AdminsController extends Controller
{
    public function index() {

        $admins = Administrator::orderBy('name') -> paginate(10);

        return view('/admin/admins/index', compact(
            'admins'
        ));
    }

    public function search(Request $request) {

        $admins = Administrator::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('email', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name') -> paginate(10);

        return view('/admin/admins/index', compact(
            'admins'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'email' => ['required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:administrators,email'],
            'password' => 'required|string|min:8|max:190|regex:/^.*(?=.{3})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|confirmed',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('admins'));
        }

        $admin = new Administrator();

        $admin -> name = $request -> name;

        $admin -> email = $request -> email;

        $admin -> password = bcrypt($request -> password);

        $admin -> save();

        Session::flash('success', [__('The administrator has been added successfully!')]);

        return redirect(route('admins'));
    }

    public function edit(Request $request, $admin_id) {

        $admin = Administrator::find($admin_id);

        if($admin === null) {

            Session::flash('success', [__('Administrator not found!')]);

            return redirect() -> back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'email' => ['sometimes', 'required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:administrators,email,' . $admin -> id],
            'password' => 'sometimes|nullable|string|min:8|max:190|regex:/^.*(?=.{3})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|confirmed',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('admins'));
        }

        $admin -> name = $request -> name ? $request -> name : $admin -> name;

        $admin -> email = $request -> email ? $request -> email : $admin -> email;

        if($request -> password) {

            $admin -> password = bcrypt($request -> password);
        }

        $admin -> save();

        Session::flash('success', [__('The administrator has been edited successfully!')]);

        return redirect(route('admins'));
    }

    public function delete($admin_id) {

        $admin = Administrator::find($admin_id);

        if($admin === null) {

            Session::flash('success', [[__('Administrator not found!')]]);

            return redirect() -> back();
        }

        $admin -> profilePromotes() -> delete();

        $admin -> delete();

        Session::flash('success', [__('The administrator has been deleted!')]);

        return redirect(route('admins'));
    }
}
