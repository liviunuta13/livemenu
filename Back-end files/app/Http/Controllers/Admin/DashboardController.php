<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Hotel;
use App\ProducerProfile;
use App\RestaurantProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index() {

        $count_users = User::count();

        $count_hotels = Hotel::count();

        $count_restaruants = RestaurantProfile::count();

        $count_producers = ProducerProfile::count();

        return view('/admin/dashboard/index', compact(
            'count_users',
            'count_restaruants',
            'count_producers',
            'count_hotels'
        ));
    }
}
