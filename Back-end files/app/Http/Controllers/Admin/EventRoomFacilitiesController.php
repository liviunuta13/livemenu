<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\County;
use App\EventRoomFacility;
use App\EventRoomHasFacility;
use App\HotelFacility;
use App\RestaurantLocationFacility;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EventRoomFacilitiesController extends Controller
{

    public function search(Request $request){
        $event_facilities = EventRoomFacility::where('name', 'LIKE', '%'.$request -> keyword.'%')
            ->orderBy('name')->paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/events_rooms_facilities/index', compact(
            'event_facilities',
            'countries',
            'counties',
            'cities'
        ));

    }

    public function index() {

        $event_facilities = EventRoomFacility::paginate(10);

        return view('/admin/events_rooms_facilities/index', compact(
            'event_facilities'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:2|max:45',
            'icon' => 'required|string|min:3|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }


        $event_facility = new EventRoomFacility();

        $event_facility -> name = $request -> name;

        $event_facility -> icon = $request -> icon;

        $event_facility -> save();

        Session::flash('success',[__('The event room facility was successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request, $event_facility_id) {

        $event_facility = EventRoomFacility::find($event_facility_id);

        if($event_facility === null) {
            Session::flash('errors',[[__('The given event room facility id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:2|max:45',
            'icon' => 'sometimes|required|string|min:3|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }


        $event_facility -> name = $request -> name ? $request -> name : $event_facility -> name;

        $event_facility -> icon = $request -> icon ? $request -> icon : $event_facility -> icon;

        $event_facility-> save();

        Session::flash('success',[__('The event room facility was successfully edited!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $event_facility_id) {

        $event_facility = EventRoomFacility::find($event_facility_id);

        if($event_facility === null) {
            Session::flash('errors',[[__('The given event room facility id is invalid!')]]);
            return redirect()->back();
        }



        $event_facility-> delete();
        Session::flash('success',[__('The event room facility has been deleted successfully!')]);
        return redirect()->back();
    }

}
