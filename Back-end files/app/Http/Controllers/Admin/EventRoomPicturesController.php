<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\County;
use App\EventRoomFacility;
use App\EventRoomHasFacility;
use App\EventRoomPicture;
use App\HotelFacility;
use App\RestaurantLocationFacility;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EventRoomPicturesController extends Controller
{

    public function index() {

        $pictures = EventRoomPicture::paginate(10);

        return view('/admin/events_rooms_pictures/index', compact(
            'pictures'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'event_room_id' => 'required|numeric|exists:event_rooms,id',
            'picture' => 'sometimes|required|file'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }


        $picture = new EventRoomPicture();

        $picture -> event_room_id = $request -> event_room_id;

        if($request -> picture) {

            $file = $request -> file('picture');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $picture -> picture = $base64;
        }


        $picture -> save();

        Session::flash('success',[__('The picture was successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request, $picture_id) {

        $picture = EventRoomPicture::find($picture_id);

        if($picture === null) {
            Session::flash('errors',[[__('The given picture id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'event_room_id' => 'required|numeric|exists:event_rooms,id',
            'picture' => 'sometimes|required|file'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }


        $picture -> event_room_id = $request -> event_room_id ? $request -> event_room_id : $picture -> event_room_id;


        if($request -> picture) {

            $file = $request -> file('picture');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $picture -> picture = $base64;
        }

        $picture-> save();

        Session::flash('success',[__('The picture was successfully edited!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $picture_id) {

        $picture = EventRoomPicture::find($picture_id);

        if($picture === null) {
            Session::flash('errors',[[__('The given picture id is invalid!')]]);
            return redirect()->back();
        }



        $picture-> delete();
        Session::flash('success',[__('The picture has been deleted successfully!')]);
        return redirect()->back();
    }

}
