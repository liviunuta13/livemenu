<?php

namespace App\Http\Controllers\Admin;

use App\EventRoom;
use App\Room;
use App\User;
use App\City;
use App\Country;
use App\County;
use Illuminate\Routing\Controller;
use App\EventRoomPicture;
use App\EventRoomHasFacility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class EventRoomsController extends Controller
{


    public function search(Request $request){
        $event_rooms = EventRoom::where('description', 'LIKE', '%'.$request -> keyword.'%')
            ->orderBy('description')->paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/rooms_events/index', compact(
            'event_rooms',
            'countries',
            'counties',
            'cities'
        ));

    }

    public function index() {

        $event_rooms = EventRoom::paginate(10);

        return view('/admin/rooms_events/index', compact(
            'event_rooms'
        ));
    }


    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'descripton' => 'required|string|min:3|max:45',
            'surface_unit_id' => 'required|numeric|exists:units,id',
            'restaurant_location_id' => 'required|numeric|exists:restaurant_locations,id',
            'restaurant_location_profile_id' => 'required|numeric|exists:restaurant_profiles,id',
            'owns_event_room' => 'required|boolean',
            'rooms_number' => 'required|numeric',
            'tables_number' => 'required|numeric',
            'persons_number' => 'required|numeric',
            'surface' => 'required|string',
            'disponible_rooms_number' => 'required|numeric',
            'facilities_id' => 'required',
            'facilities_id.*' => 'required|numeric|exists:event_room_facilities,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }

        $event_room = new EventRoom();

        $event_room -> owns_event_room = $request -> owns_event_room;

        $event_room -> descripton = $request -> descripton;

        $event_room -> rooms_number = $request -> rooms_number;

        $event_room -> tables_number = $request -> tables_number;

        $event_room -> persons_number = $request -> persons_number;

        $event_room -> surface = $request -> surface;

        $event_room -> surface_unit_id = $request -> surface_unit_id;

        $event_room -> restaurant_location_id = $request -> restaurant_location_id;

        $event_room -> restaurant_location_profile_id = $request -> restaurant_location_profile_id;

        $event_room -> save();

        foreach ($request -> facilities_id as $key => $facility) {

            $event_room_has_facility = new EventRoomHasFacility();

            $event_room_has_facility -> eventRoom() -> associate($event_room);

            $event_room_has_facility -> facility_id = $facility;

            $event_room_has_facility -> save();
        }

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $room_picture = new EventRoomPicture();

                    $room_picture -> eventRoom() -> associate($event_room);

                    $room_picture -> picture = $picture;

                    $room_picture -> save();
                }
            }
        }

        Session::flash('success',[__('The event room was successfully added!')]);
        return redirect()->back();
    }

    // Edit room
    public function save(Request $request, $event_room_id) {
        $event_room = EventRoom::find($event_room_id);

        if($event_room === null) {
            Session::flash('errors', [[__('The event room id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'descripton' => 'required|string|min:3|max:45',
            'surface_unit_id' => 'required|numeric|exists:units,id',
            'restaurant_location_id' => 'required|numeric|exists:restaurant_locations,id',
            'restaurant_location_profile_id' => 'required|numeric|exists:restaurant_profiles,id',
            'owns_event_room' => 'required|boolean',
            'rooms_number' => 'required|numeric',
            'tables_number' => 'required|numeric',
            'persons_number' => 'required|numeric',
            'surface' => 'required|string',
            'disponible_rooms_number' => 'required|numeric',
            'facilities_id' => 'required',
            'facilities_id.*' => 'required|numeric|exists:event_room_facilities,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();

        }
        $event_room -> owns_event_room = $request -> owns_event_room ? $request -> owns_event_room : $request -> owns_event_room;

        $event_room -> descripton = $request -> descripton ? $request -> descripton : $event_room -> descripton;

        $event_room -> rooms_number = $request -> rooms_number ? $request -> rooms_number : $event_room -> rooms_number;

        $event_room -> tables_number = $request -> tables_number ? $request -> tables_number : $event_room -> tables_number;

        $event_room -> persons_number = $request -> persons_number ? $request -> persons_number : $event_room -> persons_number;

        $event_room -> surface = $request -> surface ? $request -> surface : $event_room -> surface;

        $event_room -> surface_unit_id = $request -> surface_unit_id ? $request -> surface_unit_id : $event_room -> surface_unit_id;

        $event_room -> restaurant_location_id = $request -> restaurant_location_id ? $request -> restaurant_location_id : $event_room -> restaurant_location_id;

        $event_room -> restaurant_location_profile_id = $request -> restaurant_location_profile_id ? $request -> restaurant_location_profile_id : $event_room -> restaurant_location_profile_id;



        $event_room -> save();

        if($request -> facilities_id) {

            foreach ($request -> facilities_id as $key => $facility) {

                if(!(EventRoomHasFacility::where('event_room_id', $event_room -> id) -> where('facility_id', $facility) -> first())) {

                    $event_room_has_facility = new EventRoomHasFacility();

                    $event_room_has_facility -> eventRoom() -> associate($event_room);

                    $event_room_has_facility -> facility_id = $facility;

                    $event_room_has_facility -> save();
                }
            }
        }

        if(isset($request -> delete_facilities_id)) {

            foreach ($request -> delete_facilities_id as $key => $facility_id) {

                $room_has_facility = EventRoomHasFacility::find($facility_id);

                if($room_has_facility && $room_has_facility -> event_room_id == $event_room -> id) {

                    $room_has_facility -> forceDelete();
                }
            }
        }

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $room_picture = new EventRoomPicture();

                    $room_picture -> eventRoom() -> associate($event_room);

                    $room_picture -> picture = $picture;

                    $room_picture -> save();

                }
            }
        }

        $event_room -> unit;

        $event_room -> roomPictures;

        foreach ($event_room -> eventRoomHasFacilities as $key => $facility) {

            $facility -> eventRoomFacility;
        }

        foreach ($event_room -> eventRoomsHasMenus as $key => $menu) {

            $menu -> menu;
        }


        Session::flash('success', [__('The event room was successfully updated!')]);
        return redirect()->back();
    }

    public function delete( $event_room_id) {
        $event_room = EventRoom::find($event_room_id);

        if($event_room === null) {
            Session::flash('errors', [[__('The event room id is invalid!')]]);
            return redirect()->back();

        }

        $event_room -> roomPictures() -> delete();

        $event_room -> eventRoomHasFacilities() -> delete();

        $event_room -> eventRoomsHasMenus() -> delete();

        $event_room -> delete();

        Session::flash('success', [__('The event room and all data associated has been deleted!')]);
        return redirect()->back();
    }

}
