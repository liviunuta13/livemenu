<?php

namespace App\Http\Controllers\Admin;

use App\Room;
use App\User;
use App\City;
use App\Country;
use App\County;
use App\Hotel;
use Illuminate\Routing\Controller;
use App\RoomPicture;
use App\RoomBedType;
use App\RoomFacility;
use App\RoomHasBedType;
use App\RoomHasFacility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HotelRoomsController extends Controller
{


    public function search(Request $request){
        $rooms = Room::where('name', 'LIKE', '%'.$request -> keyword.'%')
            ->orderBy('name')->paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/hotels_rooms/index', compact(
            'rooms',
            'countries',
            'counties',
            'cities'
        ));

    }

    public function index() {

        $rooms = Room::paginate(10);

        return view('/admin/hotels_rooms/index', compact(
            'rooms'
        ));
    }


    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'surface' => 'required|min:1',
            'unit_id' => 'required|numeric|exists:units,id',
            'hotel_id' => 'required|numeric|exists:hotels,id',
            'seats' => 'required|numeric|min:1',
            'simple_bed_number' => 'sometimes|required|numeric|min:1',
            'double_bed_number' => 'sometimes|required|numeric|min:1',
            'triple_bed_number' => 'sometimes|required|numeric|min:1',
            'additional_seats' => 'sometimes|required|numeric|min:1',
            'facilities_id' => 'required',
            'facilities_id.*' => 'required|numeric|exists:room_facilities,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }

        $room = new Room();

        $room -> name = $request -> name;

        $room ->  surface = $request -> surface;

        $room ->  unit_id = $request -> unit_id;

        $room ->  hotel_id = $request -> hotel_id;

        $room -> seats = $request -> seats;

        $room -> additional_seats = $request -> additional_seats ? $request -> additional_seats : null;

//        $room -> hotel() -> associate($hotel);

        $room -> save();

        $room -> unit;

        if($request -> simple_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 1) -> first() -> id;

            $room_has_bed -> number = $request -> simple_bed_number;

            $room_has_bed -> save();
        }

        if($request -> double_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 2) -> first() -> id;

            $room_has_bed -> number = $request -> double_bed_number;

            $room_has_bed -> save();
        }

        if($request -> triple_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 3) -> first() -> id;

            $room_has_bed -> number = $request -> triple_bed_number;

            $room_has_bed -> save();
        }


        foreach ($request -> facilities_id as $key => $facility_id) {
                $room_has_facility = new RoomHasFacility();

                $room_has_facility->room()->associate($room);

                $room_has_facility->room_facility_id = $facility_id;

                $room_has_facility->hotel_id = $facility_id;

                $room_has_facility->save();
            }


        $room -> hotel;

        foreach ($room -> roomHasBedTypes as $key => $bed_type) {

            $bed_type -> bedType;
        }

        $room -> roomHasFacilities;

        Session::flash('success',[__('The room was successfully added!')]);
            return redirect()->back();
    }

    // Edit room
    public function save(Request $request, $room_id) {
        $room = Room::find($room_id);

        if($room === null) {
            Session::flash('errors', [[__('The room id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'surface' => 'sometimes|required|min:1',
            'unit_id' => 'sometimes|required|numeric|exists:units,id',
            'hotel_id' => 'required|numeric|exists:hotels,id',
            'seats' => 'sometimes|required|numeric|min:1',
            'simple_bed_number' => 'sometimes|required|numeric|min:1',
            'double_bed_number' => 'sometimes|required|numeric|min:1',
            'triple_bed_number' => 'sometimes|required|numeric|min:1',
            'additional_seats' => 'sometimes|required|numeric|min:1',
            'facilities_id' => 'sometimes|required',
            'facilities_id.*' => 'sometimes|required|numeric|exists:room_facilities,id',
            'delete_facilities' => 'sometimes|required',
            'delete_facilities.*' => 'numeric|exists:room_has_facility,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();

        }

        $room -> name = $request -> name ? $request -> name : $room -> name;

        $room -> surface = $request -> surface ? $request -> surface : $room -> surface;

        $room ->  unit_id = $request -> unit_id ? $request -> unit_id : $room -> unit_id;

        $room -> seats = $request -> seats ? $request -> seats : $room -> seats;

        $room -> additional_seats = $request -> additional_seats ? $request -> additional_seats : $room -> additional_seats;

        $room -> save();

        $room -> unit;

        if(isset($request -> simple_bed_number) && $request -> simple_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 1) -> first() -> id;

            $room_has_bed -> number = $request -> simple_bed_number;

            $room_has_bed -> save();
        }

        if(isset($request -> double_bed_number) && $request -> double_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 2) -> first() -> id;

            $room_has_bed -> number = $request -> double_bed_number;

            $room_has_bed -> save();
        }

        if(isset($request -> triple_bed_number) && $request -> triple_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 3) -> first() -> id;

            $room_has_bed -> number = $request -> triple_bed_number;

            $room_has_bed -> save();
        }

        if(isset($request -> facilities)) {

            foreach ($request -> facilities as $key => $facility) {

                if(!(RoomHasFacility::where('room_id', $room -> id) -> where('room_facility_id', $facility) -> first())) {

                    $room_has_facility = new RoomHasFacility();

                    $room_has_facility -> room() -> associate($room);

                    $room_has_facility -> room_facility_id = $facility;

                    $room_has_facility -> save();
                }
            }
        }

        if(isset($request -> delete_facilities)) {

            foreach ($request -> delete_facilities as $key => $facility_id) {

                $room_has_facility = RoomHasFacility::find($facility_id);

                if($room_has_facility && $room_has_facility -> room_id == $room -> id) {

                    $room_has_facility -> forceDelete();
                }
            }
        }

        if(($request -> pictures) > 0) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $room_picture = new RoomPicture();

                    $room_picture -> room() -> associate($room);

                    $room_picture -> picture = $picture;

                    $room_picture -> save();
                }
            }
        }

        $room -> hotel;

        foreach ($room -> roomHasBedTypes as $key => $bed_type) {

            $bed_type -> bedType;
        }

        foreach ($room -> roomHasFacilities as $key => $value) {

            $value -> roomFacility;
        }

        $room -> roomPictures;


        Session::flash('success', [__('The room was successfully updated!')]);
        return redirect()->back();
    }

    public function delete( $room_id) {
        $room = Room::find($room_id);

        if($room === null) {
            Session::flash('errors', [[__('The room id is invalid!')]]);
            return redirect()->back();

        }


        $room -> delete();

        Session::flash('success', [__('The room and all data associated has been deleted!')]);
        return redirect()->back();
    }

}
