<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\County;
use App\Ingredient;
use App\IngredientAllergy;
use App\RestaurantLocationFacility;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class IngredientAllergiesController extends Controller
{

    public function search(Request $request){
        $allergies = IngredientAllergy::where('name', 'LIKE', '%'.$request -> keyword.'%')
            ->orderBy('name')->paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/ingredients_allergies/index', compact(
            'allergies',
            'countries',
            'counties',
            'cities'
        ));

    }

    public function index() {

        $allergies = IngredientAllergy::paginate(10);

        return view('/admin/ingredients_allergies/index', compact(
            'allergies'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:2|max:45',

        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }


        $allergy = new IngredientAllergy();

        $allergy -> name = $request -> name;

        $allergy -> save();

        Session::flash('success',[__('The allergy ingredient was successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request, $allergy_id) {

        $allergy = IngredientAllergy::find($allergy_id);

        if($allergy === null) {
            Session::flash('errors',[[__('The given allergy ingredient id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:2|max:45',
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }


        $allergy -> name = $request -> name ? $request -> name : $allergy -> name;

        $allergy-> save();

        Session::flash('success',[__('The allergy ingredient was successfully edited!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $allergy_id) {

        $allergy = IngredientAllergy::find($allergy_id);

        if($allergy === null) {
            Session::flash('errors',[[__('The given allergy ingredient id is invalid!')]]);
            return redirect()->back();
        }



        $allergy-> delete();
        Session::flash('success',[__('The allergy ingredient has been deleted successfully!')]);
        return redirect()->back();
    }

}
