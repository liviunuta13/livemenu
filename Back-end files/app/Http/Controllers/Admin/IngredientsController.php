<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\County;
use App\Ingredient;
use App\IngredientHasAllergy;
use App\RestaurantLocationFacility;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class IngredientsController extends Controller
{

    public function search(Request $request){
        $ingredients = Ingredient::where('name', 'LIKE', '%'.$request -> keyword.'%')
            ->orderBy('name')->paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/ingredients/index', compact(
            'ingredients',
            'countries',
            'counties',
            'cities'
        ));

    }

    public function index() {

        $ingredients = Ingredient::paginate(10);

        return view('/admin/ingredients/index', compact(
            'ingredients'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:2|max:45',
            'price' => 'required|string',
            'amount' => 'required|string',
            'currency_id' => 'required|numeric|exists:currencies,id',
            'unit_id' => 'required|numeric|exists:units,id',
            'allergies_id' => 'required',
            'allergies_id.*' => 'required|numeric|exists:ingredient_allergies,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }


        $ingredient = new Ingredient();

        $ingredient -> name = $request -> name;

        $ingredient -> unit_id = $request -> unit_id;

        $ingredient -> price = $request -> price;

        $ingredient -> currency_id = $request -> currency_id;

        $ingredient -> amount = $request -> amount;

        $ingredient -> allergen = $request -> allergen;

        $ingredient -> save();


        foreach ($request->allergies_id as $key => $allergy_id) {

            $ingredient_has_allergy = new IngredientHasAllergy();

            $ingredient_has_allergy->ingredient()->associate($ingredient);

            $ingredient_has_allergy->ingredient_allergy_id= $allergy_id;

            $ingredient_has_allergy->save();
        }


        Session::flash('success',[__('The ingredient was successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request, $ingredient_id) {

        $ingredient = Ingredient::find($ingredient_id);

        if($ingredient === null) {
            Session::flash('errors',[[__('The given ingredient id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:2|max:45',
            'amount' => 'required|string',
            'price' => 'required|string',
            'currency_id' => 'required|numeric|exists:currencies,id',
            'unit_id' => 'required|numeric|exists:units,id',
            'allergies_id' => 'required',
            'allergies_id.*' => 'required|numeric|exists:ingredient_allergies,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }


        $ingredient -> name = $request -> name ? $request -> name : $ingredient -> name;

        $ingredient -> unit_id = $request -> unit_id ? $request -> unit_id : $ingredient -> unit_id;

        $ingredient -> price = $request -> price ? $request -> price : $ingredient -> price;

        $ingredient -> currency_id = $request -> currency_id ? $request -> currency_id : $ingredient -> currency_id;

        $ingredient -> allergen = $request -> allergen ? $request -> allergen : $ingredient -> allergen;

        $ingredient -> amount = $request -> amount ? $request -> amount : $ingredient -> amount;

        $ingredient-> save();

        Session::flash('success',[__('The ingredient was successfully edited!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $ingredient_id) {

        $ingredient = Ingredient::find($ingredient_id);

        if($ingredient === null) {
            Session::flash('errors',[[__('The given ingredient id is invalid!')]]);
            return redirect()->back();
        }


        $ingredient-> delete();
        Session::flash('success',[__('The ingredient has been deleted successfully!')]);
        return redirect()->back();
    }

}
