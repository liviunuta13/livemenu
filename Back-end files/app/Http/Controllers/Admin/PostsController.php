<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\User;
use App\City;
use App\County;
use App\Country;
use App\PostPicture;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{

    public function search(Request $request){
        $posts = Post::where('', 'LIKE', '%'.$request -> keyword.'%')
            ->orderBy('')->paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/posts/index', compact(
            'posts',
            'countries',
            'counties',
            'cities'
        ));

    }

    public function index() {

        $posts = Post::paginate(10);

        return view('/admin/posts/index', compact(
            'posts'
        ));
    }



    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'description' => 'required|string|min:3',
            'pictures' => 'required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();

        }

        $post = new Post();

        $post -> description = $request -> description;

        $post -> save();

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $post_picture = new PostPicture();

                    $post_picture -> post() -> associate($post);

                    $post_picture -> picture = $picture;

                    $post_picture -> save();
                }
            }
        }

        $post -> postPictures;

        Session::flash('success', [__('The post has been successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request,$post_id) {

        $post = Post::find($post_id);

        if($post === null) {
            Session::flash('errors', [__('The given post id is invalid!')]);
            return redirect()->back();

        }

        $validator = Validator::make($request -> all(), [
            'description' => 'sometimes|required|string|min:3',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|file|mimes:jpeg,jpg,png,gif'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('success', $errors);
            return redirect()->back();

        }

        $post -> description = $request -> description ? $request -> description : $post -> description;

        $post -> save();

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $post_picture = new PostPicture();

                    $post_picture -> post() -> associate($post);

                    $post_picture -> picture = $picture;

                    $post_picture -> save();
                }
            }
        }

        $post -> postPictures;
        Session::flash('success', [__('The post has been successfully updated!')]);
        return redirect()->back();
    }

    public function delete(Request $request, $post_id) {

        $post = Post::find($post_id);

        if($post === null) {
            Session::flash('success', [__('The given post id is invalid!')]);
            return redirect()->back();

        }

        foreach ($post -> postPictures as $key => $picture) {

            $picture -> delete();
        }

        $post -> delete();

        Session::flash('success', [__('The post and associated pictures has been deleted!')]);
        return redirect()->back();
    }
}
