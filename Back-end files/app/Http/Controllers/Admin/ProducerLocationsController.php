<?php

namespace App\Http\Controllers\Admin;

use App\ProducerProfile;
use App\User;
use App\City;
use App\Country;
use App\County;
use Illuminate\Http\Request;
use App\ProducerProfileLocation;
use App\ProducerProfileLocationPicture;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller;

class ProducerLocationsController extends Controller
{

    public function index()
    {
        $producer_locations = ProducerProfileLocation::paginate(10);
        return view('admin.producers_locations.index', ['producer_locations' => $producer_locations]);
    }

    public function search(Request $request)
    {

        $producer_locations = ProducerProfileLocation::where('name', 'LIKE', '%' . $request->keyword . '%')
            ->orWhere('email', 'LIKE', '%'.$request->keyword. '%')
            ->orWhere('phone_number', 'LIKE', '%'.$request->keyword. '%')
            ->orWhere('contact_person_name', 'LIKE', '%'.$request->keyword. '%')
            ->orderBy('name')->paginate(10);


        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();


        return view('admin.producers_locations.index', compact(
            'producer_locations',
            'countries',
            'counties',
            'cities'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:5|max:512',
            'city_id' => 'required|numeric|exists:cities,id',
            'producer_profile_id' => 'required|numeric|exists:producer_profiles,id',
            'county_id' => 'required|numeric|exists:counties,id',
            'country_id' => 'required|numeric|exists:countries,id',
            'street' => 'required|string|min:2|max:100',
            'number' => 'required|numeric',
            'postal_code' => 'sometimes|required|string|min:1|max:45',
            'latitude' => 'required',
            'longitude' => 'required',
            'phone_number' => 'sometimes|required|string|min:1|max:45',
            'fixed_phone_number' => 'sometimes|required|string|min:1|max:45',
            'contact_person_name' => 'required|string|min:1|max:45',
            'email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:producer_profile_locations,email',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors',$errors);
            return redirect()->back();

        }
        $producer_location = new ProducerProfileLocation();

        $producer_location -> name = $request -> name;

        $producer_location -> description = $request -> description;

        $producer_location -> city_id = $request -> city_id;

        $producer_location -> county_id = $request -> county_id;

        $producer_location -> producer_profile_id = $request -> producer_profile_id;

        $producer_location -> country_id = $request -> country_id;

        $producer_location -> street = $request -> street;

        $producer_location -> number = $request -> number;

        $producer_location -> postal_code = $request -> postal_code ? $request -> postal_code : null;

        $producer_location -> latitude = $request -> latitude;

        $producer_location -> longitude = $request -> longitude;

        $producer_location -> phone_number = $request -> phone_number ? $request -> phone_number : null;

        $producer_location -> fixed_phone_number = $request -> fixed_phone_number ? $request -> fixed_phone_number : null;

        $producer_location -> contact_person_name = $request -> contact_person_name;

        $producer_location -> email = $request -> email ? $request -> email : null;

//        $producer_location -> producerProfile() -> associate($producer_profile);

        $producer_location -> save();

        if(($request -> pictures) > 0) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $producer_location_picture = new ProducerProfileLocationPicture();

                    $producer_location_picture -> picture = $picture;

                    $producer_location_picture -> producerProfileLocation() -> associate($producer_location);

                    $producer_location_picture -> save();
                }
            }
        }

        $producer_location -> city;

        $producer_location -> county;

        $producer_location -> country;

        $producer_location -> producerProfile;

        $producer_location -> producerProfileLocationPictures;

        Session::flash('success',[__('The location was successfully added!')]);
        return redirect()->back();
    }

    // Edit producer location
    public function save(Request $request, $location_id) {

        $producer_location = ProducerProfileLocation::find($location_id);

        if($producer_location === null) {
            Session::flash('errors',[[__('The given location is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:5|max:512',
            'city_id' => 'sometimes|required|numeric|exists:cities,id',
            'producer_profile_id' => 'required|numeric|exists:producer_profiles,id',
            'county_id' => 'sometimes|required|numeric|exists:counties,id',
            'country_id' => 'sometimes|required|numeric|exists:countries,id',
            'street' => 'sometimes|required|string|min:2|max:100',
            'number' => 'sometimes|required|numeric',
            'postal_code' => 'sometimes|required|string|min:1|max:45',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'phone_number' => 'sometimes|required|string|min:1|max:45',
            'fixed_phone_number' => 'sometimes|required|string|min:1|max:45',
            'contact_person_name' => 'sometimes|required|string|min:1|max:45',
            'email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:producer_profile_locations,email',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'sometimes|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors',$errors);
            return redirect()->back();

        }
        $producer_location -> name = $request -> name ? $request -> name : $producer_location -> name;

        $producer_location -> description = $request -> description ? $request -> description : $producer_location -> description;

        $producer_location -> city_id = $request -> city_id ? $request -> city_id : $producer_location -> city_id;

        $producer_location -> producer_profile_id = $request -> producer_profile_id ? $request -> producer_profile_id : $producer_location -> producer_profile_id;

        $producer_location -> county_id = $request -> county_id ? $request -> county_id : $producer_location -> county_id;

        $producer_location -> country_id = $request -> country_id ? $request -> country_id : $producer_location -> country_id;

        $producer_location -> street = $request -> street ? $request -> street : $producer_location -> street;

        $producer_location -> number = $request -> number ? $request -> number : $producer_location -> number;

        $producer_location -> postal_code = $request -> postal_code ? $request -> postal_code : $producer_location -> postal_code;

        $producer_location -> latitude = $request -> latitude ? $request -> latitude : $producer_location -> latitude;

        $producer_location -> longitude = $request -> longitude ? $request -> longitude : $producer_location -> longitude;

        $producer_location -> phone_number = $request -> phone_number ? $request -> phone_number : $producer_location -> phone_number;

        $producer_location -> fixed_phone_number = $request -> fixed_phone_number ? $request -> fixed_phone_number : $producer_location -> fixed_phone_number;

        $producer_location -> contact_person_name = $request -> contact_person_name ?  $request -> contact_person_name :  $producer_location -> contact_person_name;

        $producer_location -> email = $request -> email ? $request -> email : $producer_location -> email;

        $producer_location -> save();

//        if(count($request -> pictures) > 0) {
//
//            foreach ($request -> pictures as $key => $picture) {
//                {
//                    $producer_location_picture = new ProducerProfileLocationPicture();
//
//                    $producer_location_picture -> picture = $picture;
//
//                    $producer_location_picture -> producerProfileLocation() -> associate($location);
//
//                    $producer_location_picture -> save();
//                }
//            }
//        }

        $producer_location -> city;

        $producer_location -> county;

        $producer_location -> country;

        $producer_location -> producerProfile;

        $producer_location -> producerProfileLocationPictures;

        Session::flash('success', [__('The producer location was successfully updated!')]);
        return redirect()->back();

    }

    public function delete($producer_location_id) {

        $producer_location = ProducerProfileLocation:: find($producer_location_id);

        if($producer_location === null) {
            Session::flash('errors', [__('The given location is invalid!')]);
            return redirect()->back();
        }

        $producer_location -> delete();

        Session::flash('success', [__('The location has been deleted!')]);
        return redirect()->back();
    }

}
