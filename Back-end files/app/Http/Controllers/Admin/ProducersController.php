<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\City;
use App\County;
use App\Country;
use App\ProducerType;
use App\ProducerProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProducersController extends Controller
{
    public function index() {

        $producers = ProducerProfile::paginate(10);

        $producer_types = ProducerType::all();

        return view('/admin/producers/index', compact(
            'producer_types',
            'producers'
        ));
    }

    public function search(Request $request) {

        $producers = ProducerProfile::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('email', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('administrator_name_surname', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('phone_number', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('fiscal_code', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name') -> paginate(10);

        $cities = City::all();

        $counties = County::all();

        $countries = Country::all();

        $producer_types = ProducerType::all();

        return view('/admin/producers/index', compact(
            'producer_types',
            'producers',
            'countries',
            'counties',
            'cities'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:1|max:45',
            'phone_number' => 'required|min:5|max:20',
            'company_name' => 'required|string|min:1|max:45',
            'fiscal_code' => 'required|string|min:3|max:45|unique:producer_profiles,fiscal_code',
            'administrator_name' => 'required|string|min:1|max:45',
            'producer_type_id' => 'required|numeric|exists:producer_types,id',
            'email' => ['sometimes', 'required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:producer_profiles,email'],
            'website' => 'sometimes|nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'logo' => 'sometimes|nullable|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'sometimes|nullable|string|min:3',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('producers'));
        }

        $producer = new ProducerProfile();

        $producer -> name = $request -> name;

        $producer -> phone_number = $request -> phone_number;

        $producer -> company_name = $request -> company_name;

        $producer -> fiscal_code = $request -> fiscal_code;

        $producer -> administrator_name_surname = $request -> administrator_name;

        $producer -> producer_type_id = $request -> producer_type_id;

        $producer -> email = $request -> email ? $request -> email : null;

        $producer -> website = $request -> website ? $request -> website : null;

        $producer -> description = $request -> description ? $request -> description : null;

        if($request -> logo) {

            $file = $request -> file('logo');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $producer -> logo = $base64;
        }

        $producer -> save();

        Session::flash('success', [__('The producer has been added successfully!')]);

        return redirect(route('producers'));
    }

    public function edit(Request $request, $producer_id) {

        $producer = ProducerProfile::find($producer_id);

        if($producer == null) {

            Session::flash('errors', [[__('Invalid producer profile!')]]);

            return redirect() -> back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:1|max:45',
            'phone_number' => 'required|min:5|max:20',
            'company_name' => 'required|string|min:1|max:45',
            'fiscal_code' => 'required|string|min:3|max:45|unique:producer_profiles,fiscal_code,' . $producer -> id,
            'administrator_name' => 'required|string|min:1|max:45',
            'producer_type_id' => 'required|numeric|exists:producer_types,id',
            'email' => ['sometimes', 'required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:producer_profiles,email,' . $producer -> id],
            'website' => 'sometimes|nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'logo' => 'sometimes|nullable|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'sometimes|nullable|string|min:3',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('producers'));
        }

        $producer -> name = $request -> name;

        $producer -> phone_number = $request -> phone_number;

        $producer -> company_name = $request -> company_name;

        $producer -> fiscal_code = $request -> fiscal_code;

        $producer -> administrator_name_surname = $request -> administrator_name;

        $producer -> producer_type_id = $request -> producer_type_id;

        $producer -> email = $request -> email ? $request -> email : null;

        $producer -> website = $request -> website ? $request -> website : null;

        $producer -> description = $request -> description ? $request -> description : null;

        if($request -> logo) {

            $file = $request -> file('logo');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $producer -> logo = $base64;
        }

        $producer -> save();

        Session::flash('success', [__('The producer has been added successfully!')]);

        return redirect(route('producers'));
    }

    public function delete($producer_id) {

        $producer = ProducerProfile::find($producer_id);

        if($producer == null) {

            Session::flash('errors', [[__('Invalid producer profile!')]]);

            return redirect() -> back();
        }

        $producer -> forceDelete();

        Session::flash('success', [__('The producer has been deleted!')]);

        return redirect() -> back();
    }

    public function suspend($producer_id) {

        $producer = ProducerProfile::find($producer_id);

        if($producer == null) {

            Session::flash('errors', [[__('Invalid producer profile!')]]);

            return redirect() -> back();
        }

        if($producer -> suspended == 0) {

            $producer -> suspended = 1;

            $producer -> save();

            Session::flash('success', [__('The producer has been suspended!')]);

        } else {

            $producer -> suspended = 0;

            $producer -> save();

            Session::flash('success', [__('The producer has been unsuspended!')]);
        }

        return redirect() -> back();
    }
}
