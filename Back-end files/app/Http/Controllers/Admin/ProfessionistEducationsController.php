<?php

namespace App\Http\Controllers\Admin;

use App\ProfessionistEducation;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use App\City;
use App\County;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProfessionistEducationsController extends Controller
{
    public function index(){
        $educations = ProfessionistEducation::paginate(10);
        return view('admin.professionists_educations.index',['educations'=>$educations]);
    }
    public function search(Request $request) {

        $educations = ProfessionistEducation::where('certificate_name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('institution_name', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('certificate_name') -> paginate(10);

        $cities = City::all();

        $counties = County::all();

        $countries = Country::all();

        return view('/admin/professionists_educations/index', compact(
            'educations',
            'producers',
            'countries',
            'counties',
            'cities'
        ));
    }

    public function add(Request $request) {

            $validator = Validator::make($request -> all(), [
                'profesionist_profile_id' => 'required|numeric|exists:profesionist_profiles,id',
                'description' => 'sometimes|required|string|min:3',
                'certificate_name' => 'sometimes|required|string|min:3|max:45',
                'institution_name' => 'sometimes|required|string|min:3|max:45',
                'city_id' => 'required|numeric|exists:cities,id',
                'country_id' => 'required|numeric|exists:countries,id',
                'begin_date' => 'sometimes|required|date',
                'end_date' => 'sometimes|required|date',
                'present' => 'sometimes|required|date_format:H:i',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect()->back();

        }

            $current_date = Carbon::now() -> format('Y-m-d');

            if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
                Session::flash('errors',[[__('The begin date must be greater or equal with current date!')]]);
                return redirect()->back();
            }

            if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
                Session::flash('errors',[[__('The begin date must be lower or equal with the end date!')]]);
                return redirect()->back();
        }

        $education = new ProfessionistEducation();

        $education -> profesionist_profile_id = $request -> profesionist_profile_id;

        $education -> begin_date = $request -> begin_date;

        $education -> present = $request -> present;

        $education -> end_date = $request -> end_date;

        $education -> certificate_name = $request -> certificate_name;

        $education -> institution_name = $request -> institution_name;

        $education -> city_id = $request -> city_id;

        $education -> country_id = $request -> country_id;

        $education -> description = $request -> description ? $request -> description : null;

        $education -> save();

        Session::flash('success', [__('The professionist education has been added successfully!')]);

        return redirect()->back();
    }

    public function edit(Request $request, $education_id) {

        $education = ProfessionistEducation::find($education_id);

        if($education == null) {

            Session::flash('errors', [[__('Invalid profesionist profile!')]]);

            return redirect() -> back();
        }

        $validator = Validator::make($request -> all(), [
            'profesionist_profile_id' => 'required|numeric|exists:profesionist_profiles,id',
            'description' => 'sometimes|required|string|min:3',
            'certificate_name' => 'sometimes|required|string|min:3|max:45',
            'institution_name' => 'sometimes|required|string|min:3|max:45',
            'city_id' => 'required|numeric|exists:cities,id',
            'country_id' => 'required|numeric|exists:countries,id',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'present' => 'sometimes|required|date_format:H:i',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect()->back();
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors', [[__('The begin date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors',[[__('The begin date must be lowe or equal with the end date!')]]);
            return redirect()->back();
        }


        $education -> profesionist_profile_id = $request -> profesionist_profile_id;

        $education -> begin_date = $request -> begin_date;

        $education -> present = $request -> present;

        $education -> end_date = $request -> end_date;

        $education -> certificate_name = $request -> certificate_name;

        $education -> institution_name = $request -> institution_name;

        $education -> city_id = $request -> city_id;

        $education -> country_id = $request -> country_id;

        $education -> description = $request -> description ? $request -> description : null;

        $education -> save();

        Session::flash('success', [__('The professionist education has been added successfully!')]);

        return redirect()->back();
    }

    public function delete($education_id) {

        $education = ProfessionistEducation::find($education_id);

        if($education == null) {

            Session::flash('errors', [[__('Invalid profesionist education profile!')]]);

            return redirect() -> back();
        }

        $education -> forceDelete();

        Session::flash('success', [__('The professionist education has been deleted!')]);

        return redirect() -> back();
    }

}
