<?php

namespace App\Http\Controllers\Admin;

use App;
use PDF;
use Illuminate\Support\Facades\Session;
use App\User;
use App\City;
use App\County;
use App\Country;
use App\ForeignLanguage;
use App\ProfesionistProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProfessionistsController extends Controller
{
    public function index() {

        $professionists = ProfesionistProfile::orderBy('name') -> paginate(10);

        $users = User::where('profesionist_profile_id', null) -> get();

        $cities = City::all();

        $counties = County::all();

        $countries = Country::all();

        return view('/admin/professionists/index', compact(
            'professionists',
            'countries',
            'counties',
            'cities',
            'users'
        ));
    }

    public function search(Request $request) {

        $professionists = ProfesionistProfile::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('email', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('phone_number', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name') -> paginate(10);

        $users = User::where('profesionist_profile_id', null) -> get();

        $cities = City::all();

        $counties = County::all();

        $countries = Country::all();

        return view('/admin/professionists/index', compact(
            'professionists',
            'countries',
            'counties',
            'cities',
            'users'
        ));
    }

    public function suspend($professionist_id) {

        $professionist = ProfesionistProfile::find($professionist_id);

        if($professionist == null) {

            Session::flash('errors', [[__('Invalid professionist profile!')]]);

            return redirect() -> back();
        }

        if($professionist -> suspended == 0) {

            $professionist -> suspended = 1;

            $professionist -> save();

            Session::flash('success', [__('The professionist has been suspended!')]);

        } else {

            $professionist -> suspended = 0;

            $professionist -> save();

            Session::flash('success', [__('The professionist has been unsuspended!')]);
        }

        return redirect() -> back();
    }

    public function exportCV($professionist_id) {

        $professionist = ProfesionistProfile::find($professionist_id);

        if($professionist == null) {

            Session::flash('errors', [[__('Invalid professionist profile!')]]);

            return redirect() -> back();
        }

        $data = $professionist;

        $pdf = App::make('dompdf.wrapper');

        $data -> city;

        $data -> county;

        $data -> country;

        $data -> educationCity;

        $data -> educationCountry;

        foreach ($data -> profesionistProfileHasForeignLanguages as $key => $value) {

            $value -> foreignLanguage;
        }

        foreach ($data -> professionalHistory as $key => $value) {

            $value -> city;

            $value -> country;
        }

        $data -> user;

        $data -> matern_language = ForeignLanguage::find($data -> matern_language_id);

        $pdf = PDF::loadView('/pdf/cv', compact('data'));

        return $pdf -> download(str_replace(' ', '', $data -> name) . '_cv.pdf');

        // return $pdf -> stream();
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'email' => ['required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:profesionist_profiles,email'],
            'profile_picture' => 'sometimes|required|file',
            'user_id' =>  'required|exists:users,id|unique:users,profesionist_profile_id'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('professionists'));
        }

        $professionist = new ProfesionistProfile();

        $professionist -> name = $request -> name;

        $professionist -> email = $request -> email;

        if($request -> picture) {

            $file = $request -> file('picture');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $professionist -> picture = $base64;
        }

        $professionist -> save();

        $user = User::find($request -> user_id);

        $user -> profesionist_profile_id = $professionist -> id;

        $user -> save();

        Session::flash('success', [__('The professionist has been added successfully!')]);

        return redirect(route('professionists'));
    }

    public function edit(Request $request, $professionist_id) {

        $professionist = ProfesionistProfile::find($professionist_id);

        if($professionist == null) {

            Session::flash('errors', [[__('Invalid professionist profile!')]]);

            return redirect() -> back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'email' => ['required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:profesionist_profiles,email,' . $professionist -> id],
            'profile_picture' => 'sometimes|required|file'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('professionists'));
        }

        $professionist -> name = $request -> name ? $request -> name : $professionist -> name;

        $professionist -> email = $request -> email ? $request -> email : $professionist -> email;

        if($request -> picture) {

            $file = $request -> file('picture');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $professionist -> picture = $base64;
        }

        $professionist -> save();

        Session::flash('success', [__('The professionist has been added successfully!')]);

        return redirect(route('professionists'));
    }

    public function delete($professionist_id) {

        $professionist = ProfesionistProfile::find($professionist_id);

        if($professionist == null) {

            Session::flash('errors', [[__('Invalid professionist profile!')]]);

            return redirect() -> back();
        }

        $professionist -> postsTo() -> delete();

        $professionist -> profesionistProfileHasForeignLanguages() -> delete();

        $professionist -> professionalHistory() -> delete();

        $professionist -> education() -> delete();

        $professionist -> ratings() -> delete();

        $professionist -> workRequests() -> delete();

        $professionist -> workingForRestaurantsLocations() -> delete();

        $professionist -> workingForProducersLocations() -> delete();

        $professionist -> delete();

        Session::flash('success', [__('The professionist has been deleted!')]);

        return redirect(route('professionists'));
    }
}
