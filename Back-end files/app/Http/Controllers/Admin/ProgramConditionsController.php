<?php

namespace App\Http\Controllers\Admin;

use App\ProgramsCondition;
use App\RestaurantLocationProgram;
use App\User;
use App\Hotel;
use App\City;
use Illuminate\Support\Carbon;
use App\County;
use App\Country;
use App\HotelPicture;
use App\HotelFacility;
use App\HotelHasFacility;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ProgramConditionsController extends Controller
{

    public function index()
    {
        $program_conditions =ProgramsCondition ::paginate(10);
        return view('admin.programs_conditions.index', ['program_conditions' => $program_conditions]);
    }

    public function search(Request $request)
    {

        $program_conditions = ProgramsCondition::where('name', 'LIKE', '%' . $request->keyword . '%')
            ->orWhere('type', 'LIKE', '%'. $request->keyword . '%')
            ->orderBy('name')->paginate(10);


        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();


        return view('admin.programs_conditions.index', compact(
            'program_conditions',
            'countries',
            'counties',
            'cities'
        ));
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:45',
            'type' => 'sometimes|required|string|min:3|max:45',
            'start_date' => 'required|date',
            'end_date' => 'required|date'
        ]);

        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }


        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors',[[__('The start date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors',[[__('The start date must be lower or equal with the end date!')]]);
            return redirect()->back();
        }

        $program_condition = new ProgramsCondition();

        $program_condition->name = $request->name;

        $program_condition->type = $request->type;

        $program_condition->start_date = $request->start_date;

        $program_condition->end_date = $request->end_date;

        $program_condition->save();

        Session::flash('success', [__('The program condition was successfully added!')]);
        return redirect()->back();
    }

    public function save(Request $request, $program_condition_id) {
        $program_condition = ProgramsCondition::find($program_condition_id);

        if($program_condition === null) {
            Session::flash('errors',[[__('The given program condition id is invalid!')]]);
            return redirect()->back();
        }


        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'type' => 'sometimes|required|string|min:3|max:45',
            'start_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date'

        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();

        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors',[[__('The start date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors',[[__('The start date must be lower or equal with the end date!')]]);
            return redirect()->back();
        }



        $program_condition -> name = $request -> name ? $request -> name : $program_condition -> name;

        $program_condition -> type = $request -> type ? $request -> type : $program_condition -> type;

        $program_condition -> start_date = $request -> start_date ? $request -> start_date : $program_condition -> start_date;

        $program_condition -> end_date = $request -> end_date ? $request -> end_date : $program_condition -> end_date;

        $program_condition -> restaurantLocation;

        $program_condition -> save();

        Session::flash('success',[__('The program condition was successfully updated!')]);
        return redirect()->back();
    }

    public function delete(Request $request, $program_condition_id) {


        $program_condition = ProgramsCondition::find($program_condition_id);

        if($program_condition === null) {
            Session::flash('errors',[[__('The given program condition id is invalid!')]]);
            return redirect()->back();
        }

        $program_condition-> delete();

        Session::flash('success',[__('The program condition and all data associated was successfully deleted!')]);
        return redirect()->back();

    }

}
