<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\County;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationEventsController extends Controller
{

    public function search(Request $request){
        $events = RestaurantLocationEvent::where('name', 'LIKE', '%'.$request -> keyword.'%')
            ->orWhere('theme', 'LIKE', '%'.$request->keyword.'%')
            ->orWhere('description', 'LIKE', '%'.$request->keyword.'%')
            ->orderBy('name')->paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/restaurants_locations_events/index', compact(
            'events',
            'countries',
               'counties',
               'cities'
        ));

    }

    public function index() {

        $events = RestaurantLocationEvent::paginate(10);

        return view('/admin/restaurants_locations_events/index', compact(
            'events'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3',
            'theme' => 'required|string|min:3|max:45',
            'banner' => 'required|string',
            'one_day_event' => 'sometimes|required|boolean',
            'multi_day_event' => 'sometimes|required|boolean',
            'begin_date' => 'required|date',
            'end_date' => 'required|date',
            'begin_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors', [[__('The begin date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors',[[__('The begin date must be lowe or equal with the end date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            Session::flash('errors',[[__('The begin time must be lower than end time!')]]);
            return redirect()->back();
        }

        $event = new RestaurantLocationEvent();

        $event -> restaurant_location_id = $request -> restaurant_location_id;

        $event -> name = $request -> name;

        $event -> description = $request -> description;

        $event -> theme = $request -> theme;

        $event -> one_day_event = $request -> one_day_event;

        $event -> multi_day_event = $request -> multi_day_event;

        $event -> begin_date = $request -> begin_date;

        $event -> end_date = $request -> end_date;

        $event -> begin_time = $request -> begin_time;

        $event -> end_time = $request -> end_time;

        $event -> save();

        Session::flash('success',[__('The event was successfully added!')]);
        return redirect()->back();

    }

    // Edit location event
    public function save(Request $request, $event_id) {

        $event = RestaurantLocationEvent::find($event_id);

        if($event === null) {
            Session::flash('errors',[[__('The given event id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'theme' => 'sometimes|required|string|min:3|max:45',
            'banner' => 'sometimes|required|string',
            'one_day_event' => 'sometimes|required|boolean',
            'multi_day_event' => 'sometimes|required|boolean',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'begin_time' => 'sometimes|required|date_format:H:i',
            'end_time' => 'sometimes|required|date_format:H:i'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors',[[__('The begin date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors',[[__('The begin date must be lower or equal with the end date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            Session::flash('errors',[[__('The begin time must be lower than end time!')]]);
            return redirect()->back();
        }

        $event -> name = $request -> name ? $request -> name : $event -> name;

        $event -> description = $request -> description ? $request -> description : $event -> description;

        $event -> theme = $request -> theme ? $request -> theme : $event -> theme;

        $event -> one_day_event = $request -> one_day_event ? $request -> one_day_event : $event -> one_day_event;

        $event -> multi_day_event = $request -> multi_day_event ? $request -> multi_day_event : $event -> multi_day_event;

        $event -> begin_date = $request -> begin_date ? $request -> begin_date : $event -> begin_date;

        $event -> end_date = $request -> end_date ? $request -> end_date : $event -> end_date;

        $event -> begin_time = $request -> begin_time ? $request -> begin_time : $event -> begin_time;

        $event -> end_time = $request -> end_time ? $request -> end_time : $event -> end_time;


        $event -> save();
        Session::flash('success',[__('The event was successfully edited!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $event_id) {

        $event = RestaurantLocationEvent::find($event_id);

        if($event === null) {
            Session::flash('errors',[[__('The given event id is invalid!')]]);
            return redirect()->back();
        }



        $event -> delete();
        Session::flash('success',[__('The event has been deleted successfully!')]);
        return redirect()->back();
    }


}
