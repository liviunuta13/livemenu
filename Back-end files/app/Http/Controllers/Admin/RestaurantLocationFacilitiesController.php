<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\County;
use App\RestaurantLocationFacility;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationFacilitiesController extends Controller
{

    public function search(Request $request){
        $facilities = RestaurantLocationFacility::where('name', 'LIKE', '%'.$request -> keyword.'%')
            ->orWhere('description', 'LIKE', '%'.$request->keyword.'%')
            ->orderBy('name')->paginate(10);

//        $cities = City::all();
//        $counties = County::all();
//        $countries = Country::all();

        return view('/admin/restaurants_locations_facilities/index', compact(
            'facilities'
//            'countries',
//            'counties',
//            'cities'
        ));

    }

    public function index() {

        $facilities = RestaurantLocationFacility::paginate(10);

        return view('/admin/restaurants_locations_facilities/index', compact(
            'facilities'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:2|max:45',
            'icon' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }


        $facility = new RestaurantLocationFacility();

        $facility -> name = $request -> name;

        $facility -> icon = $request -> icon;

        $facility -> description = $request -> description;

        $facility -> save();

        Session::flash('success',[__('The facility was successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request, $facility_id) {

        $facility = RestaurantLocationFacility::find($facility_id);

        if($facility === null) {
            Session::flash('errors',[[__('The given facility id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:2|max:45',
            'icon' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }


        $facility -> name = $request -> name ? $request -> name : $facility -> name;

        $facility -> description = $request -> description ? $request -> description : $facility -> description;

        $facility -> icon = $request -> icon ? $request -> icon : $facility -> icon;

        $facility-> save();

        Session::flash('success',[__('The facility was successfully edited!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $facility_id) {

        $facility = RestaurantLocationFacility::find($facility_id);

        if($facility === null) {
            Session::flash('errors',[[__('The given facility id is invalid!')]]);
            return redirect()->back();
        }



        $facility-> delete();
        Session::flash('success',[__('The facility has been deleted successfully!')]);
        return redirect()->back();
    }

}
