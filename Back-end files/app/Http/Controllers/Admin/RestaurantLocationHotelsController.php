<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Hotel;
use App\City;
use App\County;
use App\Country;
use App\HotelPicture;
use App\HotelFacility;
use App\HotelHasFacility;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class RestaurantLocationHotelsController extends Controller
{

    public function index(){
        $hotels = Hotel::paginate(10);
        return view('admin.restaurants_locations_hotels.index',['hotels'=>$hotels]);
    }

    public function search(Request $request) {

        $hotels = Hotel::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('description', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name')->paginate(10);



        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();


        return view('admin.restaurants_locations_hotels.index', compact(
            'hotels',
            'countries',
            'counties',
            'cities'
        ));
    }

    public function add(Request $request) {
        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3',
            'stars' => 'required|numeric|min:1|max:5',
            'email' => ['required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:hotels,email'],
            'phone_number' => 'sometimes|required|numeric',
            'capacity' => 'required|numeric',
            'facilities_id' => 'required',
            'facilities_id.*' => 'required|numeric|exists:hotel_facilities,id',
            'restaurant_location_id' => 'required|numeric|exists:restaurant_locations,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors',$errors);
            return redirect()->back();
        }

        $hotel = new Hotel();

        $hotel -> name = $request -> name;

        $hotel -> restaurant_location_id = $request -> restaurant_location_id;

        $hotel -> description = $request -> description;

        $hotel -> stars = $request -> stars;

        $hotel -> email = $request -> email;

        $hotel -> phone_number = $request -> phone_number;

        $hotel -> capacity = $request -> capacity;

//        $hotel -> restaurantLocation() -> associate($location);

        $hotel -> save();

        foreach ($request->facilities_id as $key => $facility_id) {

            $hotel_has_facility = new HotelHasFacility();

            $hotel_has_facility->hotel()->associate($hotel);

            $hotel_has_facility->facility_id= $facility_id;

            $hotel_has_facility->save();
        }

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $restaurant_has_picture = new HotelPicture();

                    $restaurant_has_picture -> hotel() -> associate($hotel);

                    $restaurant_has_picture -> picture = $picture;

                    $restaurant_has_picture -> save();
                }
            }
        }
        Session::flash('success',[__('The hotel was successfully added!')]);
        return redirect()->back();
    }

    // Edit hotel
    public function save(Request $request, $location_id, $hotel_id) {
        $location = RestaurantLocation::find($location_id);
        $hotel = Hotel::find($hotel_id);

        if($location === null) {
            Session::flash('errors',[[__('The given restaurant location id is invalid!')]]);
            return redirect()->back();
        }

        if($hotel === null) {
            Session::flash('errors',[[__('The given hotel id is invalid!')]]);
            return redirect()->back();
        }

        if($hotel -> restaurant_location_id != $location -> id) {
            Session::flash('errors',[[__('The given hotel is not associated with the given restaurant location!')]]);
            return redirect()->back();

        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'stars' => 'sometimes|required|numeric|min:1|max:5',
            'email' => ['sometimes', 'required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:hotels,email'],
            'phone_number' => 'sometimes|required|numeric',
            'capacity' => 'sometimes|required|numeric',
            'facilities_id' => 'required',
            'facilities_id.*' => 'required|numeric|exists:hotel_facilities,id',
            'delete_facilities' => 'sometimes|required',
            'delete_facilities.*' => 'required|numeric|exists:hotel_has_facility,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();

        }

        $hotel -> name = $request -> name ? $request -> name : $hotel -> name;

        $hotel -> description = $request -> description ? $request -> description : $hotel -> description;

        $hotel -> stars = $request -> stars ? $request -> stars : $hotel -> strs;

        $hotel -> email = $request -> email ? $request -> email : $hotel -> email;

        $hotel -> phone_number = $request -> phone_number ? $request -> phone_number : $hotel -> phone_number;

        $hotel -> capacity = $request -> capacity ? $request -> capacity : $hotel -> capacity;

        $hotel -> save();

        if(isset($request -> facilities) && count($request -> facilities)) {

            foreach ($request->facilities_id as $key => $facility_id) {

                $hotel_has_facility = new HotelHasFacility();

                $hotel_has_facility->hotel()->associate($hotel);

                $hotel_has_facility->facility_id= $facility_id;

                $hotel_has_facility->save();
            }
        }

        if(isset($request -> delete_facilities)) {

            foreach ($request -> delete_facilities as $key => $facility_id) {

                $hotel_has_facility = HotelHasFacility::find($facility_id);

                if($hotel_has_facility && $hotel_has_facility -> hotel_id == $hotel -> id) {

                    $hotel_has_facility -> forceDelete();
                }
            }
        }

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $restaurant_has_picture = new HotelPicture();

                    $restaurant_has_picture -> hotel() -> associate($hotel);

                    $restaurant_has_picture -> picture = $picture;

                    $restaurant_has_picture -> save();
                }
            }
        }

        $hotel -> restaurantLocation;

        foreach ($hotel -> hotelHasFacilities as $key => $value) {

            $value -> hotelFacility;
        }

        $hotel -> hotelPictures;

        $hotel -> rooms;

        Session::flash('success',[__('The hotel was successfully updated!')]);
        return redirect()->back();
    }

    public function delete(Request $request, $hotel_id) {


        $hotel = Hotel::find($hotel_id);

        if($hotel === null) {
            Session::flash('errors',[[__('The given hotel id is invalid!')]]);
            return redirect()->back();
        }

        foreach ($hotel -> hotelPictures as $key => $picture) {

            $picture -> delete();
        }

        $hotel-> delete();

        Session::flash('success',[__('The hotel and all data associated was successfully deleted!')]);
        return redirect()->back();

    }

}
