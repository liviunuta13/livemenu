<?php

namespace App\Http\Controllers\Admin;

use App\RestaurantLocationProgram;
use App\User;
use App\Hotel;
use App\City;
use App\County;
use App\Country;
use App\HotelPicture;
use App\HotelFacility;
use App\HotelHasFacility;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class RestaurantLocationProgramsController extends Controller
{

    public function index()
    {
        $programs = RestaurantLocationProgram::paginate(10);
        return view('admin.restaurants_locations_programs.index', ['programs' => $programs]);
    }

    public function search(Request $request)
    {

        $programs = RestaurantLocationProgram::where('conditions', 'LIKE', '%' . $request->keyword . '%')
            ->orderBy('conditions')->paginate(10);


        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();


        return view('admin.restaurants_locations_programs.index', compact(
            'programs',
            'countries',
            'counties',
            'cities'
        ));
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'restaurant_location_id' => 'required|numeric|exists:restaurant_locations,id',
            'conditions' => 'required|string|min:3'
        ]);

        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }

        $program = new RestaurantLocationProgram();

        $program->restaurant_location_id = $request->restaurant_location_id;

        $program->conditions = $request->conditions;

        $program->save();
        Session::flash('success', [__('The program was successfully added!')]);
        return redirect()->back();
    }

    // Edit hotel
    public function save(Request $request, $program_id) {
        $program = RestaurantLocationProgram::find($program_id);

        if($program === null) {
            Session::flash('errors',[[__('The given program id is invalid!')]]);
            return redirect()->back();
        }


        $validator = Validator::make($request -> all(), [
            'restaurant_location_id' => 'required|numeric|exists:restaurant_locations,id',
            'conditions' => 'sometimes|required|string|min:3'

        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();

        }


        $program -> conditions = $request -> conditions ? $request -> conditions : $program -> conditions;

        $program -> restaurant_location_id = $request -> restaurant_location_id ? $request -> restaurant_location_id : $program -> restaurant_location_id;

        $program -> restaurantLocation;

        $program -> save();

        Session::flash('success',[__('The program was successfully updated!')]);
        return redirect()->back();
    }

    public function delete(Request $request, $program_id) {


        $program = RestaurantLocationProgram::find($program_id);

        if($program === null) {
            Session::flash('errors',[[__('The given program id is invalid!')]]);
            return redirect()->back();
        }

        $program-> delete();

        Session::flash('success',[__('The program and all data associated was successfully deleted!')]);
        return redirect()->back();

    }

}
