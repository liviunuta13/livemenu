<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use App\Menu;
use App\City;
use App\Country;
use App\County;
use Carbon\Carbon;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationPromotion;
use App\Http\Controllers\Controller;

class RestaurantLocationPromotionsController extends Controller
{

    public function index(){
        $promotions = RestaurantLocationPromotion::paginate(10);
        return view('admin/restaurants_locations_promotions/index',['promotions'=>$promotions]);
    }

    public function search(Request $request) {

        $promotions = RestaurantLocationPromotion::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('description', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name')->paginate(10);



        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();


        return view('/admin/restaurants_locations_promotions/index', compact(
            'promotions',
            'countries',
            'counties',
            'cities'
        ));
    }



    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'restaurant_location_id' => 'required|numeric|exists:restaurant_locations,id',
            'description' => 'required|string|min:3',
            'begin_date' => 'required|date',
            'end_date' => 'required|date',
            'begin_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i',
//            'product_id' => 'required|numeric|exists:menus,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors',$errors);
            return redirect()->back();
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors',[[__('The given date must be greater or equal with current date!')]]);
            return redirect()->back();

        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors',[[__('The begin date must be lower or equal with the end date!')]]);
            return redirect()->back();

        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            Session::flash('errors',[[__('The begin time must be lower than end time!')]]);
            return redirect()->back();
        }

        $product = Menu::find($request -> product_id);

//        if($product  === null) {
//            Session::flash('errors',[__('The product id is invalid!')]);
//            return redirect()->back();
//
//        }
//
//        if($product -> menu_id !== null) {
//            Session::flash('errors',[__('The given product id is not an menu or product, is a category of menu!')]);
//            return redirect()->back();
//        }

        $promotion = new RestaurantLocationPromotion();

        $promotion -> name = $request -> name;

        $promotion -> description = $request -> description;

        $promotion -> begin_date = $request -> begin_date;

        $promotion -> end_date = $request -> end_date;

        $promotion -> begin_time = $request -> begin_time;

        $promotion -> end_time = $request -> end_time;

        $promotion -> restaurant_location_id = $request -> restaurant_location_id;

//        $promotion -> product_id = $request -> product_id;

//        $promotion -> restaurantLocation() -> associate($location);

        $promotion -> save();
        Session::flash('success',[__('The promotion was successfully added!')]);
        return redirect()->back();

    }

    // Edit promotion
    public function save(Request $request, $promotion_id) {
        $promotion = RestaurantLocationPromotion::find($promotion_id);

        if($promotion === null) {
            Session::flash('errors',[[__('The given restaurant location id is invalid!')]]);
            return redirect()->back();
        }

        if($promotion === null) {
            Session::flash('errors',[[__('The given promotion id is invalid!')]]);
            return redirect()->back();

        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'begin_time' => 'sometimes|required|date_format:H:i',
            'end_time' => 'sometimes|required|date_format:H:i',
//            'product_id' => 'sometimes|required|numeric|exists:menus,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors', [[__('The begin date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors', [[__('The begin date must be lower or equal with the end date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            Session::flash('errors', [[__('The begin time must be lower than end time!')]]);
            return redirect()->back();
        }

        $product = Menu::find($request -> product_id);

        if($product  !== null) {
            if($product -> menu_id !== null) {
                Session::flash('errors', [[__('The given product id is not an menu or product, is a category of menu!')]]);
                return redirect()->back();
            }
        }

        $promotion -> name = $request -> name ?  $request -> name : $promotion -> name;

        $promotion -> description = $request -> description ? $request -> description : $promotion -> description;

        $promotion -> begin_date = $request -> begin_date ? $request -> begin_date : $promotion -> begin_date;

        $promotion -> end_date = $request -> end_date ? $request -> end_date : $promotion -> end_date;

        $promotion -> begin_time = $request -> begin_time ? $request -> begin_time : $promotion -> begin_time;

        $promotion -> end_time = $request -> end_time ? $request -> end_time : $promotion -> end_time;

        $promotion -> product_id = $request -> product_id ? $request -> product_id : $promotion -> product_id;

//        $promotion -> restaurantLocation() -> associate($location);

        $promotion -> save();
        Session::flash('success', [__('The promotion was successfully updated!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $promotion_id) {

        $promotion = RestaurantLocationPromotion::find($promotion_id);

        if($promotion === null) {
            Session::flash('errors', [[__('The given promotion id is invalid!')]]);
            return redirect()->back();
        }


        $promotion -> delete();
        Session::flash('success', [__('The promotion has been deleted!')]);
        return redirect()->back();

    }

}
