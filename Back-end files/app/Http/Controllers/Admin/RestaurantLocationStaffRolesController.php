<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\County;
use App\RestaurantLocationFacility;
use App\RestaurantLocationStaffRole;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationStaffRolesController extends Controller
{

    public function search(Request $request){
        $roles = RestaurantLocationStaffRole::where('name', 'LIKE', '%'.$request -> keyword.'%')
            ->orderBy('name')->paginate(10);

//        $cities = City::all();
//        $counties = County::all();
//        $countries = Country::all();

        return view('/admin/staffs_roles/index', compact(
            'roles'
//            'countries',
//            'counties',
//            'cities'
        ));

    }

    public function index() {

        $roles = RestaurantLocationStaffRole::paginate(10);

        return view('/admin/staffs_roles/index', compact(
            'roles'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:2|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }


        $role = new RestaurantLocationStaffRole();

        $role -> name = $request -> name;

        $role -> save();

        Session::flash('success',[__('The role was successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request, $role_id) {

        $role = RestaurantLocationStaffRole::find($role_id);

        if($role === null) {
            Session::flash('errors',[[__('The given role id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:2|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }


        $role -> name = $request -> name ? $request -> name : $role -> name;

        $role-> save();


        Session::flash('success',[__('The role was successfully edited!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $role_id) {

        $role = RestaurantLocationStaffRole::find($role_id);

        if($role === null) {
            Session::flash('errors',[[__('The given role id is invalid!')]]);
            return redirect()->back();
        }



        $role-> delete();
        Session::flash('success',[__('The role has been deleted successfully!')]);
        return redirect()->back();
    }

}
