<?php

namespace App\Http\Controllers\Admin;

use App\RestaurantLocation;
use App\User;
use App\RestaurantProfile;
use Carbon\Carbon;
use App\City;
use App\County;
use App\Country;
use Illuminate\Http\Request;
use App\RestaurantLocationStaff;
use App\RestaurantLocationStaffHasRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;


class RestaurantLocationStaffsController extends Controller
{

    public function index() {

        $staffs = RestaurantLocationStaff::paginate(10);

        return view('/admin/restaurants_locations_staffs/index', ['staffs' => $staffs]);

    }

    public function search(Request $request) {

        $staffs = RestaurantLocationStaff::where('job', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('job') -> paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/restaurants_locations_staffs/index', compact(
            'staffs',
            'countries',
            'counties',
            'cities'
        ));
    }


    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'contract_type_id' => 'required|numeric|exists:contract_types,id',
            'salary' => 'required|numeric',
            'salary_currency_id' => 'required|numeric|exists:currencies,id',
            'start_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'job_name' => 'required|string|min:1|max:45',
            'profesionist_profile_id' => 'sometimes|required|numeric|exists:profesionist_profiles,id',
            'restaurant_location_id' => 'sometimes|required|numeric|exists:restaurant_locations,id',
            'roles_id' => 'required',
            'roles_id.*' => 'required|numeric|exists:restaurant_location_staff_roles,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();
        }

        if($request -> contract_type_id == 3 || $request -> contract_type_id == 1) {
            if($request -> start_date == null || $request -> end_date == null) {

                Session::flash('errors',[[__('For seasonal and definite contract type the begin and and end date are required!')]]);
                return redirect()->back();

            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors',[[__('The start date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors',[[__('The start date must be lower or equal with the end date!')]]);
            return redirect()->back();
        }

//        if(($request -> professionist_profile_id == null && $request -> client_profile_id == null) || ($request -> professionist_profile_id != null && $request -> client_profile_id != null)) {
//            Session::flash('errors',[__('The request must contain professtionist profile id or client profile id!')]);
//            return redirect()->back();
//        }

        $staff = new RestaurantLocationStaff();

        $staff -> contract_type_id = $request -> contract_type_id;

        $staff -> salary = $request -> salary;

        $staff -> salary_currency_id = $request -> salary_currency_id;

        $staff -> start_date = $request -> start_date;

        $staff -> end_date = $request -> end_date;

        $staff -> job = $request -> job_name;

        $staff -> restaurant_location_id = $request -> restaurant_location_id ? $request -> restaurant_location_id : $staff -> restaurant_location_id;

        $staff -> profesionist_profile_id = $request -> profesionist_profile_id ? $request -> profesionist_profile_id : $staff -> profesionist_profile_id;

        $staff -> contractType;

        $staff -> currency;

        $staff -> location;

        $staff -> clientProfile;

        $staff -> profesionistProfile;

        $staff -> save();

            foreach ($request->roles_id as $key => $role_id) {

                $restaurant_location_staff_has_roles = new RestaurantLocationStaffHasRole();

                $restaurant_location_staff_has_roles->staff()->associate($staff);

                $restaurant_location_staff_has_roles->role_id= $role_id;

                $restaurant_location_staff_has_roles->save();
            }

        Session::flash('success',[__('The staff has been successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request, $staff_id) {

        $staff = RestaurantLocationStaff::find($staff_id);

        if($staff === null) {

            Session::flash('success', [__('The staff was not found!')]);

            return redirect() -> back();
        }
        if($staff === null) {
            Session::flash('errors',[[__('The given staff id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'contract_type_id' => 'sometimes|required|numeric|exists:contract_types,id',
            'salary' => 'sometimes|required|numeric',
            'roles_id' => 'required',
            'roles_id.*' => 'required|numeric|exists:restaurant_location_staff_roles,id',
            'salary_currency_id' => 'sometimes|required|numeric|exists:currencies,id',
            'profesionist_profile_id' => 'sometimes|required|numeric|exists:profesionist_profiles,id',
            'restaurant_location_id' => 'sometimes|required|numeric|exists:restaurant_locations,id',
            'start_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'job' => 'sometimes|required|string|min:1|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors',$errors);
            return redirect()->back();
        }

        if($request -> contract_type_id == 3 || $request -> contract_type_id == 1) {
            if($request -> start_date == null || $request -> end_date == null) {
                Session::flash('errors',[[__('For seasonal and definite contract type the begin and and end date are required!')]]);
                return redirect()->back();

            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors',[[__('The start date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors',[[__('The start date must be lower or equal with the end date!')]]);
            return redirect()->back();
        }

        $staff -> contract_type_id = $request -> contract_type_id ? $request -> contract_type_id : $staff -> contract_type_id;

        $staff -> salary = $request -> salary ? $request -> salary : $staff -> salary;

        $staff -> salary_currency_id = $request -> salary_currency_id ? $request -> salary_currency_id : $staff -> salary_currency_id;

        if($request -> contract_type_id == 2) {

            $staff -> start_date = null;

            $staff -> end_date = null;

        } else {

            $staff -> start_date = $request -> start_date ? $request -> start_date : $staff -> start_date;

            $staff -> end_date = $request -> end_date ? $request -> end_date : $staff -> end_date;
        }

        $staff -> job = $request -> job_name ? $request -> job_name : $staff -> job_name;



        $staff -> save();

        Session::flash('success',[__('The staff has been successfully updated!')]);
        return redirect()->back();


    }

    public function delete(Request $request, $staff_id) {

        $staff = RestaurantLocationStaff::find($staff_id);

        if($staff === null) {

            Session::flash('errors', [[__('The staff was not found!')]]);

            return redirect() -> back();
        }

//        $location = $restaurant -> restaurantLocations -> find($location_id);

//        if($location === null) {
//
//            Session::flash('errors',[__('The given restaurant location id is invalid!')]);
//            return redirect()->back();
//        }

//        $staff = $location -> staff -> find($staff_id);

        if($staff === null) {
            Session::flash('errors',[[__('The given staff id is invalid!')]]);
            return redirect()->back();

        }

        $staff -> delete();
        Session::flash('success',[__('The staff has been deleted!')]);
        return redirect()->back();

    }
}
