<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Storage;
use App\User;
use App\Hotel;
use App\Rating;
use App\ProgramsCondition;
use App\RestaurantProfile;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationProgram;
use App\RestaurantLocationPicture;
use App\RestaurantLocationFacility;
use App\RestaurantLocationHasFacility;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\County;
use App\Country;
use App\City;
use Illuminate\Support\Facades\Session;
class RestaurantLocationsController extends Controller
{
    public function index(){
        $restaurant_locations = RestaurantLocation::paginate(10);
        return view('admin.restaurants_locations.index',['restaurant_locations'=>$restaurant_locations]);
    }

    public function search(Request $request) {

        $restaurant_locations = RestaurantLocation::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name')->paginate(10);



        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();


        return view('/admin/restaurants_locations/index', compact(
            'restaurant_locations',
            'countries',
            'counties',
            'cities'
        ));
    }

    public function add(Request $request)
    {

        if ($request->hasFile('pictures')) {
            $pictures = FilesController::uploadFile($request, 'pictures', 'restaurant_locations', array('png', 'jpg', 'jpeg', 'gif'), true);
            return $pictures;
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:45',
            'email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:producer_profile_locations,email',
            'description' => 'required|string|min:5|max:512',
            'city_id' => 'required|numeric|exists:cities,id',
            'county_id' => 'required|numeric|exists:counties,id',
            'country_id' => 'required|numeric|exists:countries,id',
            'street' => 'required|string|min:2|max:100',
            'number' => 'required|numeric',
            'postal_code' => 'sometimes|required|string|min:1|max:45',
            'latitude' => 'required',
            'longitude' => 'required',
            'phone_number' => 'sometimes|required|string|min:1|max:45',
            'fixed_phone_number' => 'sometimes|required|string|min:1|max:45',
            'contact_person_name' => 'sometimes|required|string|min:1|max:45',
            'owns_event_room' => 'required|numeric',
            'facilities_id' => 'required',
            'facilities_id.*' => 'required|numeric|exists:restaurant_location_facilities,id',
            'locality_specific' => 'required|string|min:1|max:45',
            'music_type_id' => 'required|exists:music_types,id',
            'live_music' => 'boolean',
            'tables_number' => 'required|numeric',
            'bathrooms_number' => 'required|numeric',
            'locality_surface' => 'required|numeric',
            'framing' => 'required|string|min:1|max:45',
            'number_of_seats' => 'required|numeric',
            'michelin_stars' => 'required|numeric',
            'catering' => 'required|boolean',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',
            'program_names' => 'sometimes|required',
            'program_names.*' => 'required|string|min:3|max:45',
            'program_types' => 'sometimes|required',
            'program_types.*' => 'required|string|min:3|max:45',
            'start_dates' => 'sometimes|required',
            'start_dates.*' => 'required|date',
            'end_dates' => 'sometimes|required',
            'end_dates.*' => 'required|date',
        ]);
        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }

        $restaurant_location = new RestaurantLocation();

        $restaurant_location->restaurant_profile_id = $request->restaurant_profile_id;

        $restaurant_location->name = $request->name;

        $restaurant_location->email = $request->email;

        $restaurant_location->description = $request->description;

        $restaurant_location->city_id = $request->city_id;

        $restaurant_location->county_id = $request->county_id;

        $restaurant_location->country_id = $request->country_id;

        $restaurant_location->street = $request->street;

        $restaurant_location->number = $request->number;

        $restaurant_location->postal_code = $request->postal_code;

        $restaurant_location->latitude = $request->latitude;

        $restaurant_location->longitude = $request->longitude;

        $restaurant_location->phone_number = $request->phone_number;

        $restaurant_location->fixed_phone_number = $request->fixed_phone_number;

        $restaurant_location->contact_person_name = $request->contact_person_name;

        $restaurant_location->locality_specific = $request->locality_specific;

        $restaurant_location->music_type_id = $request->music_type_id;

        $restaurant_location->live_music = $request->live_music;

        $restaurant_location->tables_number = $request->tables_number;

        $restaurant_location->bathrooms_number = $request->bathrooms_number;

        $restaurant_location->locality_surface = $request->locality_surface;

        $restaurant_location->framing = $request->framing;

        $restaurant_location->number_of_seats = $request->number_of_seats;

        $restaurant_location->michelin_stars = $request->michelin_stars;

        $restaurant_location->catering = $request->catering;

        $restaurant_location->phone_number = $request->phone_number;

        if ($request->owns_event_room == 0 || $request->owns_event_room == 1 || $request->owns_event_room == 2) {

            $restaurant_location->owns_event_room = $request->owns_event_room;
        } else {

            Session::flash('errors',[[__( 'The event room property is invalid!')]]);
            return redirect()->back();
        }
        $restaurant_location->save();

        foreach ($request->facilities_id as $key => $facility_id) {

            $restaurantt_has_facility = new RestaurantLocationHasFacility();

            $restaurantt_has_facility->restaurantLocation()->associate($restaurant_location);

            $restaurantt_has_facility->restaurant_location_facility_id = $facility_id;

            $restaurantt_has_facility->save();
        }

        $conditions = [];
//        foreach($request -> program_names as $key => $program_name) {
//
//            $program = new ProgramsCondition();
//
//            $program -> name = $program_name;
//
//            $program -> type = $request -> program_types[$key];
//
//            $program -> start_date = $request -> start_dates[$key];
//
//            $program -> end_date = $request -> end_dates[$key];
//
//            $program -> save();
//
//            $conditions[$key] = $program -> id;


        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $restaurant_location_pictures = new RestaurantLocationPicture();

                    $restaurant_location_pictures -> restaurantLocation() -> associate($restaurant_location);

                    $restaurant_location_pictures -> picture = $picture;

                    $restaurant_location_pictures -> save();
                }
            }
        }


            $restaurant_location->city;

            $restaurant_location->county;

            $restaurant_location->country;

            $restaurant_location->musicType;


        Session::flash('success',[__('Restaurant added successfully!')]);
        return redirect()->back();
    }



    // Edit restaurant location data
    public function save(Request $request, $location_id)
    {

        $restaurant_location = RestaurantLocation::find($location_id);

      if($restaurant_location === null) {
          Session::flash('errors', [[__('The restaurant location is not found!')]]);
          return redirect()->back();
      }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:producer_profile_locations,email',
            'description' => 'sometimes|required|string|min:5|max:512',
            'city_id' => 'sometimes|required|numeric|exists:cities,id',
            'county_id' => 'sometimes|required|numeric|exists:counties,id',
            'country_id' => 'sometimes|required|numeric|exists:countries,id',
            'street' => 'sometimes|required|string|min:2|max:100',
            'number' => 'sometimes|required|numeric',
            'postal_code' => 'sometimes|required|string|min:1|max:45',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'phone_number' => 'sometimes|required|string|min:1|max:45',
            'fixed_phone_number' => 'sometimes|required|string|min:1|max:45',
            'contact_person_name' => 'sometimes|required|string|min:1|max:45',
            'owns_event_room' => 'sometimes|required|numeric',
            'facilities_id' => 'sometimes|required',
            'facilities_id.*' => 'sometimes|required|numeric|exists:restaurant_location_facilities,id',
            'delete_facilities' => 'sometimes|required',
            'delete_facilities.*' => 'required|numeric|exists:restaurant_location_has_facility,id',
            'locality_specific' => 'sometimes|required|string|min:1|max:45',
            'music_type_id' => 'sometimes|required|exists:music_types,id',
            'live_music' => 'sometimes|boolean',
            'tables_number' => 'sometimes|required|numeric',
            'bathrooms_number' => 'sometimes|required|numeric',
            'locality_surface' => 'sometimes|required|numeric',
            'framing' => 'sometimes|required|string|min:1|max:45',
            'number_of_seats' => 'sometimes|required|numeric',
            'michelin_stars' => 'sometimes|required|numeric',
            'catering' => 'sometimes|required|boolean',
            'picture' => 'sometimes|required',
            'picture.*' => 'sometimes|required|string',
            'program_names' => 'sometimes|required',
            'program_names.*' => 'sometimes|required|string|min:3|max:45',
            'program_types' => 'sometimes|required',
            'program_types.*' => 'sometimes|required|string|min:3|max:45',
            'start_dates' => 'sometimes|required',
            'start_dates.*' => 'sometimes|required|date',
            'end_dates' => 'sometimes|required',
            'end_dates.*' => 'sometimes|required|date',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }

        $restaurant_location -> name = $request -> name ? $request -> name : $restaurant_location -> name;

        $restaurant_location -> description = $request -> description ? $request -> description : $restaurant_location -> description;

        $restaurant_location -> city_id = $request -> city_id ? $request -> city_id : $restaurant_location -> city_id;

        $restaurant_location -> county_id = $request -> county_id ? $request -> county_id : $restaurant_location -> county_id;

        $restaurant_location -> country_id = $request -> country_id ? $request -> country_id : $restaurant_location -> country_id;

        $restaurant_location -> street = $request -> street ? $request -> street : $restaurant_location -> street;

        $restaurant_location -> number = $request -> number ? $request -> number : $restaurant_location -> number;

        $restaurant_location -> postal_code = $request -> postal_code ? $request -> postal_code : $restaurant_location -> postal_code;

        $restaurant_location -> latitude = $request -> latitude ? $request -> latitude : $restaurant_location -> latitude;

        $restaurant_location -> longitude = $request -> longitude ? $request -> longitude : $restaurant_location -> longitude;

        $restaurant_location -> phone_number = $request -> phone_number ? $request -> phone_number : $restaurant_location -> phone_number;

        $restaurant_location -> fixed_phone_number = $request -> fixed_phone_number ? $request -> fixed_phone_number : $restaurant_location -> fixed_phone_number;

        $restaurant_location -> contact_person_name = $request -> contact_person_name ? $request -> contact_person_name : $restaurant_location -> contact_person_name;

        $restaurant_location -> locality_specific = $request -> locality_specific ? $request -> locality_specific : $restaurant_location -> locality_specific;

        $restaurant_location -> music_type_id = $request -> music_type_id ? $request -> music_type_id : $restaurant_location -> music_type_id;

        $restaurant_location -> live_music = $request -> live_music ? $request -> live_music : $restaurant_location -> live_music;

        $restaurant_location -> tables_number = $request -> tables_number ? $request -> tables_number : $restaurant_location -> tables_number;

        $restaurant_location -> bathrooms_number = $request -> bathrooms_number ? $request -> bathrooms_number : $restaurant_location -> bathrooms_number;

        $restaurant_location -> locality_surface = $request -> locality_surface ? $request -> locality_surface : $restaurant_location -> locality_surface;

        $restaurant_location -> framing = $request -> framing ? $request -> framing : $restaurant_location -> framing;

        $restaurant_location -> number_of_seats = $request -> number_of_seats ? $request -> number_of_seats : $restaurant_location -> number_of_seats;

        $restaurant_location -> michelin_stars = $request -> michelin_stars ? $request -> michelin_stars : $restaurant_location -> michelin_stars;

        $restaurant_location -> catering = $request -> catering ? $request -> catering : $restaurant_location -> catering;

        $restaurant_location -> phone_number = $request -> phone_number ? $request -> phone_number : $restaurant_location -> phone_number;

        if($request -> owns_event_room == 0 || $request -> owns_event_room == 1 || $request -> owns_event_room == 2) {

            $restaurant_location -> owns_event_room = $request -> owns_event_room ? $request -> owns_event_room : $restaurant_location -> owns_event_room;
        } else {

            Session::flash('errors', [[__('The event room property is invalid!')]]);
            return redirect()->back();

        }
        $restaurant_location -> save();

        if($request -> facilities_id != null && count($request -> facilities_id) > 0) {

            foreach ($request -> facilities_id as $key => $facility_id) {

                if(!RestaurantLocationHasFacility::where('restaurant_location_id', $restaurant_location -> id) -> where('restaurant_location_facility_id', $facility_id) -> first()) {

                    $restaurant_has_facility = new RestaurantLocationHasFacility();

                    $restaurant_has_facility -> restaurantLocation() -> associate($restaurant_location);

                    $restaurant_has_facility -> restaurant_location_facility_id = $facility_id;

                    $restaurant_has_facility -> save();
                }
            }
        }

        if(isset($request -> delete_facilities)) {

            foreach ($request -> delete_facilities as $key => $facility_id) {

                $restaurant_location_has_facility = RestaurantLocationHasFacility::find($facility_id);

                if($restaurant_location_has_facility && $restaurant_location_has_facility -> restaurant_location_id == $restaurant_location -> id) {

                    $restaurant_location_has_facility -> forceDelete();
                }
            }
        }


        $restaurant_location -> city;

        $restaurant_location -> county;

        $restaurant_location -> country;

        $restaurant_location -> musicType;



        Session::flash('success',[__('Restaurant edited successfully!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $location_id)
    {

            $restaurant_location = RestaurantLocation::find($location_id);

            if($restaurant_location === null) {

                Session::flash('errors', [[__('The restaurant location is invalid!')]]);
                return redirect()->back();
            }

            $restaurant_location -> buyedSponsorships() -> delete();

            $restaurant_location -> buyedSponsorshipsFor() -> delete();

            $restaurant_location -> dailyMenuAvailabilities() -> delete();

            $restaurant_location -> eventRooms() -> delete();

            $restaurant_location -> menus() -> delete();

            $restaurant_location -> postsFrom() -> delete();

            $restaurant_location -> postsTo() -> delete();

            $restaurant_location -> ratings() -> delete();

            $restaurant_location -> restaurantLocationEvents() -> delete();

            $restaurant_location -> restaurantLocationHasFacilities() -> delete();

            $restaurant_location -> restaurantLocationHasPosts() -> delete();

            $restaurant_location -> restaurantLocationUtilities() -> delete();

            $restaurant_location -> restaurantLocationPictures() -> delete();

            $restaurant_location -> restaurantLocationPrograms() -> delete();

            $restaurant_location -> restaurantLocationPromotions() -> delete();

//            $restaurant_location -> Staffs() -> delete();

            $restaurant_location -> sponsorshipsRequests() -> delete();

            $restaurant_location -> delete();

        Session::flash('success',[__('Restaurant has been deleted successfully!')]);
        return redirect()->back();
    }

}
