<?php

namespace App\Http\Controllers\Admin;

use App\Location;
use App\User;
use App\JobOffer;
use App\City;
use App\Country;
use App\County;
use Carbon\Carbon;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationsJobsController extends Controller
{

    public function index()
    {
//        return $jobs[0] -> restaurantLocation;

        $jobs = JobOffer::paginate(10);

        return view('admin.restaurants_job_offers.index', ['jobs' => $jobs]);
    }



    public function search(Request $request) {

        $jobs = JobOffer::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name') -> paginate(10);


        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/restaurants_job_offers/index', compact(
            'jobs',
            'countries',
            'counties',
            'cities'
        ));
    }
    //Add job
    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'restaurant_location_id'=>'required|exists:restaurant_locations,id',
            'restaurant_profile_id'=>'required|exists:restaurant_profiles,id',
            'producer_profile_id'=>'required|exists:producer_profiles,id',
            'description' => 'required|string|min:3',
            'contract_type_id' => 'required|exists:contract_types,id',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'salary' => 'required|numeric:min:1',
            'currency_id' => 'required|numeric|exists:currencies,id',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'location_id' => 'sometimes|required|exists:locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();
        }

        if($request -> location_id === null && ($request -> latitude === null || $request -> longitude === null)) {

            Session::flash('errors', [[__('The location id or latitude and longitude are required!')]]);
            return redirect()->back();
        }

        if($request -> contract_type_id == 3 || $request -> contract_type_id == 1) {
            if($request -> begin_date == null || $request -> begin_date == null) {
                Session::flash('errors', [[__('For seasonal and definite contract type the begin and and end date are required!')]]);
                return redirect()->back();
            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors', [[__('The begin date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors', [[__('The begin date must be lower or equal with the end date!')]]);
            return redirect()->back();


        }

        $job = new JobOffer();

        $job -> name = $request -> name;

        $job -> description = $request -> description;

        $job -> contract_type_id = $request -> contract_type_id;

        $job -> begin_date = $request -> begin_date;

        $job -> end_date = $request -> end_date;

        $job -> salary = $request -> salary;

        $job -> currency_id = $request -> currency_id;

        $job -> restaurant_profile_id = $request -> restaurant_profile_id;

        $job -> restaurant_location_id = $request -> restaurant_location_id;

        $job -> location_id = $request -> location_id ? $request -> location_id : null;

        if($request -> latitude && $request -> longitude) {

            $job -> latitude = $request -> latitude;

            $job -> longitude = $request -> longitude;
        }


        $job -> save();
        Session::flash('success', [__('Job was successfully added!')]);
        return redirect()->back();

    }

    // Edit job
    public function save(Request $request, $job_id) {

        $job = JobOffer::find($job_id);

        if($job === null) {

            Session::flash('errors', [[__('The given job id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'contract_type_id' => 'sometimes|required|exists:contract_types,id',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'salary' => 'sometimes|required|numeric:min:1',
            'currency_id' => 'sometimes|required|numeric|exists:currencies,id',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'location_id' => 'sometimes|required|exists:locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors',$errors);
            return redirect()->back();

        }

        if($request -> location_id === null && ($request -> latitude === null || $request -> longitude === null)) {

            Session::flash('errors',[[__('The location id or latitude and longitude are required!')]]);
            return redirect()->back();
        }

        if($request -> contract_type_id == 3) {

            if($request -> begin_date == null || $request -> end_date == null) {

                Session::flash('errors',[[__('For seasonal contract type the begin and and end date are required!')]]);
                return redirect()->back();
            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            Session::flash('errors',[[__('The begin date must be greater or equal with current date!')]]);
            return redirect()->back();
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            Session::flash('errors',[[__('The begin date must be lower or equal with the end date!')]]);
            return redirect()->back();
        }

        $job -> name = $request -> name ? $request -> name : $job -> name;

        $job -> description = $request -> description ? $request -> description : $job -> description;

        $job -> contract_type_id = $request -> contract_type_id ? $request -> contract_type_id : $job -> contract_type_id;

        $job -> begin_date = $request -> begin_date ? $request -> begin_date : $job -> begin_date;

        $job -> end_date = $request -> end_date ? $request -> end_date : $job -> end_date;

        $job -> salary = $request -> salary ? $request -> salary : $job -> salary;

        $job -> currency_id = $request -> currency_id ? $request -> currency_id : $job -> currency_id;

        $job -> location_id = $request -> location_id ? $request -> location_id : $job -> location_id;

        if($request -> latitude && $request -> longitude) {

            $job -> latitude = $request -> latitude;

            $job -> longitude = $request -> longitude;
        }

        $job -> save();

        Session::flash('success',[__('Job edited successfully!')]);
        return redirect()->back();
    }

    public function delete(Request $request, $job_id) {


        $job = JobOffer::find($request->$job_id);

        if($job === null) {
            Session::flash('errors',[[__('The given job id is invalid!')]]);
            return redirect()->back();

        }


        $job -> delete();

        Session::flash('success',[__('Job was deleted successfully!')]);
        return redirect()->back();

    }

}
