<?php
namespace App\Http\Controllers\Admin;


use App\Location;
use App\User;
use App\JobOffer;
use Carbon\Carbon;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\RestaurantSpecific;
use Illuminate\Support\Facades\Session;

class RestaurantSpecificsController extends Controller
{
    public function index() {

        $specifics = RestaurantSpecific::paginate(10);

        return view('/admin/restaurants_specifics/index', ['specifics' => $specifics]);

    }

    public function search(Request $request) {

        $specifics = RestaurantSpecific::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name')-> paginate(10);

        return view('/admin/restaurants_specifics/index', compact(
            'specifics'
        ));
    }


    //Add specific
    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();
        }

        $specific = new RestaurantSpecific();

        $specific -> name = $request -> name;

        $specific -> save();

        Session::flash('success',[_('The specific has been added successfully!')]);
        return redirect()->back();
    }


    public function edit(Request $request, $specific_id) {

        $specific = RestaurantSpecific::find($specific_id);

        if($specific == null) {

            Session::flash('errors', [[__('Invalid specific !')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }

        $specific -> name = $request -> name ? $request -> name : $specific -> name;

        $specific -> save();

        Session::flash('success',[__('The specific has been edited successfully!')]);
        return redirect()->back();
    }

    public function delete($specific_id) {

        $specific = RestaurantSpecific::find($specific_id);

        if($specific == null) {

            Session::flash('errors', [[__('Invalid specific profile!')]]);
            return redirect()->back();

        }

        $specific -> delete();

        Session::flash('success',[__('The specific has been deleted successfully!')]);
        return redirect()->back();


    }

}
