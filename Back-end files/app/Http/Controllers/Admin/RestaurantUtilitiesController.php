<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\County;
use App\HotelFacility;
use App\RestaurantLocationFacility;
use App\RestaurantLocationUtility;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RestaurantUtilitiesController extends Controller
{

    public function search(Request $request){
        $utilities = RestaurantLocationUtility::where('name', 'LIKE', '%'.$request -> keyword.'%')
            ->orderBy('name')->paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('\admin\restaurants_utilities\index', compact(
            'utilities',
            'countries',
            'counties',
            'cities'
        ));

    }

    public function index() {

        $utilities = RestaurantLocationUtility::paginate(10);

        return view('\admin\restaurants_utilities\index', compact(
            'utilities'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:2|max:45',
            'description' => 'sometimes|required|string|min:3|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }


        $utility = new RestaurantLocationUtility();

        $utility-> name = $request -> name;

        $utility -> description = $request -> description;

        $utility -> save();

        Session::flash('success',[__('The utility was successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request, $utility_id) {

        $utility = RestaurantLocationUtility::find($utility_id);

        if($utility === null) {
            Session::flash('errors',[[__('The given utility id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:2|max:45',
            'description' => 'sometimes|required|string|min:3|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }


        $utility -> name = $request -> name ? $request -> name : $utility -> name;

        $utility -> description = $request -> description ? $request -> description : $utility -> icon;

        $utility-> save();

        Session::flash('success',[__('The utility was successfully edited!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $utility_id) {

        $utility = RestaurantLocationUtility::find($utility_id);

        if($utility === null) {
            Session::flash('errors',[[__('The given utility id is invalid!')]]);
            return redirect()->back();
        }



        $utility-> delete();
        Session::flash('success',[__('The utility has been deleted successfully!')]);
        return redirect()->back();
    }

}
