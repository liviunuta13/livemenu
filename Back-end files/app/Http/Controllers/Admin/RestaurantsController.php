<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Session;
use App\City;
use App\County;
use App\Country;
use App\RestaurantProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RestaurantsController extends Controller
{
    public function index() {

        $restaurants = RestaurantProfile::paginate(10);

        return view('/admin/restaurants/index', compact(
            'restaurants'
        ));
    }

    public function search(Request $request) {

        $restaurants = RestaurantProfile::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('company_email', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('company_name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('administrator_name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('phone_number', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('fiscal_code', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name') -> paginate(10);

        $cities = City::all();

        $counties = County::all();

        $countries = Country::all();

        return view('/admin/restaurants/index', compact(
            'restaurants',
            'countries',
            'counties',
            'cities'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:1|max:45',
            'phone_number' => 'required|min:5|max:20',
            'company_name' => 'required|string|min:1|max:45',
            'fiscal_code' => 'required|string|min:3|max:45|unique:restaurant_profiles,fiscal_code',
            'administrator_name' => 'required|string|min:1|max:45',
            'company_email' => ['sometimes', 'required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:restaurant_profiles,company_email'],
            'website' => 'sometimes|required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'logo' => 'sometimes|required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'sometimes|required|string|min:3',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('restaurants'));
        }

        $admin = new RestaurantProfile();

        $admin -> name = $request -> name;

        $admin -> phone_number = $request -> phone_number;

        $admin -> company_name = $request -> company_name;

        $admin -> fiscal_code = $request -> fiscal_code;

        $admin -> administrator_name = $request -> administrator_name;

        $admin -> company_email = $request -> company_email ? $request -> company_email : null;

        $admin -> website = $request -> website ? $request -> website : null;

        $admin -> description = $request -> description ? $request -> description : null;

        if($request -> logo) {

            $file = $request -> file('logo');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $admin -> logo = $base64;
        }

        $admin -> save();

        Session::flash('success', [__('The restaurant has been added successfully!')]);

        return redirect(route('restaurants'));
    }

    public function edit(Request $request, $restaurant_id) {

        $restaurant = RestaurantProfile::find($restaurant_id);

        if($restaurant === null) {

            Session::flash('success', [__('Restaurant not found!')]);

            return redirect() -> back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|nullable|string|min:1|max:45',
            'phone_number' => 'sometimes|nullable|min:5|max:20',
            'company_name' => 'sometimes|nullable|string|min:1|max:45',
            'fiscal_code' => 'sometimes|string|min:3|max:45|unique:restaurant_profiles,fiscal_code,' . $restaurant -> id,
            'administrator_name' => 'sometimes|nullable|string|min:1|max:45',
            'company_email' => 'sometimes|nullable|max:45|email|regex:/(.*)/i|unique:restaurant_profiles,company_email,' . $restaurant -> id,
            'website' => 'sometimes|nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'logo' => 'sometimes|nullable|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'sometimes|nullable|string|min:3',
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('restaurants'));
        }

        $restaurant -> name = $request -> name ? $request -> name : $restaurant -> name;

        $restaurant -> phone_number = $request -> phone_number ?  $request -> phone_number :  $restaurant -> phone_number;

        $restaurant -> company_name = $request -> company_name ? $request -> company_name : $restaurant -> company_name;

        $restaurant -> fiscal_code = $request -> fiscal_code ? $request -> fiscal_code : $restaurant -> fiscal_code;

        $restaurant -> administrator_name = $request -> administrator_name ? $request -> administrator_name : $restaurant -> administrator_name;

        $restaurant -> company_email = $request -> company_email;

        $restaurant -> website = $request -> website;

        $restaurant -> description = $request -> description;

        if($request -> logo) {

            $file = $request -> file('logo');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $restaurant -> logo = $base64;
        }

        $restaurant -> save();

        Session::flash('success', [__('The restaurant has been edited successfully!')]);

        return redirect(route('restaurants'));
    }

    public function delete($restaurant_id) {

        $restaurant = RestaurantProfile::find($restaurant_id);

        if($restaurant === null) {

            Session::flash('errors', [[__('Restaurant not found!')]]);

            return redirect() -> back();
        }

//         $restaurant -> follows() -> delete();

        $restaurant -> jobOffers() -> delete();

        $restaurant -> postsLocationCheckin() -> delete();

        $restaurant -> postsFrom() -> delete();

        $restaurant -> postsTo() -> delete();

        $restaurant -> restaurantLocations() -> delete();

        $restaurant -> forceDelete();

        Session::flash('success', [__('The restaurant has been deleted!')]);

        return redirect(route('restaurants'));
    }

    public function suspend($restaurant_id) {

        $restaurant = RestaurantProfile::find($restaurant_id);

        if($restaurant == null) {

            Session::flash('errors', [[__('Invalid restaurant profile!')]]);

            return redirect() -> back();
        }

        if($restaurant -> suspended == 0) {

            $restaurant -> suspended = 1;

            $restaurant -> save();

            Session::flash('success', [__('The restaurant has been suspended!')]);

        } else {

            $restaurant -> suspended = 0;

            $restaurant -> save();

            Session::flash('success', [__('The restaurant has been unsuspended!')]);
        }

        return redirect() -> back();
    }
}
