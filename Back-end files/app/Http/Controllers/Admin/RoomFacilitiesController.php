<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\County;
use App\HotelFacility;
use App\RestaurantLocationFacility;
use App\RoomFacility;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RoomFacilitiesController extends Controller
{

    public function search(Request $request){
        $room_facilities = RoomFacility::where('name', 'LIKE', '%'.$request -> keyword.'%')
            ->orderBy('name')->paginate(10);

        $cities = City::all();
        $counties = County::all();
        $countries = Country::all();

        return view('/admin/rooms_facilities/index', compact(
            'room_facilities',
            'countries',
            'counties',
            'cities'
        ));

    }

    public function index() {

        $room_facilities = RoomFacility::paginate(10);

        return view('/admin/rooms_facilities/index', compact(
            'room_facilities'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:2|max:45',
            'icon' => 'required|string|min:3|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }


        $room_facility = new RoomFacility();

        $room_facility -> name = $request -> name;

        $room_facility -> icon = $request -> icon;

        $room_facility -> save();

        Session::flash('success',[__('The facility was successfully added!')]);
        return redirect()->back();

    }

    public function save(Request $request, $room_facility_id) {

        $room_facility = RoomFacility::find($room_facility_id);

        if($room_facility === null) {
            Session::flash('errors',[[__('The given facility id is invalid!')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:2|max:45',
            'icon' => 'sometimes|required|string|min:3|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors',$errors);
            return redirect()->back();
        }


        $room_facility -> name = $request -> name ? $request -> name : $room_facility -> name;

        $room_facility -> icon = $request -> icon ? $request -> icon : $room_facility -> icon;

        $room_facility-> save();

        Session::flash('success',[__('The facility was successfully edited!')]);
        return redirect()->back();

    }

    public function delete(Request $request, $room_facility_id) {

        $room_facility = RoomFacility::find($room_facility_id);

        if($room_facility === null) {
            Session::flash('errors',[[__('The given facility id is invalid!')]]);
            return redirect()->back();
        }



        $room_facility-> delete();
        Session::flash('success',[__('The facility has been deleted successfully!')]);
        return redirect()->back();
    }

}
