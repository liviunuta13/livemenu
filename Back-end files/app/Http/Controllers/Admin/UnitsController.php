<?php
namespace App\Http\Controllers\Admin;


use App\Location;
use App\Unit;
use App\User;
use App\JobOffer;
use Carbon\Carbon;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\RestaurantSpecific;
use Illuminate\Support\Facades\Session;

class UnitsController extends Controller
{
    public function index() {

        $units = Unit::paginate(10);

        return view('/admin/units/index', ['units' => $units]);

    }

    public function search(Request $request) {

        $units = Unit::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name')-> paginate(10);

        return view('/admin/units/index', compact(
            'units'
        ));
    }


    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:1|max:45'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);
            return redirect()->back();
        }

        $unit = new Unit();

        $unit -> name = $request -> name;

        $unit -> save();

        Session::flash('success',[_('The unit has been added successfully!')]);
        return redirect()->back();
    }


    public function edit(Request $request, $unit_id) {

        $unit = Unit::find($unit_id);

        if($unit == null) {

            Session::flash('errors', [[__('Invalid unit !')]]);
            return redirect()->back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }

            Session::flash('errors', $errors);
            return redirect()->back();
        }

        $unit -> name = $request -> name ? $request -> name : $unit -> name;

        $unit -> save();

        Session::flash('success',[__('The unit has been edited successfully!')]);
        return redirect()->back();
    }

    public function delete($unit_id) {

        $unit = Unit::find($unit_id);

        if($unit == null) {

            Session::flash('errors', [[__('Invalid unit profile!')]]);
            return redirect()->back();

        }

        $unit -> delete();

        Session::flash('success',[__('The unit has been deleted successfully!')]);
        return redirect()->back();


    }

}
