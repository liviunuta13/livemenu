<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\User;
use App\UserLoginToken;
use Illuminate\Http\Request;
use App\Mail\VerifyMailFromAdmin;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function index() {

        $users = User::orderBy('name') -> paginate(10);

        return view('/admin/users/index', compact(
            'users'
        ));
    }

    public function search(Request $request) {

        $users = User::where('name', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('email', 'LIKE', '%'.$request -> keyword.'%')
            -> orWhere('nickname', 'LIKE', '%'.$request -> keyword.'%')
            -> orderBy('name') -> paginate(10);

        return view('/admin/users/index', compact(
            'users'
        ));
    }

    public function add(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'nickname' => 'nullable|string|min:3|max:45',
            'email' => ['required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:users,email'],
            'password' => 'required|string|min:8|max:190|regex:/^.*(?=.{3})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|confirmed',
            'profile_picture' => 'sometimes|nullable|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'favorite_food' => 'sometimes|nullable|string|min:1|max:45',
            'secret_ingredients' => 'sometimes|nullable|string|min:1|max:45',
            'restaurant_specifics' => 'sometimes|nullable|string|min:1|max:45',
            'music_types' => 'sometimes|nullable|string|min:1|max:45',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('users'));
        }
        $user = new User();

        $user -> name = $request -> name;

        $user -> email = $request -> email;

        $user -> password = bcrypt($request -> password);

        $user -> nickname = $request -> nickname;

        $user -> favorite_food = $request -> favorite_food ? $request -> favorite_food : $user -> favorite_food;

        $user -> secret_ingredients = $request -> secret_ingredients ? $request -> secret_ingredients : $user -> secret_ingredients;

        $user -> restaurant_specifics = $request -> restaurant_specifics ? $request -> restaurant_specifics : $user -> restaurant_specifics;

        $user -> music_types = $request -> music_types ? $request -> music_types : $user -> music_types;

        if($request -> profile_picture) {

            $file = $request -> file('profile_picture');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $user -> profile_picture = $base64;
        }

        $user -> save();

        $user_login_token = new UserLoginToken();

        $user_login_token -> user() -> associate($user);

        $user_login_token -> token  = str_random(65);

        $user_login_token -> expire_date = now() -> addMonth(3);

        $user_login_token -> ip = $request -> getClientIp();

        try {

            $coordinates = explode(",", json_decode(file_get_contents("http://ipinfo.io/")) -> loc);

            $user_login_token -> latitude = $coordinates[0];

            $user_login_token -> longitude = $coordinates[1];

        } catch (\Exception $e) {}

        $user_login_token -> latitude = $coordinates[0];

        $user_login_token -> longitude = $coordinates[1];

        $user_login_token -> user_agent = $_SERVER['HTTP_USER_AGENT'];

        $user_login_token -> save();

        $user -> loginTokens() -> save($user_login_token);

        // Send email confirmation
        try {

            Mail::to($user -> email) -> send(new VerifyMailFromAdmin($user -> id, $user -> loginTokens -> sortByDesc('id') -> first() -> token));

        } catch (\Exception $e) {

            $user -> delete();

            Session::flash('error', [[__('The user\'s account was not created, it\'s a problem with the email!')]]);

            return redirect(route('users'));
        }

        Session::flash('success', [__('The user has been added and the email confirmation has been sended!')]);

        return redirect(route('users'));
    }

    public function edit(Request $request, $user_id) {

        $user = User::find($user_id);

        if($user === null) {

            Session::flash('errors', [[__('User not found!')]]);

            return redirect() -> back();
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'nickname' => 'sometimes|nullable|string|min:3|max:45',
            'email' => ['sometimes', 'required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:users,email,' . $user -> id],
            'password' => 'sometimes|nullable|string|min:8|max:190|regex:/^.*(?=.{3})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|confirmed',
            'profile_picture' => 'sometimes|nullable|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'favorite_food' => 'sometimes|nullable|string|min:1|max:45',
            'secret_ingredients' => 'sometimes|nullable|string|min:1|max:45',
            'restaurant_specifics' => 'sometimes|nullable|string|min:1|max:45',
            'music_types' => 'sometimes|nullable|string|min:1|max:45',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            Session::flash('errors', $errors);

            return redirect(route('users'));
        }

        $user -> name = $request -> name ? $request -> name : $user -> name;

        $user -> nickname = $request -> nickname;

        $user -> favorite_food = $request -> favorite_food;

        $user -> secret_ingredients = $request -> secret_ingredients;

        $user -> restaurant_specifics = $request -> restaurant_specifics;

        $user -> music_types = $request -> music_types;

        if($request -> password) {

            $user -> password = bcrypt($request -> password);
        }

        if($request -> profile_picture) {

            $file = $request -> file('profile_picture');

            $imagedata = file_get_contents($file);

            $base64 = base64_encode($imagedata);

            $user -> profile_picture = $base64;
        }

        $user -> save();

        $sendMail = false;

        if($request -> email != $user -> email) {

            $sendMail = true;

            foreach ($user -> loginTokens() as $key => $token) {

                $token -> delete();
            }

            $user -> email = $request -> email;

            $user -> confirmed_email = 0;

            $user_login_token = new UserLoginToken();

            $user_login_token -> user() -> associate($user);

            $user_login_token -> token  = str_random(65);

            $user_login_token -> expire_date = now() -> addMonth(3);

            $user_login_token -> ip = $request -> getClientIp();

            try {
                $coordinates = explode(",", json_decode(file_get_contents("http://ipinfo.io/")) -> loc);

                $user_login_token -> latitude = $coordinates[0];

                $user_login_token -> longitude = $coordinates[1];

            } catch (\Exception $e) {}

            $user_login_token -> user_agent = $_SERVER['HTTP_USER_AGENT'];

            $user_login_token -> save();

            $user -> loginTokens() -> save($user_login_token);

            $user -> save();

            // Send email confirmation
            try {

                Mail::to($user -> email) -> send(new VerifyMailFromAdmin($user -> id, $user -> loginTokens -> sortByDesc('id') -> first() -> token));

            } catch (\Exception $e) {

                $user -> delete();

                Session::flash('error', [[__('The user\'s account was not created, it\'s a problem with the email!')]]);

                return redirect(route('users'));
            }
        }
        if($sendMail == true) {

            Session::flash('success', [__('The user has been edited and the email confirmation has been sended!')]);
        } else {

            Session::flash('success', [__('The user has been edited successfully!')]);
        }
        return redirect(route('users'));
    }

    public function delete($user_id) {

        $user = User::find($user_id);

        if($user === null) {

            Session::flash('errors', [[__('User not found!')]]);

            return redirect() -> back();
        }

        $user -> clientProfile() -> delete();

        $user -> producerProfile() -> delete();

        $user -> profesionistProfile() -> delete();

        $user -> restaurantProfile() -> delete();

        $user -> follows() -> delete();

//        $user -> loginTokens() -> delete();

        $user -> delete();

        Session::flash('success', [__('The user has been deleted!')]);

        return redirect(route('users'));
    }
}
