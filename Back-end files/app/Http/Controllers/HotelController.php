<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\HotelFacility;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    public function getFacilities() {

        $facilities = HotelFacility::all();

        return json_encode([
            'success' => true,
            'message' => __('The hotel facilities have been successfully taken over!'),
            'data' => $facilities
        ]);
    }

    public function readHotels() {

        $hotels = Hotel::all();

        foreach ($hotels as $key => $hotel) {

            $hotel -> restaurantLocation;

            $hotel -> hotelPictures;

            $hotel -> rooms;
        }

        return json_encode([
            'success' => true,
            'message' => __('The hotels were successfully taken over!'),
            'data' => $hotels
        ]);
    }
}
