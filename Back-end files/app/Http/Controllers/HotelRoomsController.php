<?php

namespace App\Http\Controllers;

use App\Room;
use App\User;
use App\Hotel;
use App\RoomPicture;
use App\RoomBedType;
use App\RoomFacility;
use App\RoomHasBedType;
use App\RoomHasFacility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HotelRoomsController extends Controller
{
    public function add(Request $request, $hotel_id) {

        $hotel = Hotel::find($hotel_id);

        if($hotel === null) {
            return json_encode([
                'success' => false,
                'message' => __('The hotel id is invalid!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'surface' => 'required|min:1',
            'unit_id' => 'required|numeric|exists:units,id',
            'seats' => 'required|numeric|min:1',
            'simple_bed_number' => 'sometimes|required|numeric|min:1',
            'double_bed_number' => 'sometimes|required|numeric|min:1',
            'triple_bed_number' => 'sometimes|required|numeric|min:1',
            'additional_seats' => 'sometimes|required|numeric|min:1',
            'facilities' => 'required',
            'facilities.*' => 'required|numeric|exists:room_facilities,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $room = new Room();

        $room -> name = $request -> name;

        $room ->  surface = $request -> surface;

        $room ->  unit_id = $request -> unit_id;

        $room -> seats = $request -> seats;

        $room -> additional_seats = $request -> additional_seats ? $request -> additional_seats : null;

        $room -> hotel() -> associate($hotel);

        $room -> save();

        $room -> unit;

        if($request -> simple_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 1) -> first() -> id;

            $room_has_bed -> number = $request -> simple_bed_number;

            $room_has_bed -> save();
        }

        if($request -> double_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 2) -> first() -> id;

            $room_has_bed -> number = $request -> double_bed_number;

            $room_has_bed -> save();
        }

        if($request -> triple_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 3) -> first() -> id;

            $room_has_bed -> number = $request -> triple_bed_number;

            $room_has_bed -> save();
        }

        foreach ($request -> facilities as $key => $facility) {

            $room_has_facility = new RoomHasFacility();

            $room_has_facility -> room() -> associate($room);

            $room_has_facility -> room_facility_id = $facility;

            $room_has_facility -> save();
        }

        if(count($request -> pictures) > 0) {

            foreach ($request pictures as $key => $picture) {
             {
                    $room_picture = new RoomPicture();

                    $room_picture -> room() -> associate($room);

                    $room_picture -> picture = $picture;

                    $room_picture -> save();
                }
            }
        }

        $room -> hotel;

        foreach ($room -> roomHasBedTypes as $key => $bed_type) {

            $bed_type -> bedType;
        }

        $room -> roomHasFacilities;

        $room -> roomPictures;

        return json_encode([
            'success' => true,
            'message' => __('The room was successfully added!'),
            'data' => $room
        ]);
    }

    // Edit room
    public function save(Request $request, $hotel_id, $room_id) {

        $hotel = Hotel::find($hotel_id);

        $room = Room::find($room_id);

        if($hotel === null) {
            return json_encode([
                'success' => false,
                'message' => __('The hotel id is invalid!')
            ]);
        }

        if($room === null) {
            return json_encode([
                'success' => false,
                'message' => __('The room id is invalid!')
            ]);
        }

        if($room -> hotel_id !== $hotel -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given room is not associated with the given hotel!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'surface' => 'sometimes|required|min:1',
            'unit_id' => 'sometimes|required|numeric|exists:units,id',
            'seats' => 'sometimes|required|numeric|min:1',
            'simple_bed_number' => 'sometimes|required|numeric|min:1',
            'double_bed_number' => 'sometimes|required|numeric|min:1',
            'triple_bed_number' => 'sometimes|required|numeric|min:1',
            'additional_seats' => 'sometimes|required|numeric|min:1',
            'facilities' => 'sometimes|required',
            'facilities.*' => 'required|numeric|exists:room_facilities,id',
            'delete_facilities' => 'sometimes|required',
            'delete_facilities.*' => 'required|numeric|exists:room_has_facility,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $room -> name = $request -> name ? $request -> name : $room -> name;

        $room -> surface = $request -> surface ? $request -> surface : $room -> surface;

        $room ->  unit_id = $request -> unit_id ? $request -> unit_id : $room -> unit_id;

        $room -> seats = $request -> seats ? $request -> seats : $room -> seats;

        $room -> additional_seats = $request -> additional_seats ? $request -> additional_seats : $room -> additional_seats;

        $room -> save();

        $room -> unit;

        if(isset($request -> simple_bed_number) && $request -> simple_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 1) -> first() -> id;

            $room_has_bed -> number = $request -> simple_bed_number;

            $room_has_bed -> save();
        }

        if(isset($request -> double_bed_number) && $request -> double_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 2) -> first() -> id;

            $room_has_bed -> number = $request -> double_bed_number;

            $room_has_bed -> save();
        }

        if(isset($request -> triple_bed_number) && $request -> triple_bed_number) {

            $room_has_bed = new RoomHasBedType();

            $room_has_bed -> room_id = $room -> id;

            $room_has_bed -> bed_type_id = RoomBedType::where('number_of_people', '=', 3) -> first() -> id;

            $room_has_bed -> number = $request -> triple_bed_number;

            $room_has_bed -> save();
        }

        if(isset($request -> facilities)) {

            foreach ($request -> facilities as $key => $facility) {

                if(!(RoomHasFacility::where('room_id', $room -> id) -> where('room_facility_id', $facility) -> first())) {

                    $room_has_facility = new RoomHasFacility();

                    $room_has_facility -> room() -> associate($room);

                    $room_has_facility -> room_facility_id = $facility;

                    $room_has_facility -> save();
                }
            }
        }

        if(isset($request -> delete_facilities)) {

            foreach ($request -> delete_facilities as $key => $facility_id) {

                $room_has_facility = RoomHasFacility::find($facility_id);

                if($room_has_facility && $room_has_facility -> room_id == $room -> id) {

                    $room_has_facility -> forceDelete();
                }
            }
        }

        if(count($request -> pictures) > 0) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $room_picture = new RoomPicture();

                    $room_picture -> room() -> associate($room);

                    $room_picture -> picture = $picture;

                    $room_picture -> save();
                }
            }
        }

        $room -> hotel;

        foreach ($room -> roomHasBedTypes as $key => $bed_type) {

            $bed_type -> bedType;
        }

        foreach ($room -> roomHasFacilities as $key => $value) {

            $value -> roomFacility;
        }

        $room -> roomPictures;

        return json_encode([
            'success' => true,
            'message' => __('The room was successfully updated!'),
            'data' => $room
        ]);
    }

    public function delete($hotel_id, $room_id) {

        $hotel = Hotel::find($hotel_id);

        $room = Room::find($room_id);

        if($hotel === null) {
            return json_encode([
                'success' => false,
                'message' => __('The hotel id is invalid!')
            ]);
        }

        if($room === null) {
            return json_encode([
                'success' => false,
                'message' => __('The room id is invalid!')
            ]);
        }

        if($room -> hotel_id !== $hotel -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given room is not associated with the given hotel!')
            ]);
        }

        foreach ($room -> roomPictures as $key => $picture) {

            $picture -> delete();
        }

        $room -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The room and all data associated has been deleted!')
        ]);
    }

    public function deletePicture(Request $request, $hotel_id, $room_id, $picture_id) {

        $picture = RoomPicture::find($picture_id);

        $user = User::find($_GET['user_id']);

        $hotel = Hotel::find($hotel_id);

        $room = Room::find($room_id);

        if($picture === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given picture id is invalid!')
            ]);
        }

        if($hotel === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given hotel id is invalid!')
            ]);
        }

        if($room === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given room id is invalid!')
            ]);
        }

        if($room -> hotel_id !== $hotel -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given room is not associated with the given hotel!')
            ]);
        }

        if($picture -> room_id !== $room -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given picture is not associated with the given room!')
            ]);
        }

        $picture -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The picture was successfully deleted!')
        ]);
    }

    public function getFacilities() {

        $facilities = RoomFacility::all();

        return json_encode([
            'success' => true,
            'message' => __('The hotel rooms facilities have been successfully taken over!'),
            'data' => $facilities
        ]);
    }

    public function getRooms($hotel_id, $room_id) {

        $hotel = Hotel::find($hotel_id);

        if($hotel === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given hotel id is invalid!')
            ]);
        }

        $room = Room::where('id', '=', $room_id) -> where('hotel_id', '=', $hotel_id) -> first();

        if($room === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given room id is invalid!')
            ]);
        }

        $room -> hotel;

        $room -> unit;

        foreach ($room -> roomHasBedTypes as $key => $value) {

            $value -> bedType;
        }

        foreach ($room -> roomHasFacilities as $key => $value) {

            $value -> roomFacility;
        }

        $room -> roomPictures;

        return json_encode([
            'success' => true,
            'message' => __('The room an all data associated have been successfully taken over!'),
            'data' => $room
        ]);
    }
}
