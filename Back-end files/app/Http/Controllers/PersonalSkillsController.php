<?php

namespace App\Http\Controllers;

use App\PersonalSkill;
use App\ProfesionistProfile;
use Illuminate\Http\Request;
use Validator;

class PersonalSkillsController extends Controller
{
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'matern_language' => 'required|string|max:45',
            'profesionist_profile_id' => 'required|int',
            'managerial_skills' => 'required|boolean',
            'digital_skills' => 'required|boolean'
        ]);
        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }
        if (ProfesionistProfile::where('id', $request->profesionist_profile_id)->first() === null) {
            return json_encode(['status' => false, 'message' => __('The professionist profile is invalid')]);
        }

        $personal_skills = new PersonalSkill();
        $personal_skills->matern_language = $request->matern_language;
        $personal_skills->profesionist_profile_id = $request->profesionist_profile_id;
        $personal_skills->managerial_skills = $request->managerial_skills;
        $personal_skills->digital_skills = $request->digital_skills;

        try {
            $personal_skills->save();
        } catch (\Exception $e) {
            return response(json_encode(['status' => false, 'error' => $e->getMessage()]), 403);
        }

        return response(json_encode(['status' => true, 'message' => 'Personal Skills, added successfully!', 'personal_skills' => $personal_skills]), 200);


    }

    public function read($id)
    {
        return response(json_encode(['status' => true, 'personal_skill' => PersonalSkill::where("id", $id)->first()]), 200);
    }

    public function edit($id, Request $request)
    {
        try {
            $personal_skills = PersonalSkill::where('id', '=', $id)->first();
            if (isset($request->matern_language)) $personal_skills->matern_language = $request->matern_language;
            if (isset($request->profesionist_profile_id)) $personal_skills->profesionist_profile_id = $request->profesionist_profile_id;
            if (isset($request->managerial_skills)) $personal_skills->managerial_skills = $request->managerial_skills;
            if (isset($request->digital_skills)) $personal_skills->digital_skills = $request->digital_skills;
            $updated = $personal_skills->save();
            if ($updated) {
                return response(json_encode(['status' => true, 'message' => 'Personal Skills edited successfully!', 'personal_skills' => $personal_skills]), 200);
            } else {
                return response(json_encode(['status' => false, 'error' => 'Problems on update']), 409);
            }
        } catch (\Exception $e) {
            return response(json_encode(['status' => false, 'error' => $e->getMessage()]), 403);
        }

    }
    public function delete($id){

        $personal_skills = PersonalSkill::where("id", $id)->first();
        $personal_skills->delete();
        return response(json_encode(['status' => true,'message'=> 'Deleted successfully!' ,'id' => $id]), 200);
    }
}

