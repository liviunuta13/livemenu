<?php

namespace App\Http\Controllers;

use App\User;
use App\Product;
use Carbon\Carbon;
use App\ProducerProfile;
use App\ProductPromotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PrducerPromotionsController extends Controller
{
    public function add(Request $request) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }
        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3',
            'begin_date' => 'required|date',
            'end_date' => 'required|date',
            'begin_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i',
            'product_id' => 'required|numeric|exists:menus,id'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            return json_encode([
                'success' => false,
                'message' => __('The begin time must be lower than end time!')
            ]);
        }

        $product_promotion = new ProductPromotion();

        $product_promotion -> name = $request -> name;

        $product_promotion -> description = $request -> description;

        $product_promotion -> begin_date = $request -> begin_date;

        $product_promotion -> end_date = $request -> end_date;

        $product_promotion -> begin_time = $request -> begin_time;

        $product_promotion -> end_time = $request -> end_time;

        $product_promotion -> product_id = $request -> product_id;

        $product_promotion -> save();

        $product_promotion -> product;

        return json_encode([
            'success' => true,
            'message' => __('Promotion added successfully!'),
            'data' => [
                'promotion' => $product_promotion
            ]
        ]);
    }

    public function save(Request $request, $promotion_id) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        $product_promotion = ProductPromotion::find($promotion_id);

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        if($product_promotion === null) {
            return json_encode([
                'success' => false,
                'message' => __('The promotion id is invalid!')
            ]);
        }

        if(isset($product_promotion -> product) && isset($product_promotion -> product -> producer_id) && $product_promotion -> product -> producer_id != $producer -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The promotion is not associated with the producer profile!')
            ]);
        }
        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'begin_time' => 'sometimes|required|date_format:H:i',
            'end_time' => 'sometimes|required|date_format:H:i',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            return json_encode([
                'success' => false,
                'message' => __('The begin time must be lower than end time!')
            ]);
        }

        $product_promotion -> name = $request -> name ? $request -> name : $product_promotion -> name;

        $product_promotion -> description = $request -> description ? $request -> description : $product_promotion -> description;

        $product_promotion -> begin_date = $request -> begin_date ? $request -> begin_date : $product_promotion -> begin_date;

        $product_promotion -> end_date = $request -> end_date ? $request -> end_date : $product_promotion -> end_date;

        $product_promotion -> begin_time = $request -> begin_time ? $request -> begin_time : $product_promotion -> begin_time;

        $product_promotion -> end_time = $request -> end_time ? $request -> end_time : $product_promotion -> end_time;

        $product_promotion -> save();

        $product_promotion -> product;

        return json_encode([
            'success' => true,
            'message' => __('Promotion has successfully updated!'),
            'data' => [
                'promotion' => $product_promotion
            ]
        ]);
    }

    public function delete($promotion_id) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        $product_promotion = ProductPromotion::find($promotion_id);

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        if($product_promotion === null) {
            return json_encode([
                'success' => false,
                'message' => __('The promotion id is invalid!')
            ]);
        }

        if(isset($product_promotion -> product) && isset($product_promotion -> product -> producer_id) && $product_promotion -> product -> producer_id != $producer -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The promotion is not associated with the producer profile!')
            ]);
        }

        $product_promotion -> delete();

        return json_encode([
            'success' => true,
            'message' => __('Promotion has been deleted!')
        ]);
    }

    public function getPromotions() {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $promotions = [];

        $producer_products = Product::where('producer_id', $producer -> id) -> get();

        foreach ($producer_products as $key => $producer_product) {

            $promotions[$key] = $producer_product -> productPromotions;
        }

        return json_encode([
            'success' => true,
            'message' => __('The promotions has successfully taken over!'),
            'data' => [
                'promotions' => $promotions
            ]
        ]);
    }
}
