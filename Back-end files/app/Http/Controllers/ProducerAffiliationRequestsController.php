<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\AffiliatedProductsAd;
use Illuminate\Support\Facades\Validator;

class ProducerAffiliationRequestsController extends Controller
{
    public function add(Request $request) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'title' => 'required|string|min:2|max:45',
            'description' => 'required|string|min:2',
            'product_name' => 'sometimes|required|string|min:2',
            'product_id' => 'sometimes|required|numeric|exists:products,id',
            'final_amount' => 'required|numeric',
            'final_amount_unit_id' => 'required|numeric|exists:units,id',
            'current_amount' => 'required|numeric',
            'current_amount_unit_id' => 'required|numeric|exists:units,id',
            'final_price' => 'required|numeric',
            'currency_id' => 'required|numeric|exists:currencies,id',
            'location_id' => 'required|numeric|exists:locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if(($request -> product_name == null && $request -> product_id == null) || ($request -> product_name != null && $request -> product_id != null)) {
            return json_encode([
                'success' => false,
                'message' => __('The product name or id is required!')
            ]);
        }

        $affiliation_ad = new AffiliatedProductsAd();

        $affiliation_ad -> title = $request -> title;

        $affiliation_ad -> description = $request -> description;

        $affiliation_ad -> product_name = $request -> product_name ? $request -> product_name : null;

        $affiliation_ad -> product_id = $request -> product_id ? $request -> product_id : null;

        $affiliation_ad -> final_amount = $request -> final_amount;

        $affiliation_ad -> final_amount_unit_id = $request -> final_amount_unit_id;

        $affiliation_ad -> current_amount = $request -> current_amount;

        $affiliation_ad -> current_amount_unit_id = $request -> current_amount_unit_id;

        $affiliation_ad -> final_price = $request -> final_price;

        $affiliation_ad -> currency_id = $request -> currency_id;

        $affiliation_ad -> location_id = $request -> location_id;

        $affiliation_ad -> producer() -> associate($producer);

        $affiliation_ad -> save();

        $affiliation_ad -> producer;

        $affiliation_ad -> currency;

        $affiliation_ad -> location;

        $affiliation_ad -> product;

        $affiliation_ad -> unitFinal;

        $affiliation_ad -> unitCurrent;

        $affiliation_ad -> affiliatedProducersRequests;

        return json_encode([
            'success' => true,
            'message' => __('The affiliation request has been added successfully!'),
            'data' => [
                'affiliation_request' => $affiliation_ad
            ]
        ]);
    }

    public function save(Request $request, $affiliation_request_id) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $affiliation_ad = AffiliatedProductsAd::find($affiliation_request_id);

        if($affiliation_ad === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given affiliation request is invalid!')
            ]);
        }

        if($affiliation_ad -> producer_id != $producer -> id) {
            return json_encode([
                'success' => false,
                'message' => __('You have no permission to edit given affiliation request!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'title' => 'sometimes|required|string|min:2|max:45',
            'description' => 'sometimes|required|string|min:2',
            'product_name' => 'sometimes|required|string|min:2',
            'product_id' => 'sometimes|required|numeric|exists:products,id',
            'final_amount' => 'sometimes|required|numeric',
            'final_amount_unit_id' => 'sometimes|required|numeric|exists:units,id',
            'current_amount' => 'sometimes|required|numeric',
            'current_amount_unit_id' => 'sometimes|required|numeric|exists:units,id',
            'final_price' => 'sometimes|required|numeric',
            'currency_id' => 'sometimes|required|numeric|exists:currencies,id',
            'location_id' => 'sometimes|required|numeric|exists:locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if(($request -> product_name == null && $request -> product_id == null) || ($request -> product_name != null && $request -> product_id != null)) {
            return json_encode([
                'success' => false,
                'message' => __('The product name or id is required!')
            ]);
        }

        $affiliation_ad -> title = $request -> title ? $request -> title : $affiliation_ad -> title;

        $affiliation_ad -> description = $request -> description ? $request -> description : $affiliation_ad -> description;;

        $affiliation_ad -> product_name = $request -> product_name ? $request -> product_name : $affiliation_ad -> product_name;

        $affiliation_ad -> product_id = $request -> product_id ? $request -> product_id : $affiliation_ad -> product_id;

        $affiliation_ad -> final_amount = $request -> final_amount ? $request -> final_amount : $affiliation_ad -> final_amount;

        $affiliation_ad -> final_amount_unit_id = $request -> final_amount_unit_id ? $request -> final_amount_unit_id : $affiliation_ad -> final_amount_unit_id;

        $affiliation_ad -> current_amount = $request -> current_amount ? $request -> current_amount : $affiliation_ad -> current_amount;

        $affiliation_ad -> current_amount_unit_id = $request -> current_amount_unit_id ? $request -> current_amount_unit_id : $affiliation_ad -> current_amount_unit_id;

        $affiliation_ad -> final_price = $request -> final_price ? $request -> final_price : $affiliation_ad -> final_price;

        $affiliation_ad -> currency_id = $request -> currency_id ? $request -> currency_id : $affiliation_ad -> currency_id;

        $affiliation_ad -> location_id = $request -> location_id ? $request -> location_id : $affiliation_ad -> location_id;

        $affiliation_ad -> save();

        $affiliation_ad -> producer;

        $affiliation_ad -> currency;

        $affiliation_ad -> location;

        $affiliation_ad -> product;

        $affiliation_ad -> unitFinal;

        $affiliation_ad -> unitCurrent;

        $affiliation_ad -> affiliatedProducersRequests;

        return json_encode([
            'success' => true,
            'message' => __('The affiliation request has been successfully updated!'),
            'data' => [
                'affiliation_request' => $affiliation_ad
            ]
        ]);
    }

    public function delete($affiliation_request_id) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $affiliation_ad = AffiliatedProductsAd::find($affiliation_request_id);

        if($affiliation_ad === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given affiliation request is invalid!')
            ]);
        }

        if($affiliation_ad -> producer_id != $producer -> id) {
            return json_encode([
                'success' => false,
                'message' => __('You have no permission to delete given affiliation request!')
            ]);
        }

        $affiliation_ad -> delete();

        $affiliation_ad -> affiliatedProducersRequests() -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The affiliation request has been deleted!')
        ]);
    }
}
