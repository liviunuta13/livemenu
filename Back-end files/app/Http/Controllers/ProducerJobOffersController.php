<?php

namespace App\Http\Controllers;

use App\User;
use App\JobOffer;
use Carbon\Carbon;
use App\ProducerProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProducerJobOffersController extends Controller
{
    public function add(Request $request) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }
        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3',
            'contract_type_id' => 'required|exists:contract_types,id',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'location_id' => 'sometimes|required|exists:locations,id',
            'begin_date' => 'required|date',
            'end_date' => 'required|date',
            'salary' => 'required|numeric:min:1',
            'currency_id' => 'required|numeric|exists:currencies,id',
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> contract_type_id == 3 || $request -> contract_type_id == 1) {
            if($request -> begin_date == null || $request -> begin_date == null) {
                return json_encode([
                    'success' => false,
                    'message' => __('For seasonal and definite contract type the begin and and end date are required!')
                ]);
            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        $job = new JobOffer();

        $job -> name = $request -> name;

        $job -> description = $request -> description;

        $job -> contract_type_id = $request -> contract_type_id;

        $job -> begin_date = $request -> begin_date;

        $job -> end_date = $request -> end_date;

        $job -> salary = $request -> salary;

        $job -> currency_id = $request -> currency_id;

        $job -> location_id = $request -> location_id ? $request -> location_id : null;

        if($request -> latitude && $request -> longitude) {

            $job -> latitude = $request -> latitude;

            $job -> longitude = $request -> longitude;
        }

        $job -> producerProfile() -> associate($producer);

        $job -> save();

        $job -> contractType;

        $job -> currency;

        $job -> location;

        $job -> producerProfile;

        $job -> restaurantProfile;

        $job -> restaurantLocation;

        return json_encode([
            'success' => true,
            'message' => __('The job was successfully added!'),
            'data' => [
                'job' => $job
            ]
        ]);
    }

    public function save(Request $request, $job_offer_id) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        $job = JobOffer::find($job_offer_id);

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        if($job === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given job offer id is invalid!')
            ]);
        }

        if($job -> producer_profile_id != $producer -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The job offer is not associated with the producer profile!')
            ]);
        }
        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'contract_type_id' => 'sometimes|required|exists:contract_types,id',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'location_id' => 'sometimes|required|exists:locations,id',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'salary' => 'sometimes|required|numeric:min:1',
            'currency_id' => 'sometimes|required|numeric|exists:currencies,id',
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> contract_type_id == 3 || $request -> contract_type_id == 1) {
            if($request -> begin_date == null || $request -> begin_date == null) {
                return json_encode([
                    'success' => false,
                    'message' => __('For seasonal and definite contract type the begin and and end date are required!')
                ]);
            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        $job -> name = $request -> name;

        $job -> description = $request -> description;

        $job -> contract_type_id = $request -> contract_type_id;

        $job -> begin_date = $request -> begin_date;

        $job -> end_date = $request -> end_date;

        $job -> salary = $request -> salary;

        $job -> currency_id = $request -> currency_id;

        $job -> location_id = $request -> location_id ? $request -> location_id : null;

        if($request -> latitude && $request -> longitude) {

            $job -> latitude = $request -> latitude;

            $job -> longitude = $request -> longitude;
        }

        $job -> save();

        $job -> contractType;

        $job -> currency;

        $job -> location;

        $job -> producerProfile;

        $job -> restaurantProfile;

        $job -> restaurantLocation;

        return json_encode([
            'success' => true,
            'message' => __('The job was successfully added!'),
            'data' => [
                'job' => $job
            ]
        ]);
    }

    public function delete($job_offer_id) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        $job = JobOffer::find($job_offer_id);

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        if($job === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given job offer id is invalid!')
            ]);
        }

        if($job -> producer_profile_id != $producer -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The job offer is not associated with the producer profile!')
            ]);
        }

        $job -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The job has been deleted!')
        ]);
    }

    public function getJobOffers() {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $jobs = $producer -> jobOffers;

        return json_encode([
            'success' => true,
            'message' => __('The producer jobs offers has successfully taken over!'),
            'data' => [
                'job_offers' => $jobs
            ]
        ]);
    }
}
