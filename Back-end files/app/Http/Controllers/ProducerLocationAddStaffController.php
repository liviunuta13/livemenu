<?php

namespace App\Http\Controllers;

use App\ContractType;
use App\Currency;
use App\ProducerLocationStaff;
use App\ProducerLocationStaffHasRole;
use App\ProfesionistProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
class ProducerLocationAddStaffController extends Controller
{
    public function add(Request $request, $location_id, $staff_id)
    {
        $user = User::find($_GET['user_id']);

        $producer = $user->producerProfile;

        if ($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $location = $producer->producerProfileLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given producer location id is invalid!')
            ]);
        }
        $staff = $location->staff->find($staff_id);
        if ($staff === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given staff id is invalid!')
            ]);
        }

        $staff_has_role = ProducerLocationStaffHasRole::where('staff_id',$staff_id)->where('role_id',4)->first();
        if($staff_has_role === null){
            return json_encode([
                'success' => false,
                'message' => __('U do not have permissions to add a staff!')
            ]);
        }
        $if_contract_type_exists = ContractType::where('id',$request->contract_type_id)->first();
        if($if_contract_type_exists===null){
            return json_encode([
                'success' => false,
                'message' => __('Contract type does not exist!')
            ]);
        }
        $currency_if_exists = Currency::where('id',$request->salary_currency_id)->first();
        if($currency_if_exists ===null){
            return json_encode([
                'success' => false,
                'message' => __('Currency does not exists!')
            ]);
        }
        $if_profesionist_profile_exists = ProfesionistProfile::where('id',$request->profesionist_profile_id)->first();
        if($if_profesionist_profile_exists===null){
            return json_encode([
                'success' => false,
                'message' => __('Profesionist does not exist!')
            ]);
        }

        $validator = Validator::make($request->all(), [
            'contract_type_id' => 'required|integer',
            'salary' => 'required|string|max:45',
            'salary_currency_id' => 'required|integer',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'job' => 'required|string|max:45',
            'profesionist_profile_id' => 'required|integer',

        ]);

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') > Carbon::parse($request ->end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $producer_staff = new ProducerLocationStaff();

        $producer_staff->producer_location_id = $location_id;
        $producer_staff->contract_type_id = $request->contract_type_id;
        $producer_staff->salary = $request->salary;
        $producer_staff->salary_currency_id = $request->salary_currency_id;
        $producer_staff->start_date = $request->start_date;
        $producer_staff->end_date = $request->end_date;
        $producer_staff->job = $request->job;
        $producer_staff->profesionist_profile_id = $request->profesionist_profile_id;

        $producer_staff->save();

        return json_encode([
            'success' => true,
            'message' => __('The staff has been successfully added!'),
            'data' => [
                'staff' => $producer_staff
            ]
        ]);
    }
}
