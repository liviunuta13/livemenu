<?php

namespace App\Http\Controllers;

use App\ProducerLocationStaff;
use App\ProducerLocationStaffHasRole;
use App\User;
use Illuminate\Http\Request;

class ProducerLocationStaffCloseContractsController extends Controller
{
    public function delete($location_id, $staff_id, $staff_delete_id)
    {

        $user = User::find($_GET['user_id']);

        $producer = $user->producerProfile;

        if ($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $location = $producer->producerProfileLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given producer location id is invalid!')
            ]);
        }
        $staff = $location->staff->find($staff_id);
        if ($staff === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given staff id is invalid!')
            ]);
        }


        $staff_has_role = ProducerLocationStaffHasRole::where('staff_id', $staff_id)->where('role_id', 5)->first();
        if ($staff_has_role === null) {
            return json_encode([
                'success' => false,
                'message' => __('You do not have permissions to close a contract!')
            ]);
        }

        if ($staff_id === $staff_delete_id) {
            return json_encode([
                'success' => false,
                'message' => __('U can not close ur own contract!')
            ]);
        }

        $verify_if_exist = ProducerLocationStaff::where('id', $staff_delete_id)->first();
        if ($verify_if_exist === null) {
            return json_encode([
                'success' => false,
                'message' => __('Staff does not exist!')
            ]);
        }

        $verify_if_exist->delete();

        return json_encode([
            'success' => true,
            'message' => __('Congratulations, the contact has been closed successfully!'),
            'id' => $staff_delete_id,
        ]);


    }
}
