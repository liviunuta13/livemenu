<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostPicture;
use App\ProducerLocationStaffHasRole;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
class ProducerLocationStaffPostsController extends Controller
{
    public function add(Request $request, $location_id, $staff_id)
    {
        $user = User::find($_GET['user_id']);

        $producer = $user->producerProfile;

        if ($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $location = $producer->producerProfileLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given producer location id is invalid!')
            ]);
        }
        $staff = $location->staff->find($staff_id);
        if ($staff === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given staff id is invalid!')
            ]);
        }

        $staff_has_role = ProducerLocationStaffHasRole::where('staff_id', $staff_id)->where('role_id', 2)->first();
        if ($staff_has_role === null) {
            return json_encode([
                'success' => false,
                'message' => __('U do not have permissions to add a post!')
            ]);
        }

        $validator = Validator::make($request->all(), ['description' => 'required|string|min:3',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',]);

        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $staff_post_description = new Post();

        $staff_post_description->description = $request->description;

        $staff_post_description->from_restaurant_location_id = $location_id;

        $staff_post_description->save();

        if (count($request -> pictures) > 0) {

            foreach ($request -> pictures as $key => $picture) {
                {
                    $post_picture = new PostPicture();

                    $post_picture->picture = $picture;

                    $post_picture->post_id = $staff_post_description->id;

                    $post_picture->save();
                }
            }
        }

        return json_encode([
            'success' => true,
            'message' => __('The post has been successfully added!'),
            'data' => [
                'post' => $staff_post_description
            ]
        ]);
    }

}
