<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\ProducerProfileLocation;
use App\ProducerProfileLocationPicture;
use Illuminate\Support\Facades\Validator;
use App\ProducerProfileLocationsController;

class ProducerLocationsController extends Controller
{
    public function add(Request $request) {

        $user = User::find($_GET['user_id']);

        $producer_profile = $user -> producerProfile;

        if($producer_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:5|max:512',
            'city_id' => 'required|numeric|exists:cities,id',
            'county_id' => 'required|numeric|exists:counties,id',
            'country_id' => 'required|numeric|exists:countries,id',
            'street' => 'required|string|min:2|max:100',
            'number' => 'required|numeric',
            'postal_code' => 'sometimes|required|string|min:1|max:45',
            'latitude' => 'required',
            'longitude' => 'required',
            'phone_number' => 'sometimes|required|string|min:1|max:45',
            'fixed_phone_number' => 'sometimes|required|string|min:1|max:45',
            'contact_person_name' => 'required|string|min:1|max:45',
            'email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:producer_profile_locations,email',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }
        $producer_location = new ProducerProfileLocation();

        $producer_location -> name = $request -> name;

        $producer_location -> description = $request -> description;

        $producer_location -> city_id = $request -> city_id;

        $producer_location -> county_id = $request -> county_id;

        $producer_location -> country_id = $request -> country_id;

        $producer_location -> street = $request -> street;

        $producer_location -> number = $request -> number;

        $producer_location -> postal_code = $request -> postal_code ? $request -> postal_code : null;

        $producer_location -> latitude = $request -> latitude;

        $producer_location -> longitude = $request -> longitude;

        $producer_location -> phone_number = $request -> phone_number ? $request -> phone_number : null;

        $producer_location -> fixed_phone_number = $request -> fixed_phone_number ? $request -> fixed_phone_number : null;

        $producer_location -> contact_person_name = $request -> contact_person_name;

        $producer_location -> email = $request -> email ? $request -> email : null;

        $producer_location -> producerProfile() -> associate($producer_profile);

        $producer_location -> save();

        if(count($request -> pictures) > 0) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $producer_location_picture = new ProducerProfileLocationPicture();

                    $producer_location_picture -> picture = $picture;

                    $producer_location_picture -> producerProfileLocation() -> associate($producer_location);

                    $producer_location_picture -> save();
                }
            }
        }

        $producer_location -> city;

        $producer_location -> county;

        $producer_location -> country;

        $producer_location -> producerProfile;

        $producer_location -> producerProfileLocationPictures;

        return json_encode([
            'success' => true,
            'message' => __('The location was successfully added!'),
            'data' => [
                'location' => $producer_location
            ]
        ]);
    }

    // Edit producer location
    public function save(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $producer_profile = $user -> producerProfile;

        if($producer_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $location = $producer_profile -> producerProfileLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:5|max:512',
            'city_id' => 'sometimes|required|numeric|exists:cities,id',
            'county_id' => 'sometimes|required|numeric|exists:counties,id',
            'country_id' => 'sometimes|required|numeric|exists:countries,id',
            'street' => 'sometimes|required|string|min:2|max:100',
            'number' => 'sometimes|required|numeric',
            'postal_code' => 'sometimes|required|string|min:1|max:45',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'phone_number' => 'sometimes|required|string|min:1|max:45',
            'fixed_phone_number' => 'sometimes|required|string|min:1|max:45',
            'contact_person_name' => 'sometimes|required|string|min:1|max:45',
            'email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:producer_profile_locations,email',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'sometimes|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }
        $location -> name = $request -> name ? $request -> name : $location -> name;

        $location -> description = $request -> description ? $request -> description : $location -> description;

        $location -> city_id = $request -> city_id ? $request -> city_id : $location -> city_id;

        $location -> county_id = $request -> county_id ? $request -> county_id : $location -> county_id;

        $location -> country_id = $request -> country_id ? $request -> country_id : $location -> country_id;

        $location -> street = $request -> street ? $request -> street : $location -> street;

        $location -> number = $request -> number ? $request -> number : $location -> number;

        $location -> postal_code = $request -> postal_code ? $request -> postal_code : $location -> postal_code;

        $location -> latitude = $request -> latitude ? $request -> latitude : $location -> latitude;

        $location -> longitude = $request -> longitude ? $request -> longitude : $location -> longitude;

        $location -> phone_number = $request -> phone_number ? $request -> phone_number : $location -> phone_number;

        $location -> fixed_phone_number = $request -> fixed_phone_number ? $request -> fixed_phone_number : $location -> fixed_phone_number;

        $location -> contact_person_name = $request -> contact_person_name ?  $request -> contact_person_name :  $location -> contact_person_name;

        $location -> email = $request -> email ? $request -> email : $location -> email;

        $location -> save();

        if(count($request -> pictures) > 0) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $producer_location_picture = new ProducerProfileLocationPicture();

                    $producer_location_picture -> picture = $picture;

                    $producer_location_picture -> producerProfileLocation() -> associate($location);

                    $producer_location_picture -> save();
                }
            }
        }

        $location -> city;

        $location -> county;

        $location -> country;

        $location -> producerProfile;

        $location -> producerProfileLocationPictures;

        $cont = 0;

        $rating_val = 0;

        foreach ($location -> ratings as $key => $rating) {

            $rating_val += $rating -> stars;

            $cont ++;
        }

        if($cont == 0) {

            $location -> rating = null;
        } else {

            $location -> rating = $rating_val / $cont;
        }

        return json_encode([
            'success' => true,
            'message' => __('The location was successfully updated!'),
            'data' => [
                'location' => $location
            ]
        ]);
    }

    public function delete($location_id) {

        $user = User::find($_GET['user_id']);

        $producer_profile = $user -> producerProfile;

        if($producer_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $location = $producer_profile -> producerProfileLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }

        foreach ($location -> producerProfileLocationPictures as $key => $picture) {

            $picture -> delete();
        }

        $location -> producerProfileLocationPictures() -> delete();

        $location -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The location has been deleted!')
        ]);
    }

    public function deletePicture($location_id, $picture_id) {

        $user = User::find($_GET['user_id']);

        $producer_profile = $user -> producerProfile;

        if($producer_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $location = $producer_profile -> producerProfileLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }

        $picture = $location -> producerProfileLocationPictures -> find($picture_id);

        if($picture === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given picture is invalid!')
            ]);
        }

        $picture -> forceDelete();

        return json_encode([
            'success' => true,
            'message' => __('The picture has been deleted!')
        ]);
    }

    public function readLocation($location_id) {

        $user = User::find($_GET['user_id']);

        $producer_profile = $user -> producerProfile;

        if($producer_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $location = $producer_profile -> producerProfileLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }

        $location -> city;

        $location -> county;

        $location -> country;

        $location -> producerProfile;

        $location -> producerProfileLocationPictures;

        $cont = 0;

        $rating_val = 0;

        foreach ($location -> ratings as $key => $rating) {

            $rating_val += $rating -> stars;

            $cont ++;
        }

        if($cont == 0) {

            $location -> rating = null;
        } else {

            $location -> rating = $rating_val / $cont;
        }

        return json_encode([
            'success' => true,
            'message' => __('The producer location and all data has been taken over!'),
            'data' => [
                'location' => $location
            ]
        ]);
    }
}
