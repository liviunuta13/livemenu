<?php

namespace App\Http\Controllers;

use App\User;
use App\Partner;
use Illuminate\Http\Request;
use App\ProducerProfileHasPartner;
use Illuminate\Support\Facades\Validator;

class ProducerPartnersController extends Controller
{
    public function add(Request $request) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }
        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $partner = new Partner();

        $partner -> name = $request -> name;

        $partner -> save();

        $producer_has_partner = new ProducerProfileHasPartner();

        $producer_has_partner -> partner() -> associate($partner);

        $producer_has_partner -> producerProfile() -> associate($producer);

        $producer_has_partner -> save();

        return json_encode([
            'success' => true,
            'message' => __('The partner has successfully added and associated with your producer profile!'),
            'data' => [
                'partner' => $partner,
                'producer_has_partner' => $producer_has_partner
            ]
        ]);
    }
}
