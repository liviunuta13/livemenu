<?php

namespace App\Http\Controllers;

use App\User;
use App\Unit;
use App\Product;
use App\Currency;
use App\ProductPicture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProducerProductsController extends Controller
{
    private $jsonIsValid = true;

    private $parentsQueue = [];

    public function productValidate($json_category) {

        if(isset($json_category['name']) && isset($json_category['products'])) {

            if($json_category['name'] == null || $json_category['products'] == null) {

                $this -> jsonIsValid = false;
            }

            foreach ($json_category['products'] as $key => $value) {

                if(!isset($value['name']) || strlen($value['name']) == 0) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['price'])) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['currency_id']) || Currency::find($value['currency_id']) == null) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['amount'])) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['unit_id']) || $value['unit_id'] == null) {

                    $this -> jsonIsValid = false; return;

                } else if(Unit::find($value['unit_id']) == null) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['pictures']) || count($value['pictures']) == 0) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['description'])) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['special_product']) || ($value['special_product'] != 0 && $value['special_product'] != 1)) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['unique_product']) || ($value['unique_product'] != 0 && $value['unique_product'] != 1)) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['specific_area_product']) || ($value['specific_area_product'] != 0 && $value['specific_area_product'] != 1)) {

                    $this -> jsonIsValid = false; return;
                }
            }
            // return $json_category['products'];

        } else if(isset($json_category['name']) && isset($json_category['categories'])) {

            if(isset($json_category['categories'])) {

                foreach ($json_category['categories'] as $key => $category) {

                    if(!isset($category['name']) || strlen($category['name']) == 0) {

                        $this -> jsonIsValid = false; return;
                    }

                    $this -> productValidate($category);
                }
            }
        }
    }

    public function productAdd($json_category, $parentId, $location) {

        if(isset($json_category['name']) && isset($json_category['products'])) {

            if($json_category['name'] == null || $json_category['products'] == null) {

                $this -> jsonIsValid = false;
            }

            foreach ($json_category['products'] as $key => $value) {

                $product = new Product();

                $product -> name = $value['name'];

                $product -> currency_id = $value['currency_id'];

                $product -> price = $value['price'];

                $product -> price_per_amount = $value['price_per'];

                $product -> description = $value['description'];

                $product -> special_product = $value['special_product'];

                $product -> unique_product = $value['unique_product'];

                $product -> specific_area_product = $value['specific_area_product'];

                $product -> special_product = $value['special_product'];

                if(isset($this -> parentsQueue[$value['parent_id'] - 1])) {

                    $search = $this -> parentsQueue[$value['parent_id'] - 1];

                    if($search != null) {

                        $product -> product_id = $search;
                    }
                }

                $product -> producer_location_id = $location -> id;

                if(isset($value['cat_id']) && $value['cat_id'] != null) {

                    $product -> cat_id = $value['cat_id'];
                }

                if(isset($value['parent_id']) && $value['parent_id'] != null) {

                    $product -> parent_id = $value['parent_id'];
                }

                $product -> save();

                array_push($this -> parentsQueue, $product -> id);

                $location -> products() -> save($product);

                foreach ($value['pictures'] as $key => $picture) {

                    $product_picture = new ProductPicture();

                    $product_picture -> picture = $picture;

                    $product_picture -> product() -> associate($product);

                    $product_picture -> save();
                 }
            }

        } else if(isset($json_category['name']) && isset($json_category['categories'])) {

            foreach ($json_category['categories'] as $key => $category) {

                $product = new Product();

                $product -> name = $category['name'];

                if(isset($this -> parentsQueue[$category['parent_id'] - 1])) {

                    $search = $this -> parentsQueue[$category['parent_id'] - 1];

                    if($search != null) {

                        $product -> product_id = $search;
                    }
                }

                if(isset($json_category['currency_id'])) {

                    $product -> currency_id = $json_category['currency_id'];
                }

                $product -> producer_location_id = $location -> id;

                if(isset($category['cat_id']) && $category['cat_id'] != null) {

                    $product -> cat_id = $category['cat_id'];
                }

                if(isset($category['parent_id']) && $category['parent_id'] != null) {

                    $product -> parent_id = $category['parent_id'];
                }

                $product -> save();

                array_push($this -> parentsQueue, $product -> id);

                $location -> products() -> save($product);

                // $parentId = $product -> id;

                $this -> productAdd($category, $parentId, $location);
            }
        }
    }

    public function add(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $location = $producer -> producerProfileLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }

        $json_category = json_decode($request -> json_product, true);

        $this -> productValidate($json_category);

        if($this -> jsonIsValid == false) {
            return json_encode([
                'success' => false,
                'message' => __('Invalid json format!')
            ]);
        }

        $parentId = null;

        $this -> productAdd($json_category, $parentId, $location);

        // return $this -> parentsQueue[0];

        foreach ($location -> products as $key => $prod) {

            $prod -> pictures;
        }

        return json_encode([
            'success' => true,
            'message' => __('The product has been added successfully!'),
            'data' => [
                'location' => $location
            ]
        ]);
    }

    public function delete(Request $request, $location_id) {

        $validator = Validator::make($request -> all(), [
            'delete_products' => 'required',
            'delete_products.*' => 'required|numeric|exists:products,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $location = $producer -> producerProfileLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }

        $sorted_ids = $request -> delete_products;

        for($i = 0; $i < count($sorted_ids) - 1; $i ++) {

            for($j = $i + 1; $j < count($sorted_ids); $j ++) {

                if($sorted_ids[$i] < $sorted_ids[$j]) {

                    $c0 = $sorted_ids[$i];

                    $sorted_ids[$i] = $sorted_ids[$j];

                    $sorted_ids[$j] = $c0;
                }
            }
        }

        foreach ($sorted_ids as $key => $delete_product) {

            $product = $location -> products -> find($delete_product);

            if($product) {

                $product -> pictures() -> delete();

                $product -> delete();
            }
        }

        return json_encode([
            'success' => true,
            'message' => __('The product has been deleted!')
        ]);
    }
}
