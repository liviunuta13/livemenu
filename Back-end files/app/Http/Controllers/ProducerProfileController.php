<?php

namespace App\Http\Controllers;

use App\User;
use App\ProducerProfile;
use Illuminate\Http\Request;
use App\ProducerProfilePicture;
use Illuminate\Support\Facades\Validator;

class ProducerProfileController extends Controller
{
    // Edit producer profile of current logged user
    public function save(Request $request) {

        $user = User::find($_GET['user_id']);

        $producer_profile = $user -> producerProfile;

        if($producer_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'fiscal_code' => 'sometimes|required|min:1|max:45',
            'company_name' => 'sometimes|required|min:3|max:100',
            'administrator_name_surname' => 'sometimes|required|min:3|max:100',
            'website' => 'sometimes|required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:producer_profiles,email',
            'phone_number' => 'sometimes|required|numeric',
            'logo' => 'sometimes|required|file|mimes:jpeg,jpg,png,gif',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $producer_profile -> name = $request -> name ? $request -> name : $producer_profile -> name;

        $producer_profile -> description = $request -> description ? $request -> description : $producer_profile -> name;

        $producer_profile -> fiscal_code = $request -> fiscal_code ? $request -> fiscal_code : $producer_profile -> fiscal_code;

        $producer_profile -> company_name = $request -> company_name ? $request -> company_name : $producer_profile -> company_name;

        $producer_profile -> administrator_name_surname = $request -> administrator_name_surname ? $request -> administrator_name_surname : $producer_profile -> administrator_name_surname;

        $producer_profile -> website = $request -> website ? $request -> website : $producer_profile -> website;

        $producer_profile -> email = $request -> email ? $request -> email : $producer_profile -> email;

        $producer_profile -> phone_number = $request -> phone_number ? $request -> phone_number : $producer_profile -> phone_number;

        if(count($request -> logo) > 0) {

            $producer_profile -> logo = $request -> logo;
        }

        if(count($request -> pictures) > 0) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $producer_profile_picture = new ProducerProfilePicture();

                    $producer_profile_picture -> name = $picture;

                    $producer_profile_picture -> producerProfile() -> associate($producer_profile);

                    $producer_profile_picture -> save();
                }
            }
        }

        $producer_profile -> save();

        $user -> producer_profile_id = $producer_profile -> id;

        $user -> save();

        $producer_profile -> producerType;

        $producer_profile -> affiliatedProducers;

        $producer_profile -> buyedSponsorshipsFor;

        $producer_profile -> buyedSponsorships;

        $producer_profile -> jobOffers;

        $producer_profile -> postsLocationCheckin;

        $producer_profile -> postsFrom;

        $producer_profile -> postsTo;

        $producer_profile -> producerProfileLocations;

        $producer_profile -> producerProfilePictures;

        $producer_profile -> user;

        foreach ($producer_profile -> producerProfileHasPartners as $key => $value) {

            $value -> partner;
        }

        $cont = 0;

        $r_follows = 0;

        $rating_val = 0;

        foreach ($producer_profile -> producerProfileLocations as $key => $location) {

            foreach ($location -> ratings as $key => $rating) {

                $rating_val += $rating -> stars;

                $cont ++;
            }

            $r_follows += count($location -> follows);
        }

        if($cont == 0) {

            $producer_profile -> rating = null;
        } else {

            $producer_profile -> rating = $rating_val / $cont;
        }

        $producer_profile -> follows_number = $r_follows;

        return json_encode([
            'success' => true,
            'message' => __('The producer profile was successfully updated!'),
            'data' => [
                'producer_profile' => $producer_profile
            ]
        ]);
    }

    public function delete() {

        $user = User::find($_GET['user_id']);

        $producer_profile = $user -> producerProfile;

        if($producer_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $producer_profile -> follows() -> delete();

        $producer_profile -> jobOffers() -> delete();

        $producer_profile -> postsLocationCheckin() -> delete();

        $producer_profile -> postsFrom() -> delete();

        $producer_profile -> postsTo() -> delete();

        $producer_profile -> producerProfileHasPartners() -> delete();

        $producer_profile -> producerProfileLocations() -> delete();

        foreach ($producer_profile -> producerProfilePictures as $key => $picture) {

            $picture -> delete();
        }

        $producer_profile -> producerProfilePictures() -> delete();

        $producer_profile -> ratingsFrom() -> delete();

        $producer_profile -> ratingsTo() -> delete();

        $producer_profile -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The producer profile and all data associated has been deleted!')
        ]);
    }

    public function deletePicture($picture_id) {

        $user = User::find($_GET['user_id']);

        $producer_profile = $user -> producerProfile;

        $picture = ProducerProfilePicture::find($picture_id);

        if($producer_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        if($picture === null || $picture -> producer_profile_id != $producer_profile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given picture id is invalid!')
            ]);
        }

        $picture -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The picture has been deleted!')
        ]);
    }

    public function readProducer($producer_id) {

        $user = User::find($_GET['user_id']);

        $producer_profile = ProducerProfile::find($producer_id);

        if($producer_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The required producer is invalid!')
            ]);
        }

        $producer_profile -> producerType;

        $producer_profile -> affiliatedProducers;

        $producer_profile -> buyedSponsorshipsFor;

        $producer_profile -> buyedSponsorships;

        $producer_profile -> jobOffers;

        $producer_profile -> postsLocationCheckin;

        $producer_profile -> postsFrom;

        $producer_profile -> postsTo;

        $producer_profile -> producerProfileLocations;

        $producer_profile -> producerProfilePictures;

        $producer_profile -> user;

        // $producer_profile -> producerStaff;

        foreach ($producer_profile -> producerProfileHasPartners as $key => $value) {

            $value -> partner;
        }

        $cont = 0;

        $r_follows = 0;

        $rating_val = 0;

        foreach ($producer_profile -> producerProfileLocations as $key => $location) {

            foreach ($location -> ratings as $key => $rating) {

                $rating_val += $rating -> stars;

                $cont ++;
            }

            $r_follows += count($location -> follows);
        }

        if($cont == 0) {

            $producer_profile -> rating = null;
        } else {

            $producer_profile -> rating = $rating_val / $cont;
        }

        $producer_profile -> follows_number = $r_follows;

        return json_encode([
            'success' => true,
            'message' => __('The producer profile and all data associated has been taken over!'),
            'data' => [
                'producer_profile' => $producer_profile
            ]
        ]);
    }
}
