<?php

namespace App\Http\Controllers;

use App\User;
use App\ProducerProfile;
use App\AffiliatedProducer;
use Illuminate\Http\Request;
use App\AffiliatedProductsAd;

class ProducersAffiliateController extends Controller
{
    public function affiliateToProducer($affiliation_request_id) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $affiliation_request_ad = AffiliatedProductsAd::find($affiliation_request_id);

        if($affiliation_request_ad === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given affiliation request id is invalid!')
            ]);
        }

        if($affiliation_request_ad -> producer_id == $producer -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The affiliation request must be sent from another producer profile!')
            ]);
        }

        $affiliated_request = $affiliation_request_ad -> affiliatedProducersRequests -> where('confirmed', 1);

        if($affiliated_request -> first()) {
            return json_encode([
                'success' => false,
                'message' => __('The affiliate request is closed!')
            ]);
        }

        $affiliation = new AffiliatedProducer();

        $affiliation -> producerProfileFrom() -> associate($producer);

        $affiliation -> producerProfileFor() -> associate($affiliation_request_ad);

        $affiliation -> affiliated_product_ad_id = $affiliation_request_ad -> id;

        $affiliation -> save();

        $affiliation -> producerProfileFrom;

        $affiliation -> producerProfileFor;

        return json_encode([
            'success' => true,
            'message' => __('The request for affiliation was sent. Now you have to wait for the producer to confirm!'),
            'data' => [
                'affiliaton' => $affiliation
            ]
        ]);
    }

    public function confirmAffiliatonRequest($affiliation_request_id, $confirm_value) {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $affiliation = $producer -> affiliatedProducers -> find($affiliation_request_id);

        if($affiliation === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given affiliation request is invalid!')
            ]);
        }

        if($affiliation -> producer_profile_for_id != $producer -> id) {
            return json_encode([
                'success' => false,
                'message' => __('You has no permission to confirm or delete the affiliation request!')
            ]);
        }

        if($confirm_value == 0) {

            $affiliation -> forceDelete();

            return json_encode([
                'success' => true,
                'message' => __('The affiliation request has been successfully declined!')
            ]);

        } else if($confirm_value == 1) {

            if($affiliation -> confirmed == 1) {
                return json_encode([
                    'success' => false,
                    'message' => __('The affiliation already has been confirmed!')
                ]);
            }

            $affiliation -> confirmed = 1;

            $affiliation -> save();

            return json_encode([
                'success' => true,
                'message' => __('The affiliation request has been successfully accepted! Now you can propose products for ads!')
            ]);
        }
        return json_encode([
            'success' => false,
            'message' => __('The confimation value must be 0 or 1!')
        ]);
    }
}
