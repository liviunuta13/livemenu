<?php

namespace App\Http\Controllers;

use App\ProducerProfile;
use Illuminate\Http\Request;

use App\ProducerProfileLocation;

class ProducersController extends Controller
{
    public function read() {

        $producers = ProducerProfile::all();

        foreach ($producers as $key => $producer) {

            $producer -> jobOffers;

            $producer -> postsLocationCheckin;

            $producer -> postsFrom;

            $producer -> postsTo;

            $producer -> restaurantLocations;

            $producer -> user;

            foreach ($producer -> producerProfileLocations as $key => $location) {

                $cont = 0;

                $rating_val = 0;

                foreach ($location -> ratings as $key => $rating) {

                    $rating_val += $rating -> stars;

                    $cont ++;
                }

//                $r_follows += count($location -> follows);
            }
            if($cont == 0) {

                $producer -> rating = null;
            } else {

                $producer -> rating = $rating_val / $cont;
            }
        }

//        $producer_profile -> follows_number = $r_follows;

        return json_encode([
            'success' => true,
            'message' => __('The producers and associated data for each were successfully taken over!'),
            'data' => $producers
        ]);
    }
}
