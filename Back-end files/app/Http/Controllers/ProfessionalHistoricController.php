<?php

namespace App\Http\Controllers;

use App\City;
use App\ProfesionistProfile;
use App\ProfessionalHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\ProfesstionalHistoric;
use phpDocumentor\Reflection\Types\Null_;
use Validator;
class ProfessionalHistoricController extends Controller
{

        public function add (Request $request){
            $validator = Validator::make($request -> all(), [
                'begin_date' => 'required|date',
                'is_working' => 'required|boolean',
                'end_date' => 'required|date',
                'working_as' => 'required|string|max:100',
                'employer_company_name' => 'required|string|max:45',
                'description' => 'required|string',
                'driving_license' => 'sometimes|string|max:45',
                'other_skills' => 'sometimes|string|max:45',
                'profesionist_profile_id' => 'required|integer',
                'city_id'=> 'required|integer'
                ]);
            if($validator -> fails()) {
                $errors = [];
                foreach ($validator -> errors() -> messages() as $key => $value){
                    $errors[$key] = [];
                    foreach ($value as $suberror){
                        $errors[$key][] = __($suberror);
                    }
                }
                return json_encode([
                    'success' => false,
                    'message' => $errors
                ]);
            }
            if(ProfesionistProfile::where('id',$request->profesionist_profile_id)->first()===null)
            {
                return json_encode(['status' => false ,'message' => __('The professionist profile is invalid')]);
            }
            if(City::where('id',$request->city_id)->first()===null){
                return json_encode(['status' => false ,'message' => __('The given city id is invalid')]);
            }
            $current_date = Carbon::now() -> format('Y-m-d');

            if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
                return json_encode([
                    'success' => false,
                    'message' => __('The begin date must be greater or equal with current date!')
                ]);
            }

            if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request ->end_date) -> format('Y-m-d')) {
                return json_encode([
                    'success' => false,
                    'message' => __('The begin date must be lower or equal with the end date!')
                ]);
            }


                $professional = new ProfessionalHistory();
                $professional->begin_date = $request->begin_date;
                $professional->is_working = $request->is_working;
                $professional->end_date = $request->end_date;
                $professional->working_as = $request->working_as;
                $professional->employer_company_name = $request->employer_company_name;
                $professional->description = $request->description;
                $professional->driving_license = $request->driving_license;
                $professional->other_skills = $request->other_skills;
                $professional->profesionist_profile_id = $request->profesionist_profile_id;
                $professional->city_id = $request->city_id;
                $professional->save();

                return json_encode([
                    'success' => true,
                    'professional_historic' => $professional,
                    'message' => __('Professional Historic was successfully added!')
                ]);


        }

         public function read($id){
             return response(json_encode(['status' => true, 'professional_historic' => ProfessionalHistory::where("id", $id)->first()]), 200);
         }

         public function edit($id,Request $request){

             $professional = ProfessionalHistory::where('id','=',$id)->first();
             if(isset($request->begin_date)) $professional->begin_date = $request->begin_date;
             if(isset($request->is_working)) $professional->is_working = $request->is_working;
             if(isset($request->end_date)) $professional->end_date = $request->end_date;
             if(isset($request->working_as)) $professional->working_as = $request->working_as;
             if(isset($request->employer_company_name)) $professional->employer_company_name = $request->employer_company_name;
             if(isset($request->description)) $professional->description = $request->description;
             if(isset($request->driving_license)) $professional->driving_license = $request->driving_license;
             if(isset($request->other_skills)) $professional->other_skills = $request->other_skills;
             if(isset($request->profesionist_profile_id)) $professional->profesionist_profile_id = $request->profesionist_profile_id;
             if(isset($request->city_id)) $professional->city_id = $request->city_id;
             $updated=$professional->save();
             if ($updated) {
                 return response(json_encode(['status' => true, 'professional_historic' => $professional]), 200);
             } else {
                 return response(json_encode(['status' => false, 'error' => 'Problems on update']), 409);
             }
         }

         public function delete($id){

             $professional = ProfessionalHistory::where("id", $id)->first();
             $professional->delete();
             return response(json_encode(['status' => true, 'id' => $id]), 200);
         }
}
