<?php

namespace App\Http\Controllers;

use Storage;
use App\Post;
use App\User;
use App\PostPicture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfessionistPostsController extends Controller
{
    public function add(Request $request) {

        $user = User::find($_GET['user_id']);

        $professionist = $user -> profesionistProfile;

        if($professionist === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'description' => 'required|string|min:3',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',
            'location_checkin_producer_profile_id' => 'sometimes|required|exists:producer_profiles,id',
            'location_checkin_restaurant_profile_id' => 'sometimes|required|exists:restaurant_profiles,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> location_checkin_producer_profile_id && $request -> location_checkin_restaurant_profile_id) {
            return json_encode([
                'success' => false,
                'message' => __('For post adding checkin you must request a producer profile or a restaurant profile!')
            ]);
        }

        $post = new Post();

        $post -> description = $request -> description;

        $post -> location_checkin_producer_profile_id = $request -> location_checkin_producer_profile_id ? $request -> location_checkin_producer_profile_id : null;

        $post -> location_checkin_restaurant_profile_id = $request -> location_checkin_restaurant_profile_id ? $request -> location_checkin_restaurant_profile_id : null;

        $post -> profesionistProfileFrom() -> associate($professionist);

        $post -> save();

        if(count($request -> pictures) > 0) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $post_picture = new PostPicture();

                    $post_picture -> post() -> associate($post);

                    $post_picture -> picture = $picture;

                    $post_picture -> save();
                }
            }
        }

        $post -> clientProfileFrom;

        $post -> clientProfileTo;

        $post -> producerProfile;

        $post -> producerProfileFrom;

        $post -> producerProfileTo;

        $post -> profesionistProfileFrom;

        $post -> restaurantLocationFrom;

        $post -> restaurantLocationTo;

        $post -> restaurantProfile;

        $post -> restaurantProfileFrom;

        $post -> restaurantProfileTo;

        $post -> postPictures;

        foreach ($post -> restaurantLocationHasPosts as $key => $value) {

            $value -> restaurantLocation;
        }

        return json_encode([
            'success' => true,
            'message' => __('The post has been successfully added!'),
            'data' => $post
        ]);
    }

    public function edit(Request $request, $post_id) {

        $user = User::find($_GET['user_id']);

        $professionist = $user -> profesionistProfile;

        if($professionist === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $post = $professionist -> postsFrom -> find($post_id);

        if($post === null) {
            return json_encode([
                'success' => false,
                'message' => __('The post is invalid!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'description' => 'sometimes|required|string|min:3',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',
            'location_checkin_producer_profile_id' => 'sometimes|required|exists:producer_profiles,id',
            'location_checkin_restaurant_profile_id' => 'sometimes|required|exists:restaurant_profiles,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> location_checkin_producer_profile_id && $request -> location_checkin_restaurant_profile_id) {
            return json_encode([
                'success' => false,
                'message' => __('For post adding checkin you must request a producer profile or a restaurant profile!')
            ]);
        }

        $post -> description = $request -> description ? $request -> description : $post -> description;

        $post -> location_checkin_producer_profile_id = $request -> location_checkin_producer_profile_id ? $request -> location_checkin_producer_profile_id : $post -> location_checkin_producer_profile_id;

        $post -> location_checkin_restaurant_profile_id = $request -> location_checkin_restaurant_profile_id ? $request -> location_checkin_restaurant_profile_id : $post -> location_checkin_restaurant_profile_id;

        $post -> save();

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $post_picture = new PostPicture();

                    $post_picture -> post() -> associate($post);

                    $post_picture -> picture = $picture;

                    $post_picture -> save();
                }
            }
        }

        $post -> clientProfileFrom;

        $post -> clientProfileTo;

        $post -> producerProfile;

        $post -> producerProfileFrom;

        $post -> producerProfileTo;

        $post -> profesionistProfileFrom;

        $post -> restaurantLocationFrom;

        $post -> restaurantLocationTo;

        $post -> restaurantProfile;

        $post -> restaurantProfileFrom;

        $post -> restaurantProfileTo;

        $post -> postPictures;

        foreach ($post -> restaurantLocationHasPosts as $key => $value) {

            $value -> restaurantLocation;
        }

        return json_encode([
            'success' => true,
            'message' => __('The post has been successfully updated!'),
            'data' => $post
        ]);
    }

    public function delete($post_id) {

        $user = User::find($_GET['user_id']);

        $professionist = $user -> profesionistProfile;

        if($professionist === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $post = $professionist -> postsFrom -> find($post_id);

        if($post === null) {
            return json_encode([
                'success' => false,
                'message' => __('The post is invalid!')
            ]);
        }

        foreach ($post -> postPictures as $key => $picture) {

            Storage::delete('/public/posts/' . $picture -> picture);

            $picture -> forceDelete();
        }

        $post -> forceDelete();

        return json_encode([
            'success' => true,
            'message' => __('The post has been deleted!')
        ]);
    }
}
