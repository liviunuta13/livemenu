<?php

namespace App\Http\Controllers;

use App;
use PDF;
use App\User;
use App\City;
use Carbon\Carbon;
use App\ClientProfile;
use App\ProducerProfile;
use App\Mail\VerifyMail;
use App\ForeignLanguage;
use App\RestaurantProfile;
use App\ProfessionalHistory;
use App\ProfesionistProfile;
use Illuminate\Http\Request;
use App\ProfessionistEducation;
use App\ProducerProfilePicture;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\ProfesionistProfileHasForeignLanguage;

class ProfessionistProfileController extends Controller
{
    public function save(Request $request) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professional_profile = $user -> profesionistProfile;

        if($professional_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:profesionist_profiles,email,' . $professional_profile -> id,
            'street_and_number' => 'sometimes|required|string|min:3|max:190',
            'postal_code' => 'sometimes|required|string|min:1|max:10',
            'county_id' => 'sometimes|required|numeric|exists:counties,id',
            'city_id' => 'sometimes|required|numeric|exists:cities,id',
            'country_id' => 'sometimes|required|numeric|exists:countries,id',
            'phone_number' => 'sometimes|required|numeric',
            'website' => 'sometimes|required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'professional_experience' => 'sometimes|required|string|min:1',
            'professional_history_begin_dates' => 'sometimes|required',
            'professional_history_begin_dates.*' => 'sometimes|required|date',
            'professional_history_presents' => 'sometimes|required',
            'professional_history_presents.*' => 'sometimes|required|boolean',
            'professional_history_end_dates' => 'sometimes|required',
            'professional_history_end_dates.*' => 'sometimes|required|date',
            'professional_history_cities' => 'sometimes|required',
            'professional_history_cities.*' => 'sometimes|required|numeric|exists:cities,id',
            'professional_history_countries' => 'sometimes|required',
            'professional_history_countries.*' => 'sometimes|required|numeric|exists:countries,id',
            'professional_history_descriptions' => 'sometimes|required',
            'professional_history_descriptions.*' => 'sometimes|required|string|min:1',

            'education_begin_dates' => 'sometimes|required',
            'education_begin_dates.*' => 'required',
            'education_presents' => 'sometimes|required',
            'education_presents.*' => 'required|boolean',
            'education_end_dates' => 'sometimes|required',
            'education_end_dates.*' => 'required',
            'education_certification_names' => 'sometimes|required',
            'education_certification_names.*' => 'required',
            'education_institution_names' => 'sometimes|required',
            'education_institution_names.*' => 'required',
            'education_city_ids' => 'sometimes|required',
            'education_city_ids.*' => 'required|numeric|exists:cities,id',
            'education_country_ids' => 'sometimes|required',
            'education_country_ids.*' => 'required|numeric|exists:countries,id',
            'education_descriptions' => 'sometimes|required',
            'education_descriptions.*' => 'required|string',

            'matern_language_id' => 'sometimes|required|numeric|exists:foreign_languages,id',
            'foreign_languages' => 'required',
            'managerial_skills' => 'sometimes|required|string|min:1',
            'skills_at_working_place' => 'sometimes|required|string|min:1',
            'digital_skills' => 'sometimes|required|string|min:1',
            'driving_licence' => 'sometimes|required|string|min:1',
            'other_skills' => 'sometimes|required|string|min:1',
            'personal_description' => 'sometimes|required|string|min:1',
            'is_looking_for_job' => 'sometimes|required|boolean',
            'available_for_working' => 'sometimes|required|boolean'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> education_begin_dates && $request -> education_end_dates) {

            foreach ($request -> education_begin_dates as $key => $request -> education_begin_date) {

                if($request -> education_begin_date && $request -> education_end_dates[$key]) {

                    // $current_date = Carbon::now() -> format('Y-m-d');

                    // if(Carbon::parse($request -> education_begin_dates[$key]) -> format('Y-m-d') >= $current_date) {
                    //     return json_encode([
                    //         'success' => false,
                    //         'message' => __('The begin date must be greater or equal with current date!')
                    //     ]);
                    // }

                    if(Carbon::parse($request -> education_begin_dates[$key]) -> format('Y-m-d') > Carbon::parse($request -> education_end_dates[$key]) -> format('Y-m-d')) {
                        return json_encode([
                            'success' => false,
                            'message' => __('The begin date must be lower or equal with the end date!')
                        ]);
                    }
                }
            }
        }

        if($request -> professional_history_begin_dates) {

            foreach ($request -> professional_history_begin_dates as $key => $professional_history_begin_date) {

                if($request -> profesional_history_presents[$key] == 0) {

                    if($request -> professional_history_end_dates[$key] == null) {
                        return json_encode([
                            'success' => false,
                            'message' => __('If the history present field is 0 then the professional history end date is required!')
                        ]);
                    }
                }

                $current_date = Carbon::now() -> format('Y-m-d');

                if(Carbon::parse($professional_history_begin_date) -> format('Y-m-d') >= $current_date) {
                    return json_encode([
                        'success' => false,
                        'message' => __('The professional history begin date must be greater or equal with the professional history current date!')
                    ]);
                }

                if(Carbon::parse($professional_history_begin_date) -> format('Y-m-d') > Carbon::parse($request -> professional_history_end_dates[$key]) -> format('Y-m-d')) {
                    return json_encode([
                        'success' => false,
                        'message' => __('The professional history begin date must be lower or equal with the professional history end date!')
                    ]);
                }

            }
        }

        $professional_profile -> name = $request -> name ? $request -> name : $professional_profile -> name;

        $professional_profile -> email = $request -> email ? $request -> email : $professional_profile -> email;

        $professional_profile -> street_and_number = $request -> street_and_number ? $request -> street_and_number: $professional_profile -> street_and_number;

        $professional_profile -> postal_code = $request -> postal_code ? $request -> postal_code : $professional_profile -> postal_code;

        $professional_profile -> city_id = $request -> city_id ? $request -> city_id : $professional_profile -> city_id;

        $professional_profile -> county_id = $request -> county_id ? $request -> county_id : $professional_profile -> county_id;

        $professional_profile -> country_id = $request -> country_id ? $request -> country_id : $professional_profile -> country_id;

        $professional_profile -> phone_number = $request -> phone_number ? $request -> phone_number : $professional_profile -> phone_number;

        $professional_profile -> website = $request -> website ? $request -> website : $professional_profile -> website;

        $professional_profile -> professional_experience = $request -> professional_experience ? $request -> professional_experience : $professional_profile -> professional_experience;

        $professional_profile -> matern_language_id = $request -> matern_language_id ? $request -> matern_language_id : $professional_profile -> matern_language_id;

        $professional_profile -> managerial_skills = $request -> managerial_skills ? $request -> managerial_skills : $professional_profile -> managerial_skills;

        $professional_profile -> skills_at_working_place = $request -> skills_at_working_place ? $request -> skills_at_working_place : $professional_profile -> skills_at_working_place;

        $professional_profile -> digital_skills = $request -> digital_skills ? $request -> digital_skills : $professional_profile -> digital_skills;

        $professional_profile -> driving_licence = $request -> driving_licence ? $request -> driving_licence : $professional_profile -> driving_licence;

        $professional_profile -> other_skills = $request -> other_skills ? $request -> other_skills : $professional_profile -> other_skills;

        $professional_profile -> personal_description = $request -> personal_description ? $request -> personal_description : $professional_profile -> personal_description;

        $professional_profile -> is_looking_for_job = $request -> is_looking_for_job ? $request -> is_looking_for_job : $professional_profile -> is_looking_for_job;

        $professional_profile -> available_for_working = $request -> available_for_working ? $request -> available_for_working : $professional_profile -> available_for_working;

        $professional_profile -> foreign_languages = $request -> foreign_languages ? $request -> foreign_languages : $professional_profile -> foreign_languages;

        $professional_profile -> save();

        // if($request -> foreign_languages_ids) {
        //
        //     $professional_profile -> profesionistProfileHasForeignLanguages() -> forceDelete();
        //
        //     foreach ($request -> foreign_languages_ids as $key => $foreign_language_index) {
        //
        //         $foreign_language = new ProfesionistProfileHasForeignLanguage();
        //
        //         $foreign_language -> profesionist_profile_id = $professional_profile -> id;
        //
        //         $foreign_language -> foreign_language_id = $foreign_language_index;
        //
        //         $foreign_language -> save();
        //     }
        // }


        if($request -> professional_history_begin_dates) {

            $professional_profile -> professionalHistory() -> forceDelete();

            foreach ($request -> professional_history_begin_dates as $key => $professional_history_begin_date) {

                $history = new ProfessionalHistory();

                $history -> begin_date = $professional_history_begin_date;

                $history -> present = $request -> professional_history_presents[$key];

                if($request -> professional_history_presents[$key] == 0) {

                    if($request -> professional_hsitory_end_dates[$key] == null) {

                        return json_encode([
                            'success' => false,
                            'message' => __('If the education present fied is 0 then the professional history end date is required!')
                        ]);
                    }
                    $history -> end_date = $request -> professional_history_end_dates[$key];
                }

                $history -> city_id = $request -> professional_history_cities[$key];

                $history -> country_id = $request -> professional_history_countries[$key];

                $history -> description = $request -> professional_history_descriptions[$key];

                $history -> profesionist_profile_id = $professional_profile -> id;

                $history -> save();

            }
        }
        // Save education section

        if($request -> education_begin_dates) {

            $professional_profile -> education() -> forceDelete();

            foreach ($request -> education_begin_dates as $key => $education_begin_date) {

                $education = new ProfessionistEducation();

                $education -> begin_date = $request -> begin_dates[$key] ? $request -> $request -> begin_dates[$key] : null;

                $education -> present = $request -> education_presents[$key] ? $request -> education_presents[$key] : null;

                if($request -> education_presents[$key] != null && $request -> education_presents[$key] == 0) {

                    if($education_begin_date == null) {

                        return json_encode([
                            'success' => false,
                            'message' => __('If the education present field is 0 then the education end date is required!')
                        ]);
                    }
                    $education -> end_date = $request -> education_begin_dates[$key];
                }

                $education -> certificate_name = $request -> education_certificate_names[$key] ? $request -> education_certificate_names[$key] : null;

                $education -> institution_name = $request -> education_institution_names[$key] ? $request -> education_institution_names[$key] : null;

                $education -> city_id = $request -> education_city_ids[$key] ? $request -> education_city_ids[$key] : null;

                $education -> country_id = $request -> education_country_ids[$key] ? $request -> education_country_ids[$key] : null;

                $education -> description = $request -> education_descriptions[$key] ? $request -> education_descriptions[$key] : null;

                $education -> profesionistProfile() -> associate($professional_profile -> id);

                $education -> save();
            }
        }

        $user -> save();

        $professional_profile -> city;

        $professional_profile -> county;

        $professional_profile -> country;

        $professional_profile -> educationCity;

        $professional_profile -> educationCountry;

        foreach ($professional_profile -> profesionistProfileHasForeignLanguages as $key => $value) {

            $value -> foreignLanguage;
        }

        $professional_profile -> professionalHistory;

        $professional_profile -> education;

        $professional_profile -> user;

        $cont = 0;

        $rating_val = 0;

        foreach ($professional_profile -> ratings as $key => $rating) {

            $rating_val += $rating -> stars;

            $cont ++;
        }

        if($cont == 0) {

            $professional_profile -> rating = null;
        } else {

            $professional_profile -> rating = $rating_val / $cont;
        }

        return json_encode([
            'success' => true,
            'message' => __('The professionist profile was successfully updated!'),
            'data' => [
                'professionist_profile' => $professional_profile
            ]
        ]);
    }

    public function delete() {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professional_profile = $user -> profesionistProfile;

        if($professional_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $professional_profile -> followsFrom() -> delete();

        $professional_profile -> followsTo() -> delete();

        $professional_profile -> postsFrom() -> delete();

        $professional_profile -> postsTo() -> delete();

        $professional_profile -> profesionistProfileHasForeignLanguages() -> delete();

        $professional_profile -> professionalHistory() -> delete();

        // $professional_profile -> ratingsFrom() -> delete();

        $professional_profile -> ratings() -> delete();

        $professional_profile -> workRequests() -> delete();

        $professional_profile -> delete();

        $user -> profesionist_profile_id = null;

        $user -> save();

        return json_encode([
            'success' => true,
            'message' => __('The professionist profile has been deleted!')
        ]);
    }

    public function availableForWorking($value) {

        if($value != 0 && $value != 1) {
            return json_encode([
                'success' => false,
                'message' => __('The given value is invalid. It must be boolean!')
            ]);
        }

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professional_profile = $user -> profesionistProfile;

        if($professional_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $professional_profile -> available_for_working = $value;

        $professional_profile -> save();

        $professional_profile -> city;

        $professional_profile -> county;

        $professional_profile -> country;

        $professional_profile -> educationCity;

        $professional_profile -> educationCountry;

        foreach ($professional_profile -> profesionistProfileHasForeignLanguages as $key => $value) {

            $value -> foreignLanguage;
        }

        $professional_profile -> professionalHistory;

        $professional_profile -> user;

        return json_encode([
            'success' => true,
            'message' => __('The working status has been updated!'),
            'data' => [
                'professionist_profile' => $professional_profile
            ]
        ]);
    }

    public function workingStatus($value) {

        if($value != 0 && $value != 1) {
            return json_encode([
                'success' => false,
                'message' => __('The given value is invalid. It must be boolean!')
            ]);
        }

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professional_profile = $user -> profesionistProfile;

        if($professional_profile === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $professional_profile -> is_at_working = $value;

        $professional_profile -> save();

        $professional_profile -> city;

        $professional_profile -> county;

        $professional_profile -> country;

        $professional_profile -> educationCity;

        $professional_profile -> educationCountry;

        foreach ($professional_profile -> profesionistProfileHasForeignLanguages as $key => $value) {

            $value -> foreignLanguage;
        }

        $professional_profile -> professionalHistory;

        $professional_profile -> user;

        return json_encode([
            'success' => true,
            'message' => __('The working status has been updated!'),
            'data' => [
                'professionist_profile' => $professional_profile
            ]
        ]);
    }

    public function read() {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professionist = $user -> profesionistProfile;

        if($professionist === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $cont = 0;

        $rating_val = 0;

        foreach ($professionist -> ratings as $key => $rating) {

            $rating_val += $rating -> stars;

            $cont ++;
        }

        if($cont == 0) {

            $professionist -> rating = null;
        } else {

            $professionist -> rating = $rating_val / $cont;
        }

        $professionist -> city;

        $professionist -> county;

        $professionist -> country;

        $professionist -> educationCity;

        $professionist -> educationCountry;

        $professionist -> buyedSponsorships;

        // $professionist -> followsFrom;

        $professionist -> follows;

        $professionist -> postsFrom;

        $professionist -> postsTo;

        $professionist -> professionalHistory;

        $professionist -> ratings;

        $professionist -> user;

        $professionist -> workRequests;

        foreach ($professionist -> profesionistProfileHasForeignLanguages as $key => $value) {

            $value -> foreignLanguage;
        }

        return json_encode([
            'success' => true,
            'message' => __('Your professiononist profile has successfully taken over!'),
            'data' => [
                'professionist_profile' => $professionist
            ]
        ]);
    }

    public function readProfessionist($professionist_id) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professionist = ProfesionistProfile::find($professionist_id);

        if($professionist === null) {
            return json_encode([
                'success' => false,
                'message' => __('The solicitated professionist profile is invalid!')
            ]);
        }

        if($user -> professionistProfile != null && $user -> restaurantProfile != null && $user -> producerProfile != null) {

            $cont = 0;

            $rating_val = 0;

            foreach ($professionist -> ratings as $key => $rating) {

                $rating_val += $rating -> stars;

                $cont ++;
            }

            if($cont == 0) {

                $professionist -> rating = null;
            } else {

                $professionist -> rating = $rating_val / $cont;
            }

            $professionist -> city;

            $professionist -> county;

            $professionist -> country;

            $professionist -> educationCity;

            $professionist -> educationCountry;

            $professionist -> buyedSponsorships;

            $professionist -> follows;

            // $professionist -> followsTo;

            $professionist -> postsFrom;

            $professionist -> postsTo;

            $professionist -> professionalHistory;

            $professionist -> ratings;

            $professionist -> user;

            $professionist -> workRequests;

            foreach ($professionist -> profesionistProfileHasForeignLanguages as $key => $value) {

                $value -> foreignLanguage;
            }

            return json_encode([
                'success' => true,
                'message' => __('Theprofessiononist profile has successfully taken over!'),
                'data' => [
                    'professionist_profile' => $professionist
                ]
            ]);
        } else {

            $professionist -> city;

            $professionist -> county;

            $professionist -> country;

            $professionist -> educationCity;

            $professionist -> educationCountry;

            $professionist -> user;

            foreach ($professionist -> profesionistProfileHasForeignLanguages as $key => $value) {

                $value -> foreignLanguage;
            }

            return json_encode([
                'success' => true,
                'message' => __('Theprofessiononist profile has successfully taken over!'),
                'data' => [
                    'professionist_profile' => $professionist
                ]
            ]);
        }
    }

    public function readProfessionists() {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professionists = ProfesionistProfile::all();

        foreach ($professionists as $key => $professionist) {

            // $cont = 0;
            //
            // $rating_val = 0;
            //
            // foreach ($professionist -> ratings as $key => $rating) {
            //
            //     $rating_val += $rating -> stars;
            //
            //     $cont ++;
            // }
            //
            // if($cont == 0) {
            //
            //     $professionist -> rating = null;
            // } else {
            //
            //     $professionist -> rating = $rating_val / $cont;
            // }

            $professionist -> city;

            $professionist -> county;

            $professionist -> country;

            $professionist -> educationCity;

            $professionist -> educationCountry;

            // $professionist -> ratings;

            $professionist -> user;


            foreach ($professionist -> profesionistProfileHasForeignLanguages as $key => $value) {

                $value -> foreignLanguage;
            }
        }

        return json_encode([
            'success' => true,
            'message' => __('The professionists profiles has successfully taken over!'),
            'data' => [
                'professionists_profiles' => $professionists
            ]
        ]);
    }

    public function exportCV() {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $data = $user -> profesionistProfile;

        if($data === null) {
            return json_encode([
                'success' => false,
                'message' => __('The solicitated professionist profile is invalid!')
            ]);
        }

        $pdf = App::make('dompdf.wrapper');

        $data -> city;

        $data -> county;

        $data -> country;

        $data -> educationCity;

        $data -> educationCountry;

        foreach ($data -> profesionistProfileHasForeignLanguages as $key => $value) {

            $value -> foreignLanguage;
        }

        foreach ($data -> professionalHistory as $key => $value) {

            $value -> city;

            $value -> country;
        }

        $data -> user;

        $data -> matern_language = ForeignLanguage::find($data -> matern_language_id);

        $pdf = PDF::loadView('/pdf/cv', compact('data'));

        return $pdf -> download(str_replace(' ', '', $data -> name) . '_cv.pdf');

        // return $pdf -> stream();
    }
}
