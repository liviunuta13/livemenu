<?php

namespace App\Http\Controllers;

use App\User;
use App\WorkRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfessionistWorkRequestsController extends Controller
{
    public function add(Request $request) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professionist = $user -> profesionistProfile;

        if($professionist === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'work_position' => 'required|string|min:1|max:100',
            'location_id' => 'required|numeric|exists:locations,id',
            'desired_salary' => 'required|string|min:1|max:45',
            'contract_type_id' => 'required|numeric|exists:contract_types,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $work_request = new WorkRequest();

        $work_request -> work_position = $request -> work_position;

        $work_request -> location_id = $request -> location_id;

        $work_request -> desired_salary = $request -> desired_salary;

        $work_request -> contract_type_id = $request -> contract_type_id;

        $work_request -> profesionistProfile() -> associate($professionist);

        $work_request -> save();

        return json_encode([
            'success' => true,
            'message' => __('The work request has been saved!'),
            'data' => [
                'work_request' => $work_request
            ]
        ]);
    }

    public function update(Request $request, $work_request_id) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professionist = $user -> profesionistProfile;

        if($professionist === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $work_request = WorkRequest::find($work_request_id);

        if($work_request === null) {
            return json_encode([
                'success' => false,
                'message' => __('You given work request is invalid!')
            ]);
        }

        if($work_request -> profesionist_profile_id != $professionist -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given work request is not your!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'work_position' => 'sometimes|required|string|min:1|max:100',
            'location_id' => 'sometimes|required|numeric|exists:locations,id',
            'desired_salary' => 'sometimes|required|string|min:1|max:45',
            'contract_type_id' => 'sometimes|required|numeric|exists:contract_types,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $work_request -> work_position = $request -> work_position ? $request -> work_position : $work_request -> work_position;

        $work_request -> location_id = $request -> location_id ? $request -> location_id : $work_request -> location_id;

        $work_request -> desired_salary = $request -> desired_salary ? $request -> desired_salary : $work_request -> desired_salary;

        $work_request -> contract_type_id = $request -> contract_type_id ? $request -> contract_type_id : $work_request -> contract_type_id;

        $work_request -> save();

        return json_encode([
            'success' => true,
            'message' => __('The work request has been updated!'),
            'data' => [
                'work_request' => $work_request
            ]
        ]);
    }

    public function delete($work_request_id) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professionist = $user -> profesionistProfile;

        if($professionist === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $work_request = WorkRequest::find($work_request_id);

        if($work_request === null) {
            return json_encode([
                'success' => false,
                'message' => __('You given work request is invalid!')
            ]);
        }

        if($work_request -> profesionist_profile_id != $professionist -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given work request is not your!')
            ]);
        }

        $work_request -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The work request has been deleted!')
        ]);
    }
}
