<?php

namespace App\Http\Controllers;

use App\User;
use App\Rating;
use App\RatingPicture;
use App\ProducerStaff;
use App\RestaurantStaff;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RatingsController extends Controller
{
    public function addRatingFromProducerLocation(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no producer profile created!')
            ]);
        }

        $location = $producer -> producerProfileLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'stars' => 'sometimes|required|numeric|min:1|max:5',
            'professionist_id' => 'sometimes|required|numeric|exists:profesionist_profiles,id',
            'restaurant_location_id' => 'sometimes|required|numeric|exists:restaurant_locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> professionist_id === null && $request -> restaurant_location_id === null) {
            return json_encode([
                'success' => false,
                'message' => __('The restaurant location or profesionist profile id is required!')
            ]);
        }

        if($request -> professionist_id != null && $request -> restaurant_location_id != null) {
            return json_encode([
                'success' => false,
                'message' => __('The request must contain restaurant location or profesionist profile id!')
            ]);
        }

        $rating = new Rating();

        $rating -> stars = $request -> stars;

        $rating -> producerProfileLocationFrom() -> associate($location);

        if($request -> restaurant_location_id) {

            $rating -> to_restaurant_location_id = $request -> restaurant_location_id;
        }

        if($request -> professionist_id) {

            $rating -> to_profesionist_profile_id = $request -> professionist_id;
        }

        $rating -> save();

        return json_encode([
            'success' => true,
            'message' => __('The rating has successfully submitted!')
        ]);
    }

    public function addRatingFromProfessionist(Request $request) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $professionist = $user -> profesionistProfile;

        if($professionist === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no professionist profile created!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'stars' => 'sometimes|required|numeric|min:1|max:5',
            'producer_location_id' => 'sometimes|required|numeric|exists:producer_profile_locations,id',
            'restaurant_location_id' => 'sometimes|required|numeric|exists:restaurant_locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> restaurant_location_id === null && $request -> producer_location_id === null) {
            return json_encode([
                'success' => false,
                'message' => __('The producer location or restaurant location id is required!')
            ]);
        }

        if($request -> restaurant_location_id != null && $request -> producer_location_id != null) {
            return json_encode([
                'success' => false,
                'message' => __('The request must contain producer location or restaurant location id!')
            ]);
        }

        // Verify if the professionist is a part of the restaurant location

        if($request -> restaurant_location_id) {

            if(RestaurantStaff::where('profesionist_profile_id', $professionist -> id) -> where('restaurant_location_id', $request -> restaurant_location_id) -> first() == null) {

                return json_encode([
                    'success' => false,
                    'message' => __('You cannot leave a rating if you are not a part of the staff!')
                ]);
            }
        }

        // Verify if the professionist is a part of the restaurant location

        if($request -> producer_location_id) {

            if(ProducerStaff::where('profesionist_profile_id', $professionist -> id) -> where('producer_location_id', $request -> producer_location_id) -> first() == null) {

                return json_encode([
                    'success' => false,
                    'message' => __('You cannot leave a rating if you are not a part of the staff!')
                ]);
            }
        }

        $rating = new Rating();

        $rating -> stars = $request -> stars;

        $rating -> profesionistProfileFrom() -> associate($professionist);

        if($request -> restaurant_location_id) {

            $rating -> to_restaurant_location_id = $request -> restaurant_location_id;
        }

        if($request -> professionist_location_id) {

            $rating -> to_producer_location_id = $request -> producer_location_id;
        }

        $rating -> save();

        return json_encode([
            'success' => true,
            'message' => __('The rating has successfully submitted!')
        ]);
    }

    public function addRatingFromRestaurantLocation(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $restaurant = $user -> restaurantProfile;

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('You have no restaurant profile created!')
            ]);
        }

        $location = $restaurant -> restaurantLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'stars' => 'sometimes|required|numeric|min:1|max:5',
            'professionist_id' => 'sometimes|required|numeric|exists:profesionist_profiles,id',
            'producer_location_id' => 'sometimes|required|numeric|exists:producer_profile_locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> professionist_id === null && $request -> restaurant_location_id === null) {
            return json_encode([
                'success' => false,
                'message' => __('The restaurant location or profesionist profile id is required!')
            ]);
        }

        if($request -> professionist_id != null && $request -> producer_location_id != null) {
            return json_encode([
                'success' => false,
                'message' => __('The request must contain producer location or profesionist profile id!')
            ]);
        }

        $rating = new Rating();

        $rating -> stars = $request -> stars;

        $rating -> producerProfileLocationFrom() -> associate($location);

        if($request -> producer_location_id) {

            $rating -> to_producer_location_id = $request -> producer_location_id;
        }

        if($request -> professionist_id) {

            $rating -> to_profesionist_profile_id = $request -> professionist_id;
        }

        $rating -> save();

        return json_encode([
            'success' => true,
            'message' => __('The rating has successfully submitted!')
        ]);
    }

    // public function submitRating(Request $request) {
    //
    //     $user = User::find($_GET['user_id']);
    //
    //     $producer = $user -> producerProfile;
    //
    //     if($producer === null) {
    //         return json_encode([
    //             'success' => false,
    //             'message' => __('The logged user does not have a producer profile created!')
    //         ]);
    //     }
    //
    //     $validator = Validator::make($request -> all(), [
    //         'description' => 'sometimes|required|string|min:3',
    //         'stars' => 'sometimes|required|numeric|min:0',
    //         'professionist_id' => 'sometimes|required|numeric|exists:profesionist_profiles,id',
    //         'restaurant_id' => 'sometimes|required|numeric|exists:restaurant_profiles,id',
    //         'pictures' => 'sometimes|required',
    //         'pictures.*' => 'required|file|mimes:jpeg,jpg,png,gif'
    //     ]);
    //
    //     if($validator -> fails()) {
    //         $errors = [];
    //         foreach ($validator -> errors() -> messages() as $key => $value){
    //             $errors[$key] = [];
    //             foreach ($value as $suberror){
    //                 $errors[$key][] = __($suberror);
    //             }
    //         }
    //         return json_encode([
    //             'success' => false,
    //             'message' => $errors
    //         ]);
    //     }
    //
    //     if($request -> professionist_id === null && $request -> producer_id === null) {
    //         return json_encode([
    //             'success' => false,
    //             'message' => __('The producer or proffesionist profile id is required!')
    //         ]);
    //     }
    //
    //     if($request -> professionist_id != null && $request -> producer_id != null) {
    //         return json_encode([
    //             'success' => false,
    //             'message' => __('The request must contain producer or proffesionist profile id!')
    //         ]);
    //     }
    //
    //     $rating = new Rating();
    //
    //     $rating -> producerProfileFrom() -> associate($producer);
    //
    //     if($request -> restaurant_id) {
    //
    //         $rating -> to_restaurant_profile_id = $request -> restaurant_id;
    //     }
    //
    //     if($request -> professionist_id) {
    //
    //         $rating -> to_profesionist_profile_id = $request -> professionist_id;
    //     }
    //
    //     $rating -> description = $request -> description ? $request -> description : null;
    //
    //     $rating -> stars = $request -> stars ? $request -> stars : null;
    //
    //     $rating -> save();
    //
    //     if($request -> hasFile('pictures')) {
    //
    //         foreach ($request -> file('pictures') as $key => $picture) {
    //          {
    //                 $picture_name = $rating -> id . time() . rand(1,99999) . '.' . $picture -> extension();
    //
    //                 $rating_picture = new RatingPicture();
    //
    //                 $rating_picture -> picture = $picture_name;
    //
    //                 $rating_picture -> rating() -> associate($rating);
    //
    //                 $rating_picture -> save();
    //
    //                 Storage::putFileAs('/public/ratings', $picture, $picture_name);
    //             }
    //         }
    //     }
    //
    //     $rating -> producerProfileFrom;
    //
    //     $rating -> producerProfileTo;
    //
    //     $rating -> profesionistProfileFrom;
    //
    //     $rating -> profesionistProfileTo;
    //
    //     $rating -> restaurantLocation;
    //
    //     $rating -> restaurantProfile;
    //
    //     $rating -> user;
    //
    //     $rating -> ratingPictures;
    //
    //     return json_encode([
    //         'success' => true,
    //         'message' => __('The rating has successfully submitted!'),
    //         'data' => [
    //             'rating' => $rating
    //         ]
    //     ]);
    // }
    //
    // public function addReview(Request $request, $location_id) {
    //
    //     $user = User::find($_GET['user_id']);
    //
    //     $location = RestaurantLocation::find($location_id);
    //
    //     if($location === null) {
    //         return json_encode([
    //             'success' => false,
    //             'message' => __('The given restaurant location id is invalid!')
    //         ]);
    //     }
    //
    //     if($user -> restaurantProfile === null) {
    //         return json_encode([
    //             'success' => false,
    //             'message' => __('The given user does not have an associated restaurant profile!')
    //         ]);
    //     }
    //
    //     if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
    //         return json_encode([
    //             'success' => false,
    //             'message' => __('The given user is not associated with the given restaurant profile!')
    //         ]);
    //     }
    //
    //     $validator = Validator::make($request -> all(), [
    //         'description' => 'sometimes|required|string|min:3',
    //         'stars' => 'sometimes|required|numeric|min:0',
    //         'professionist_id' => 'sometimes|required|numeric|exists:profesionist_profiles,id',
    //         'producer_id' => 'sometimes|required|numeric|exists:producer_profiles,id',
    //         'pictures' => 'sometimes|required',
    //         'pictures.*' => 'required|file|mimes:jpeg,jpg,png,gif'
    //     ]);
    //
    //     if($validator -> fails()) {
    //         $errors = [];
    //         foreach ($validator -> errors() -> messages() as $key => $value){
    //             $errors[$key] = [];
    //             foreach ($value as $suberror){
    //                 $errors[$key][] = __($suberror);
    //             }
    //         }
    //         return json_encode([
    //             'success' => false,
    //             'message' => $errors
    //         ]);
    //     }
    //
    //     if($request -> professionist_id === null && $request -> producer_id === null) {
    //         return json_encode([
    //             'success' => false,
    //             'message' => __('The producer or proffesionist profile id is required!')
    //         ]);
    //     }
    //
    //     if($request -> professionist_id != null && $request -> producer_id != null) {
    //         return json_encode([
    //             'success' => false,
    //             'message' => __('The request must contain producer or proffesionist profile id!')
    //         ]);
    //     }
    //
    //     $rating = new Rating();
    //
    //     $rating -> restaurantLocation() -> associate($location);
    //
    //     if($request -> producer_id) {
    //
    //         $rating -> to_producer_profile_id = $request -> producer_id;
    //     }
    //
    //     if($request -> professionist_id) {
    //
    //         $rating -> to_profesionist_profile_id = $request -> professionist_id;
    //     }
    //
    //     $rating -> description = $request -> description ? $request -> description : null;
    //
    //     $rating -> stars = $request -> stars ? $request -> stars : null;
    //
    //     $rating -> save();
    //
    //     if($request -> hasFile('pictures')) {
    //
    //         foreach ($request -> file('pictures') as $key => $picture) {
    //          {
    //                 $picture_name = $rating -> id . time() . rand(1,99999) . '.' . $picture -> extension();
    //
    //                 $rating_picture = new RatingPicture();
    //
    //                 $rating_picture -> picture = $picture_name;
    //
    //                 $rating_picture -> rating() -> associate($rating);
    //
    //                 $rating_picture -> save();
    //
    //                 Storage::putFileAs('/public/ratings', $picture, $picture_name);
    //             }
    //         }
    //     }
    //
    //     $rating -> producerProfileFrom;
    //
    //     $rating -> producerProfileTo;
    //
    //     $rating -> profesionistProfileFrom;
    //
    //     $rating -> profesionistProfileTo;
    //
    //     $rating -> restaurantLocation;
    //
    //     $rating -> restaurantProfile;
    //
    //     $rating -> user;
    //
    //     $rating -> ratingPictures;
    //
    //     return json_encode([
    //         'success' => true,
    //         'message' => __('The rating has successfully submitted!'),
    //         'data' => [
    //             'rating' => $rating
    //         ]
    //     ]);
    // }
}
