<?php

namespace App\Http\Controllers;

use App\ContractType;
use App\Currency;
use App\ProfesionistProfile;
use App\RestaurantLocationStaff;
use App\RestaurantLocationStaffHasRole;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Validator;

class RestaurantLocationAddStaffController extends Controller
{

    public function add(Request $request, $location_id, $staff_id)
    {
        $user = User::find($_GET['user_id']);

        $restaurant = $user->restaurantProfile;

        if ($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }

        $location = $restaurant->restaurantLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }
        $staff = $location->staff->find($staff_id);
        if ($staff === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given staff id is invalid!')
            ]);
        }

        $staff_has_role = RestaurantLocationStaffHasRole::where('staff_id', $staff_id)->where('role_id', 4)->first();
        if ($staff_has_role === null) {
            return json_encode([
                'success' => false,
                'message' => __('You do not have permissions to add a post!')
            ]);
        }

        $if_contract_type_exists = ContractType::where('id',$request->contract_type_id)->first();
        if($if_contract_type_exists===null){
            return json_encode([
                'success' => false,
                'message' => __('Contract type does not exist!')
            ]);
        }
        $currency_if_exists = Currency::where('id',$request->salary_currency_id)->first();
        if($currency_if_exists ===null){
            return json_encode([
                'success' => false,
                'message' => __('Currency does not exists!')
            ]);
        }
        $if_profesionist_profile_exists = ProfesionistProfile::where('id',$request->profesionist_profile_id)->first();
        if($if_profesionist_profile_exists===null){
            return json_encode([
                'success' => false,
                'message' => __('Professionist does not exist!')
            ]);
        }


        $validator = Validator::make($request->all(), [
            'contract_type_id' => 'required|integer',
            'salary' => 'required|string|max:45',
            'salary_currency_id' => 'required|integer',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'job' => 'required|string|max:45',
            'profesionist_profile_id' => 'required|integer',

        ]);

        $current_date = Carbon::now()->format('Y-m-d');

        if (Carbon::parse($request->start_date)->format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if (Carbon::parse($request->start_date)->format('Y-m-d') > Carbon::parse($request->end_date)->format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $restaurant_staff = new RestaurantLocationStaff();

        $restaurant_staff->restaurant_location_id = $location_id;
        $restaurant_staff->contract_type_id = $request->contract_type_id;
        $restaurant_staff->salary = $request->salary;
        $restaurant_staff->salary_currency_id = $request->salary_currency_id;
        $restaurant_staff->start_date = $request->start_date;
        $restaurant_staff->end_date = $request->end_date;
        $restaurant_staff->job = $request->job;
        $restaurant_staff->profesionist_profile_id = $request->profesionist_profile_id;
        $restaurant_staff->client_profile_id = $request->client_profile_id;

        $restaurant_staff->save();


        return json_encode([
            'success' => true,
            'message' => __('The staff has been successfully added!'),
            'data' => [
                'staff' => $restaurant_staff
            ]
        ]);
    }
}
