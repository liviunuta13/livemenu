<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationEvent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationEventsController extends Controller
{
    public function add(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3',
            'theme' => 'required|string|min:3|max:45',
            'banner' => 'required|string',
            'one_day_event' => 'sometimes|required|boolean',
            'multi_day_event' => 'sometimes|required|boolean',
            'begin_date' => 'required|date',
            'end_date' => 'required|date',
            'begin_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            return json_encode([
                'success' => false,
                'message' => __('The begin time must be lower than end time!')
            ]);
        }

        $event = new RestaurantLocationEvent();

        $event -> name = $request -> name;

        $event -> description = $request -> description;

        $event -> theme = $request -> theme;

        $event -> one_day_event = $request -> one_day_event;

        $event -> multi_day_event = $request -> multi_day_event;

        $event -> begin_date = $request -> begin_date;

        $event -> end_date = $request -> end_date;

        $event -> begin_time = $request -> begin_time;

        $event -> end_time = $request -> end_time;

        if ($request -> banner) {

            // Love you <3

            $event -> banner = $picture;
        }

        $event -> restaurantLocation() -> associate($location);

        $event -> save();

        return json_encode([
            'success' => true,
            'message' => __('The event was successfully added!'),
            'data' => $event
        ]);
    }

    // Edit location event
    public function save(Request $request, $location_id, $event_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $event = RestaurantLocationEvent::find($event_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($event === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given event id is invalid!')
            ]);
        }

        if($event -> restaurant_location_id !== $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The event id is not associated with the restaurant location id!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'theme' => 'sometimes|required|string|min:3|max:45',
            'banner' => 'sometimes|required|string',
            'one_day_event' => 'sometimes|required|boolean',
            'multi_day_event' => 'sometimes|required|boolean',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'begin_time' => 'sometimes|required|date_format:H:i',
            'end_time' => 'sometimes|required|date_format:H:i'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            return json_encode([
                'success' => false,
                'message' => __('The begin time must be lower than end time!')
            ]);
        }

        $event -> name = $request -> name ? $request -> name : $event -> name;

        $event -> description = $request -> description ? $request -> description : $event -> description;

        $event -> theme = $request -> theme ? $request -> theme : $event -> theme;

        $event -> one_day_event = $request -> one_day_event ? $request -> one_day_event : $event -> one_day_event;

        $event -> multi_day_event = $request -> multi_day_event ? $request -> multi_day_event : $event -> multi_day_event;

        $event -> begin_date = $request -> begin_date ? $request -> begin_date : $event -> begin_date;

        $event -> end_date = $request -> end_date ? $request -> end_date : $event -> end_date;

        $event -> begin_time = $request -> begin_time ? $request -> begin_time : $event -> begin_time;

        $event -> end_time = $request -> end_time ? $request -> end_time : $event -> end_time;

        if ($request -> banner) {

            $event -> banner = $picture;
        }

        $event -> save();

        return json_encode([
            'success' => true,
            'message' => __('The event was successfully added!'),
            'data' => $event
        ]);
    }

    public function delete(Request $request, $location_id, $event_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $event = RestaurantLocationEvent::find($event_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($event === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given event id is invalid!')
            ]);
        }

        if($event -> restaurant_location_id !== $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The event id is not associated with the restaurant location id!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $event -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The event has been deleted successfully!')
        ]);
    }

    public function getEvents($location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $events = $location -> restaurantLocationEvents;

        return json_encode([
            'success' => true,
            'message' => __('The restaurant events were successfully taken over!'),
            'data' => $events
        ]);
    }
}
