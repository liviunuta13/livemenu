<?php

namespace App\Http\Controllers;

use App\User;
use App\Hotel;
use App\HotelPicture;
use App\HotelFacility;
use App\HotelHasFacility;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationHotelsController extends Controller
{
    public function add(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3',
            'stars' => 'required|numeric|min:1|max:5',
            'email' => ['required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:hotels,email'],
            'phone_number' => 'sometimes|required|numeric',
            'capacity' => 'required|numeric',
            'facilities' => 'required',
            'facilities.*' => 'required|numeric|exists:hotel_facilities,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $hotel = new Hotel();

        $hotel -> name = $request -> name;

        $hotel -> description = $request -> description;

        $hotel -> stars = $request -> stars;

        $hotel -> email = $request -> email;

        $hotel -> phone_number = $request -> phone_number;

        $hotel -> capacity = $request -> capacity;

        $hotel -> restaurantLocation() -> associate($location);

        $hotel -> save();

        foreach ($request -> facilities as $key => $facility) {

            $hotel_has_facility = new HotelHasFacility();

            $hotel_has_facility -> hotel() -> associate($hotel);

            $hotel_has_facility -> facility_id = $facility;

            $hotel_has_facility -> save();
        }

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $restaurant_has_picture = new HotelPicture();

                    $restaurant_has_picture -> hotel() -> associate($hotel);

                    $restaurant_has_picture -> picture = $picture;

                    $restaurant_has_picture -> save();
                }
            }
        }

        return json_encode([
            'success' => true,
            'message' => __('The hotel was successfully added!'),
            'data' => $hotel
        ]);
    }

    // Edit hotel
    public function save(Request $request, $location_id, $hotel_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $hotel = Hotel::find($hotel_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($hotel === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given hotel id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($hotel -> restaurant_location_id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given hotel is not associated with the given restaurant location!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'stars' => 'sometimes|required|numeric|min:1|max:5',
            'email' => ['sometimes', 'required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:hotels,email'],
            'phone_number' => 'sometimes|required|numeric',
            'capacity' => 'sometimes|required|numeric',
            'facilities' => 'sometimes|required',
            'facilities.*' => 'required|numeric|exists:hotel_facilities,id',
            'delete_facilities' => 'sometimes|required',
            'delete_facilities.*' => 'required|numeric|exists:hotel_has_facility,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $hotel -> name = $request -> name ? $request -> name : $hotel -> name;

        $hotel -> description = $request -> description ? $request -> description : $hotel -> description;

        $hotel -> stars = $request -> stars ? $request -> stars : $hotel -> strs;

        $hotel -> email = $request -> email ? $request -> email : $hotel -> email;

        $hotel -> phone_number = $request -> phone_number ? $request -> phone_number : $hotel -> phone_number;

        $hotel -> capacity = $request -> capacity ? $request -> capacity : $hotel -> capacity;

        $hotel -> save();

        if(isset($request -> facilities) && count($request -> facilities)) {

            foreach ($request -> facilities as $key => $facility) {

                $hotel_has_facility = new HotelHasFacility();

                $hotel_has_facility -> hotel() -> associate($hotel);

                $hotel_has_facility -> facility_id = $facility;

                $hotel_has_facility -> save();
            }
        }

        if(isset($request -> delete_facilities)) {

            foreach ($request -> delete_facilities as $key => $facility_id) {

                $hotel_has_facility = HotelHasFacility::find($facility_id);

                if($hotel_has_facility && $hotel_has_facility -> hotel_id == $hotel -> id) {

                    $hotel_has_facility -> forceDelete();
                }
            }
        }

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $restaurant_has_picture = new HotelPicture();

                    $restaurant_has_picture -> hotel() -> associate($hotel);

                    $restaurant_has_picture -> picture = $picture;

                    $restaurant_has_picture -> save();
                }
            }
        }

        $hotel -> restaurantLocation;

        foreach ($hotel -> hotelHasFacilities as $key => $value) {

            $value -> hotelFacility;
        }

        $hotel -> hotelPictures;

        $hotel -> rooms;

        return json_encode([
            'success' => true,
            'message' => __('The hotel was successfully updated!'),
            'data' => $hotel
        ]);
    }

    public function delete(Request $request, $location_id, $hotel_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $hotel = Hotel::find($hotel_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($hotel === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given hotel id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($hotel -> restaurant_location_id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given hotel is not associated with the given restaurant location!')
            ]);
        }

        foreach ($hotel -> hotelPictures as $key => $picture) {

            $picture -> delete();
        }

        return json_encode([
            'success' => true,
            'message' => __('The hotel and all data associated was successfully deleted!')
        ]);
    }

    public function deletePicture(Request $request, $location_id, $hotel_id, $picture_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $hotel = Hotel::find($hotel_id);

        $picture = HotelPicture::find($picture_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($hotel === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given hotel id is invalid!')
            ]);
        }

        if($picture === null) {
            return json_encode([
                'success' => false,
                'message' => __('The picture id is invalid!')
            ]);
        }

        if($picture -> hotel_id != $hotel -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The picture does is not associated with the hotel!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($hotel -> restaurant_location_id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given hotel is not associated with the given restaurant location!')
            ]);
        }

        $picture -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The picture has been deleted!')
        ]);
    }
}
