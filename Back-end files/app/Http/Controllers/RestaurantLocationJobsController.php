<?php

namespace App\Http\Controllers;

use App\User;
use App\JobOffer;
use Carbon\Carbon;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationJobsController extends Controller
{
    public function add(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3',
            'contract_type_id' => 'required|exists:contract_types,id',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'salary' => 'required|numeric:min:1',
            'currency_id' => 'required|numeric|exists:currencies,id',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'location_id' => 'sometimes|required|exists:locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> location_id === null && ($request -> latitude === null || $request -> longitude === null)) {

            return json_encode([
                'success' => false,
                'message' => __('The location id or latidude and logitude are required!')
            ]);
        }

        if($request -> contract_type_id == 3 || $request -> contract_type_id == 1) {
            if($request -> begin_date == null || $request -> begin_date == null) {
                return json_encode([
                    'success' => false,
                    'message' => __('For seasonal and definite contract type the begin and and end date are required!')
                ]);
            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        $job = new JobOffer();

        $job -> name = $request -> name;

        $job -> description = $request -> description;

        $job -> contract_type_id = $request -> contract_type_id;

        $job -> begin_date = $request -> begin_date;

        $job -> end_date = $request -> end_date;

        $job -> salary = $request -> salary;

        $job -> currency_id = $request -> currency_id;

        $job -> location_id = $request -> location_id ? $request -> location_id : null;

        if($request -> latitude && $request -> longitude) {

            $job -> latitude = $request -> latitude;

            $job -> longitude = $request -> longitude;
        }

        $job -> restaurantLocation() -> associate($location);

        $job -> save();

        return json_encode([
            'success' => true,
            'message' => __('The job was successfully added!'),
            'data' => $job
        ]);
    }

    // Edit job
    public function save(Request $request, $location_id, $job_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $job = JobOffer::find($job_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($job === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given job id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($job -> restaurant_location_id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The job restaurant location is not associated with restaurant location id!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'contract_type_id' => 'sometimes|required|exists:contract_types,id',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'salary' => 'sometimes|required|numeric:min:1',
            'currency_id' => 'sometimes|required|numeric|exists:currencies,id',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'location_id' => 'sometimes|required|exists:locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> location_id === null && ($request -> latitude === null || $request -> longitude === null)) {

            return json_encode([
                'success' => false,
                'message' => __('The location id or latidude and logitude are required!')
            ]);
        }

        if($request -> contract_type_id == 3) {
            if($request -> begin_date == null || $request -> end_date == null) {
                return json_encode([
                    'success' => false,
                    'message' => __('For seasonal contract type the begin and and end date are required!')
                ]);
            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        $job -> name = $request -> name ? $request -> name : $job -> name;

        $job -> description = $request -> description ? $request -> description : $job -> description;

        $job -> contract_type_id = $request -> contract_type_id ? $request -> contract_type_id : $job -> contract_type_id;

        $job -> begin_date = $request -> begin_date ? $request -> begin_date : $job -> begin_date;

        $job -> end_date = $request -> end_date ? $request -> end_date : $job -> end_date;

        $job -> salary = $request -> salary ? $request -> salary : $job -> salary;

        $job -> currency_id = $request -> currency_id ? $request -> currency_id : $job -> currency_id;

        $job -> location_id = $request -> location_id ? $request -> location_id : $job -> location_id;

        if($request -> latitude && $request -> longitude) {

            $job -> latitude = $request -> latitude;

            $job -> longitude = $request -> longitude;
        }

        $job -> save();

        return json_encode([
            'success' => true,
            'message' => __('The job was successfully updated!'),
            'data' => $job
        ]);
    }

    public function delete(Request $request, $location_id, $job_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $job = JobOffer::find($job_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($job === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given job id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($job -> restaurant_location_id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The job restaurant location is not associated with restaurant location id!')
            ]);
        }

        $job -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The job was successfully deleted!')
        ]);
    }

    public function getJobs($location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $jobs = $location -> jobs;

        return json_encode([
            'success' => true,
            'message' => __('The restaurant jobs were successfully taken over!'),
            'data' => $jobs
        ]);
    }
}
