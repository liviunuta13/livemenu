<?php

namespace App\Http\Controllers;

use Storage;
use App\Unit;
use App\Menu;
use App\User;
use App\Currency;
use Carbon\Carbon;
use App\Ingredient;
use App\FoodPicture;
use App\ProducerProfile;
use App\MenuHasIngredient;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\DailyMenuAvailability;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationMenuController extends Controller
{

    private $jsonIsValid = true;

    private $parentsQueue = [];

    public function menuValidate($json_category) {

        if(isset($json_category['name']) && isset($json_category['menus'])) {

            // array_push($this -> parentsQueue, $json_category['menus']);

            if($json_category['name'] == null || $json_category['menus'] == null) {

                $this -> jsonIsValid = false; return;
            }

            foreach ($json_category['menus'] as $key => $value) {

                if(!isset($value['parent_id']) || strlen($value['cat_id']) == 0) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['name']) || strlen($value['name']) == 0) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['price'])) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['unit_id']) || $value['unit_id'] == null) {

                    $this -> jsonIsValid = false; return;

                } else if(Unit::find($value['unit_id']) == null) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['ingredients'])) {

                    $this -> jsonIsValid = false; return;
                } else {
                    // Validate ingredients ids
                    foreach ($value['ingredients'] as $key => $ingredient) {

                        if(!isset($ingredient['ingredient_id']) || $ingredient['ingredient_id'] == null) {

                            $this -> jsonIsValid = false; return;

                        } else if(Ingredient::find($ingredient['ingredient_id']) == null) {

                            // $this -> jsonIsValid = false; return;
                        }

                        if(!isset($ingredient['amount'])) {

                            $this -> jsonIsValid = false; return;
                        }

                        if(!isset($ingredient['unit_id']) || $ingredient['unit_id'] == null) {

                            $this -> jsonIsValid = false; return;

                        } else if(Unit::find($ingredient['unit_id']) == null) {

                            $this -> jsonIsValid = false; return;
                        }

                        if(!isset($ingredient['allergen']) || ($ingredient['allergen'] != 1 && $ingredient['allergen'] != 0)) {

                            $this -> jsonIsValid = false; return;
                        }
                    }
                }

                if(!isset($value['custom_ingredients'])) {

                    // $this -> jsonIsValid = false; return;
                } else {
                    // Validate custom ingredients
                    foreach ($value['custom_ingredients'] as $key => $ingredient) {

                        if(!isset($ingredient['name']) || $ingredient['name'] == null) {

                            $this -> jsonIsValid = false; return;
                        }

                        if(!isset($ingredient['amount'])) {

                            $this -> jsonIsValid = false; return;
                        }

                        if(!isset($ingredient['unit_id']) || $ingredient['unit_id'] == null) {

                            $this -> jsonIsValid = false; return;

                        } else if(Unit::find($ingredient['unit_id']) == null) {

                            $this -> jsonIsValid = false; return;
                        }

                        if(!isset($ingredient['allergen']) || ($ingredient['allergen'] != 1 && $ingredient['allergen'] != 0)) {

                            $this -> jsonIsValid = false; return;
                        }
                    }
                }

                if(!isset($value['pictures']) || count($value['pictures']) == 0) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['description'])) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['number_of_persons_that_can_eat']) || $value['number_of_persons_that_can_eat'] < 1) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['cooking_time'])) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['specialty_of_the_house']) || ($value['specialty_of_the_house'] != 0 && $value['specialty_of_the_house'] != 1)) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['specific_area_food']) || ($value['specific_area_food'] != 0 && $value['specific_area_food'] != 1)) {

                    $this -> jsonIsValid = false; return;
                }
            }
        } else if(isset($json_category['name']) && isset($json_category['categories'])) {

            foreach ($json_category['categories'] as $key => $category) {

                if(!isset($category['name']) || strlen($category['name']) == 0) {

                    $this -> jsonIsValid = false; return;
                }

                $this -> menuValidate($category);
            }
        }
    }

    public function drinkValidate($json_category) {

        if(isset($json_category['name']) && isset($json_category['menus'])) {

            // array_push($this -> parentsQueue, $json_category['menus']);

            if($json_category['name'] == null || $json_category['menus'] == null) {

                $this -> jsonIsValid = false; return;
            }

            foreach ($json_category['menus'] as $key => $value) {

                if(!isset($value['parent_id']) || strlen($value['cat_id']) == 0) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['name']) || strlen($value['name']) == 0) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['price'])) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['unit_id']) || $value['unit_id'] == null) {

                    $this -> jsonIsValid = false; return;

                } else if(Unit::find($value['unit_id']) == null) {

                    $this -> jsonIsValid = false; return;
                }

                if(!isset($value['producer_id']) || $value['producer_id'] == null) {

                    $this -> jsonIsValid = false; return;

                } else if(ProducerProfile::find($value['producer_id']) == null) {

                    $this -> jsonIsValid = false; return;
                }
            }
        } else if(isset($json_category['name']) && isset($json_category['categories'])) {

            foreach ($json_category['categories'] as $key => $category) {

                if(!isset($category['name']) || strlen($category['name']) == 0) {

                    $this -> jsonIsValid = false; return;
                }

                $this -> drinkValidate($category);
            }
        }
    }

    public function menuAdd($json_category, $parentId, $location) {

        if(isset($json_category['name']) && isset($json_category['menus'])) {

            if($json_category['name'] == null || $json_category['menus'] == null) {

                $this -> jsonIsValid = false;
            }

            foreach ($json_category['menus'] as $key => $value) {

                $menu = new Menu();

                $menu -> name = $value['name'];

                $menu -> price = $value['price'];

                $menu -> unit_id = $value['unit_id'];

                $menu -> description = $value['description'];

                $menu -> number_of_persons_that_can_eat = $value['number_of_persons_that_can_eat'];

                $menu -> cooking_time = $value['cooking_time'];

                $menu -> specialty_of_the_house = $value['specialty_of_the_house'];

                $menu -> specific_area_food = $value['specific_area_food'];

                if(isset($this -> parentsQueue[$value['parent_id'] - 1])) {

                    $search = $this -> parentsQueue[$value['parent_id'] - 1];

                    if($search != null) {

                        $menu -> menu_id = $search;
                    }
                }

                $menu -> restaurant_location_id = $location -> id;

                if(isset($value['cat_id']) && $value['cat_id'] != null) {

                    $menu -> cat_id = $value['cat_id'];
                }

                if(isset($value['parent_id']) && $value['parent_id'] != null) {

                    $menu -> parent_id = $value['parent_id'];
                }

                $menu -> save();

                foreach ($value['ingredients'] as $key => $ing_val) {

                    $db_ingredient = new Ingredient();

                    $db_ingredient -> name = $ing_val['ingredient_id'];

                    $db_ingredient -> amount = $ing_val['amount'];

                    $db_ingredient -> unit_id = $ing_val['unit_id'];

                    $db_ingredient -> allergen = $ing_val['allergen'];

                    $db_ingredient -> save();

                    $menu_has_ingredient = new MenuHasIngredient();

                    $menu_has_ingredient -> menu() -> associate($menu);

                    $menu_has_ingredient -> ingredient() -> associate($db_ingredient);

                    $menu_has_ingredient -> save();
                }

                array_push($this -> parentsQueue, $menu -> id);

                $location -> menus() -> save($menu);

                // foreach ($value['pictures'] as $key => $picture) {
                //
                //     $product_picture = new ProductPicture();
                //
                //     $product_picture -> picture = $picture;
                //
                //     $product_picture -> product() -> associate($product);
                //
                //     $product_picture -> save();
                //  }
            }

        } else if(isset($json_category['name']) && isset($json_category['categories'])) {

            foreach ($json_category['categories'] as $key => $category) {

                $menu = new Menu();

                $menu -> name = $category['name'];

                if(isset($this -> parentsQueue[$category['parent_id'] - 1])) {

                    $search = $this -> parentsQueue[$category['parent_id'] - 1];

                    if($search != null) {

                        $menu -> menu_id = $search;
                    }
                }

                if(isset($json_category['currency_id'])) {

                    $menu -> currency_id = $json_category['currency_id'];
                }

                $menu -> restaurant_location_id = $location -> id;

                if(isset($category['cat_id']) && $category['cat_id'] != null) {

                    $menu -> cat_id = $category['cat_id'];
                }

                if(isset($category['parent_id']) && $category['parent_id'] != null) {

                    $menu -> parent_id = $category['parent_id'];
                }

                $menu -> menu_id = $parentId;

                $menu -> save();

                array_push($this -> parentsQueue, $menu -> id);

                $location -> menus() -> save($menu);

                $parentId = $menu -> id;

                $this -> menuAdd($category, $parentId, $location);
            }
        }
    }

    public function drinkAdd($json_category, $parentId, $location) {

        if(isset($json_category['name']) && isset($json_category['menus'])) {

            if($json_category['name'] == null || $json_category['menus'] == null) {

                $this -> jsonIsValid = false;
            }

            foreach ($json_category['menus'] as $key => $value) {

                $menu = new Menu();

                $menu -> name = $value['name'];

                $menu -> price = $value['price'];

                $menu -> unit_id = $value['unit_id'];

                // $menu -> description = $value['description'];
                //
                // $menu -> number_of_persons_that_can_eat = $value['number_of_persons_that_can_eat'];
                //
                // $menu -> cooking_time = $value['cooking_time'];
                //
                // $menu -> specialty_of_the_house = $value['specialty_of_the_house'];
                //
                // $menu -> specific_area_food = $value['specific_area_food'];

                if(isset($this -> parentsQueue[$value['parent_id'] - 1])) {

                    $search = $this -> parentsQueue[$value['parent_id'] - 1];

                    if($search != null) {

                        $menu -> menu_id = $search;
                    }
                }

                $menu -> restaurant_location_id = $location -> id;

                if(isset($value['cat_id']) && $value['cat_id'] != null) {

                    $menu -> cat_id = $value['cat_id'];
                }

                if(isset($value['parent_id']) && $value['parent_id'] != null) {

                    $menu -> parent_id = $value['parent_id'];
                }

                $menu -> save();

                if(isset($value['ingredients']) != null) {
                    foreach ($value['ingredients'] as $key => $ing_val) {

                        $db_ingredient = new Ingredient();

                        $db_ingredient -> name = $ing_val['ingredient_id'];

                        $db_ingredient -> amount = $ing_val['amount'];

                        $db_ingredient -> unit_id = $ing_val['unit_id'];

                        $db_ingredient -> allergen = $ing_val['allergen'];

                        $db_ingredient -> save();

                        $menu_has_ingredient = new MenuHasIngredient();

                        $menu_has_ingredient -> menu() -> associate($menu);

                        $menu_has_ingredient -> ingredient() -> associate($db_ingredient);

                        $menu_has_ingredient -> save();
                    }
                }

                array_push($this -> parentsQueue, $menu -> id);

                $location -> menus() -> save($menu);
            }

        } else if(isset($json_category['name']) && isset($json_category['categories'])) {

            foreach ($json_category['categories'] as $key => $category) {

                $menu = new Menu();

                $menu -> name = $category['name'];

                if(isset($this -> parentsQueue[$category['parent_id'] - 1])) {

                    $search = $this -> parentsQueue[$category['parent_id'] - 1];

                    if($search != null) {

                        $menu -> menu_id = $search;
                    }
                }

                if(isset($json_category['currency_id'])) {

                    $menu -> currency_id = $json_category['currency_id'];
                }

                $menu -> restaurant_location_id = $location -> id;

                if(isset($category['cat_id']) && $category['cat_id'] != null) {

                    $menu -> cat_id = $category['cat_id'];
                }

                if(isset($category['parent_id']) && $category['parent_id'] != null) {

                    $menu -> parent_id = $category['parent_id'];
                }

                $menu -> menu_id = $parentId;

                $menu -> save();

                array_push($this -> parentsQueue, $menu -> id);

                $location -> menus() -> save($menu);

                $parentId = $menu -> id;

                $this -> drinkAdd($category, $parentId, $location);
            }
        }
    }

    public function add(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => true,
                'message' => __('The given location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => true,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => true,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $json_category = json_decode($request -> json_food_menu, true);

        $this -> menuValidate($json_category);

        if($this -> jsonIsValid == false) {
            return json_encode([
                'success' => false,
                'message' => __('Invalid json format!')
            ]);
        }

        $menu = new Menu();

        $menu -> name = $request -> name;

        $menu -> picture = $request -> picture;

        $menu -> save();

        $parentId = $menu -> id;

        $this -> menuAdd($json_category, $parentId, $location);

        $json_category = json_decode($request -> json_drink_menu, true);

        $this -> drinkValidate($json_category);

        if($this -> jsonIsValid == false) {
            return json_encode([
                'success' => false,
                'message' => __('Invalid json format!')
            ]);
        }

        $parentId = $menu -> id;

        $this -> parentsQueue = null;

        $this -> parentsQueue = [];

        $this -> drinkAdd($json_category, $parentId, $location);

        foreach ($location -> menus as $key => $menu) {
            // icefart
            $menu -> pictures;
        }

        return json_encode([
            'success' => true,
            'message' => __('The menu was successfully added to your restaurant location!'),
            'data' => [
                'menu' => $menu
            ]
        ]);
    }


    // public function dailyMenuDelete($location_id, $menu_id) {
    //
    //     $user = User::find($_GET['user_id']);
    //
    //     $location = RestaurantLocation::find($location_id);
    //
    //     if($location === null) {
    //         return json_encode([
    //             'success' => true,
    //             'message' => __('The given restaurant location id is invalid!')
    //         ]);
    //     }
    //
    //     if($user -> restaurantProfile === null) {
    //         return json_encode([
    //             'success' => true,
    //             'message' => __('The given user does not have an associated restaurant profile!')
    //         ]);
    //     }
    //
    //     if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
    //         return json_encode([
    //             'success' => true,
    //             'message' => __('The given user is not associated with the given restaurant profile!')
    //         ]);
    //     }
    //
    //     $menu = DailyMenuAvailability::find($menu_id);
    //
    //     if($menu  === null) {
    //         return json_encode([
    //             'success' => true,
    //             'message' => __('The daily menu id is invalid!')
    //         ]);
    //     }
    //
    //     if($menu -> restaurantLocation -> id != $location -> id) {
    //         return json_encode([
    //             'success' => true,
    //             'message' => __('The given daily menu is not associated with the given restaurant location id!')
    //         ]);
    //     }
    //
    //     $menu -> delete();
    //
    //     return json_encode([
    //         'success' => true,
    //         'message' => __('The daily menu has been deleted!')
    //     ]);
    // }

}
