<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Menu;
use Carbon\Carbon;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationPromotion;

class RestaurantLocationPromotionsController extends Controller
{
    public function add(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3',
            'begin_date' => 'required|date',
            'end_date' => 'required|date',
            'begin_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i',
            'product_id' => 'required|numeric|exists:menus,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            return json_encode([
                'success' => false,
                'message' => __('The begin time must be lower than end time!')
            ]);
        }

        $product = Menu::find($request -> product_id);

        if($product  === null) {
            return json_encode([
                'success' => false,
                'message' => __('The product id is invalid!')
            ]);
        }

        if($product -> menu_id !== null) {
            return json_encode([
                'success' => false,
                'message' => __('The given product id is not an menu or product, is a category of menu!')
            ]);
        }

        $promotion = new RestaurantLocationPromotion();

        $promotion -> name = $request -> name;

        $promotion -> description = $request -> description;

        $promotion -> begin_date = $request -> begin_date;

        $promotion -> end_date = $request -> end_date;

        $promotion -> begin_time = $request -> begin_time;

        $promotion -> end_time = $request -> end_time;

        $promotion -> product_id = $request -> product_id;

        $promotion -> restaurantLocation() -> associate($location);

        $promotion -> save();

        return json_encode([
            'success' => true,
            'message' => __('The promotion was successfully added!'),
            'data' => $promotion
        ]);
    }

    // Edit promotion
    public function save(Request $request, $location_id, $promotion_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $promotion = RestaurantLocationPromotion::find($promotion_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($promotion === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given promotion id is invalid!')
            ]);
        }

        if($promotion -> restaurant_location_id !== $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The promotion id is not associated with the restaurant location id!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3',
            'begin_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'begin_time' => 'sometimes|required|date_format:H:i',
            'end_time' => 'sometimes|required|date_format:H:i',
            'product_id' => 'sometimes|required|numeric|exists:menus,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            return json_encode([
                'success' => false,
                'message' => __('The begin time must be lower than end time!')
            ]);
        }

        $product = Menu::find($request -> product_id);

        if($product  !== null) {
            if($product -> menu_id !== null) {
                return json_encode([
                    'success' => false,
                    'message' => __('The given product id is not an menu or product, is a category of menu!')
                ]);
            }
        }

        $promotion -> name = $request -> name ?  $request -> name : $promotion -> name;

        $promotion -> description = $request -> description ? $request -> description : $promotion -> description;

        $promotion -> begin_date = $request -> begin_date ? $request -> begin_date : $promotion -> begin_date;

        $promotion -> end_date = $request -> end_date ? $request -> end_date : $promotion -> end_date;

        $promotion -> begin_time = $request -> begin_time ? $request -> begin_time : $promotion -> begin_time;

        $promotion -> end_time = $request -> end_time ? $request -> end_time : $promotion -> end_time;

        $promotion -> product_id = $request -> product_id ? $request -> product_id : $promotion -> product_id;

        $promotion -> restaurantLocation() -> associate($location);

        $promotion -> save();

        return json_encode([
            'success' => true,
            'message' => __('The promotion was successfully updated!'),
            'data' => $promotion
        ]);
    }

    public function delete(Request $request, $location_id, $promotion_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $promotion = RestaurantLocationPromotion::find($promotion_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($promotion === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given promotion id is invalid!')
            ]);
        }

        if($promotion -> restaurant_location_id !== $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The promotion id is not associated with the restaurant location id!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $promotion -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The promotion has been deleted!')
        ]);
    }

    public function getPromotions($location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $promotions = $location -> restaurantLocationPromotions;

        return json_encode([
            'success' => true,
            'message' => __('The restaurant promotions were successfully taken over!'),
            'data' => $promotions
        ]);
    }
}
