<?php

namespace App\Http\Controllers;

use App\RestaurantLocationStaff;
use App\RestaurantLocationStaffHasRole;
use App\User;
use Illuminate\Http\Request;

class RestaurantLocationStaffCloseContractsController extends Controller
{
    public function delete($location_id, $staff_id, $staff_delete_id)
    {

        $user = User::find($_GET['user_id']);

        $restaurant = $user->restaurantProfile;

        if ($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }

        $location = $restaurant->restaurantLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }
        $staff = $location->staff->find($staff_id);
        if ($staff === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given staff id is invalid!')
            ]);
        }


        $staff_has_role = RestaurantLocationStaffHasRole::where('staff_id', $staff_id)->where('role_id', 6)->first();
        if ($staff_has_role === null) {
            return json_encode([
                'success' => false,
                'message' => __('U do not have permissions to close a contract!')
            ]);
        }

        if ($staff_id === $staff_delete_id) {
            return json_encode([
                'success' => false,
                'message' => __('U can not close ur own contract!')
            ]);
        }

        $verify_if_exist = RestaurantLocationStaff::where('id', $staff_delete_id)->first();
        if ($verify_if_exist === null) {
            return json_encode([
                'success' => false,
                'message' => __('Staff does not exist!')
            ]);
        }

        $verify_if_exist->delete();

        return json_encode([
            'success' => true,
            'message' => __('Congratulations, the contact has been closed successfully!'),
            'id' => $staff_delete_id,
        ]);


    }
}
