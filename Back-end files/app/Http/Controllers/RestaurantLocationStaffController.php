<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\RestaurantLocationStaff;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationStaffController extends Controller
{
    public function add(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }

        $location = $restaurant -> restaurantLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'contract_type_id' => 'required|numeric|exists:contract_types,id',
            'salary' => 'required|numeric',
            'salary_currency_id' => 'required|numeric|exists:currencies,id',
            'start_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'job_name' => 'required|string|min:1|max:45',
            'professionist_profile_id' => 'sometimes|required|numeric|exists:profesionist_profiles,id',
            'client_profile_id' => 'sometimes|required|numeric|exists:client_profiles,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> contract_type_id == 3 || $request -> contract_type_id == 1) {
            if($request -> start_date == null || $request -> end_date == null) {
                return json_encode([
                    'success' => false,
                    'message' => __('For seasonal and definite contract type the begin and and end date are required!')
                ]);
            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The start date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The start date must be lower or equal with the end date!')
            ]);
        }

        if(($request -> professionist_profile_id == null && $request -> client_profile_id == null) || ($request -> professionist_profile_id != null && $request -> client_profile_id != null)) {
            return json_encode([
                'success' => false,
                'message' => __('The request must contain professtionist profile id or client profile id!')
            ]);
        }

        $staff = new RestaurantLocationStaff();

        $staff -> contract_type_id = $request -> contract_type_id;

        $staff -> salary = $request -> salary;

        $staff -> salary_currency_id = $request -> salary_currency_id;

        $staff -> start_date = $request -> start_date;

        $staff -> end_date = $request -> end_date;

        $staff -> job = $request -> job_name;

        $staff -> restaurant_location_id = $request -> restaurant_location_id ? $request -> restaurant_location_id : $staff -> restaurant_location_id;

        $staff -> profesionist_profile_id = $request -> professionist_profile_id ? $request -> professionist_profile_id : $staff -> professionist_profile_id;

        // $staff -> client_profile_id = $request -> client_profile_id ? $staff -> client_profile_id = $request -> client_profile_id : $staff -> client_profile_id;

        $staff -> location() -> associate($location);

        $staff -> save();

        $staff -> contractType;

        $staff -> currency;

        $staff -> location;

        $staff -> clientProfile;

        $staff -> profesionistProfile;

        return json_encode([
            'success' => true,
            'message' => __('The staff has been successfully added!'),
            'data' => [
                'staff' => $staff
            ]
        ]);
    }

    public function save(Request $request, $location_id, $staff_id) {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }

        $location = $restaurant -> restaurantLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        $staff = $location -> staff -> find($staff_id);

        if($staff === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given staff id is invalid!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'contract_type_id' => 'sometimes|required|numeric|exists:contract_types,id',
            'salary' => 'sometimes|required|numeric',
            'salary_currency_id' => 'sometimes|required|numeric|exists:currencies,id',
            'start_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'job_name' => 'sometimes|required|string|min:1|max:45'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> contract_type_id == 3 || $request -> contract_type_id == 1) {
            if($request -> start_date == null || $request -> end_date == null) {
                return json_encode([
                    'success' => false,
                    'message' => __('For seasonal and definite contract type the begin and and end date are required!')
                ]);
            }
        }

        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The start date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> start_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The start date must be lower or equal with the end date!')
            ]);
        }

        $staff -> contract_type_id = $request -> contract_type_id ? $request -> contract_type_id : $staff -> contract_type_id;

        $staff -> salary = $request -> salary ? $request -> salary : $staff -> salary;

        $staff -> salary_currency_id = $request -> salary_currency_id ? $request -> salary_currency_id : $staff -> salary_currency_id;

        if($request -> contract_type_id == 2) {

            $staff -> start_date = null;

            $staff -> end_date = null;

        } else {

            $staff -> start_date = $request -> start_date ? $request -> start_date : $staff -> start_date;

            $staff -> end_date = $request -> end_date ? $request -> end_date : $staff -> end_date;
        }

        $staff -> job = $request -> job_name ? $request -> job_name : $staff -> job_name;

        $staff -> save();

        $staff -> contractType;

        $staff -> currency;

        $staff -> restaurantLocation;

        $staff -> clientProfile;

        $staff -> profesionistProfile;

        return json_encode([
            'success' => true,
            'message' => __('The staff has been successfully updated!'),
            'data' => [
                'staff' => $staff
            ]
        ]);
    }

    public function delete(Request $request, $location_id, $staff_id) {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }

        $location = $restaurant -> restaurantLocations -> find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        $staff = $location -> staff -> find($staff_id);

        if($staff === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given staff id is invalid!')
            ]);
        }

        $staff -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The staff has been deleted!')
        ]);
    }
}
