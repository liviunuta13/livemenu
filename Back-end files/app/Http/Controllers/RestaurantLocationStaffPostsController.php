<?php

namespace App\Http\Controllers;

use App\Post;
use App\RestaurantLocationPicture;
use App\RestaurantLocationStaffHasRole;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
class RestaurantLocationStaffPostsController extends Controller
{
    public function add(Request $request, $location_id, $staff_id)
    {
        $user = User::find($_GET['user_id']);

        $restaurant = $user->restaurantProfile;

        if ($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }

        $location = $restaurant->restaurantLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }
        $staff = $location->staff->find($staff_id);
        if ($staff === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given staff id is invalid!')
            ]);
        }

        $staff_has_role = RestaurantLocationStaffHasRole::where('staff_id', $staff_id)->where('role_id', 3)->first();
        if ($staff_has_role === null) {
            return json_encode([
                'success' => false,
                'message' => __('You do not have permissions to add a post!')
            ]);
        }

        $validator = Validator::make($request->all(), ['description' => 'required|string|min:3',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',]);

        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $staff_post_description = new Post();

        $staff_post_description->description = $request->description;

        $staff_post_description->from_restaurant_location_id = $location_id;

        $staff_post_description->save();

        if ($request->pictures) {

            foreach ($request->pictures as $key => $picture) {
                {
                    $post_picture = new RestaurantLocationPicture();

                    $post_picture->picture = $picture;

                    $post_picture->restaurant_location_id = $location_id;

                    $post_picture->save();
                }
            }
        }

        return json_encode([
            'success' => true,
            'message' => __('The post has been successfully added!'),
            'data' => [
                'post' => $staff_post_description
            ]
        ]);
    }


}
