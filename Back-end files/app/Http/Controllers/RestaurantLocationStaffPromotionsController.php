<?php

namespace App\Http\Controllers;

use App\Product;
use App\RestaurantLocationPromotion;
use App\RestaurantLocationStaffHasRole;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class RestaurantLocationStaffPromotionsController extends Controller
{
    public function add(Request $request, $location_id, $staff_id)
    {

        $user = User::find($_GET['user_id']);

        $restaurant = $user->restaurantProfile;

        if ($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }

        $location = $restaurant->restaurantLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }
        $staff = $location->staff->find($staff_id);
        if ($staff === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given staff id is invalid!')
            ]);
        }

        $staff_has_role = RestaurantLocationStaffHasRole::where('staff_id', $staff_id)->where('role_id', 4)->first();
        if ($staff_has_role === null) {
            return json_encode([
                'success' => false,
                'message' => __('You do not have permissions to add a promotion!')
            ]);
        }

        $verify_if_product_exists = Product::where('id', $request->product_id)->first();
        if ($verify_if_product_exists === null) {
            return json_encode([
                'success' => false,
                'message' => __('The product id entered is invalid!')
            ]);
        } elseif ($verify_if_product_exists->producer_location_id != $location_id) {
            return json_encode([
                'success' => false,
                'message' => __("The product id's location differ!")
            ]);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:45',
            'description' => 'required|string',
            'begin_date' => 'required|date',
            'end_date' => 'required|date',
            'begin_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i',
            'product_id' => 'required|integer',
        ]);
        $current_date = Carbon::now() -> format('Y-m-d');

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') < $current_date) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be greater or equal with current date!')
            ]);
        }

        if(Carbon::parse($request -> begin_date) -> format('Y-m-d') > Carbon::parse($request -> end_date) -> format('Y-m-d')) {
            return json_encode([
                'success' => false,
                'message' => __('The begin date must be lower or equal with the end date!')
            ]);
        }

        if(Carbon::parse($request -> begin_time) >= Carbon::parse($request -> end_time)) {
            return json_encode([
                'success' => false,
                'message' => __('The begin time must be lower than end time!')
            ]);
        }


        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $promotion = new RestaurantLocationPromotion();

        $promotion->name = $request->name;
        $promotion->description = $request->description;
        $promotion->begin_date = $request->begin_date;
        $promotion->end_date = $request->end_date;
        $promotion->begin_time = $request->begin_time;
        $promotion->end_time = $request->end_time;
        $promotion->restaurant_location_id = $location_id;
        $promotion->product_id = $request->product_id;

        return json_encode([
            'success' => true,
            'message' => __('The promotion has been successfully added!'),
            'data' => [
                'promotion' => $promotion
            ]
        ]);
    }
}
