<?php

namespace App\Http\Controllers;

use Storage;
use App\User;
use App\Hotel;
use App\Rating;
use App\ProgramsCondition;
use App\RestaurantProfile;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationProgram;
use App\RestaurantLocationPicture;
use App\RestaurantLocationFacility;
use App\RestaurantLocationHasFacility;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationsController extends Controller
{
    public function add(Request $request) {
        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:5|max:512',
            'city_id' => 'required|numeric|exists:cities,id',
            'county_id' => 'required|numeric|exists:counties,id',
            'country_id' => 'required|numeric|exists:countries,id',
            'street' => 'required|string|min:2|max:100',
            'number' => 'required|numeric',
            'postal_code' => 'sometimes|required|string|min:1|max:45',
            'latitude' => 'required',
            'longitude' => 'required',
            'phone_number' => 'sometimes|required|string|min:1|max:45',
            'fixed_phone_number' => 'sometimes|required|string|min:1|max:45',
            'contact_person_name' => 'sometimes|required|string|min:1|max:45',
            'owns_event_room' => 'required|numeric',
            'facilities_id' => 'required',
            'facilities_id.*' => 'required|numeric|exists:restaurant_location_facilities,id',
            'locality_specific' => 'required|string|min:1|max:45',
            'music_type_id' => 'required|exists:music_types,id',
            'live_music' => 'boolean',
            'tables_number' => 'required|numeric',
            'bathrooms_number' => 'required|numeric',
            'locality_surface' => 'required|numeric',
            'framing' => 'required|string|min:1|max:45',
            'number_of_seats' => 'required|numeric',
            'michelin_stars' => 'required|numeric',
            'catering' => 'required|boolean',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',
            'program_names' => 'sometimes|required',
            'program_names.*' => 'required|string|min:3|max:45',
            'program_types' => 'sometimes|required',
            'program_types.*' => 'required|string|min:3|max:45',
            'start_dates' => 'sometimes|required',
            'start_dates.*' => 'required|date',
            'end_dates' => 'sometimes|required',
            'end_dates.*' => 'required|date',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        if($restaurant == null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant!')
            ]);
        }

        $restaurant_location = new RestaurantLocation();

        $restaurant_location -> name = $request -> name;

        $restaurant_location -> description = $request -> description;

        $restaurant_location -> city_id = $request -> city_id;

        $restaurant_location -> county_id = $request -> county_id;

        $restaurant_location -> country_id = $request -> country_id;

        $restaurant_location -> street = $request -> street;

        $restaurant_location -> number = $request -> number;

        $restaurant_location -> postal_code = $request -> postal_code;

        $restaurant_location -> latitude = $request -> latitude;

        $restaurant_location -> longitude = $request -> longitude;

        $restaurant_location -> phone_number = $request -> phone_number;

        $restaurant_location -> fixed_phone_number = $request -> fixed_phone_number;

        $restaurant_location -> contact_person_name = $request -> contact_person_name;

        $restaurant_location -> locality_specific = $request -> locality_specific;

        $restaurant_location -> music_type_id = $request -> music_type_id;

        $restaurant_location -> live_music = $request -> live_music;

        $restaurant_location -> tables_number = $request -> tables_number;

        $restaurant_location -> bathrooms_number = $request -> bathrooms_number;

        $restaurant_location -> locality_surface = $request -> locality_surface;

        $restaurant_location -> framing = $request -> framing;

        $restaurant_location -> number_of_seats = $request -> number_of_seats;

        $restaurant_location -> michelin_stars = $request -> michelin_stars;

        $restaurant_location -> catering = $request -> catering;

        $restaurant_location -> phone_number = $request -> phone_number;

        if($request -> owns_event_room == 0 || $request -> owns_event_room == 1 || $request -> owns_event_room == 2) {

            $restaurant_location -> owns_event_room = $request -> owns_event_room;
        } else {
            return json_encode([
                'success' => false,
                'message' => __('The event room proprety is invalid!')
            ]);
        }
        $restaurant_location -> restaurantProfile() -> associate($restaurant);
        $restaurant_location -> save();

        foreach ($request -> facilities_id as $key => $facility_id) {

            $restaurantt_has_facility = new RestaurantLocationHasFacility();

            $restaurantt_has_facility -> restaurantLocation() -> associate($restaurant_location);

            $restaurantt_has_facility -> restaurant_location_facility_id = $facility_id;

            $restaurantt_has_facility -> save();
        }
        $conditions = [];
        foreach($request -> program_names as $key => $program_name) {

            $program = new ProgramsCondition();

            $program -> name = $program_name;

            $program -> type = $request -> program_types[$key];

            $program -> start_date = $request -> start_dates[$key];

            $program -> end_date = $request -> end_dates[$key];

            $program -> save();

            $conditions[$key] = $program -> id;
        }
        if(count($conditions)) {
            $restaurant_location_programs = new RestaurantLocationProgram();

            $restaurant_location_programs -> conditions = json_encode($conditions);

            $restaurant_location_programs -> restaurantLocation() -> associate($restaurant_location);

            $restaurant_location_programs -> save();
        }

        if($request -> pictures) {
            foreach ($request -> pictures as $key => $picture) {
                {
                    $restaurant_has_picture = new RestaurantLocationPicture();

                    $restaurant_has_picture -> restaurantLocation() -> associate($restaurant_location);

                    $restaurant_has_picture -> picture = $picture;

                    $restaurant_has_picture -> save();
                }
            }
        } else {
            return json_encode([
                'success' => false,
                'message' => __('There is a problem to save the pictures!')
            ]);
        }
        $restaurant_location -> city;

        $restaurant_location -> county;

        $restaurant_location -> country;

        $restaurant_location -> musicType;

        return json_encode([
            'success' => true,
            'message' => __('The restaurant location was successfully added!'),
            'data' => $restaurant_location
        ]);
    }

    // Edit restaurant location data
    public function save(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        $restaurant_location = RestaurantLocation::find($location_id);

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The restaurant is invalid!')
            ]);
        }

        if($restaurant_location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The restaurant location is invalid!')
            ]);
        }

        if($restaurant_location -> restaurant_profile_id != $restaurant -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The restaurant location is associated with another user restaurant profile!')
            ]);
        }

        if($restaurant_location == null) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }
        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:5|max:512',
            'city_id' => 'sometimes|required|numeric|exists:cities,id',
            'county_id' => 'sometimes|required|numeric|exists:counties,id',
            'country_id' => 'sometimes|required|numeric|exists:countries,id',
            'street' => 'sometimes|required|string|min:2|max:100',
            'number' => 'sometimes|required|numeric',
            'postal_code' => 'sometimes|required|string|min:1|max:45',
            'latitude' => 'sometimes|required',
            'longitude' => 'sometimes|required',
            'phone_number' => 'sometimes|required|string|min:1|max:45',
            'fixed_phone_number' => 'sometimes|required|string|min:1|max:45',
            'contact_person_name' => 'sometimes|required|string|min:1|max:45',
            'owns_event_room' => 'sometimes|requied|numeric',
            'facilities_id' => 'sometimes|required',
            'facilities_id.*' => 'sometimes|required|numeric|exists:restaurant_location_facilities,id',
            'delete_facilities' => 'sometimes|required',
            'delete_facilities.*' => 'required|numeric|exists:restaurant_location_has_facility,id',
            'locality_specific' => 'sometimes|required|string|min:1|max:45',
            'music_type_id' => 'sometimes|required|exists:music_types,id',
            'live_music' => 'sometimes|boolean',
            'tables_number' => 'sometimes|required|numeric',
            'bathrooms_number' => 'sometimes|required|numeric',
            'locality_surface' => 'sometimes|required|numeric',
            'framing' => 'sometimes|required|string|min:1|max:45',
            'number_of_seats' => 'sometimes|required|numeric',
            'michelin_stars' => 'sometimes|required|numeric',
            'catering' => 'sometimes|required|boolean',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'sometimes|required|string',
            'program_names' => 'sometimes|required',
            'program_names.*' => 'sometimes|required|string|min:3|max:45',
            'program_types' => 'sometimes|required',
            'program_types.*' => 'sometimes|required|string|min:3|max:45',
            'start_dates' => 'sometimes|required',
            'start_dates.*' => 'sometimes|required|date',
            'end_dates' => 'sometimes|required',
            'end_dates.*' => 'sometimes|required|date',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $restaurant_location -> name = $request -> name ? $request -> name : $restaurant_location -> name;

        $restaurant_location -> description = $request -> description ? $request -> description : $restaurant_location -> description;

        $restaurant_location -> city_id = $request -> city_id ? $request -> city_id : $restaurant_location -> city_id;

        $restaurant_location -> county_id = $request -> county_id ? $request -> county_id : $restaurant_location -> county_id;

        $restaurant_location -> country_id = $request -> country_id ? $request -> country_id : $restaurant_location -> country_id;

        $restaurant_location -> street = $request -> street ? $request -> street : $restaurant_location -> street;

        $restaurant_location -> number = $request -> number ? $request -> number : $restaurant_location -> number;

        $restaurant_location -> postal_code = $request -> postal_code ? $request -> postal_code : $restaurant_location -> postal_code;

        $restaurant_location -> latitude = $request -> latitude ? $request -> latitude : $restaurant_location -> latitude;

        $restaurant_location -> longitude = $request -> longitude ? $request -> longitude : $restaurant_location -> longitude;

        $restaurant_location -> phone_number = $request -> phone_number ? $request -> phone_number : $restaurant_location -> phone_number;

        $restaurant_location -> fixed_phone_number = $request -> fixed_phone_number ? $request -> fixed_phone_number : $restaurant_location -> fixed_phone_number;

        $restaurant_location -> contact_person_name = $request -> contact_person_name ? $request -> contact_person_name : $restaurant_location -> contact_person_name;

        $restaurant_location -> locality_specific = $request -> locality_specific ? $request -> locality_specific : $restaurant_location -> locality_specific;

        $restaurant_location -> music_type_id = $request -> music_type_id ? $request -> music_type_id : $restaurant_location -> music_type_id;

        $restaurant_location -> live_music = $request -> live_music ? $request -> live_music : $restaurant_location -> live_music;

        $restaurant_location -> tables_number = $request -> tables_number ? $request -> tables_number : $restaurant_location -> tables_number;

        $restaurant_location -> bathrooms_number = $request -> bathrooms_number ? $request -> bathrooms_number : $restaurant_location -> bathrooms_number;

        $restaurant_location -> locality_surface = $request -> locality_surface ? $request -> locality_surface : $restaurant_location -> locality_surface;

        $restaurant_location -> framing = $request -> framing ? $request -> framing : $restaurant_location -> framing;

        $restaurant_location -> number_of_seats = $request -> number_of_seats ? $request -> number_of_seats : $restaurant_location -> number_of_seats;

        $restaurant_location -> michelin_stars = $request -> michelin_stars ? $request -> michelin_stars : $restaurant_location -> michelin_stars;

        $restaurant_location -> catering = $request -> catering ? $request -> catering : $restaurant_location -> catering;

        $restaurant_location -> phone_number = $request -> phone_number ? $request -> phone_number : $restaurant_location -> phone_number;

        if($request -> owns_event_room == 0 || $request -> owns_event_room == 1 || $request -> owns_event_room == 2) {

            $restaurant_location -> owns_event_room = $request -> owns_event_room ? $request -> owns_event_room : $restaurant_location -> owns_event_room;
        } else {
            return json_encode([
                'success' => false,
                'message' => __('The event room proprety is invalid!')
            ]);
        }
        $restaurant_location -> save();

        if($request -> facilities_id != null && count($request -> facilities_id) > 0) {

            foreach ($request -> facilities_id as $key => $facility_id) {

                if(!RestaurantLocationHasFacility::where('restaurant_location_id', $restaurant_location -> id) -> where('restaurant_location_facility_id', $facility_id) -> first()) {

                    $restaurantt_has_facility = new RestaurantLocationHasFacility();

                    $restaurantt_has_facility -> restaurantLocation() -> associate($restaurant_location);

                    $restaurantt_has_facility -> restaurant_location_facility_id = $facility_id;

                    $restaurantt_has_facility -> save();
                }
            }
        }

        if(isset($request -> delete_facilities)) {

            foreach ($request -> delete_facilities as $key => $facility_id) {

                $restaurant_location_has_facility = RestaurantLocationHasFacility::find($facility_id);

                if($restaurant_location_has_facility && $restaurant_location_has_facility -> restaurant_location_id == $restaurant_location -> id) {

                    $restaurant_location_has_facility -> forceDelete();
                }
            }
        }

        $conditions = [];

        if(count($request -> program_names)) {

            foreach($request -> program_names as $key => $program_name) {

                $program = new ProgramsCondition();

                $program -> name = $program_name;

                $program -> type = $request -> program_types[$key];

                $program -> start_date = $request -> start_dates[$key];

                $program -> end_date = $request -> end_dates[$key];

                $program -> save();

                $conditions[$key] = $program -> id;
            }
        }
        if(count($conditions)) {

            $restaurant_location_programs = new RestaurantLocationProgram();

            $restaurant_location_programs -> conditions = json_encode($conditions);

            $restaurant_location_programs -> restaurantLocation() -> associate($restaurant_location);

            $restaurant_location_programs -> save();
        }

        if($request -> pictures) {
            foreach ($request -> pictures as $key => $picture) {
                {
                    $restaurant_has_picture = new RestaurantLocationPicture();

                    $restaurant_has_picture -> restaurantLocation() -> associate($restaurant_location);

                    $restaurant_has_picture -> picture = $picture;

                    $restaurant_has_picture -> save();
                }
            }
        }
        $restaurant_location -> city;

        $restaurant_location -> county;

        $restaurant_location -> country;

        $restaurant_location -> musicType;

        foreach ($restaurant_location -> restaurantLocationHasFacilities as $key => $value) {

            $value -> restaurantLocationFacility;
        }

        $cont = 0;

        $rating_val = 0;

        foreach ($restaurant_location -> ratings as $key => $rating) {

            $rating_val += $rating -> stars;

            $cont ++;
        }

        if($cont == 0) {

            $restaurant_location -> rating = null;
        } else {

            $restaurant_location -> rating = $rating_val / $cont;
        }

        return json_encode([
            'success' => true,
            'message' => __('The restaurant location was successfully updated!'),
            'data' => $restaurant_location
        ]);
    }

    public function delete(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        $restaurant_location = RestaurantLocation::find($location_id);

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The restaurant is invalid!')
            ]);
        }

        if($restaurant_location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The restaurant location is invalid!')
            ]);
        }

        if($restaurant_location -> restaurant_profile_id != $restaurant -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The restaurant location is associated with another user restaurant profile!')
            ]);
        }

        if(count($restaurant_location) == 0) {
            return json_encode([
                'success' => false,
                'message' => __('The given location is invalid!')
            ]);
        }

        $restaurant_location -> buyedSponsorships() -> delete();

        $restaurant_location -> buyedSponsorshipsFor() -> delete();

        $restaurant_location -> dailyMenuAvailabilities() -> delete();

        $restaurant_location -> eventRooms() -> delete();

        $restaurant_location -> menuses() -> delete();

        $restaurant_location -> postsFrom() -> delete();

        $restaurant_location -> postsTo() -> delete();

        $restaurant_location -> ratings() -> delete();

        $restaurant_location -> restaurantLocationEvents() -> delete();

        $restaurant_location -> restaurantLocationHasFacilities() -> delete();

        $restaurant_location -> restaurantLocationHasPosts() -> delete();

        $restaurant_location -> restaurantLocationUtilities() -> delete();

        $restaurant_location -> restaurantLocationPictures() -> delete();

        $restaurant_location -> restaurantLocationPrograms() -> delete();

        $restaurant_location -> restaurantLocationPromotions() -> delete();

        $restaurant_location -> restaurantStaffs() -> delete();

        $restaurant_location -> sponsorshipsRequests() -> delete();

        $restaurant_location -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The restaurant location and all data associated is deleted!')
        ]);
    }

    public function deletePicture(Request $request, $location_id, $picture_id) {

        $picture = RestaurantLocationPicture::find($picture_id);

        $restaurant_location = RestaurantLocation::find($location_id);

        if($picture === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given picture id is invalid!')
            ]);
        }

        if($restaurant_location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($picture -> restaurant_location_id != $restaurant_location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given picture is not associated with given restaurant location!')
            ]);
        }

        $user = User::find($_GET['user_id']);

        $restaurant_profile = RestaurantProfile::find($restaurant_location -> restaurant_profile_id);

        if($user -> restaurantProfile == null || ($restaurant_profile -> id !== $user -> restaurantProfile -> id)) {
            return json_encode([
                'success' => false,
                'message' => __('The picture is not asociated with right user restaurant location!')
            ]);
        }

        $picture -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The picture was successfully deleted!')
        ]);
    }

    public function getFacilities() {

        $facilities = RestaurantLocationFacility::all();

        return json_encode([
            'success' => true,
            'message' => __('The restaurant facilities have been successfully taken over!'),
            'data' => $facilities
        ]);
    }

    public function readLocation($location_id) {

        $location = RestaurantLocation::find($location_id);

        if($location === null) {

            return json_encode([
                'success' => false,
                'message' => __('The given location id is invalid!')
            ]);
        }

        $location -> city;

        $location -> county;

        $location -> country;

        $location -> musicType;

        $location -> restaurantProfile;

        $location -> buyedSponsorships;

        $location -> buyedSponsorshipsFor;

        $location -> dailyMenuAvailabilities;

        $location -> eventRooms;

        $location -> menus;

        $location -> postsFrom;

        $location -> postsTo;

        $location -> ratings;

        $location -> restaurantLocationEvents;

        $location -> restaurantLocationHasPosts;

        $location -> restaurantLocationUtilities;

        $location -> restaurantLocationPictures;

        $location -> restaurantLocationPromotions;

        $location -> restaurantLocationStaff;

        $location -> sponsorshipsRequests;

        $location -> hotels;

        foreach ($location -> restaurantLocationHasFacilities as $key => $value) {

            $value -> restaurantLocationFacility;
        }

        $customKey = 0;

        $cond_array = [];

        foreach ($location -> restaurantLocationPrograms as $key => $value) {

            $vars = str_replace(']', '', $value -> conditions);

            $vars = str_replace('[', '', $vars);

            $vars = explode(', ', $vars);

            foreach ($vars as $key => $cond) {

                $find_cond = ProgramsCondition::find($cond);

                if($find_cond) {

                    $cond_array[$customKey ++] = $find_cond;
                }
            }
        }

        $cont = 0;

        $rating_val = 0;

        foreach ($location -> ratings as $key => $rating) {

            $rating_val += $rating -> stars;

            $cont ++;
        }

        if($cont == 0) {

            $location -> rating = null;
        } else {

            $location -> rating = $rating_val / $cont;
        }

        return json_encode([
            'success' => true,
            'message' => __('The restaurant location and associated data were successfully taken over!'),
            'data' => [
                'restaurant_location' => $location,
                'restaurant_location_programs_conditions' => $cond_array
            ]
        ]);
    }
}
