<?php

namespace App\Http\Controllers;

use App\User;
use App\EventRoom;
use App\EventRoomPicture;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\EventRoomHasFacility;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationsEventRoomsController extends Controller
{
    public function add(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'owns_event_room' => 'required|boolean',
            'description' => 'required|string|min:3',
            'rooms_number' => 'required|numeric',
            'tables_number' => 'required|numeric',
            'persons_number' => 'required|numeric',
            'surface' => 'required|numeric',
            'surface_unit_id' => 'required|numeric|exists:units,id',
            'facilities' => 'required',
            'facilities.*' => 'required|numeric|exists:event_room_facilities,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $event_room = new EventRoom();

        $event_room -> owns_event_room = $request -> owns_event_room;

        $event_room -> descripton = $request -> description;

        $event_room -> rooms_number = $request -> rooms_number;

        $event_room -> tables_number = $request -> tables_number;

        $event_room -> persons_number = $request -> persons_number;

        $event_room -> surface = $request -> surface;

        $event_room -> surface_unit_id = $request -> surface_unit_id;

        $event_room -> restaurantLocation() -> associate($location);

        $event_room -> restaurant_location_profile_id = $location -> restaurant_profile_id;

        $event_room -> save();

        foreach ($request -> facilities as $key => $facility) {

            $event_room_has_facility = new EventRoomHasFacility();

            $event_room_has_facility -> eventRoom() -> associate($event_room);

            $event_room_has_facility -> facility_id = $facility;

            $event_room_has_facility -> save();
        }

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $room_picture = new EventRoomPicture();

                    $room_picture -> eventRoom() -> associate($event_room);

                    $room_picture -> picture = $picture;

                    $room_picture -> save();
                }
            }
        }

        return json_encode([
            'success' => true,
            'message' => __('The event room was successfully added!'),
            'data' => $event_room
        ]);
    }

    // Edit event room
    public function save(Request $request, $location_id, $eventroom_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $event_room = EventRoom::find($eventroom_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($event_room === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given event room id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($event_room -> restaurant_location_id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given event room is not associated with the given restaurant location!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'owns_event_room' => 'sometimes|required|boolean',
            'description' => 'sometimes|required|string|min:3',
            'rooms_number' => 'sometimes|required|numeric',
            'tables_number' => 'sometimes|required|numeric',
            'persons_number' => 'sometimes|required|numeric',
            'surface' => 'sometimes|required|numeric',
            'surface_unit_id' => 'sometimes|required|numeric|exists:units,id',
            'facilities' => 'sometimes|required',
            'facilities.*' => 'sometimes|required|numeric|exists:event_room_has_facility,id',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'sometimes|required|string',
            'delete_facilities' => 'sometimes|required',
            'delete_facilities.*' => 'required|numeric|exists:event_room_facilities,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $event_room -> owns_event_room = $request -> owns_event_room ? $request -> owns_event_room : $request -> owns_event_room;

        $event_room -> descripton = $request -> description ? $request -> description : $event_room -> description;

        $event_room -> rooms_number = $request -> rooms_number ? $request -> rooms_number : $event_room -> rooms_number;

        $event_room -> tables_number = $request -> tables_number ? $request -> tables_number : $event_room -> tables_number;

        $event_room -> persons_number = $request -> persons_number ? $request -> persons_number : $event_room -> persons_number;

        $event_room -> surface = $request -> surface ? $request -> surface : $event_room -> surface;

        $event_room -> surface_unit_id = $request -> surface_unit_id ? $request -> surface_unit_id : $event_room -> surface_unit_id;

        $event_room -> save();

        if($request -> facilities) {

            foreach ($request -> facilities as $key => $facility) {

                if(!(EventRoomHasFacility::where('event_room_id', $event_room -> id) -> where('facility_id', $facility) -> first())) {

                    $event_room_has_facility = new EventRoomHasFacility();

                    $event_room_has_facility -> eventRoom() -> associate($event_room);

                    $event_room_has_facility -> facility_id = $facility;

                    $event_room_has_facility -> save();
                }
            }
        }

        if(isset($request -> delete_facilities)) {

            foreach ($request -> delete_facilities as $key => $facility_id) {

                $room_has_facility = EventRoomHasFacility::find($facility_id);

                if($room_has_facility && $room_has_facility -> event_room_id == $event_room -> id) {

                    $room_has_facility -> forceDelete();
                }
            }
        }

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
            {
                $room_picture = new EventRoomPicture();

                $room_picture -> eventRoom() -> associate($event_room);

                $room_picture -> picture = $picture;

                $room_picture -> save();

                }
            }
        }

        $event_room -> unit;

        $event_room -> roomPictures;

        foreach ($event_room -> eventRoomHasFacilities as $key => $facility) {

            $facility -> eventRoomFacility;
        }

        foreach ($event_room -> eventRoomsHasMenus as $key => $menu) {

            $menu -> menu;
        }

        return json_encode([
            'success' => true,
            'message' => __('The event room was successfully added!'),
            'data' => $event_room
        ]);
    }

    public function delete(Request $request, $location_id, $eventroom_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $event_room = EventRoom::find($eventroom_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($event_room === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given event room id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($event_room -> restaurant_location_id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given event room is not associated with the given restaurant location!')
            ]);
        }

        foreach ($event_room -> roomPictures as $key => $picture) {

            $picture -> delete();
        }

        $event_room -> roomPictures() -> delete();

        $event_room -> eventRoomHasFacilities() -> delete();

        $event_room -> eventRoomsHasMenus() -> delete();

        $event_room -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The event room and all data associated has been deleted!')
        ]);
    }
}
