<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\PostPicture;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RestaurantLocationsPostsController extends Controller
{
    public function add(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'description' => 'required|string|min:3',
            'pictures' => 'required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $post = new Post();

        $post -> description = $request -> description;

        $post -> restaurantLocationFrom() -> associate($location);

        $post -> save();

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $post_picture = new PostPicture();

                    $post_picture -> post() -> associate($post);

                    $post_picture -> picture = $picture;

                    $post_picture -> save();
                }
            }
        }

        $post -> postPictures;

        return json_encode([
            'success' => true,
            'message' => __('The post has been successfully added!'),
            'data' => $post
        ]);
    }

    // Edit post of restaurant locations
    public function save(Request $request, $location_id, $post_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $post = Post::find($post_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($post === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given post id is invalid!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($post -> restaurantLocationFrom -> id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given post is not associated with the given restaurant location!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'description' => 'sometimes|required|string|min:3',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|file|mimes:jpeg,jpg,png,gif'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $post -> description = $request -> description ? $request -> description : $post -> description;

        $post -> save();

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $post_picture = new PostPicture();

                    $post_picture -> post() -> associate($post);

                    $post_picture -> picture = $picture;

                    $post_picture -> save();
                }
            }
        }

        $post -> postPictures;

        return json_encode([
            'success' => true,
            'message' => __('The post has been successfully updated!'),
            'data' => $post
        ]);
    }

    public function delete(Request $request, $location_id, $post_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $post = Post::find($post_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($post === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given post id is invalid!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($post -> restaurantLocationFrom -> id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given post is not associated with the given restaurant location!')
            ]);
        }

        foreach ($post -> postPictures as $key => $picture) {

            $picture -> delete();
        }

        $post -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The post and associated pictures has been deleted!')
        ]);
    }

    public function deletePicture(Request $request, $location_id, $post_id, $picture_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        $post = Post::find($post_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($post === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given post id is invalid!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        if($post -> restaurantLocationFrom -> id != $location -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given post is not associated with the given restaurant location!')
            ]);
        }

        $post_picture = $post -> postPictures -> find($picture_id);

        if($post_picture === null) {
            return json_encode([
                'success' => false,
                'message' => __('The post\'s picture is invalid!')
            ]);
        }

        $post_picture -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The post\'s picture has been deleted!')
        ]);
    }
}
