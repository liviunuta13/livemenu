<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\PostPicture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RestaurantPostsController extends Controller
{
    public function add(Request $request) {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant id is invalid!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'description' => 'required|string|min:3',
            'pictures' => 'required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $post = new Post();

        $post -> description = $request -> description;

        $post -> restaurantProfileFrom() -> associate($restaurant);

        $post -> save();

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $post_picture = new PostPicture();

                    $post_picture -> post() -> associate($post);

                    $post_picture -> picture = $picture;

                    $post_picture -> save();
                }
            }
        }

        $post -> postPictures;

        return json_encode([
            'success' => true,
            'message' => __('The post has been successfully added!'),
            'data' => $post
        ]);
    }

    // Edit post of restaurant locations
    public function save(Request $request, $post_id) {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        $post = Post::find($post_id);

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant id is invalid!')
            ]);
        }

        if($post === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given post id is invalid!')
            ]);
        }

        if(!isset($post -> restaurantProfileFrom)) {
            return json_encode([
                'success' => false,
                'message' => __('The given post id is invalid!')
            ]);
        }

        if($post -> restaurantProfileFrom -> id != $restaurant -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given post is not associated with the given restaurant!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'description' => 'sometimes|required|string|min:3',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $post -> description = $request -> description ? $request -> description : $post -> description;

        $post -> save();

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $post_picture = new PostPicture();

                    $post_picture -> post() -> associate($post);

                    $post_picture -> picture = $picture;

                    $post_picture -> save();
                }
            }
        }

        $post -> postPictures;

        return json_encode([
            'success' => true,
            'message' => __('The post has been successfully updated!'),
            'data' => $post
        ]);
    }

    public function delete(Request $request, $post_id) {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        $post = Post::find($post_id);

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant id is invalid!')
            ]);
        }

        if($post === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given post id is invalid!')
            ]);
        }

        if(!isset($post -> restaurantProfileFrom)) {
            return json_encode([
                'success' => false,
                'message' => __('The given post id is invalid!')
            ]);
        }

        if($post -> restaurantProfileFrom -> id != $restaurant -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given post is not associated with the given restaurant!')
            ]);
        }

        foreach ($post -> postPictures as $key => $picture) {

            $picture -> delete();
        }

        $post -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The post and associated pictures has been deleted!')
        ]);
    }

    public function deletePicture(Request $request, $post_id, $picture_id) {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        $post = Post::find($post_id);

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($post === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given post id is invalid!')
            ]);
        }

        if(!isset($post -> restaurantProfileFrom)) {
            return json_encode([
                'success' => false,
                'message' => __('The given post id is invalid!')
            ]);
        }

        if($post -> restaurantProfileFrom -> id != $restaurant -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given post is not associated with the given restaurant!')
            ]);
        }

        $post_picture = $post -> postPictures -> find($picture_id);

        if($post_picture === null) {
            return json_encode([
                'success' => false,
                'message' => __('The post\'s picture is invalid!')
            ]);
        }

        $post_picture -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The post\'s picture has been deleted!')
        ]);
    }
}
