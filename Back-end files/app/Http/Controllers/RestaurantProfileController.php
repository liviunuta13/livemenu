<?php

namespace App\Http\Controllers;

use App\User;
use App\RestaurantPicture;
use App\RestaurantProfile;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use App\RestaurantLocationPicture;
use Illuminate\Support\Facades\Validator;

class RestaurantProfileController extends Controller
{
    // Edit restaurant profile data
    public function save(Request $request) {

        $user = User::find($_GET['user_id']);

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'description' => 'sometimes|required|string|min:3|max:255',
            'fiscal_code' => 'sometimes|required|min:1|max:45',
            'company_name' => 'sometimes|required|min:3|max:100',
            'administrator_name' => 'sometimes|required|min:3|max:100',
            'website' => 'sometimes|required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'company_email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:restaurant_profiles,company_email,'.$user -> company_email,
            'phone_number' => 'sometimes|required|numeric',
            'logo' => 'sometimes|required|string',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }


        $restaurant = $user -> restaurantProfile;

        if($restaurant == null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant!')
            ]);
        }

        $restaurant -> name = $request -> name ? $request -> name : $restaurant -> name;

        $restaurant -> description = $request -> description ? $request -> description : $restaurant -> description;

        $restaurant -> fiscal_code = $request -> fiscal_code ? $request -> fiscal_code : $restaurant -> fiscal_code;

        $restaurant -> company_name = $request -> company_name ? $request -> company_name : $restaurant -> company_email;

        $restaurant -> administrator_name = $request -> administrator_name ? $request -> administrator_name : $restaurant -> administrator_name;

        $restaurant -> website = $request -> website ? $request -> website : $restaurant -> website;

        $restaurant -> company_email = $request -> company_email ? $request -> company_email : $restaurant -> company_email;

        $restaurant -> phone_number = $request -> phone_number ? $request -> phone_number : $restaurant -> phone_number;

        if($request -> logo) {

            $restaurant -> logo = $request -> logo;
        }

        if($request -> pictures) {

            foreach ($request -> pictures as $key => $picture) {
             {
                    $restaurant_has_picture = new RestaurantPicture();
                    $restaurant_has_picture -> restaurantProfile() -> associate($restaurant);
                    $restaurant_has_picture -> picture = $picture;
                    $restaurant_has_picture -> save();
                }
            }
        }

        $restaurant -> save();

        $restaurant -> jobOffers;

        $restaurant -> postsLocationCheckin;

        $restaurant -> postsFrom;

        $restaurant -> postsTo;

        $restaurant -> restaurantLocations;

        $restaurant -> user;

        $cont = 0;

        $r_follows = 0;

        $rating_val = 0;

        foreach ($restaurant -> restaurantLocations as $key => $location) {

            foreach ($location -> ratings as $key => $rating) {

                $rating_val += $rating -> stars;

                $cont ++;
            }

            $r_follows += count($location -> follows);
        }

        if($cont == 0) {

            $restaurant -> rating = null;
        } else {

            $restaurant -> rating = $rating_val / $cont;
        }

        $restaurant -> follows_number = $r_follows;

        return json_encode([
            'success' => true,
            'message' => __('The restaurant profile was successfully updated!'),
            'data' => [
                'restaurant' => $restaurant
            ]
        ]);
    }

    public function delete() {

        $user = User::find($_GET['user_id']);

        $restaurant = $user -> restaurantProfile;

        if($restaurant == null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant!')
            ]);
        }

        $restaurant -> follows() -> delete();

        $restaurant -> jobOffers() -> delete();

        $restaurant -> postsLocationCheckin() -> delete();

        $restaurant -> postsFrom() -> delete();

        $restaurant -> postsTo() -> delete();

        $restaurant -> ratings() -> delete();

        $restaurant -> restaurantAdminstrators() -> delete();

        $restaurant -> restaurantLocations() -> delete();

        $restaurant -> delete();

        $user -> restaurant_profile_id = null;

        $user -> save();

        return json_encode([
            'success' => true,
            'message' => __('The restaurant was successfully deleted!')
        ]);
    }

    public function deletePicture($picture_id) {

        $user = User::find($_GET['user_id']);

        $picture = RestaurantPicture::find($picture_id);

        if($picture === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given picture id is invalid!')
            ]);
        }
        $restaurant = RestaurantProfile::find($picture -> restaurant_id);

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant id is invalid!')
            ]);
        }

        if($user -> restaurantProfile -> id != $restaurant -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The picture is not asociated with right restaurant!')
            ]);
        }

        $picture -> delete();

        return json_encode([
            'success' => true,
            'message' => __('The picture was successfully deleted!')
        ]);
    }

    public function readRestaurant($restaurant_id) {

        $restaurant = RestaurantProfile::find($restaurant_id);

        if($restaurant === null) {

            return json_encode([
                'success' => false,
                'message' => __('The given restaurant id is invalid!')
            ]);
        }

        $restaurant -> jobOffers;

        $restaurant -> postsLocationCheckin;

        $restaurant -> postsFrom;

        $restaurant -> postsTo;

        $restaurant -> restaurantLocations;

        $restaurant -> user;

        $cont = 0;

        $r_follows = 0;

        $rating_val = 0;

        foreach ($restaurant -> restaurantLocations as $key => $location) {

            foreach ($location -> ratings as $key => $rating) {

                $rating_val += $rating -> stars;

                $cont ++;
            }

            $r_follows += count($location -> follows);
        }

        if($cont == 0) {

            $restaurant -> rating = null;
        } else {

            $restaurant -> rating = $rating_val / $cont;
        }

        $restaurant -> follows_number = $r_follows;

        return json_encode([
            'success' => true,
            'message' => __('The restaurant and associated data were successfully taken over!'),
            'data' => $restaurant
        ]);
    }
}
