<?php

namespace App\Http\Controllers;

use App\User;
use App\Partner;
use App\RestaurantProfile;
use App\SponsorshipsRequest;
use Illuminate\Http\Request;

class RestaurantsController extends Controller
{
    public function read() {

        $restaurants = RestaurantProfile::all();

        foreach ($restaurants as $key => $restaurant) {

            $restaurant -> jobOffers;

            $restaurant -> postsLocationCheckin;

            $restaurant -> postsFrom;

            $restaurant -> postsTo;

            $restaurant -> restaurantLocations;

            $restaurant -> user;

            $cont = 0;

            $r_follows = 0;

            $rating_val = 0;

            foreach ($restaurant -> restaurantLocations as $key => $location) {

                foreach ($location -> ratings as $key => $rating) {

                    $rating_val += $rating -> stars;

                    $cont ++;
                }

                $r_follows += count($location -> follows);
            }

            if($cont == 0) {

                $restaurant -> rating = null;
            } else {

                $restaurant -> rating = $rating_val / $cont;
            }

            $restaurant -> follows_number = $r_follows;
        }

        return json_encode([
            'success' => true,
            'message' => __('The restaurants and associated data for each were successfully taken over!'),
            'data' => $restaurants
        ]);
    }

    public function sendSponsorship($restaurant_id, $partner_id) {

        $user = User::find($_GET['user_id']);

        $restaurant = RestaurantProfile::find($restaurant_id);

        $partner = Partner::find($partner_id);

        if($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The restaurant is invalid!')
            ]);
        }

        if($partner === null) {
            return json_encode([
                'success' => false,
                'message' => __('The partner is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($restaurant -> id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $sponsorship_request = new SponsorshipsRequest();

        $sponsorship_request -> partner() -> associate($partner);

        $sponsorship_request -> restaurant_id = $restaurant -> id;

        $sponsorship_request -> save();

        return json_encode([
            'success' => true,
            'message' => __('Sponsorship was sent to the partner!')
        ]);
    }
}
