<?php

namespace App\Http\Controllers;

use App\Shift;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use Validator;

class ShiftController extends Controller
{
    public function restaurantOpen($location_id)
    {
        date_default_timezone_set('Europe/Bucharest');
        $ziua_de_azi = date("l");
        $closed = true;
        $restaurant_exact = Shift::where('restaurant_location_id', $location_id)->where('day', $ziua_de_azi)->first();
        $now_full = now()->format('H:i:s');
        $six = mktime(6, 00, 00);
        $eleven_night = mktime(00, 00, 00);
        $eleven_almost_full = mktime(23, 59, 59);
        $elevem_almost_full_string = strtotime($eleven_almost_full);
        $starttime = Carbon::parse($restaurant_exact->openingTime)->format('H:i:s');
        $endtime = Carbon::parse($restaurant_exact->closingTime)->format('H:i:s');
        $starttime_string = strtotime($starttime);
        $endtime_string = strtotime($endtime);
        $opening_ziua_de_ieri = Shift::where('restaurant_location_id', $location_id)->where('id', $restaurant_exact->id - 1)->first();
        $opening_time_ieri_serios = $opening_ziua_de_ieri->openingTime;
        $opening_time_ieri_serios_string = strtotime($opening_time_ieri_serios);
        $strtotime_now_full = strtotime($now_full);
        if ($strtotime_now_full >= $eleven_night && $strtotime_now_full <= $six)
            $noapte = true;
        else
            $noapte = false;
        $closing_dupa_12_ieri_in_ora = Carbon::parse($opening_ziua_de_ieri->closingTimeAfter00)->format('H:i:s');
        $closing_dupa_12_string = strtotime($closing_dupa_12_ieri_in_ora);
        if ($noapte === false) {
            if ($restaurant_exact->closingTimeAfter00 === null && $restaurant_exact->closingTime != null) {
                if ($strtotime_now_full >= $starttime_string && $strtotime_now_full < $endtime_string) {
                    $closed = false;
                }
            } else if ($restaurant_exact->closingTimeAfter00 != null && $restaurant_exact->closingTime === null) {
                if ($strtotime_now_full <= $elevem_almost_full_string && $strtotime_now_full >= $opening_time_ieri_serios_string) {
                    $closed = false;
                }
            }
        } elseif ($noapte === true) {
            if ($opening_ziua_de_ieri->closingTimeAfter00 != null && $opening_ziua_de_ieri->closingTime === null)
                if ($strtotime_now_full < $closing_dupa_12_string) {
                    $closed = false;
                }


        }
        if ($closed === true) {
            echo "Restaurantul este inchis momentan";
        } else echo "Restaurantul este deschis, va asteptam";
    }

    public function addShifts(Request $request, $location_id)
    {
        $user = User::find($_GET['user_id']);

        $restaurant = $user->restaurantProfile;

        if ($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }

        $location = $restaurant->restaurantLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        $validator = Validator::make($request->all(), [
            'day' => 'required|string|max:50',
            'openingTime' => 'required|date_format:H:i:s',
            'closingTime' => 'sometimes|date_format:H:i:s',
            'closingTimeAfter00' => 'sometimes|date_format:H:i:s']);

        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }
        $shift = new Shift();

        $shift->restaurant_location_id = $location_id;
        $shift->day = $request->date;
        $shift->openingTime = $request->openingTime;
        $shift->closingTime = $request->closingTime;
        $shift->closingTimeAfter00 = $request->closingTimeAfter00;
        $shift->save();

        return json_encode([
            'success' => true,
            'message' => __('The shift has been successfully added!'),
            'data' => [
                'shift' => $shift
            ]
        ]);

    }

    public function editShifts(Request $request, $location_id, $id)
    {

        $user = User::find($_GET['user_id']);

        $restaurant = $user->restaurantProfile;

        if ($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }
        $test = array(1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday');

        $location = $restaurant->restaurantLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }
        $validator = Validator::make($request->all(), [
            'openingTime' => 'sometimes|date_format:H:i:s',
            'closingTime' => 'sometimes|date_format:H:i:s',
            'closingTimeAfter00' => 'sometimes|date_format:H:i:s']);

        if ($validator->fails()) {
            $errors = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[$key] = [];
                foreach ($value as $suberror) {
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }
        $shift_day = Shift::where('day', $test[$id])->where('restaurant_location_id', $location_id)->first();
        $shift_day->openingTime = $request->openingTime ? $request->openingTime : $shift_day->openingTime;
        $shift_day->closingTime = $request->closingTime ? $request->closingTime : $shift_day->closingTime;
        $shift_day->closingTimeAfter00 = $request->closingTimeAfter00 ? $request->closingTimeAfter00 : $shift_day->closingTimeAfter00;
        $shift_day->save();

        return json_encode([
            'success' => true,
            'message' => __('The post has been successfully updated!'),
            'data' => $shift_day
        ]);
    }
}
