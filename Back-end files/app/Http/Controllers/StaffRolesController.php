<?php

namespace App\Http\Controllers;

use App\User;
use App\StaffRole;
use Illuminate\Http\Request;

class StaffRolesController extends Controller
{
    public function getStaffRoles() {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        if($producer === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a producer profile created!')
            ]);
        }

        $staff_roles = StaffRole::all();

        return json_encode([
            'success' => true,
            'message' => __('The staff roles has been taken over!'),
            'staff_roles' => $staff_roles
        ]);
    }
}
