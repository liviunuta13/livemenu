<?php

namespace App\Http\Controllers;

use App\Tip;
use App\User;
use App\RestaurantLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TipsController extends Controller
{
    public function restaurantLocationGive(Request $request, $location_id) {

        $user = User::find($_GET['user_id']);

        $location = RestaurantLocation::find($location_id);

        if($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }

        if($user -> restaurantProfile === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given user does not have an associated restaurant profile!')
            ]);
        }

        if($location -> restaurant_profile_id != $user -> restaurantProfile -> id) {
            return json_encode([
                'success' => false,
                'message' => __('The given user is not associated with the given restaurant profile!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'tip_type_id' => 'required|numeric|exists:tips_types,id',
            'number' => 'required|numeric|min:1',
            // 'from_user_id' => 'sometimes|required|numeric|exists:users,id',
            'to_user_id' => 'sometimes|required|numeric|exists:users,id',
            // 'from_client_profile_id' => 'sometimes|required|numeric|exists:client_profiles,id',
            'to_client_profile_id' => 'sometimes|required|numeric|exists:client_profiles,id',
            // 'from_producer_profile_id' => 'sometimes|required|numeric|exists:producer_profiles,id',
            'to_producer_profile_id' => 'sometimes|required|numeric|exists:producer_profiles,id',
            // 'from_profesionist_profile_id' => 'sometimes|required|numeric|exists:profesionist_profiles,id',
            'to_profesionist_profile_id' => 'sometimes|required|numeric|exists:profesionist_profiles,id',
            // 'from_restaurant_profile_id' => 'sometimes|required|numeric|exists:restaurant_profiles,id',
            'to_restaurant_profile_id' => 'sometimes|required|numeric|exists:restaurant_profiles,id',
            // 'from_restaurant_location_id' => 'sometimes|required|numeric|exists:restaurant_locations,id',
            'to_restaurant_location_id' => 'sometimes|required|numeric|exists:restaurant_locations,id',
            // 'from_administrator_id' => 'sometimes|required|numeric|exists:administrators,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if(count($request -> all()) - 3 != 3) {
            return json_encode([
                'success' => false,
                'message' => __('The request must contain tip_type_id, number and the user id!')
            ]);
        }

        if($request -> to_user_id === null && $request -> to_user_id && $request -> to_client_profile_id && $request -> to_producer_profile_id && $request -> to_profesionist_profile_id && $request -> to_restaurant_profile_id && $request -> to_restaurant_location_id) {
            return json_encode([
                'success' => false,
                'message' => __('The user id is required!')
            ]);
        }

        if($request -> number < 1) {
            return json_encode([
                'success' => false,
                'message' => __('The number of tips must be greather than 0!')
            ]);
        }

        if($request -> to_user_id) {

            $tip = new Tip;

            $tip -> tip_type_id = $request -> tip_type_id;

            $tip -> number = $request -> number;

            $tip -> restaurantLocationFrom -> associate($location);

            $tip -> to_user_id = $request -> to_user_id;

            $tip -> save();

        } else if($request -> to_client_profile_id) {

            $tip = new Tip;

            $tip -> tip_type_id = $request -> tip_type_id;

            $tip -> number = $request -> number;

            $tip -> restaurantLocationFrom -> associate($location);

            $tip -> to_client_profile_id = $request -> to_client_profile_id;

            $tip -> save();

        } else if($request -> to_producer_profile_id) {

            $tip = new Tip;

            $tip -> tip_type_id = $request -> tip_type_id;

            $tip -> number = $request -> number;

            $tip -> restaurantLocationFrom -> associate($location);

            $tip -> to_producer_profile_id = $request -> to_producer_profile_id;

            $tip -> save();

        } else if($request -> to_profesionist_profile_id) {

            $tip = new Tip;

            $tip -> tip_type_id = $request -> tip_type_id;

            $tip -> number = $request -> number;

            $tip -> restaurantLocationFrom -> associate($location);

            $tip -> to_profesionist_profile_id = $request -> to_profesionist_profile_id;

            $tip -> save();

        } else if($request -> to_restaurant_profile_id) {

            $tip = new Tip;

            $tip -> tip_type_id = $request -> tip_type_id;

            $tip -> number = $request -> number;

            $tip -> restaurantLocationFrom -> associate($location);

            $tip -> to_restaurant_profile_id = $request -> to_restaurant_profile_id;

            $tip -> save();

        } else if($request -> to_restaurant_location_id) {

            $tip = new Tip;

            $tip -> tip_type_id = $request -> tip_type_id;

            $tip -> number = $request -> number;

            $tip -> restaurantLocationFrom -> associate($location);

            $tip -> to_restaurant_location_id = $request -> to_restaurant_location_id;

            $tip -> save();

        } else {
            return json_encode([
                'success' => false,
                'message' => __('The user id is required!')
            ]);
        }

        return json_encode([
            'success' => true,
            'message' => __('The tips has successfully given!')
        ]);
    }
}
