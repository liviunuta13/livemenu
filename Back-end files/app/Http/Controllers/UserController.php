<?php

namespace App\Http\Controllers;

use App\City;
use App\County;
use App\Country;
use App\TipsType;
use App\MusicType;
use App\ForeignLanguage;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getTipsTypes() {

        $tips = TipsType::all();

        return json_encode([
            'success' => true,
            'message' => __('The tips types was successfully taken over!'),
            'data' => [
                'tips' => $tips
            ]
        ]);
    }

    public function getMusicTypes() {

        $music_types = MusicType::all();

        return json_encode([
            'success' => true,
            'message' => __('The music types was successfully taken over!'),
            'data' => [
                'music_types' => $music_types
            ]
        ]);
    }

    public function languages() {

        return json_encode([
            'success' => true,
            'messages' => ForeignLanguage::all()
        ]);
    }

    public function cities() {

        $cities = City::all();

        foreach ($cities as $key => $city) {

            $city -> county;
        }

        return json_encode([
            'success' => true,
            'messages' => $cities
        ]);
    }

    public function counties() {

        $counties = County::all();

        foreach ($counties as $key => $county) {

            $county -> country;

            $county -> cities;
        }

        return json_encode([
            'success' => true,
            'messages' => $counties
        ]);
    }

    public function countries() {

        $countries = County::all();

        foreach ($countries as $key => $country) {

            $country -> counties;
        }

        return json_encode([
            'success' => true,
            'messages' => $countries
        ]);
    }
}
