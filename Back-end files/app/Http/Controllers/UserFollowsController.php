<?php

namespace App\Http\Controllers;

use Storage;
use App\User;
use App\Follow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserFollowsController extends Controller
{
    public function follow(Request $request) {

        $user = User::find($_GET['user_id']);

        $validator = Validator::make($request -> all(), [
            'restaurant_location_id' => 'sometimes|required|exists:restaurant_locations,id',
            'professionist_id' => 'sometimes|required|exists:profesionist_profiles,id',
            'producer_location_id' => 'sometimes|required|exists:producer_profile_locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> professionist_id == null && $request -> producer_location_id == null && $request -> restaurant_location_id == null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        if($request -> professionist_id != null && $request -> producer_location_id != null && $request -> restaurant_location_id == null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        if($request -> professionist_id != null && $request -> producer_location_id == null && $request -> restaurant_location_id != null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        if($request -> professionist_id == null && $request -> producer_location_id != null && $request -> restaurant_location_id != null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        if($request -> professionist_id != null && $request -> producer_location_id != null && $request -> restaurant_location_id != null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        $follow = new Follow();

        $follow -> user() -> associate($user);

        if($request -> professionist_id) {

            $follow -> to_profesionist_profile_id = $request -> profesionist_id;

        } else if($request -> restaurant_location_id) {

            $follow -> to_restaurant_location_id = $request -> restaurant_location_id;

        } else if($request -> producer_location_id) {

            $follow -> to_producer_location_id = $request -> producer_location_id;

        } else {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        $follow -> save();

        $follow -> user;

        $follow -> producerLocation;

        $follow -> profesionistProfile;

        $follow -> restaurantLocation;

        return json_encode([
            'success' => true,
            'message' => __('You have successfully followed!'),
            'data' => [
                'follow' => $follow
            ]
        ]);
    }

    public function unfollow(Request $request) {

        $user = User::find($_GET['user_id']);

        $validator = Validator::make($request -> all(), [
            'restaurant_location_id' => 'sometimes|required|exists:restaurant_locations,id',
            'professionist_id' => 'sometimes|required|exists:profesionist_profiles,id',
            'producer_location_id' => 'sometimes|required|exists:producer_profile_locations,id'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> professionist_id == null && $request -> producer_location_id == null && $request -> restaurant_location_id == null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        if($request -> professionist_id != null && $request -> producer_location_id != null && $request -> restaurant_location_id == null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        if($request -> professionist_id != null && $request -> producer_location_id == null && $request -> restaurant_location_id != null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        if($request -> professionist_id == null && $request -> producer_location_id != null && $request -> restaurant_location_id != null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        if($request -> professionist_id != null && $request -> producer_location_id != null && $request -> restaurant_location_id != null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }

        if($request -> professionist_id) {

            $follow = $user -> follows -> where('to_profesionist_profile_id', $request -> professionist_id) -> first();

            if($follow) {

                $follow -> delete();

                return json_encode([
                    'success' => true,
                    'message' => __('The follow has been removed!')
                ]);
            }

        } else if($request -> restaurant_location_id) {

            $follow = $user -> follows -> where('to_restaurant_location_id', $request -> restaurant_location_id) -> first();

            if($follow) {

                $follow -> delete();

                return json_encode([
                    'success' => true,
                    'message' => __('The follow has been removed!')
                ]);
            }

        } else if($request -> producer_location_id) {

            $follow = $user -> follows -> where('to_producer_location_id', $request -> producer_location_id) -> first();

            if($follow) {

                $follow -> delete();

                return json_encode([
                    'success' => true,
                    'message' => __('The follow has been removed!')
                ]);
            }

        }
        return json_encode([
            'success' => false,
            'message' => __('The requested follow is invalid!')
        ]);
    }
}
