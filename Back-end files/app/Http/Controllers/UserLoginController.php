<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use Carbon\Carbon;
use App\UserLoginToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserLoginController extends Controller
{
    public function login(Request $request) {

        if(!isset($_SERVER['HTTP_USER_AGENT'])) {
            return json_encode([
                'success' => false,
                'message' => __('The user agent is required!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'email' => ['required', 'max:45', 'email', 'regex:/(.*)/i', 'exists:users'],
            'password' => 'required|string|min:8|max:190|regex:/^.*(?=.{3})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|confirmed'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $user = User::where('email', $request -> email) -> first();

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        if($user -> confirmed_email == 0) {
            return json_encode([
                'success' => false,
                'message' => __('Unverified email address!')
            ]);
        }

        $current_token = UserLoginToken::where('user_id', $user -> id) -> where('user_agent', $_SERVER['HTTP_USER_AGENT']) -> first();

        // return json_encode([
        //     'user' => $user,
        //     'token' => $current_token
        // ]);

        if($current_token != null) {

            // && Carbon::now() -> format('Y-m-d') < Carbon::parse($current_token -> expire_date) -> format('Y-m-d')
            return json_encode([
                'success' => false,
                'message' => __('You are already logged in!')
            ]);
        }

        if(Hash::check($request -> password, $user -> password) === true) {

            if($current_token) {

                $current_token -> delete();
            }

            $user_login_token = new UserLoginToken();

            $user_login_token -> user() -> associate($user);

            $user_login_token -> token = str_random(65);

            $user_login_token -> expire_date = now() -> addMonth(3);

            $user_login_token -> ip = $request -> getClientIp();

            try {
                $coordinates = explode(",", json_decode(file_get_contents("http://ipinfo.io/")) -> loc);

                $user_login_token -> latitude = $coordinates[0];

                $user_login_token -> longitude = $coordinates[1];

            } catch (\Exception $e) {}

            $user_login_token -> user_agent = $_SERVER['HTTP_USER_AGENT'];

            $user_login_token -> user_id = $user -> id;

            $user_login_token -> save();

            $user -> makeHidden('loginTokens');

            $user -> login_token = UserLoginToken::where('user_id', $user -> id) -> first();

            $user -> clientProfile;

            $user -> producerProfile;

            $user -> profesionistProfile;

            $user -> restaurantProfile;

            $user -> restaurantSpecific;

            $user -> balances;

            $user -> balancesTo;

            $user -> follows;

            $user -> ratings;

            // return $user -> login_token -> token;

            // return json_encode(array('success'=>true, 'message'=>'Invata java script!','data'=>$user));
            return json_encode([
                'success' => true,
                'message' => __('You have been successfully logged in!'),
                'data' => [
                    'user' => $user
                ]
            ]);
        } else {
            return json_encode([
                'success' => false,
                'message' => __('The password is wrong!')
            ]);
        }
    }

    public function logout(Request $request) {

        $user_token = UserLoginToken::where([
            'user_id' => $_GET['user_id'],
            'user_agent' => $_GET['user_agent'],
            'token' => $_GET['token']
        ]) -> first();

        if($user_token == null) {
            return json_encode([
                'success' => false,
                'message' => __('You are not logged in!')
            ]);
        }

        $user_token -> delete();

        return json_encode([
            'success' => true,
            'message' => __('You have successfully logged out!')
        ]);

    }
}
