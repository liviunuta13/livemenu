<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use App\City;
use Carbon\Carbon;
use App\ClientProfile;
use App\UserLoginToken;
use App\ProducerProfile;
use App\Mail\VerifyMail;
use App\RestaurantProfile;
use App\ProfessionalHistory;
use App\ProfesionistProfile;
use Illuminate\Http\Request;
use App\ProfessionistEducation;
use App\ProducerProfilePicture;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\ProfesionistProfileHasForeignLanguage;

class UserProfileController extends Controller
{
    // Read user profile data
    public function read($id) {
        $user = User::find($id);
        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        } else {

            $user -> clientProfile;

            $user -> producerProfile;

            $user -> profesionistProfile;

            $user -> restaurantProfile;

            $user -> clientProfile;

            $user -> producerProfile;

            $user -> profesionistProfile;

            $user -> restaurantProfile;

            $user -> restaurantSpecific;

            $user -> balances;

            $user -> balancesTo;

            $user -> follows;

            $user -> ratings;

            return json_encode([
                'success' => true,
                'messages' => __('The user profile was successfully retrieved!'),
                'data' => $user
            ]);
        }
    }

    // Edit user profile data
    public function save(Request $request) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        $validator = Validator::make($request -> all(), [
            'name' => 'sometimes|required|string|min:3|max:45',
            'email' => 'sometimes|required|max:45|email|regex:/(.*)/i|unique:users,email,'.$user -> id,
            'password' => 'sometimes|required|string|min:8|max:190|regex:/^.*(?=.{3})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|confirmed',
            'old_password' => 'sometimes|required|string|min:8|max:190|regex:/^.*(?=.{3})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
            'profile_picture' => 'sometimes|required|string|min:1',

            'favorite_food' => 'sometimes|required|string|min:1|max:45',
            'secret_ingredients' => 'sometimes|required|string|min:1|max:45',
            'restaurant_specifics' => 'sometimes|required|string|min:1|max:45',
            'music_types' => 'sometimes|required|string|min:1|max:45',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        $user -> name = $request -> name ? $request -> name : $user -> name;

        if($request -> email && $request -> email != $user -> email) {
            // Send email confirmation
            // try {

                Mail::to($request -> email) -> send(new VerifyMail($user -> id, $user -> loginTokens -> sortByDesc('id') -> first() -> token, $request -> email));

            // }
            // catch (\Exception $e){

                // return json_encode([
                //     'success' => false,
                //     'message' => __('The account was not created, it\'s a problem with the email!')
                // ]);
            // }
        }

        if($request -> old_password && $request -> password && $request -> password_confirmation) {
            if(Hash::check($request -> old_password, $user -> password) === true) {
                if($request -> password == $request -> password_confirmation) {
                    $user -> password = bcrypt($request -> password);
                }
            } else {
                return json_encode([
                    'success' => false,
                    'message' => __('The old password and current password does not match!')
                ]);
            }
        } else if($request -> old_password || $request -> password || $request -> password_confirmation) {
            return json_encode([
                'success' => false,
                'message' => __('In order to change the password, it is necessary to fill in the old password, the new password and confirm the new password!')
            ]);
        }

        $user -> favorite_food = $request -> favorite_food ? $request -> favorite_food : $user -> favorite_food;

        $user -> secret_ingredients = $request -> secret_ingredients ? $request -> secret_ingredients : $user -> secret_ingredients;

        $user -> restaurant_specifics = $request -> restaurant_specifics ? $request -> restaurant_specifics : $user -> restaurant_specifics;

        $user -> music_types = $request -> music_types ? $request -> music_types : $user -> music_types;

        if($request -> profile_picture) {

            $user -> profile_picture = $request -> profile_picture;
        }

        $user -> save();

        // if ($request -> hasFile('profile_picture')) {
        //
        //     $picture_name = $user -> id . time() . rand(1,99999) . '.' . $request -> file('profile_picture') -> extension();
        //
        //     Storage::delete('/public/users/profile_pictures/' . $user -> profile_picture);
        //
        //     Storage::putFileAs('/public/users/profile_pictures/', $request -> file('profile_picture'), $picture_name);
        //
        //     $user -> profile_picture = $picture_name;
        // }

        $user -> clientProfile;

        $user -> producerProfile;

        $user -> profesionistProfile;

        $user -> restaurantProfile;

        $user -> restaurantSpecific;

        $user -> balances;

        $user -> balancesTo;

        $user -> follows;

        $user -> ratings;

        return json_encode([
            'success' => false,
            'message' => __('The profile was successfully updated!'),
            'data' => $user
        ]);
    }

    public function delete(Request $request) {

        $user = User::find($_GET['user_id']);

        if($user === null) {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }

        if(Hash::check($request -> password, $user -> password) === true) {

            $user -> delete();

            return json_encode([
                'success' => true,
                'message' => __('The user has been deleted!')
            ]);
        } else {

            return json_encode([
                'success' => false,
                'message' => __('The password is not matching!')
            ]);
        }
    }

    // Confirm email at edit profile action
    public function confirmEditEmail($user_id, $new_email , $confirmed_token) {
        $user = User::find($user_id);
        if($user) {
            if($user -> loginTokens -> sortByDesc('id') -> first() -> token == $confirmed_token) {
                $user -> confirmed_email = true;
                $user -> email = $new_email;
                $user -> save();
                return json_encode([
                    'success' => true,
                    'message' => __('The email address is confirmed!')
                ]);
            } else {
                return json_encode([
                    'success' => false,
                    'message' => __('Invalid token!')
                ]);
            }
        } else {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }
    }

    public function addProfessionistProfile(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'email' => 'required|max:45|email|regex:/(.*)/i|unique:profesionist_profiles,email',
            'street_and_number' => 'required|string|min:3|max:190',
            'postal_code' => 'sometimes|required|string|min:1|max:10',
            'county_id' => 'required|numeric|exists:counties,id',
            'city_id' => 'required|numeric|exists:cities,id',
            'country_id' => 'required|numeric|exists:countries,id',
            'phone_number' => 'required|numeric',
            'website' => 'sometimes|required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'professional_experience' => 'required|string|min:1',
            'professional_history_begin_dates' => 'required',
            'professional_history_begin_dates.*' => 'required|date',
            'professional_history_presents' => 'required',
            'professional_history_presents.*' => 'required|boolean',
            'professional_history_end_dates' => 'sometimes|required',
            'professional_history_end_dates.*' => 'required|date',
            'professional_history_cities' => 'required',
            'professional_history_cities.*' => 'required|numeric|exists:cities,id',
            'professional_history_countries' => 'required',
            'professional_history_countries.*' => 'required|numeric|exists:countries,id',
            'professional_history_descriptions' => 'required',
            'professional_history_descriptions.*' => 'required|string|min:1',

            'education_begin_dates' => 'sometimes|required',
            'education_begin_dates.*' => 'required',
            'education_presents' => 'sometimes|required',
            'education_presents.*' => 'required|boolean',
            'education_end_dates' => 'sometimes|required',
            'education_end_dates.*' => 'required',
            'education_certification_names' => 'sometimes|required',
            'education_certification_names.*' => 'required',
            'education_institution_names' => 'sometimes|required',
            'education_institution_names.*' => 'required',
            'education_city_ids' => 'sometimes|required',
            'education_city_ids.*' => 'required|numeric|exists:cities,id',
            'education_country_ids' => 'sometimes|required',
            'education_country_ids.*' => 'required|numeric|exists:countries,id',
            'education_descriptions' => 'sometimes|required',
            'education_descriptions.*' => 'required|string',

            'matern_language_id' => 'required|numeric|exists:foreign_languages,id',
            'foreign_languages' => 'required|string',
            'managerial_skills' => 'sometimes|required|string|min:1',
            'skills_at_working_place' => 'sometimes|required|string|min:1',
            'digital_skills' => 'sometimes|required|string|min:1',
            'driving_licence' => 'sometimes|required|string|min:1',
            'other_skills' => 'sometimes|required|string|min:1',
            'personal_description' => 'sometimes|required|string|min:1',
            'is_looking_for_job' => 'required|boolean',
            'available_for_working' => 'required|boolean'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }
        $user = User::find($_GET['user_id']);

        if($user -> profesionistProfile === null) {

            $current_date = Carbon::now() -> format('Y-m-d');
            //
            // if(Carbon::parse($request -> education_begin_date) -> format('Y-m-d') >= $current_date) {
            //     return json_encode([
            //         'success' => false,
            //         'message' => __('The begin date must be greater or equal with current date!')
            //     ]);
            // }

            if(Carbon::parse($request -> education_begin_date) -> format('Y-m-d') > Carbon::parse($request -> education_end_date) -> format('Y-m-d')) {
                return json_encode([
                    'success' => false,
                    'message' => __('The begin date must be lower or equal with the end date!')
                ]);
            }

            foreach ($request -> professional_history_begin_dates as $key => $professional_history_begin_date) {

                if($request -> profesional_history_presents[$key] == 0) {

                    if($request -> professional_history_end_dates[$key] == null) {

                        return json_encode([
                            'success' => false,
                            'message' => __('If the history present field is 0 then the professional history end date is required!')
                        ]);
                    }
                }

                if(Carbon::parse($professional_history_begin_date) -> format('Y-m-d') >= $current_date) {
                    return json_encode([
                        'success' => false,
                        'message' => __('The professional history begin date must be lower or equal with the current date!')
                    ]);
                }

                if(Carbon::parse($professional_history_begin_date) -> format('Y-m-d') > Carbon::parse($request -> professional_history_end_dates[$key]) -> format('Y-m-d')) {
                    return json_encode([
                        'success' => false,
                        'message' => __('The professional history begin date must be lower or equal with the professional history end date!')
                    ]);
                }

            }

            $professional_profile = new ProfesionistProfile();

            $professional_profile -> name = $request -> name;

            $professional_profile -> confirmed = false;

            $professional_profile -> is_at_working = false;

            $professional_profile -> available_for_working = false;

            $professional_profile -> email = $request -> email;

            $professional_profile -> street_and_number = $request -> street_and_number;

            $professional_profile -> postal_code = $request -> postal_code ? $request -> postal_code : null;

            $professional_profile -> city_id = $request -> city_id;

            $professional_profile -> county_id = $request -> county_id;

            $professional_profile -> country_id = $request -> country_id;

            $professional_profile -> phone_number = $request -> phone_number;

            $professional_profile -> website = $request -> website ? $request -> website : null;

            // Save education section

            // $professional_profile -> education_begin_date = $request -> education_begin_date;
            //
            // $professional_profile -> education_present = $request -> education_present;
            //
            // if($request -> education_presents[$key] == 0) {
            //
            //     if($request -> education_end_dates[$key] == null) {
            //
            //         return json_encode([
            //             'success' => false,
            //             'message' => __('If the education present fied is 0 then the education end date is required!')
            //         ]);
            //     }
            //     $professional_profile -> education_end_date = $request -> education_end_date;
            // }
            //
            // $professional_profile -> education_certification_name = $request -> education_certification_name;
            //
            // $professional_profile -> education_institution_name = $request -> education_institution_name;
            //
            // $professional_profile -> education_city_id = $request -> education_city_id;
            //
            // $professional_profile -> education_country_id = $request -> education_country_id;
            //
            // $professional_profile -> education_description = $request -> education_description;

            $professional_profile -> matern_language_id = $request -> matern_language_id;

            $professional_profile -> managerial_skills = $request -> managerial_skills ? $request -> managerial_skills : null;

            $professional_profile -> skills_at_working_place = $request -> skills_at_working_place ? $request -> skills_at_working_place : null;

            $professional_profile -> digital_skills = $request -> digital_skills ? $request -> digital_skills : null;

            $professional_profile -> driving_licence = $request -> driving_licence ? $request -> driving_licence : null;

            $professional_profile -> other_skills = $request -> other_skills ? $request -> other_skills : null;

            $professional_profile -> personal_description = $request -> personal_description ? $request -> personal_description : null;

            $professional_profile -> is_looking_for_job = $request -> is_looking_for_job;

            $professional_profile -> available_for_working = $request -> available_for_working;

            $professional_profile -> foreign_languages = $request -> foreign_languages ? $request -> foreign_languages : null;

            $professional_profile -> professional_experience = $request -> professional_experience ? $request -> professional_experience : null;

            $professional_profile -> save();

            // foreach ($request -> foreign_languages_ids as $key => $foreign_language_index) {
            //
            //     $foreign_language = new ProfesionistProfileHasForeignLanguage();
            //
            //     $foreign_language -> profesionist_profile_id = $professional_profile -> id;
            //
            //     $foreign_language -> foreign_language_id = $foreign_language_index;
            //
            //     $foreign_language -> save();
            // }

            foreach ($request -> professional_history_begin_dates as $key => $professional_history_begin_date) {

                $history = new ProfessionalHistory();

                $history -> begin_date = $professional_history_begin_date;

                $history -> present = $request -> professional_history_presents[$key];

                if($request -> professional_history_presents[$key] == 0) {

                    if($request -> professional_history_end_dates[$key] == null) {

                        return json_encode([
                            'success' => false,
                            'message' => __('If the education present fied is 0 then the professional history end date is required!')
                        ]);
                    }
                    $history -> end_date = $request -> professional_history_end_dates[$key];
                }

                $history -> city_id = $request -> professional_history_cities[$key];

                $history -> country_id = $request -> professional_history_countries[$key];

                $history -> description = $request -> professional_history_descriptions[$key];

                $history -> profesionist_profile_id = $professional_profile -> id;

                $history -> save();

            }

            // Save education section

            if($request -> education_begin_dates) {

                foreach ($request -> education_begin_dates as $key => $education_begin_date) {

                    $education = new ProfessionistEducation();

                    $education -> begin_date = $request -> begin_dates[$key] ? $request -> $request -> begin_dates[$key] : null;

                    $education -> present = $request -> education_presents[$key] ? $request -> education_presents[$key] : null;

                    if($request -> education_presents[$key] != null && $request -> education_presents[$key] == 0) {

                        if($education_begin_date == null) {

                            return json_encode([
                                'success' => false,
                                'message' => __('If the education present field is 0 then the education end date is required!')
                            ]);
                        }
                        $education -> end_date = $request -> education_begin_dates[$key];
                    }

                    $education -> certificate_name = $request -> education_certificate_names[$key] ? $request -> education_certificate_names[$key] : null;

                    $education -> institution_name = $request -> education_institution_names[$key] ? $request -> education_institution_names[$key] : null;

                    $education -> city_id = $request -> education_city_ids[$key] ? $request -> education_city_ids[$key] : null;

                    $education -> country_id = $request -> education_country_ids[$key] ? $request -> education_country_ids[$key] : null;

                    $education -> description = $request -> education_descriptions[$key] ? $request -> education_descriptions[$key] : null;

                    $education -> profesionistProfile() -> associate($professional_profile -> id);

                    $education -> save();
                }
            }

            $user -> profesionistProfile() -> associate($professional_profile);

            $user -> save();

            $professional_profile -> city;

            $professional_profile -> county;

            $professional_profile -> country;

            $professional_profile -> educationCity;

            $professional_profile -> educationCountry;

            foreach ($professional_profile -> profesionistProfileHasForeignLanguages as $key => $value) {

                $value -> foreignLanguage;
            }

            $professional_profile -> professionalHistory;

            $professional_profile -> education;

            $professional_profile -> user;

            return json_encode([
                'success' => true,
                'message' => __('The professional profile was successfully created!'),
                'data' => [
                    'professionist_profile' => $professional_profile
                ]
            ]);
        } else {
            return json_encode([
                'success' => false,
                'message' => __('You already have a professionist profile created!'),
                'data' => [
                    'professionist_profile' => $user -> profesionistProfile
                ]
            ]);
        }
    }

    public function addRestarantProfile(Request $request) {
        $user = User::find($_GET['user_id']);
        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3|max:255',
            'fiscal_code' => 'required|min:1|max:45',
            'company_name' => 'required|min:3|max:100',
            'administrator_name' => 'required|min:3|max:100',
            'website' => 'sometimes|required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'company_email' => 'required|max:45|email|regex:/(.*)/i|unique:restaurant_profiles,company_email,'.$user -> company_email,
            'phone_number' => 'required|numeric',
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }
        if($user -> restaurantProfile === null) {

            $restaurant_profile = new RestaurantProfile();

            $restaurant_profile -> name = $request -> name;

            $restaurant_profile -> description = $request -> description;

            $restaurant_profile -> fiscal_code = $request -> fiscal_code;

            $restaurant_profile -> company_name = $request -> company_name;

            $restaurant_profile -> administrator_name = $request -> administrator_name;

            $restaurant_profile -> website = $request -> website ? $request -> website : null;

            $restaurant_profile -> company_email = $request -> company_email;

            $restaurant_profile -> phone_number = $request -> phone_number;

            $restaurant_profile -> save();

            $user -> restaurantProfile() -> associate($restaurant_profile);

            $user -> save();

            return json_encode([
                'success' => true,
                'message' => __('The restaurant profile was successfully created!')
            ]);
        } else {
            return json_encode([
                'success' => false,
                'message' => __('You already have a restaurant profile created!')
            ]);
        }
    }


    public function addProducerProfile(Request $request) {
        $user = User::find($_GET['user_id']);
        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'description' => 'required|string|min:3',
            'fiscal_code' => 'required|min:1|max:45',
            'company_name' => 'required|min:3|max:100',
            'administrator_name_surname' => 'required|min:3|max:100',
            'website' => 'sometimes|required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'email' => 'required|max:45|email|regex:/(.*)/i|unique:producer_profiles,email',
            'phone_number' => 'required|numeric',
            'logo' => 'sometimes|required|string',
            'pictures' => 'sometimes|required',
            'pictures.*' => 'required|string'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }
        if($user -> producerProfile === null) {

            $producer_profile = new ProducerProfile();

            $producer_profile -> producer_type_id = 3;

            $producer_profile -> name = $request -> name;

            $producer_profile -> description = $request -> description;

            $producer_profile -> fiscal_code = $request -> fiscal_code;

            $producer_profile -> company_name = $request -> company_name;

            $producer_profile -> administrator_name_surname = $request -> administrator_name_surname;

            $producer_profile -> website = $request -> website;

            $producer_profile -> email = $request -> email;

            $producer_profile -> phone_number = $request -> phone_number;

            if($request -> logo) {

                $producer_profile -> logo = $request -> logo;
            }

            $producer_profile -> save();

            $user -> producerProfile() -> associate($producer_profile);

            $user -> save();

            if($request -> pictures) {

                foreach ($request -> pictures as $key => $picture) {
                 {
                        $producer_profile_picture = new ProducerProfilePicture();

                        $producer_profile_picture -> name = $picture;

                        $producer_profile_picture -> producerProfile() -> associate($producer_profile);

                        $producer_profile_picture -> save();
                    }
                }
            }

            $producer_profile -> producerType;

            $producer_profile -> producerProfilePictures;

            $producer_profile -> user;

            return json_encode([
                'success' => true,
                'message' => __('The producer profile was successfully created!'),
                'data' => [
                    'producer_profile' => $producer_profile
                ]
            ]);

        } else {
            return json_encode([
                'success' => false,
                'message' => __('You already have a producer profile created!')
            ]);
        }
    }

    public function addClientProfile(Request $request) {
        $validator = Validator::make($request -> all(), [
            'profile_picture' => 'required|string'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }
        $user = User::find($_GET['user_id']);

        if($user -> clientProfile === null) {

            $client_profile = new ClientProfile();

            if($request -> profile_picture) {

                $client_profile -> profile_picture = $picture;
            }
            $client_profile -> save();

            $user -> clientProfile() -> associate($client_profile);

            $user -> save();

            return json_encode([
                'success' => true,
                'message' => __('The clent profile was successfully created!')
            ]);

        } else {
            return json_encode([
                'success' => false,
                'message' => __('You already have a client profile created!')
            ]);
        }
    }
}
