<?php

namespace App\Http\Controllers;

use Storage;
use App\User;
use App\Rating;
use App\RatingPicture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserRatingController extends Controller
{
    public function add(Request $request) {

        $user = User::find($_GET['user_id']);

        $validator = Validator::make($request -> all(), [
            'description' => 'sometimes|required|string|min:3',
            'stars' => 'sometimes|required|numeric|min:0',
            'restaurant_id' => 'sometimes|required|exists:restaurant_profiles,id',
            'proffesionist_id' => 'sometimes|required|exists:profesionist_profiles,id',
            'producer_id' => 'sometimes|required|exists:producer_profiles,id',
            'pictures' => 'required',
            'pictures.*' => 'required|string'
        ]);

        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return json_encode([
                'success' => false,
                'message' => $errors
            ]);
        }

        if($request -> restaurant_id == null && $request -> proffesionist_id == null && $request -> producer_id == null) {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        } else if(($request -> restaurant_id != null && $request -> proffesionist_id == null && $request -> producer_id == null) ||
            ($request -> restaurant_id == null && $request -> proffesionist_id != null && $request -> producer_id == null) ||
            ($request -> restaurant_id == null && $request -> proffesionist_id == null && $request -> producer_id != null)) {

            $rating = new Rating();

            if($request -> restaurant_id) {

                $rating -> to_restaurant_profile_id = $request -> restaurant_id;

            } else if($request -> proffesionist_id) {

                $rating -> to_profesionist_profile_id = $request -> proffesionist_id;

            } else if($request -> producer_id) {

                $rating -> to_producer_profile_id = $request -> producer_id;
            }

            $rating -> description = $request -> description ? $request -> description : null;

            $rating -> stars = $request -> stars ? $request -> stars : null;

            $rating -> user() -> associate($user);

            $rating -> save();

            if($request -> pictures) {

                foreach ($request -> pictures as $key => $picture) {
                 {
                        $picture_name = $rating -> id . time() . rand(1,99999) . '.' . $picture -> extension();

                        $rating_picture = new RatingPicture();

                        $rating_picture -> picture = $picture;

                        $rating_picture -> rating() -> associate($rating);

                        $rating_picture -> save();
                    }
                }
            }

            $rating -> producerProfileFrom;

            $rating -> producerProfileTo;

            $rating -> profesionistProfileFrom;

            $rating -> profesionistProfileTo;

            $rating -> restaurantLocation;

            $rating -> restaurantProfile;

            $rating -> user;

            $rating -> ratingPictures;

            return json_encode([
                'success' => true,
                'message' => __('The rating has successfully submitted!'),
                'data' => [
                    'rating' => $rating
                ]
            ]);

        } else {
            return json_encode([
                'success' => false,
                'message' => __('Restaurant id or profesionist id or producer id is required!')
            ]);
        }
    }
}
