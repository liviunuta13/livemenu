<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use App\UserLoginToken;
use App\Mail\VerifyMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserRegisterController extends Controller
{
    public function register(Request $request) {

        $validator = Validator::make($request -> all(), [
            'name' => 'required|string|min:3|max:45',
            'email' => ['required', 'max:45', 'email', 'regex:/(.*)/i', 'unique:users,email'],
            'password' => 'required|string|min:8|max:190|regex:/^.*(?=.{3})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|confirmed',
            'profile_picture' => 'sometimes|required|string|min:1'
        ]);
        if($validator -> fails()) {
            $errors = [];
            foreach ($validator -> errors() -> messages() as $key => $value){
                $errors[$key] = [];
                foreach ($value as $suberror){
                    $errors[$key][] = __($suberror);
                }
            }
            return Response::json([
                'success' => false,
                'message' => $errors
            ]);
        }
        $user = new User();

        $user -> name = $request -> name;

        $user -> email = $request -> email;

        $user -> password = bcrypt($request -> password);

        if($request -> profile_picture) {

            $user -> profile_picture = $request -> profile_picture;
        }

        $user -> save();

        $user_login_token = new UserLoginToken();

        $user_login_token -> user() -> associate($user);

        // $user_login_token -> token = bcrypt($user -> email . time() . rand(1111, 999999) . rand(1110, 93939));

        $user_login_token -> token  = str_random(65);

        $user_login_token -> expire_date = now() -> addMonth(3);

        $user_login_token -> ip = $request -> getClientIp();

        try {
            $coordinates = explode(",", json_decode(file_get_contents("http://ipinfo.io/")) -> loc);

            $user_login_token -> latitude = $coordinates[0];

            $user_login_token -> longitude = $coordinates[1];

        } catch (\Exception $e) {}

        $user_login_token -> user_agent = $_SERVER['HTTP_USER_AGENT'];

        $user -> loginTokens() -> save($user_login_token);

        $user_login_token -> save();

        // Send email confirmation

        try {

            Mail::to($user -> email) -> send(new VerifyMail($user -> id, $user -> loginTokens -> sortByDesc('id') -> first() -> token, null));

        } catch (\Exception $e){

            $user -> delete();

            $user_login_token -> delete();

            return json_encode([
                'success' => false,
                'message' => __('The account was not created, it\'s a problem with the email!')
            ]);
        }

        $user -> clientProfile;

        $user -> producerProfile;

        $user -> profesionistProfile;

        $user -> restaurantProfile;

        $user -> restaurantSpecific;

        $user -> balances;

        $user -> balancesTo;

        $user -> follows;

        $user -> ratings;

        return Response::json([
            'success' => true,
            'message' => __('Your account has been successfully created! Check your email address!'),
            'data' => $user
        ]);
    }

    public function confirmEmail($user_id, $confirmed_token) {
        $user = User::find($user_id);
        if($user) {
            if($user -> loginTokens -> sortByDesc('id') -> first() -> token == $confirmed_token) {
                $user -> confirmed_email = true;
                $user -> save();
                return json_encode([
                    'success' => true,
                    'message' => __('The email address is confirmed!')
                ]);
            } else {
                return json_encode([
                    'success' => false,
                    'message' => __('Invalid token!')
                ]);
            }
        } else {
            return json_encode([
                'success' => false,
                'message' => __('User not found!')
            ]);
        }
    }
}
