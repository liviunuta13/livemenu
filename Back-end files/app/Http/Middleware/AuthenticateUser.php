<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Response;

class AuthenticateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($_GET['user_id'])) {
            return Response::json([
                'success' => false,
                'message' =>  __('Invalid user id!')
            ], 403);
        }
        if(!isset($_GET['token'])) {
            return Response::json([
                'success' => false,
                'message' => __('Invalid user token!')
            ], 403);
        }
        if(!isset($_SERVER['HTTP_USER_AGENT'])) {
            return Response::json([
                'success' => false,
                'message' => __('Invalid user agent!')
            ], 403);
        }
        $user = User::find($_GET['user_id']);
        if($user === null) {
            return Response::json([
                'success' => false,
                'message' => 'User not found!'
            ]);
        } else {
            if($user -> confirmed_email == 0) {
                return Response::json([
                    'success' => false,
                    'message' => __('Unverified email address!')
                ], 403);
            }
        }

        $last_user_token = $user -> loginTokens -> where('user_agent', $_SERVER['HTTP_USER_AGENT']) -> last();

        if($last_user_token == null) {
            return Response::json([
                'success' => false,
                'message' => __('You are not logged in!')
            ], 403);
        }

        if($last_user_token -> token != $_GET['token']) {
            return Response::json([
                'success' => false,
                'message' => __('The token is invalid!')
            ], 403);
        }
        return $next($request);
    }
}
