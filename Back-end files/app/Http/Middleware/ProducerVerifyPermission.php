<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Response;

class ProducerVerifyPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = User::find($_GET['user_id']);

        $producer = $user -> producerProfile;

        $staff = ProducerStaff::find($request -> route('staff_id'));

        $role = StaffRole::find($request -> route('role_id'));

        return $next($request);
    }
}
