<?php

namespace App\Http\Middleware;

use Closure;
use App\Hotel;
use Illuminate\Support\Facades\Response;

class RestaurantHasHotel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request -> route('hotel_id') === null) {
            return Response::json([
                'success' => false,
                'message' => __('The hotel id is required!')
            ], 403);
        }

        $hotel = Hotel::find($request -> route('hotel_id'));

        if($hotel === null) {
            return Response::json([
                'success' => false,
                'message' => __('The hotel id is invalid!')
            ], 403);
        }

        if($hotel -> restaurantLocation === null) {
            return Response::json([
                'success' => false,
                'message' => __('The hotel id is not associated with a restaurant location!')
            ], 403);
        }

        if($hotel -> restaurantLocation -> restaurantProfile === null) {
            return Response::json([
                'success' => false,
                'message' => __('The restaurant location does not have an restaurant profile associated!')
            ], 403);
        }

        if($hotel -> restaurantLocation -> restaurantProfile -> user[0] -> id != $_GET['user_id']) {
            return Response::json([
                'success' => false,
                'message' => __('The hotel is not associated with logged user!')
            ], 403);
        }
        return $next($request);
    }
}
