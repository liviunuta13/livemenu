<?php

namespace App\Http\Middleware;

use App\User;
use Carbon\Carbon;
use Closure;

class checkRestaurantifOpen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $location_id = $request->route('location_id');
        $user = User::find($_GET['user_id']);

        $restaurant = $user->restaurantProfile;

        if ($restaurant === null) {
            return json_encode([
                'success' => false,
                'message' => __('The logged user does not have a restaurant profile created!')
            ]);
        }

        $location = $restaurant->restaurantLocations->find($location_id);
        if ($location === null) {
            return json_encode([
                'success' => false,
                'message' => __('The given restaurant location id is invalid!')
            ]);
        }
        date_default_timezone_set('Europe/Bucharest');
        $ziua_de_azi = date("l");
        $closed = true;
        $restaurant_exact = Shift::where('restaurant_location_id', $location_id)->where('day', $ziua_de_azi)->first();
        $now_full = now()->format('H:i:s');
        $six = mktime(6, 00, 00);
        $eleven_night = mktime(00, 00, 00);
        $eleven_almost_full = mktime(23, 59, 59);
        $elevem_almost_full_string = strtotime($eleven_almost_full);
        $starttime = Carbon::parse($restaurant_exact->openingTime)->format('H:i:s');
        $endtime = Carbon::parse($restaurant_exact->closingTime)->format('H:i:s');
        $starttime_string = strtotime($starttime);
        $endtime_string = strtotime($endtime);
        $opening_ziua_de_ieri = Shift::where('restaurant_location_id', $location_id)->where('id', $restaurant_exact->id - 1)->first();
        $opening_time_ieri_serios = $opening_ziua_de_ieri->openingTime;
        $opening_time_ieri_serios_string = strtotime($opening_time_ieri_serios);
        $strtotime_now_full = strtotime($now_full);
        if ($strtotime_now_full >= $eleven_night && $strtotime_now_full <= $six)
            $noapte = true;
        else
            $noapte = false;
        $closing_dupa_12_ieri_in_ora = Carbon::parse($opening_ziua_de_ieri->closingTimeAfter00)->format('H:i:s');
        $closing_dupa_12_string = strtotime($closing_dupa_12_ieri_in_ora);
        if ($noapte === false) {
            if ($restaurant_exact->closingTimeAfter00 === null && $restaurant_exact->closingTime != null) {
                if ($strtotime_now_full >= $starttime_string && $strtotime_now_full < $endtime_string) {
                    $closed = false;
                }
            } else if ($restaurant_exact->closingTimeAfter00 != null && $restaurant_exact->closingTime === null) {
                if ($strtotime_now_full <= $elevem_almost_full_string && $strtotime_now_full >= $opening_time_ieri_serios_string) {
                    $closed = false;
                }
            }
        } elseif ($noapte === true) {
            if ($opening_ziua_de_ieri->closingTimeAfter00 != null && $opening_ziua_de_ieri->closingTime === null)
                if ($strtotime_now_full < $closing_dupa_12_string) {
                    $closed = false;
                }


        }
        if ($closed === true) {
            return back();
        }
        return $next($request);
    }
}
