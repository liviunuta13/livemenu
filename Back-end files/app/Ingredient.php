<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $currency_id
 * @property int $unit_id
 * @property string $name
 * @property string $price
 * @property string $amount
 * @property string $allergen
 * @property Currency $currency
 * @property Unit $unit
 * @property IngredientHasAllergy[] $ingredientHasAllergies
 * @property MenuHasIngredient[] $menuHasIngredients
 */
class Ingredient extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['currency_id', 'unit_id', 'name', 'price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ingredientHasAllergies()
    {
        return $this->hasMany('App\IngredientHasAllergy', 'food_ingredient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menuHasIngredients()
    {
        return $this->hasMany('App\MenuHasIngredient');
    }
}
