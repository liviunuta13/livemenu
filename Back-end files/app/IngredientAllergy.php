<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property IngredientHasAllergy[] $ingredientHasAllergies
 */
class IngredientAllergy extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function c()
    {
        return $this->hasMany('App\IngredientHasAllergy');
    }
}
