<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $food_ingredient_id
 * @property int $ingredient_allergy_id
 * @property Ingredient $ingredient
 * @property IngredientAllergy $ingredientAllergy
 */
class IngredientHasAllergy extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ingredient_has_allergy';

    /**
     * @var array
     */
    protected $fillable = ['food_ingredient_id', 'ingredient_allergy_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ingredient()
    {
        return $this->belongsTo('App\Ingredient', 'food_ingredient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ingredientAllergy()
    {
        return $this->belongsTo('App\IngredientAllergy');
    }
}
