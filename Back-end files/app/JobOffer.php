<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $location_id
 * @property int $restaurant_profile_id
 * @property int $restaurant_location_id
 * @property int $contract_type_id
 * @property int $currency_id
 * @property int $producer_profile_id
 * @property string $begin_date
 * @property string $end_date
 * @property string $name
 * @property string $latitude
 * @property string $longitude
 * @property string $salary
 * @property string $description
 * @property ContractType $contractType
 * @property Currency $currency
 * @property Location $location
 * @property ProducerProfile $producerProfile
 * @property RestaurantProfile $restaurantProfile
 * @property RestaurantLocation $restaurantLocation
 */
class JobOffer extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['location_id', 'restaurant_location_id', 'restaurant_profile_id', 'contract_type_id', 'currency_id', 'producer_profile_id', 'name', 'latitude', 'longitude', 'salary', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contractType()
    {
        return $this->belongsTo('App\ContractType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfile()
    {
        return $this->belongsTo('App\ProducerProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfile()
    {
        return $this->belongsTo('App\RestaurantProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }
}
