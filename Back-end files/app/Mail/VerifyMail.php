<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $user_id, $confirmed_token, $new_email;

     public function __construct($user_id, $confirmed_token, $new_email)
     {
         $this -> user_id = $user_id;
         $this -> confirmed_token = $confirmed_token;
         if($new_email) { $this -> new_email = $new_email; }
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this -> view('emails.confirm_email') -> from('sender@d-soft.ro');
    }
}
