<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyMailFromAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $user_id, $confirmed_token;

     public function __construct($user_id, $confirmed_token)
     {
         $this -> user_id = $user_id;
         $this -> confirmed_token = $confirmed_token;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this -> view('emails.confirm_email_from_admin') -> from('sender@d-soft.ro');
    }
}
