<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_currency_id
 * @property int $drink_unit_id
 * @property int $food_unit_id
 * @property int $menu_id
 * @property int $restaurant_location_id
 * @property string $name
 * @property string $picture
 * @property float $price
 * @property boolean $specialty_of_the_house
 * @property boolean $specific_area_food
 * @property int $number_of_persons_that_can_eat
 * @property string $cooking_time
 * @property boolean $food
 * @property string $drink_producer
 * @property string $drink_amount
 * @property string $food_amount
 * @property string $recommended_for
 * @property Currency $currency
 * @property Menu $menumenu_conditions
 * @property RestaurantLocation $restaurantLocation
 * @property Unit $unit
 * @property EventRoomsHasMenu[] $eventRoomsHasMenus
 * @property FoodHistoric[] $foodHistorics
 * @property FoodPicture[] $foodPictures
 * @property MenuHasIngredient[] $menuHasIngredients
 */
class Menu extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'menus';

    /**
     * @var array
     */
    protected $fillable = ['restaurant_currency_id', 'drink_unit_id', 'food_unit_id', 'menu_id', 'restaurant_location_id', 'name', 'picture', 'price', 'specialty_of_the_house', 'specific_area_food', 'number_of_persons_that_can_eat', 'cooking_time', 'food', 'drink_producer', 'drink_amount', 'food_amount', 'recommended_for'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency', 'restaurant_currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitDrink()
    {
        return $this->belongsTo('App\Unit', 'drink_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitFood()
    {
        return $this->belongsTo('App\Unit', 'food_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventRoomsHasMenus()
    {
        return $this->hasMany('App\EventRoomsHasMenu');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function foodHistorics()
    {
        return $this->hasMany('App\FoodHistoric');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function foodPictures()
    {
        return $this->hasMany('App\FoodPicture');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menuHasIngredients()
    {
        return $this->hasMany('App\MenuHasIngredient');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany('App\Menu');
    }
}
