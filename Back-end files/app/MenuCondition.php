<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $start_date
 * @property string $end_date
 */
class MenuCondition extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['days_of_week', 'menu_type', 'start_time', 'end_time'];

}
