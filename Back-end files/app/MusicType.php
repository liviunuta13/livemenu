<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property ClientProfileHasMusicType[] $clientProfileHasMusicTypes
 * @property RestaurantLocation[] $restaurantLocations
 */
class MusicType extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientProfileHasMusicTypes()
    {
        return $this->hasMany('App\ClientProfileHasMusicType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocations()
    {
        return $this->hasMany('App\RestaurantLocation');
    }
}
