<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $profesionist_profile_id
 * @property string $matern_language
 * @property boolean $managerial_skills
 * @property boolean $digital_skills
 * @property ProfesionistProfile $profesionistProfile
 */
class PersonalSkill extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['profesionist_profile_id', 'matern_language', 'managerial_skills', 'digital_skills'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile');
    }
}
