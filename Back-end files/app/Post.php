<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $location_checkin_producer_profile_id
 * @property int $location_checkin_restaurant_profile_id
 * @property int $from_restaurant_location_id
 * @property int $to_restaurant_location_id
 * @property int $from_profile_id
 * @property int $to_client_profile_id
 * @property int $from_producer_profile_id
 * @property int $to_producer_profile_id
 * @property int $from_profesionist_profile_id
 * @property int $to_profesionist_profile_id
 * @property int $from_restaurant_profile_id
 * @property int $to_restaurant_profile_id
 * @property string $description
 * @property ClientProfile $clientProfile
 * @property ProducerProfile $producerProfile
 * @property ProfesionistProfile $profesionistProfile
 * @property RestaurantLocation $restaurantLocation
 * @property RestaurantProfile $restaurantProfile
 * @property PostPicture[] $postPictures
 * @property RestaurantLocationHasPost[] $restaurantLocationHasPosts
 */
class Post extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['location_checkin_producer_profile_id', 'location_checkin_restaurant_profile_id', 'from_restaurant_location_id', 'to_restaurant_location_id', 'from_client_profile_id', 'to_client_profile_id', 'from_producer_profile_id', 'to_producer_profile_id', 'from_profesionist_profile_id', 'to_profesionist_profile_id', 'from_restaurant_profile_id', 'to_restaurant_profile_id', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientProfileFrom()
    {
        return $this->belongsTo('App\ClientProfile', 'from_client_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientProfileTo()
    {
        return $this->belongsTo('App\ClientProfile', 'to_client_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfile()
    {
        return $this->belongsTo('App\ProducerProfile', 'location_checkin_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileFrom()
    {
        return $this->belongsTo('App\ProducerProfile', 'from_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileTo()
    {
        return $this->belongsTo('App\ProducerProfile', 'to_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfileFrom()
    {
        return $this->belongsTo('App\ProfesionistProfile', 'from_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfileTo()
    {
        return $this->belongsTo('App\ProfesionistProfile', 'to_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationFrom()
    {
        return $this->belongsTo('App\RestaurantLocation', 'from_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationTo()
    {
        return $this->belongsTo('App\RestaurantLocation', 'to_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfile()
    {
        return $this->belongsTo('App\RestaurantProfile', 'location_checkin_restaurant_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfileFrom()
    {
        return $this->belongsTo('App\RestaurantProfile', 'from_restaurant_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfileTo()
    {
        return $this->belongsTo('App\RestaurantProfile', 'to_restaurant_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postPictures()
    {
        return $this->hasMany('App\PostPicture');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocationHasPosts()
    {
        return $this->hasMany('App\RestaurantLocationHasPost');
    }
}
