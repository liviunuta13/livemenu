<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $post_id
 * @property string $picture
 * @property Post $post
 */
class PostPicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['post_id', 'picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
