<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProducerLocationPromotion extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['producer_location_id', 'product_id','begin_date', 'end_date', 'begin_time', 'end_time', 'description', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerLocation()
    {
        return $this->belongsTo('App\ProducerProfileLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
