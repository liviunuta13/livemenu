<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProducerLocationStaff extends Model 
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'producer_location_staff';

    /**
     * @var array
     */
    protected $fillable = ['producer_location_id', 'contract_type_id', 'salary_currency_id', 'profesionist_profile_id', 'client_profile_id', 'salary', 'start_date', 'end_date', 'job'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contractType()
    {
        return $this->belongsTo('App\ContractType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency', 'salary_currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerLocation()
    {
        return $this->belongsTo('App\ProducerProfileLocation');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  */
    // public function clientProfile()
    // {
    //     return $this->belongsTo('App\ClientProfile');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
    public function role()
    {
       return $this->belongsToMany('App\ProducerLocationStaffRole', 'producer_location_staff_has_role', 'staff_id', 'role_id');
    }
}
