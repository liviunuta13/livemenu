<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProducerLocationStaffRole extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'producer_location_staff_roles';

    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producerLocationStaffHasRole()
    {
        return $this->hasMany('App\ProducerLocationStaffHasRole');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
    public function roleStaff()
    {
       return $this->belongsToMany('App\RestaurantLocationStaff', 'restaurant_location_staff_has_role', 'role_id', 'staff_id');
    }
}
