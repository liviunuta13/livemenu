<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $producer_type_id
 * @property string $name
 * @property string $logo
 * @property string $description
 * @property string $fiscal_code
 * @property string $company_name
 * @property string $administrator_name_surname
 * @property string $website
 * @property string $email
 * @property string $phone_number
 * @property string $suspended
 * @property ProducerType $producerType
 * @property AffiliatedProducer[] $affiliatedProducers
 * @property BuyedSponsorship[] $buyedSponsorships
 * @property BuyedSponsorship[] $buyedSponsorships
 * @property BuyedSponsorship[] $buyedSponsorships
 * @property Follow[] $follows
 * @property JobOffer[] $jobOffers
 * @property Post[] $posts
 * @property Post[] $posts
 * @property Post[] $posts
 * @property ProducerProfileHasPartner[] $producerProfileHasPartners
 * @property ProducerProfileLocation[] $producerProfileLocations
 * @property ProducerProfilePicture[] $producerProfilePictures
 * @property Rating[] $ratings
 * @property Rating[] $ratings
 * @property User[] $users
 */
class ProducerProfile extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['producer_type_id', 'name', 'logo', 'description', 'fiscal_code', 'company_name', 'administrator_name_surname', 'website', 'email', 'phone_number', 'suspended', 'confirmed'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerType()
    {
        return $this->belongsTo('App\ProducerType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliatedProducers()
    {
        return $this->hasMany('App\AffiliatedProducer', 'producer_profile_for_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buyedSponsorshipsFor()
    {
        return $this->hasMany('App\BuyedSponsorship', 'for_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buyedSponsorships()
    {
        return $this->hasMany('App\BuyedSponsorship');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function buyedSponsorships()
    // {
    //     return $this->hasMany('App\BuyedSponsorship', 'for_producer_profile_id');
    // }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function follows()
    // {
    //     return $this->hasMany('App\Follow', 'to_producer_profile_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobOffers()
    {
        return $this->hasMany('App\JobOffer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsLocationCheckin()
    {
        return $this->hasMany('App\Post', 'location_checkin_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsFrom()
    {
        return $this->hasMany('App\Post', 'from_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsTo()
    {
        return $this->hasMany('App\Post', 'to_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producerProfileHasPartners()
    {
        return $this->hasMany('App\ProducerProfileHasPartner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producerProfileLocations()
    {
        return $this->hasMany('App\ProducerProfileLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producerProfilePictures()
    {
        return $this->hasMany('App\ProducerProfilePicture');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function ratingsTo()
    // {
    //     return $this->hasMany('App\Rating', 'from_producer_profile_id');
    // }
    //
    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function ratings()
    // {
    //     return $this->hasMany('App\Rating', 'to_producer_profile_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasOne('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producerStaff()
    {
        return $this->hasMany('App\ProducerLocationStaff', 'producer_id');
    }
}
