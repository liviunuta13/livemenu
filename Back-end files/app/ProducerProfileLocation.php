<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $city_id
 * @property int $county_id
 * @property int $country_id
 * @property int $producer_profile_id
 * @property string $name
 * @property string $street
 * @property string $number
 * @property string $postal_code
 * @property string $latitude
 * @property string $longitude
 * @property string $description
 * @property string $email
 * @property string $phone_number
 * @property City $city
 * @property County $county
 * @property Country $country
 * @property ProducerProfile $producerProfile
 * @property ProducerProfileLocationPicture[] $producerProfileLocationPictures
 */
class ProducerProfileLocation extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['city_id', 'county_id', 'country_id', 'producer_profile_id', 'name', 'street', 'number', 'postal_code', 'latitude', 'longitude', 'description', 'email', 'phone_number', 'fixed_phone_number', 'contact_person_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function county()
    {
        return $this->belongsTo('App\County');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfile()
    {
        return $this->belongsTo('App\ProducerProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producerProfileLocationPictures()
    {
        return $this->hasMany('App\ProducerProfileLocationPicture');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany('App\Rating', 'to_producer_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratingsFor()
    {
        return $this->hasMany('App\Rating', 'form_producer_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function follows()
    {
        return $this->hasMany('App\Follow', 'to_producer_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function staff()
    {
        return $this->hasMany('App\ProducerLocationStaff', 'producer_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Product', 'producer_location_has_products', 'producer_location_id', 'product_id');
    }
}
