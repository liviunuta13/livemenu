<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $producer_profile_location_id
 * @property string $picture
 * @property ProducerProfileLocation $producerProfileLocation
 */
class ProducerProfileLocationPicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['producer_profile_location_id', 'picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileLocation()
    {
        return $this->belongsTo('App\ProducerProfileLocation');
    }
}
