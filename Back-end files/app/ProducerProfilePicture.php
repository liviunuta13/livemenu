<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $producer_profile_id
 * @property string $name
 * @property ProducerProfile $producerProfile
 */
class ProducerProfilePicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['producer_profile_id', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfile()
    {
        return $this->belongsTo('App\ProducerProfile');
    }
}
