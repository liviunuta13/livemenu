<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProducerStaffHasRole extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'producer_staff_has_roles';

    /**
     * @var array
     */
    protected $fillable = ['staff_id', 'role_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
     public function staff()
     {
         return $this->belongsTo('App\ProducerStaff', 'staff_id');
     }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('App\StaffRole', 'role_id');
    }
}
