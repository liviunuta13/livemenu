<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $currency_id
 * @property string $name
 * @property int $price
 * @property boolean $price_per_amount
 * @property string $description
 * @property boolean $special_product
 * @property boolean $unique_product
 * @property boolean $specific_area_product
 * @property Currency $currency
 * @property AffiliatedProductsAd[] $affiliatedProductsAds
 * @property ProductPromotion[] $productPromotions
 * @property ProfilePromote[] $profilePromotes
 */
class Product extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['currency_id', 'name', 'price', 'price_per_amount', 'description', 'special_product', 'unique_product', 'specific_area_product', 'product_id', 'producer_location_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliatedProductsAds()
    {
        return $this->hasMany('App\AffiliatedProductsAd');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productPromotions()
    {
        return $this->hasMany('App\ProductPromotion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profilePromotes()
    {
        return $this->hasMany('App\ProfilePromote');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany('App\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pictures()
    {
        return $this->hasMany('App\ProductPicture');
    }
}
