<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $partner_id
 * @property string $name
 * @property Partner $partner
 */
class ProductForSponsorship extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['partner_id', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }
}
