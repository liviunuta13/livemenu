<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $product_id
 * @property string $name
 * @property string $description
 * @property string $begin_date
 * @property string $end_date
 * @property string $begin_time
 * @property string $end_time
 * @property Product $product
 */
class ProductPromotion extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'name', 'description', 'begin_date', 'end_date', 'begin_time', 'end_time'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
