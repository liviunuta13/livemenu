<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $street_and_number
 * @property string $postal_code
 * @property string $phone_number
 * @property string $website
 * @property boolean $is_looking_for_job
 * @property boolean $activated_accont
 * @property boolean $is_at_working
 * @property boolean $available_for_working
 * @property boolean $suspended
 * @property City $city
 * @property BuyedSponsorship[] $buyedSponsorships
 * @property Follow[] $follows
 * @property PersonalSkill[] $personalSkills
 * @property Post[] $posts
 * @property ProfesionistProfileHasForeignLanguage[] $profesionistProfileHasForeignLanguages
 * @property ProfesstionalHistoric[] $professtionalHistorics
 * @property Rating[] $ratings
 * @property RestaurantStaff[] $restaurantStaffs
 * @property SkillsAtWorkingPlace[] $skillsAtWorkingPlaces
 * @property User[] $users
 * @property WorkRequest[] $workRequests
 */
class ProfesionistProfile extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'personal_description',
        'email', 'street_and_number',
        'postal_code',
        'city_id',
        'county_id',
        'country_id',
        'phone_number',
        'website',
        'professional_experience',
        'education_begin_date',
        'education_end_date',
        'education_certification_name',
        'education_institution_name',
        'education_city_id',
        'education_description',
        'matern_language_id',
        'managerial_skills',
        'skills_at_working_place',
        'digital_skills',
        'driving_licence',
        'other_skills',
        'is_looking_for_job',
        'activated_accont',
        'is_at_working',
        'available_for_working',
        'suspended'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function county()
    {
        return $this->belongsTo('App\County');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  */
    // public function educationCity()
    // {
    //     return $this->belongsTo('App\City');
    // }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  */
    // public function educationCountry()
    // {
    //     return $this->belongsTo('App\Country');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buyedSponsorships()
    {
        return $this->hasMany('App\BuyedSponsorship', 'for_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function follows()
    {
        return $this->hasMany('App\Follow', 'to_profesionist_profile_id');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function followsFrom()
    // {
    //     return $this->hasMany('App\Follow', 'to_profesionist_profile_id');
    // }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function personalSkills()
    // {
    //     return $this->hasMany('App\PersonalSkill');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsFrom()
    {
        return $this->hasMany('App\Post', 'to_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsTo()
    {
        return $this->hasMany('App\Post', 'from_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profesionistProfileHasForeignLanguages()
    {
        return $this->hasMany('App\ProfesionistProfileHasForeignLanguage');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function professionalHistory()
    {
        return $this->hasMany('App\ProfessionalHistory');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function education()
    {
        return $this->hasMany('App\ProfessionistEducation');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function ratingsFor()
    // {
    //     return $this->hasMany('App\Rating', 'from_profesionist_profile_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany('App\Rating', 'to_profesionist_profile_id');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function skillsAtWorkingPlaces()
    // {
    //     return $this->hasMany('App\SkillsAtWorkingPlace');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workRequests()
    {
        return $this->hasMany('App\WorkRequest');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workingForRestaurantsLocations()
    {
        return $this->belongsTo('App\RestaurantLocationStaff');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workingForProducersLocations()
    {
        return $this->belongsTo('App\RestaurantLocationStaff');
    }
}
