<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @property int $id
 * @property int $profesionist_profile_id
 * @property int $foreign_language_id
 * @property ForeignLanguage $foreignLanguage
 * @property ProfesionistProfile $profesionistProfile
 */
class ProfesionistProfileHasForeignLanguage extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'profesionist_profile_has_foreign_language';

    /**
     * @var array
     */
    protected $fillable = ['profesionist_profile_id', 'foreign_language_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function foreignLanguage()
    {
        return $this->belongsTo('App\ForeignLanguage');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile');
    }
}
