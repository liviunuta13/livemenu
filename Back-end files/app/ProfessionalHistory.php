<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $profesionist_profile_id
 * @property int $city_id
 * @property string $begin_date
 * @property boolean $is_working
 * @property string $end_date
 * @property string $working_as
 * @property string $employer_company_name
 * @property string $description
 * @property string $driving_license
 * @property string $other_skills
 * @property City $city
 * @property ProfesionistProfile $profesionistProfile
 */
class ProfessionalHistory extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'professional_history';

    /**
     * @var array
     */
    protected $fillable = [
        'profesionist_profile_id',
        'begin_date',
        'present',
        'end_date',
        'working_as',
        'company_name',
        'description',
        'city_id',
        'country_id',
        'driving_license',
        'other_skills'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile');
    }
}
