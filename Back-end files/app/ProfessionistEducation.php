<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $description
 * @property string $certificate_name
 * @property string $institution_name
 * @property int $city_id
 * @property int $country_id
 * @property int $profesionist_profile_id
 * @property string  $begin_date
 * @property string  $end_date
 * @property string  $present
 * @property RestaurantLocationHasFacility[] $restaurantLocationHasFacilities
 * @property City $city
 * @property Country $country
 * @property ProfesionistProfile $profesionistProfile
 */

class ProfessionistEducation extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'professionist_education';

    /**
     * @var array
     */
    protected $fillable = [
        'profesionist_profile_id',
        'begin_date',
        'present',
        'end_date',
        'certificate_name',
        'institution_name',
        'description',
        'city_id',
        'country_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile');
    }
}
