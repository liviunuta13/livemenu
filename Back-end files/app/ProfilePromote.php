<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $administrator_id
 * @property int $product_id
 * @property int $event_id
 * @property Administrator $administrator
 * @property Product $product
 * @property RestaurantLocationEvent $restaurantLocationEvent
 */
class ProfilePromote extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['administrator_id', 'product_id', 'event_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function administrator()
    {
        return $this->belongsTo('App\Administrator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationEvent()
    {
        return $this->belongsTo('App\RestaurantLocationEvent', 'event_id');
    }
}
