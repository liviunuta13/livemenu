<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $from_user_id
 * @property int $from_profesionist_profile_id
 * @property int $from_producer_profile_id
 * @property int $from_restaurant_location_id
 * @property int $to_restaurant_profile_id
 * @property int $to_producer_profile_id
 * @property int $to_profesionist_profile_id
 * @property string $description
 * @property int $stars
 * @property ProducerProfile $producerProfile
 * @property ProducerProfile $producerProfile
 * @property ProfesionistProfile $profesionistProfile
 * @property ProfesionistProfile $profesionistProfile
 * @property RestaurantLocation $restaurantLocation
 * @property RestaurantProfile $restaurantProfile
 * @property User $user
 * @property RatingPicture[] $ratingPictures
 */
class Rating extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
         // 'from_user_id',
         'from_profesionist_profile_id',
         'to_profesionist_profile_id',
         // 'from_producer_profile_id',
         'from_restaurant_location_id',
         // 'to_restaurant_profile_id',
         'to_restaurant_location_id',
         'from_producer_location_id',
         'to_producer_location_id',
         // 'to_profesionist_profile_id',
         'description',
         'stars'
    ];

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  */
    // public function producerProfileFrom()
    // {
    //     return $this->belongsTo('App\ProducerProfile', 'from_producer_profile_id');
    // }
    //
    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  */
    // public function producerProfileTo()
    // {
    //     return $this->belongsTo('App\ProducerProfile', 'to_producer_profile_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfileFrom()
    {
        return $this->belongsTo('App\ProfesionistProfile', 'from_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfileTo()
    {
        return $this->belongsTo('App\ProfesionistProfile', 'to_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationFrom()
    {
        return $this->belongsTo('App\RestaurantLocation', 'from_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationTo()
    {
        return $this->belongsTo('App\RestaurantLocation', 'to_restaurant_location_id');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  */
    // public function restaurantProfile()
    // {
    //     return $this->belongsTo('App\RestaurantProfile', 'to_restaurant_profile_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileLocationFrom()
    {
        return $this->belongsTo('App\ProducerProfileLocation', 'from_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileLocationTo()
    {
        return $this->belongsTo('App\ProducerProfileLocation', 'to_restaurant_location_id');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  */
    // public function user()
    // {
    //     return $this->belongsTo('App\User', 'from_user_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratingPictures()
    {
        return $this->hasMany('App\RatingPicture');
    }
}
