<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $rating_id
 * @property string $picture
 * @property Rating $rating
 */
class RatingPicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['rating_id', 'picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rating()
    {
        return $this->belongsTo('App\Rating');
    }
}
