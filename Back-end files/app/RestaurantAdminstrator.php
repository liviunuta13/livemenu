<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_profile_id
 * @property string $name
 * @property string $email
 * @property string $phone_number
 * @property string $password
 * @property RestaurantProfile $restaurantProfile
 */
class RestaurantAdminstrator extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['restaurant_profile_id', 'name', 'email', 'phone_number', 'password'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfile()
    {
        return $this->belongsTo('App\RestaurantProfile');
    }
}
