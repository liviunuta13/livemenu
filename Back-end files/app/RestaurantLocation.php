<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_profile_id
 * @property int $city_id
 * @property int $county_id
 * @property int $country_id
 * @property int $music_type_id
 * @property string $name
 * @property string $street
 * @property string $number
 * @property string $postal_code
 * @property string $latitude
 * @property string $longitude
 * @property string $description
 * @property string $phone_number
 * @property string $email
 * @property string $locality_specific
 * @property int $tables_number
 * @property int $bathrooms_number
 * @property int $locality_surface
 * @property boolean $live_music
 * @property boolean $catering
 * @property string $number_of_seats
 * @property string $framing
 * @property string $michelin_stars
 * @property boolean $owns_event_room
 * @property City $city
 * @property County $county
 * @property Country $country
 * @property MusicType $musicType
 * @property RestaurantProfile $restaurantProfile
 * @property BuyedSponsorship[] $buyedSponsorships
 * @property BuyedSponsorship[] $buyedSponsorships
 * @property DailyMenuAvailability[] $dailyMenuAvailabilities
 * @property EventRoom[] $eventRooms
 * @property Menu[] $menus
 * @property Post[] $posts
 * @property Post[] $post
 * @property Rating[] $ratings
 * @property RestaurantLocationEvent[] $restaurantLocationEvents
 * @property RestaurantLocationHasFacility[] $restaurantLocationHasFacilities
 * @property RestaurantLocationHasPost[] $restaurantLocationHasPosts
 * @property RestaurantLocationUtility[] $restaurantLocationUtilities
 * @property RestaurantLocationPicture[] $restaurantLocationPictures
 * @property RestaurantLocationProgram[] $restaurantLocationPrograms
 * @property RestaurantLocationPromotion[] $restaurantLocationPromotions
 * @property RestaurantStaff[] $restaurantStaffs
 * @property SponsorshipsRequest[] $sponsorshipsRequests
 */
class RestaurantLocation extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['restaurant_profile_id', 'city_id', 'county_id', 'country_id', 'music_type_id', 'name', 'street', 'number', 'postal_code', 'latitude', 'longitude', 'description', 'phone_number', 'email', 'locality_specific', 'tables_number', 'bathrooms_number', 'locality_surface', 'live_music', 'catering', 'number_of_seats', 'framing', 'michelin_stars', 'owns_event_room'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function county()
    {
        return $this->belongsTo('App\County');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function musicType()
    {
        return $this->belongsTo('App\MusicType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function follows()
    {
        return $this->hasMany('App\Follow', 'to_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfile()
    {
        return $this->belongsTo('App\RestaurantProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buyedSponsorships()
    {
        return $this->hasMany('App\BuyedSponsorship');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buyedSponsorshipsFor()
    {
        return $this->hasMany('App\BuyedSponsorship', 'for_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dailyMenuAvailabilities()
    {
        return $this->hasMany('App\DailyMenuAvailability');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventRooms()
    {
        return $this->hasMany('App\EventRoom');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menus()
    {
        return $this->hasMany('App\Menu');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsFrom()
    {
        return $this->hasMany('App\Post', 'from_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsTo()
    {
        return $this->hasMany('App\Post', 'to_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany('App\Rating', 'to_restaurant_location_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratingsFor()
    {
        return $this->hasMany('App\Rating', 'from_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocationEvents()
    {
        return $this->hasMany('App\RestaurantLocationEvent');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocationHasFacilities()
    {
        return $this->hasMany('App\RestaurantLocationHasFacility');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocationHasPosts()
    {
        return $this->hasMany('App\RestaurantLocationHasPost');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function restaurantLocationUtilities()
    {
        return $this->belongsToMany('App\RestaurantLocationUtility', 'restaurant_location_has_utility', null, 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocationPictures()
    {
        return $this->hasMany('App\RestaurantLocationPicture');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocationPrograms()
    {
        return $this->hasMany('App\RestaurantLocationProgram');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocationPromotions()
    {
        return $this->hasMany('App\RestaurantLocationPromotion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sponsorshipsRequests()
    {
        return $this->hasMany('App\SponsorshipsRequest', 'restaurant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hotels()
    {
        return $this->hasMany('App\Hotel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs()
    {
        return $this->hasMany('App\JobOffer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function staff()
    {
        return $this->hasMany('App\RestaurantLocationStaff', 'restaurant_location_id');
    }
}
