<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_id
 * @property string $name
 * @property string $theme
 * @property string $description
 * @property string $banner
 * @property boolean $one_day_event
 * @property boolean $multi_day_event
 * @property string $begin_date
 * @property string $end_date
 * @property string $begin_time
 * @property string $end_time
 * @property RestaurantLocation $restaurantLocation
 * @property ProfilePromote[] $profilePromotes
 */
class RestaurantLocationEvent extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['restaurant_location_id', 'name', 'theme', 'description', 'banner', 'one_day_event', 'multi_day_event', 'begin_date', 'end_date', 'begin_hour', 'end_hour'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profilePromotes()
    {
        return $this->hasMany('App\ProfilePromote', 'event_id');
    }
}
