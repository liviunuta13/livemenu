<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $icon
 * @property string $description
 * @property RestaurantLocationHasFacility[] $restaurantLocationHasFacilities
 */
class RestaurantLocationFacility extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name', 'icon', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocationHasFacilities()
    {
        return $this->hasMany('App\RestaurantLocationHasFacility');
    }
}
