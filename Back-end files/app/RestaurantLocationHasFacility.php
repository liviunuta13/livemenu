<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_id
 * @property int $restaurant_location_facility_id
 * @property RestaurantLocationFacility $restaurantLocationFacility
 * @property RestaurantLocation $restaurantLocation
 */
class RestaurantLocationHasFacility extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurant_location_has_facility';

    /**
     * @var array
     */
    protected $fillable = ['restaurant_location_id', 'restaurant_location_facility_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationFacility()
    {
        return $this->belongsTo('App\RestaurantLocationFacility');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }
}
