<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_id
 * @property int $post_id
 * @property Post $post
 * @property RestaurantLocation $restaurantLocation
 */
class RestaurantLocationHasPost extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['restaurant_location_id', 'post_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }
}
