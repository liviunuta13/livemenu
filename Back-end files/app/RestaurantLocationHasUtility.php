<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_id
 * @property RestaurantLocationUtility $restaurantLocationUtility
 * @property RestaurantLocation $restaurantLocation
 */
class RestaurantLocationHasUtility extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurant_location_has_utility';

    /**
     * @var array
     */
    protected $fillable = ['restaurant_location_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationUtility()
    {
        return $this->belongsTo('App\RestaurantLocationUtility', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }
}
