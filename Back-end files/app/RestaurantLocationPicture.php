<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_id
 * @property string $picture
 * @property RestaurantLocation $restaurantLocation
 */
class RestaurantLocationPicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $table= 'restaurant_location_pictures';

    protected $fillable = ['restaurant_location_id', 'picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }
}
