<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_id
 * @property int $drinks_menu_category_id
 * @property int $food_menu_category_id
 * @property int $product_id
 * @property string $begin_date
 * @property string $end_date
 * @property string $begin_time
 * @property string $end_time
 * @property string $description
 * @property string $name
 * @property RestaurantLocation $restaurantLocation
 */
class RestaurantLocationPromotion extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['restaurant_location_id', 'product_id', 'drinks_menu_category_id', 'food_menu_category_id', 'begin_date', 'end_date', 'begin_time', 'end_time', 'description', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocation()
    {
        return $this->belongsTo('App\RestaurantLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo('App\Menu', 'product_id');
    }

}
