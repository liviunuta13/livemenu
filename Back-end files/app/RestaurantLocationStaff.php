<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $restaurant_location_ids
 * @property int $contract_type_id
 * @property int $salary_currency_id
 * @property int $profesionist_profile_id
 * @property int $client_profile_id
 * @property int $restaurant_location_id
 * @property string $salary
 * @property string $start_date
 * @property string $end_date
 * @property string $job
 * @property ContractType $contractType
 * @property Currency $currency
 * @property RestaurantLocation $restaurantLocation
 * @property ClientProfile $clientProfile
 * @property ProfesionistProfile $profesionist
 * @property Location $location
 * @property RestaurantLocationStaffRole $role
 */
class RestaurantLocationStaff extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurant_location_staff';

    /**
     * @var array
     */
    protected $fillable = ['restaurant_location_id', 'contract_type_id', 'salary_currency_id', 'profesionist_profile_id', 'client_profile_id', 'salary', 'start_date', 'end_date', 'job'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contractType()
    {
        return $this->belongsTo('App\ContractType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency', 'salary_currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo('App\RestaurantLocation', 'restaurant_location_id');
    }

     /**
      * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
      */
     public function clientProfile()
     {
         return $this->belongsTo('App\ClientProfile');
     }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionist()
    {
        return $this->belongsTo('App\ProfesionistProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function role()
    {
        return $this->belongsToMany('App\RestaurantLocationStaffRole', 'restaurant_location_staff_has_role', 'staff_id', 'role_id');
    }
}
