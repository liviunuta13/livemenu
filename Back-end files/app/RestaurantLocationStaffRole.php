<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantLocationStaffRole extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurant_location_staff_roles';

    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocationStaffHasRole()
    {
        return $this->hasMany('App\RestaurantLocationStaffHasRole');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
    public function roleStaff()
    {
       return $this->belongsToMany('App\RestaurantLocationStaff', 'restaurant_location_staff_has_role', 'role_id', 'staff_id');
    }
}
