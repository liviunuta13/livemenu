<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property RestaurantLocation[] $restaurantLocations
 */
class RestaurantLocationUtility extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function restaurantLocations()
    {
        return $this->belongsToMany('App\RestaurantLocation', 'restaurant_location_has_utility', 'id');
    }
}
