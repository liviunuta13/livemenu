<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property string $description
 * @property string $fiscal_code
 * @property string $company_name
 * @property string $administrator_name
 * @property string $website
 * @property string $cmpany_email
 * @property string $phone_number
 * @property string $suspended
 * @property Follow[] $follows
 * @property JobOffer[] $jobOffers
 * @property Post[] $posts
 * @property Post[] $posts
 * @property Post[] $posts
 * @property Rating[] $ratings
 * @property RestaurantAdminstrator[] $restaurantAdminstrators
 * @property RestaurantLocation[] $restaurantLocations
 * @property User[] $users
 */
class RestaurantProfile extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name', 'logo', 'description', 'fiscal_code', 'company_name', 'administrator_name', 'website', 'cmpany_email', 'phone_number', 'suspended', 'confirmed'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function follows()
    {
        return $this->hasMany('App\Follow', 'to_restaurant_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobOffers()
    {
        return $this->hasMany('App\JobOffer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsLocationCheckin()
    {
        return $this->hasMany('App\Post', 'location_checkin_restaurant_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsFrom()
    {
        return $this->hasMany('App\Post', 'from_restaurant_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsTo()
    {
        return $this->hasMany('App\Post', 'to_restaurant_profile_id');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function ratings()
    // {
    //     return $this->hasMany('App\Rating', 'to_restaurant_profile_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantAdminstrators()
    {
        return $this->hasMany('App\RestaurantAdminstrator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantLocations()
    {
        return $this->hasMany('App\RestaurantLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany('App\User');
    }

//     /**
//      * @return \Illuminate\Database\Eloquent\Relations\HasMany
//      */
//     public function restaurantStaffs()
//     {
//         return $this->hasMany('App\RestaurantStaff');
//     }
}
