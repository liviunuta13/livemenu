<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $hotel_id
 * @property int $unit_id
 * @property string $name
 * @property string $surface
 * @property int $seats
 * @property int $additional_seats
 * @property int $beds_number
 * @property Hotel $hotel
 * @property Unit $unit
 * @property RoomBedType[] $roomBedTypes
 * @property RoomHasFacility[] $roomHasFacilities
 * @property RoomPicture[] $roomPictures
 */
class Room extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['hotel_id', 'unit_id', 'name', 'surface', 'seats', 'additional_seats', 'beds_number'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomHasBedTypes()
    {
        return $this->hasMany('App\RoomHasBedType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomHasFacilities()
    {
        return $this->hasMany('App\RoomHasFacility');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomPictures()
    {
        return $this->hasMany('App\RoomPicture');
    }
}
