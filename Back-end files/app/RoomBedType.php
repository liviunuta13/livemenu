<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $room_id
 * @property string $name
 * @property int $hotel_id
 * @property Room $room
 */
class RoomBedType extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['room_id', 'name', 'hotel_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo('App\Room');
    }
}
