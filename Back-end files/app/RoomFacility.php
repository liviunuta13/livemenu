<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $icon
 * @property RoomHasFacility[] $roomHasFacilities
 */
class RoomFacility extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name', 'icon'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomHasFacilities()
    {
        return $this->hasMany('App\RoomHasFacility');
    }
}
