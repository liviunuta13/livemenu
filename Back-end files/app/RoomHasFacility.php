<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $room_id
 * @property int $room_facility_id
 * @property int $hotel_id
 * @property RoomFacility $roomFacility
 * @property Room $room
 * @property Hotel $hotel
 */
class RoomHasFacility extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'room_has_facility';

    /**
     * @var array
     */
    protected $fillable = ['room_id', 'room_facility_id', 'hotel_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function roomFacility()
    {
        return $this->belongsTo('App\RoomFacility');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }
}
