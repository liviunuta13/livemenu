<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $room_id
 * @property string $picture
 * @property Room $room
 */
class RoomPicture extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['room_id', 'picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo('App\Room');
    }
}
