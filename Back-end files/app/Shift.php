<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $day
 * @property string $openingTime
 * @property string $closingTime
 * @property string $closingTimeAfter00
 */
class Shift extends Model
{
    protected  $fillable = ['restaurant_location_id','day','openingTime','closingTime'];

    public function restaurantLocation(){
        $this->belongsTo('App\RestaurantLocation');
    }
}
