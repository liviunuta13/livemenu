<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $profesionist_profile_id
 * @property string $name
 * @property ProfesionistProfile $profesionistProfile
 */
class SkillsAtWorkingPlace extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'skills_at_working_place';

    /**
     * @var array
     */
    protected $fillable = ['profesionist_profile_id', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile');
    }
}
