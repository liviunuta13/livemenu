<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $partner_id
 * @property int $restaurant_location_id
 * @property Partner $partner
 * @property RestaurantLocation $restaurantLocation
 */
class SponsorshipsRequest extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['partner_id', 'restaurant_id', 'accepted'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfile()
    {
        return $this->belongsTo('App\RestaurantProfile');
    }
}
