<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tip extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'tip_type_id',
        'number',
        'from_user_id',
        'to_user_id',
        'from_client_profile_id',
        'to_client_profile_id',
        'from_producer_profile_id',
        'to_producer_profile_id',
        'from_profesionist_profile_id',
        'to_profesionist_profile_id',
        'from_restaurant_profile_id',
        'to_restaurant_profile_id',
        'from_restaurant_location_id',
        'to_restaurant_location_id',
        'from_administrator_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userFrom()
    {
        return $this->belongsTo('App\User', 'from_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userTo()
    {
        return $this->belongsTo('App\User', 'to_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientProfileFrom()
    {
        return $this->belongsTo('App\ClientProfile', 'from_client_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientProfileTo()
    {
        return $this->belongsTo('App\ClientProfile', 'to_client_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileFrom()
    {
        return $this->belongsTo('App\ProducerProfile', 'from_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfileTo()
    {
        return $this->belongsTo('App\ProducerProfile', 'to_producer_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfileFrom()
    {
        return $this->belongsTo('App\ProfesionistProfile', 'from_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfileTo()
    {
        return $this->belongsTo('App\ProfesionistProfile', 'to_profesionist_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationFrom()
    {
        return $this->belongsTo('App\RestaurantLocation', 'from_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantLocationTo()
    {
        return $this->belongsTo('App\RestaurantLocation', 'to_restaurant_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfileFrom()
    {
        return $this->belongsTo('App\RestaurantProfile', 'from_restaurant_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfileTo()
    {
        return $this->belongsTo('App\RestaurantProfile', 'to_restaurant_profile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function administratorTo()
    {
        return $this->belongsTo('App\Administrator', 'from_administrator_id');
    }

}
