<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $currency_id
 * @property string $name
 * @property string $price
 * @property Currency $currency
 */
class TipsType extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['currency_id', 'name', 'price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }
}
