<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property AffiliatedProductsAd[] $affiliatedProductsAds
 * @property EventRoom[] $eventRooms
 * @property Ingredient[] $ingredients
 * @property Menu[] $menus
 * @property Room[] $rooms
 */
class Unit extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliatedProductsAdsFinal()
    {
        return $this->hasMany('App\AffiliatedProductsAd', 'final_amount_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliatedProductsAdsCurrent()
    {
        return $this->hasMany('App\AffiliatedProductsAd', 'current_amount_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventRooms()
    {
        return $this->hasMany('App\EventRoom', 'surface_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ingredients()
    {
        return $this->hasMany('App\Ingredient');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menusesDrink()
    {
        return $this->hasMany('App\Menu', 'drink_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menusesFood()
    {
        return $this->hasMany('App\Menu', 'food_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany('App\Room');
    }
}
