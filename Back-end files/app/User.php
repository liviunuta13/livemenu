<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $client_profile_id
 * @property int $profesionist_profile_id
 * @property int $restaurant_profile_id
 * @property int $producer_profile_id
 * @property int $restaurant_specific_id
 * @property string $email
 * @property string $password
 * @property string $nickname
 * @property string $profile_picture
 * @property string $secret_ingredient
 * @property string $favorite_food
 * @property ClientProfile $clientProfile
 * @property ProducerProfile $producerProfile
 * @property ProfesionistProfile $profesionistProfile
 * @property RestaurantProfile $restaurantProfile
 * @property RestaurantSpecific $restaurantSpecific
 * @property Balance[] $balances
 * @property Balance[] $balances
 * @property Follow[] $follows
 * @property Rating[] $ratings
 */
class User extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['client_profile_id', 'profesionist_profile_id', 'restaurant_profile_id', 'producer_profile_id', 'restaurant_specific_id', 'email', 'password', 'nickname', 'profile_picture', 'secret_ingredient', 'favorite_food'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientProfile()
    {
        return $this->belongsTo('App\ClientProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producerProfile()
    {
        return $this->belongsTo('App\ProducerProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantProfile()
    {
        return $this->belongsTo('App\RestaurantProfile','restaurant_profile_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantSpecific()
    {
        return $this->belongsTo('App\RestaurantSpecific');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balances()
    {
        return $this->hasMany('App\Balance', 'from_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balancesTo()
    {
        return $this->hasMany('App\Balance', 'to_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function follows() 
    {
        return $this->hasMany('App\Follow', 'from_user_id');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function ratings()
    // {
    //     return $this->hasMany('App\Rating', 'from_user_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function loginTokens()
    {
        return $this->hasMany('App\UserLoginToken', 'user_id', 'id');
    }
}
