<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $location_id
 * @property int $contract_type_id
 * @property int $profesionist_profile_id
 * @property string $work_position
 * @property string $desired_salary
 * @property ContractType $contractType
 * @property Location $location
 * @property ProfesionistProfile $profesionistProfile
 */
class WorkRequest extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['location_id', 'contract_type_id', 'profesionist_profile_id', 'work_position', 'desired_salary'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contractType()
    {
        return $this->belongsTo('App\ContractType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesionistProfile()
    {
        return $this->belongsTo('App\ProfesionistProfile');
    }
}
