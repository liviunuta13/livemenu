<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hotels';

    /**
     * Run the migrations.
     * @table hotels
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->longText('description')->nullable();
            $table->integer('stars')->nullable();
            $table->string('email', 45)->nullable();
            $table->string('phone_number', 45)->nullable();
            $table->integer('capacity')->nullable();
            $table->unsignedInteger('restaurant_location_id');
            $table->timestamps();
			$table->softDeletes();

            $table->unique(["id"], 'id_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
