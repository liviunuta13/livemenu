<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantProfilesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_profiles';

    /**
     * Run the migrations.
     * @table restaurant_profiles
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->longText('logo')->nullable();
            $table->string('description')->nullable();
            $table->string('fiscal_code', 45)->nullable();
            $table->string('company_name', 100)->nullable();
            $table->string('administrator_name', 100)->nullable();
            $table->string('website', 190)->nullable();
            $table->string('company_email', 45)->nullable();
            $table->string('phone_number', 45)->nullable();
            $table->string('suspended', 45)->nullable()->comment('Suspended restaurant profile by admin');
            $table->tinyInteger('confirmed') -> default(false);
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
