<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttractionPicturesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'attraction_pictures';

    /**
     * Run the migrations.
     * @table attraction_pictures
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('picture')->nullable();
            $table->unsignedInteger('attraction_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["attraction_id"], 'fk_attraction_pictures_tourist_attractions1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('attraction_id', 'fk_attraction_pictures_tourist_attractions1_idx')
                ->references('id')->on('tourist_attractions')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
