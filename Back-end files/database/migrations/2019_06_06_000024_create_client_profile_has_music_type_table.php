<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientProfileHasMusicTypeTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'client_profile_has_music_type';

    /**
     * Run the migrations.
     * @table client_profile_has_music_type
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('client_profile_id');
            $table->unsignedInteger('music_type_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["music_type_id"], 'fk_client_profiles_has_music_types_music_types1_idx');

            $table->index(["client_profile_id"], 'fk_client_profiles_has_music_types_client_profiles1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('client_profile_id', 'fk_client_profiles_has_music_types_client_profiles1_idx')
                ->references('id')->on('client_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('music_type_id', 'fk_client_profiles_has_music_types_music_types1_idx')
                ->references('id')->on('music_types')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
