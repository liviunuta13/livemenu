<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductForSponsorshipsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'product_for_sponsorships';

    /**
     * Run the migrations.
     * @table product_for_sponsorships
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable()->comment('The name of the product like Cola');
            $table->unsignedInteger('partner_id')->comment('The partner that can offer the sponsorships');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["partner_id"], 'fk_product_for_sponsorships_partners1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('partner_id', 'fk_product_for_sponsorships_partners1_idx')
                ->references('id')->on('partners')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
