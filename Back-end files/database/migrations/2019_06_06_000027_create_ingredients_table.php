<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ingredients';

    /**
     * Run the migrations.
     * @table ingredients
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('price', 45)->nullable();
            $table->unsignedInteger('amount')->nullable();
            $table->unsignedInteger('currency_id')->nullable();
            $table->unsignedInteger('unit_id')->nullable();
            $table->boolean('allergen')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["unit_id"], 'fk_food_ingredients_units1_idx');

            $table->index(["currency_id"], 'fk_food_ingredients_currencies1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('unit_id', 'fk_food_ingredients_units1_idx')
                ->references('id')->on('units')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('currency_id', 'fk_food_ingredients_currencies1_idx')
                ->references('id')->on('currencies')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
