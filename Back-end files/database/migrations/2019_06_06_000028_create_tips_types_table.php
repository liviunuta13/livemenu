<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipsTypesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tips_types';

    /**
     * Run the migrations.
     * @table tips_types
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->unsignedInteger('price')->default(0);
            $table->unsignedInteger('currency_id')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["currency_id"], 'fk_tips_types_currencies1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('currency_id', 'fk_tips_types_currencies1_idx')
                ->references('id')->on('currencies')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
