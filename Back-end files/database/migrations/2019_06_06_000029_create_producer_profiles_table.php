<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducerProfilesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'producer_profiles';

    /**
     * Run the migrations.
     * @table producer_profiles
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('producer_type_id');
            $table->string('name', 45)->nullable();
            $table->longText('logo')->nullable();
            $table->longText('description')->nullable();
            $table->string('fiscal_code', 45)->nullable();
            $table->string('company_name', 100)->nullable();
            $table->string('administrator_name_surname', 100)->nullable();
            $table->string('website', 190)->nullable();
            $table->string('email', 45)->nullable();
            $table->string('phone_number', 45)->nullable();
            $table->string('suspended', 45)->nullable()->comment('Suspended producer profiles by admin');
            $table->tinyInteger('confirmed') -> default(false);
            $table->timestamps();
			$table->softDeletes();

            $table->index(["producer_type_id"], 'fk_producer_profiles_producer_types1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('producer_type_id', 'fk_producer_profiles_producer_types1_idx')
                ->references('id')->on('producer_types')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
