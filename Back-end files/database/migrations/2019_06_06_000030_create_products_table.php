<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'products';

    /**
     * Run the migrations.
     * @table products
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->integer('price')->nullable();
            $table->tinyInteger('price_per_amount')->nullable()->comment('Price per amount or per unit');
            $table->unsignedInteger('currency_id')->nullable();
            $table->longText('description')->nullable();
            $table->tinyInteger('special_product')->nullable();
            $table->tinyInteger('unique_product')->nullable();
            $table->tinyInteger('specific_area_product')->nullable();
            $table->unsignedInteger('product_id') -> nullable();
            $table->unsignedInteger('producer_location_id') -> nullable();
            $table->unsignedInteger('cat_id') -> nullable();
            $table->unsignedInteger('parent_id') -> nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["currency_id"], 'fk_products_currencies1_idx');

            // $table->index(["product_id"], 'fk_products_1_idx');

            $table->index(["producer_location_id"], 'fk_producer_locations__idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('currency_id', 'fk_products_currencies1_idx')
                ->references('id')->on('currencies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('product_id', 'fk_products_1_idx')
                ->references('id')->on('products')
                ->onDelete('cascade')
                ->onUpdate('no action');

            // $table->foreign('producer_location_id', 'fk_producer_locations__idx')
            //     ->references('id')->on('producer_profile_locations')
            //     ->onDelete('cascade')
            //     ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
