<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'rooms';

    /**
     * Run the migrations.
     * @table rooms
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->unsignedInteger('hotel_id');
            $table->string('surface', 45)->nullable();
            $table->unsignedInteger('unit_id');
            $table->unsignedInteger('seats')->nullable()->comment('Bads number of the room - how much persons that can live');
            $table->unsignedInteger('additional_seats')->nullable()->comment('Number of beds that can be used in plus');
            $table->unsignedInteger('beds_number')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["unit_id"], 'fk_rooms_units1_idx');

            $table->index(["hotel_id"], 'fk_rooms_hotels1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('hotel_id', 'fk_rooms_hotels1_idx')
                ->references('id')->on('hotels')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('unit_id', 'fk_rooms_units1_idx')
                ->references('id')->on('units')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
