<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelFacilitiesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hotel_facilities';

    /**
     * Run the migrations.
     * @table hotel_facilities
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('icon', 45)->nullable();
            // $table->unsignedInteger('hotel_id');
            $table->timestamps();
			$table->softDeletes();

            // $table->index(["hotel_id"], 'fk_hotel_facilities_hotels1_idx');
            //
            // $table->unique(["id"], 'id_UNIQUE');
            //
            //
            // $table->foreign('hotel_id', 'fk_hotel_facilities_hotels1_idx')
            //     ->references('id')->on('hotels')
            //     ->onDelete('cascade')
            //     ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
