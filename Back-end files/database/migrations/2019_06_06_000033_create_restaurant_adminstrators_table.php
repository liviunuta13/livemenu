<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantAdminstratorsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_adminstrators';

    /**
     * Run the migrations.
     * @table restaurant_adminstrators
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->string('phone_number', 45)->nullable();
            $table->string('password', 45)->nullable();
            $table->unsignedInteger('restaurant_profile_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["restaurant_profile_id"], 'fk_restaurant_adminstrators_restaurant_profiles1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_profile_id', 'fk_restaurant_adminstrators_restaurant_profiles1_idx')
                ->references('id')->on('restaurant_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
