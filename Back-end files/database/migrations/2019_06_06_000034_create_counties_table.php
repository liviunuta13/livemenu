<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountiesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'counties';

    /**
     * Run the migrations.
     * @table counties
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->unsignedInteger('country_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["country_id"], 'fk_counties_countries1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('country_id', 'fk_counties_countries1_idx')
                ->references('id')->on('countries')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
