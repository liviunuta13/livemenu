<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomHasFacilityTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'room_has_facility';

    /**
     * Run the migrations.
     * @table room_has_facility
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('room_id');
            $table->unsignedInteger('hotel_id')->nullable();
            $table->unsignedInteger('room_facility_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["room_id", "hotel_id"], 'fk_rooms_has_room_facilities_rooms1_idx');

            $table->index(["room_facility_id"], 'fk_rooms_has_room_facilities_room_facilities1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('room_id', 'fk_rooms_has_room_facilities_rooms1_idx')
                ->references('id')->on('rooms')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('room_facility_id', 'fk_rooms_has_room_facilities_room_facilities1_idx')
                ->references('id')->on('room_facilities')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
