<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomPicturesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'room_pictures';

    /**
     * Run the migrations.
     * @table room_pictures
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('picture')->nullable();
            $table->unsignedInteger('room_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["room_id"], 'fk_room_pictures_rooms1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('room_id', 'fk_room_pictures_rooms1_idx')
                ->references('id')->on('rooms')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
