<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOffersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'job_offers';

    /**
     * Run the migrations.
     * @table job_offers
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('description')->nullable();
            $table->string('latitude', 45)->nullable();
            $table->string('longitude', 45)->nullable();
            $table->unsignedInteger('location_id')->nullable();
            $table->unsignedInteger('restaurant_location_id')->nullable();
            $table->unsignedInteger('restaurant_profile_id')->nullable()->comment('This field has value if the job is posted by a restaurant location');
            $table->unsignedInteger('contract_type_id');
            $table->date('begin_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('salary', 45)->nullable()->nullable()->comment('Gives salary by who offers');
            $table->unsignedInteger('currency_id')->nullable();
            $table->unsignedInteger('producer_profile_id')->nullable()->comment('This field has value if the job is posted by a producer');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["location_id"], 'fk_jobs_locations1_idx');

            $table->index(["restaurant_profile_id"], 'fk_jobs_restaurant_profiles1_idx');

            $table->index(["contract_type_id"], 'fk_jobs_contract_types1_idx');

            $table->index(["currency_id"], 'fk_jobs_currencies1_idx');

            $table->index(["producer_profile_id"], 'fk_jobs_producer_profiles1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('location_id', 'fk_jobs_locations1_idx')
                ->references('id')->on('locations')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('restaurant_profile_id', 'fk_jobs_restaurant_profiles1_idx')
                ->references('id')->on('restaurant_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');




            $table->foreign('contract_type_id', 'fk_jobs_contract_types1_idx')
                ->references('id')->on('contract_types')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('currency_id', 'fk_jobs_currencies1_idx')
                ->references('id')->on('currencies')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('producer_profile_id', 'fk_jobs_producer_profiles1_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
