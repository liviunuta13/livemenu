<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomBedTypesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'room_bed_types';

    /**
     * Run the migrations.
     * @table room_bed_types
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->unsignedInteger('number_of_people')->default(2);
            // $table->unsignedInteger('room_id');
            // $table->unsignedInteger('hotel_id');
            $table->timestamps();
			$table->softDeletes();

            // $table->index(["room_id", "hotel_id"], 'fk_room_bed_types_rooms1_idx');
            //
            // $table->unique(["id"], 'id_UNIQUE');
            //
            //
            // $table->foreign('room_id', 'fk_room_bed_types_rooms1_idx')
            //     ->references('id')->on('rooms')
            //     ->onDelete('no action')
            //     ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
