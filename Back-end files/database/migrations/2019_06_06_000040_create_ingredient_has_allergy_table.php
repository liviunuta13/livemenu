<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientHasAllergyTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ingredient_has_allergy';

    /**
     * Run the migrations.
     * @table ingredient_has_allergy
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('food_ingredient_id');
            $table->unsignedInteger('ingredient_allergy_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["ingredient_allergy_id"], 'fk_food_ingredients_has_ingredient_allergies_ingredient_all_idx');

            $table->index(["food_ingredient_id"], 'fk_food_ingredients_has_ingredient_allergies_food_ingredien_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('food_ingredient_id', 'fk_food_ingredients_has_ingredient_allergies_food_ingredien_idx')
                ->references('id')->on('ingredients')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('ingredient_allergy_id', 'fk_food_ingredients_has_ingredient_allergies_ingredient_all_idx')
                ->references('id')->on('ingredient_allergies')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
