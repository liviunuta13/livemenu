<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducerProfilePicturesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'producer_profile_pictures';

    /**
     * Run the migrations.
     * @table producer_profile_pictures
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->unsignedInteger('producer_profile_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["producer_profile_id"], 'fk_producer_profile_pictures_producer_profiles1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('producer_profile_id', 'fk_producer_profile_pictures_producer_profiles1_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
