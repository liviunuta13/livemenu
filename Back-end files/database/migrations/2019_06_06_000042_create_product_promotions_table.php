<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPromotionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'product_promotions';

    /**
     * Run the migrations.
     * @table product_promotions
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('description', 45)->nullable();
            $table->date('begin_date')->nullable();
            $table->date('end_date')->nullable();
            $table->time('begin_time')->nullable();
            $table->time('end_time')->nullable();
            $table->unsignedInteger('product_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["product_id"], 'fk_product_promotions_products1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('product_id', 'fk_product_promotions_products1_idx')
                ->references('id')->on('products')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
