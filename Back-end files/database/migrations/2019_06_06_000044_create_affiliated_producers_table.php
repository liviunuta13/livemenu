<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatedProducersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'affiliated_producers';

    /**
     * Run the migrations.
     * @table affiliated_producers
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->boolean('confirmed')->default(0)->comment('The producer profile for can accepts or decline this collaboration.');
            $table->unsignedInteger('affiliated_product_ad_id');
            $table->unsignedInteger('producer_profile_from_id');
            $table->unsignedInteger('producer_profile_for_id') -> nullable() -> comment('This is the producer profile id wich posted the request.');
            $table->timestamps();
			$table->softDeletes();

            // $table->index(["affiliated_product_ad_id"], 'fk_affiliated_producers_affiliated_product_ad_idx');

            $table->index(["producer_profile_from_id"], 'fk_affiliated_producers_producer_from_profiles1_idx');

            $table->index(["producer_profile_for_id"], 'fk_affiliated_producers_producer_for_profiles1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            // $table->foreign('affiliated_product_ad_id', 'fk_affiliated_producers_affiliated_product_ad_idx')
            //     ->references('id')->on('affiliated_products_ads')
            //     ->onDelete('no action')
            //     ->onUpdate('no action');

            $table->foreign('producer_profile_from_id', 'fk_affiliated_producers_producer_from_profiles1_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('producer_profile_for_id', 'fk_affiliated_producers_producer_for_profiles1_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
