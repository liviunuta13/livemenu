<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantLocationsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_locations';

    /**
     * Run the migrations.
     * @table restaurant_locations
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->unsignedInteger('restaurant_profile_id');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('county_id');
            $table->unsignedInteger('country_id');
            $table->string('street', 100)->nullable();
            $table->string('number', 45)->nullable();
            $table->string('postal_code', 45)->nullable();
            $table->string('latitude', 45)->nullable();
            $table->string('longitude', 45)->nullable();
            $table->string('description', 190)->nullable();
            $table->string('phone_number', 45)->nullable();
            $table->string('fixed_phone_number', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->string('contact_person_name', 45)->nullable();
            $table->string('locality_specific', 45)->nullable();
            $table->unsignedInteger('music_type_id');
            $table->unsignedInteger('tables_number')->nullable();
            $table->unsignedInteger('bathrooms_number')->nullable();
            $table->unsignedInteger('locality_surface')->nullable();
            $table->tinyInteger('live_music')->nullable();
            $table->tinyInteger('catering')->nullable();
            $table->string('number_of_seats', 45)->nullable();
            $table->string('framing', 45)->nullable();
            $table->string('michelin_stars', 45)->nullable();
            $table->integer('owns_event_room')->nullable()->comment('The restaurant location: 0 no event room, 1 and room, 2 is just event room.');
            $table->tinyInteger('suspended')->nullable()->comment('Suspended restaurant location');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["city_id"], 'fk_restaurant_locations_cities1_idx');

            $table->index(["country_id"], 'fk_restaurant_locations_countries1_idx');

            $table->index(["music_type_id"], 'fk_restaurant_locations_music_types1_idx');

            $table->index(["restaurant_profile_id"], 'fk_restaurant_locations_restaurant_profiles1_idx');

            $table->index(["county_id"], 'fk_restaurant_locations_counties1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_profile_id', 'fk_restaurant_locations_restaurant_profiles1_idx')
                ->references('id')->on('restaurant_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('city_id', 'fk_restaurant_locations_cities1_idx')
                ->references('id')->on('cities')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('county_id', 'fk_restaurant_locations_counties1_idx')
                ->references('id')->on('counties')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('country_id', 'fk_restaurant_locations_countries1_idx')
                ->references('id')->on('countries')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('music_type_id', 'fk_restaurant_locations_music_types1_idx')
                ->references('id')->on('music_types')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
