<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesionistProfilesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'profesionist_profiles';

    /**
     * Run the migrations.
     * @table profesionist_profiles
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->longText('picture')->nullable();
            $table->longText('personal_description') -> nullable();
            $table->string('email', 45)->nullable();
            $table->string('street_and_number', 190)->nullable();
            $table->string('postal_code', 10)->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('county_id')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->string('phone_number', 45)->nullable();
            $table->string('website', 190)->nullable();
            $table->string('professional_experience', 256) -> nullable() -> comment('Functiile pentru care sunt dispus sa lucrez');

            // $table->date('education_begin_date') -> nullable();
            // $table->tinyInteger('education_present') -> nullable();
            // $table->date('education_end_date') -> nullable();
            $table->string('foreign_languages', 256) -> nullable();
            // $table->string('education_institution_name') -> nullable();
            // $table->unsignedInteger('education_city_id')->nullable();
            // $table->unsignedInteger('education_country_id')->nullable();
            // $table->longText('education_description')->nullable();

            $table->unsignedInteger('matern_language_id')->nullable();

            $table->string('managerial_skills', 256) -> nullable();
            $table->string('skills_at_working_place', 256) -> nullable();
            $table->string('digital_skills', 256) -> nullable();

            $table->string('driving_licence', 256) -> nullable();
            $table->string('other_skills', 256) -> nullable();

            $table->tinyInteger('is_looking_for_job')->nullable();
            $table->tinyInteger('is_at_working')->nullable();
            $table->tinyInteger('available_for_working')->nullable();
            $table->tinyInteger('suspended')->nullable()->comment('Suspended account profile');
            $table->tinyInteger('confirmed') -> default(false);
            $table->timestamps();
			$table->softDeletes();

            $table->index(["city_id"], 'fk_profesionist_profiles_cities1_idx');

            $table->index(["county_id"], 'fk_profesionist_profiles_counties1_idx');

            $table->index(["country_id"], 'fk_profesionist_profiles_countries1_idx');

            // $table->index(["education_city_id"], 'fk_profesionist_profiles_education_city1_idx');
            //
            // $table->index(["education_country_id"], 'fk_profesionist_profiles_education_country1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('city_id', 'fk_profesionist_profiles_cities1_idx')
                ->references('id')->on('cities')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('county_id', 'fk_profesionist_profiles_counties1_idx')
                ->references('id')->on('counties')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('country_id', 'fk_profesionist_profiles_countries1_idx')
                ->references('id')->on('countries')
                ->onDelete('no action')
                ->onUpdate('no action');

            // $table->foreign('education_city_id', 'fk_profesionist_profiles_education_city1_idx')
            //     ->references('id')->on('cities')
            //     ->onDelete('no action')
            //     ->onUpdate('no action');
            //
            // $table->foreign('education_country_id', 'fk_profesionist_profiles_education_country1_idx')
            //     ->references('id')->on('countries')
            //     ->onDelete('no action')
            //     ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
