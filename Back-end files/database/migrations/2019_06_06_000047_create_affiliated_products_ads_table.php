<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatedProductsAdsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'affiliated_products_ads'; 

    /**
     * Run the migrations.
     * @table affiliated_products_ads
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // $table->unsignedInteger('affiliated_producers_id') -> nullable();
            $table->unsignedInteger('producer_id') -> comment('The producer id wich posted the affiliating request');
            $table->string('title', 45)->nullable();
            $table->longText('description')->nullable();
            $table->unsignedInteger('product_id') -> nullable();
            $table->string('product_name', 190) -> nullable();
            $table->integer('final_amount')->nullable();
            $table->unsignedInteger('final_amount_unit_id')->nullable()->comment('Unit for final amount field like kg, g, l');
            $table->integer('current_amount')->nullable()->comment('Actual amount of product - like stock');
            $table->unsignedInteger('current_amount_unit_id')->nullable()->comment('Unit for current amount field like kg, g, l');
            $table->integer('final_price')->nullable();
            $table->unsignedInteger('currency_id')->nullable();
            $table->unsignedInteger('location_id')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["final_amount_unit_id"], 'fk_affiliated_products_ads_units1_idx');

            $table->index(["location_id"], 'fk_affiliated_products_ads_locations1_idx');

            $table->index(["current_amount_unit_id"], 'fk_affiliated_products_ads_units2_idx');

            $table->index(["currency_id"], 'fk_affiliated_products_ads_currencies1_idx');

            $table->index(["product_id"], 'fk_affiliated_products_ads_products1_idx');

            // $table->index(["affiliated_producers_id"], 'fk_affiliated_products_ads_affiliated_producers1_idx');

            $table->index(["producer_id"], 'fk_affiliated_products_ads_producer2_idx');

            $table->unique(["id"], 'id_UNIQUE');


            // $table->foreign('affiliated_producers_id', 'fk_affiliated_products_ads_affiliated_producers1_idx')
            //     ->references('id')->on('affiliated_producers')
            //     ->onDelete('no action')
            //     ->onUpdate('no action');

            $table->foreign('producer_id', 'fk_affiliated_products_ads_producer2_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('product_id', 'fk_affiliated_products_ads_products1_idx')
                ->references('id')->on('products')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('final_amount_unit_id', 'fk_affiliated_products_ads_units1_idx')
                ->references('id')->on('units')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('current_amount_unit_id', 'fk_affiliated_products_ads_units2_idx')
                ->references('id')->on('units')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('currency_id', 'fk_affiliated_products_ads_currencies1_idx')
                ->references('id')->on('currencies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('location_id', 'fk_affiliated_products_ads_locations1_idx')
                ->references('id')->on('locations')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
