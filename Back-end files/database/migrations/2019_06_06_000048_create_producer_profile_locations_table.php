<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducerProfileLocationsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'producer_profile_locations';

    /**
     * Run the migrations.
     * @table producer_profile_locations
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('county_id')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->string('street', 100)->nullable();
            $table->string('number', 45)->nullable();
            $table->string('postal_code', 45)->nullable();
            $table->string('latitude', 45)->nullable();
            $table->string('longitude', 45)->nullable();
            $table->longText('description')->nullable();
            $table->string('email', 45)->nullable();
            $table->string('phone_number', 45)->nullable();
            $table->string('fixed_phone_number', 45)->nullable();
            $table->string('contact_person_name', 45)->nullable();
            $table->unsignedInteger('producer_profile_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["county_id"], 'fk_producer_profile_locations_counties1_idx');

            $table->index(["producer_profile_id"], 'fk_producer_profile_locations_producer_profiles1_idx');

            $table->index(["country_id"], 'fk_producer_profile_locations_countries1_idx');

            $table->index(["city_id"], 'fk_producer_profile_locations_cities1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('city_id', 'fk_producer_profile_locations_cities1_idx')
                ->references('id')->on('cities')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('county_id', 'fk_producer_profile_locations_counties1_idx')
                ->references('id')->on('counties')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('country_id', 'fk_producer_profile_locations_countries1_idx')
                ->references('id')->on('countries')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('producer_profile_id', 'fk_producer_profile_locations_producer_profiles1_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
