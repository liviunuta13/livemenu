<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsAtWorkingPlaceTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'skills_at_working_place';

    /**
     * Run the migrations.
     * @table skills_at_working_place
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->unsignedInteger('profesionist_profile_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["profesionist_profile_id"], 'fk_skills_at_working_place_profesionist_profiles1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('profesionist_profile_id', 'fk_skills_at_working_place_profesionist_profiles1_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
