<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRoomsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'event_rooms';

    /**
     * Run the migrations.
     * @table event_rooms
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('restaurant_location_id');
            $table->unsignedInteger('restaurant_location_profile_id');
            $table->tinyInteger('owns_event_room')->nullable()->comment('Get value from restaurant location profile
The restaurant location has events hall or has just events hall');
            $table->longText('descripton')->nullable();
            $table->integer('rooms_number')->nullable();
            $table->integer('tables_number')->nullable();
            $table->integer('persons_number')->nullable();
            $table->integer('surface')->nullable();
            $table->unsignedInteger('surface_unit_id');
            $table->integer('disponible_rooms_number')->nullable()->comment('The number of rooms of event room that are disponible');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["surface_unit_id"], 'fk_event_rooms_units1_idx');

            $table->index(["restaurant_location_id", "restaurant_location_profile_id"], 'fk_event_rooms_restaurant_locations1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_location_id', 'fk_event_rooms_restaurant_locations1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('surface_unit_id', 'fk_event_rooms_units1_idx')
                ->references('id')->on('units')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
