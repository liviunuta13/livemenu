<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantLocationPromotionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_location_promotions';

    /**
     * Run the migrations.
     * @table restaurant_location_promotions
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->longText('description')->nullable();
            $table->date('begin_date');
            $table->date('end_date');
            $table->time('begin_time');
            $table->time('end_time');
            $table->unsignedInteger('restaurant_location_id');
            $table->unsignedInteger('product_id')->nullable();
            // $table->unsignedInteger('food_menu_category_id')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["restaurant_location_id"], 'fk_restaurant_location_promotions_restaurant_locations1_idx');

            // $table->index(["product_id"], 'fk_product_promotions_restaurant_locations2_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_location_id', 'fk_restaurant_location_promotions_restaurant_locations1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            // $table->foreign('product_id', 'fk_product_promotions_restaurant_locations2_idx')
            //     ->references('id')->on('menus')
            //     ->onDelete('cascade')
            //     ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
