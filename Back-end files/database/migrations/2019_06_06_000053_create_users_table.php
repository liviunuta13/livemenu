<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->tinyInteger('confirmed_email')->default(false);
            $table->string('password', 190)->nullable();
            $table->string('reset_password_token', 190)->nullable();
            $table->string('nickname', 45)->nullable();
            $table->longText('profile_picture')->nullable();
            // $table->string('secret_ingredient', 45)->nullable();
            $table->unsignedInteger('client_profile_id')->nullable();
            $table->unsignedInteger('profesionist_profile_id')->nullable();
            $table->unsignedInteger('restaurant_profile_id')->nullable();
            $table->unsignedInteger('producer_profile_id')->nullable();
            // $table->unsignedInteger('restaurant_specific_id')->nullable();

            $table->string('favorite_food', 45)->nullable();
            $table->string('secret_ingredients', 45)->nullable();
            $table->string('restaurant_specifics', 45)->nullable();
            $table->string('music_types', 45)->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["restaurant_profile_id"], 'fk_restaurant_profiles1_idx');

            // $table->index(["restaurant_specific_id"], 'fk_restaurant_specifics1_idx');

            $table->index(["client_profile_id"], 'fk_client_profiles_idx');

            $table->index(["producer_profile_id"], 'fk_producer_profiles1_idx');

            $table->index(["profesionist_profile_id"], 'fk_profesionist_profiles1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('client_profile_id', 'fk_client_profiles_idx')
                ->references('id')->on('client_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('profesionist_profile_id', 'fk_profesionist_profiles1_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('restaurant_profile_id', 'fk_restaurant_profiles1_idx')
                ->references('id')->on('restaurant_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('producer_profile_id', 'fk_producer_profiles1_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            // $table->foreign('restaurant_specific_id', 'fk_restaurant_specifics1_idx')
            //     ->references('id')->on('restaurant_specifics')
            //     ->onDelete('no action')
            //     ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
