<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantLocationEventsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_location_events';

    /**
     * Run the migrations.
     * @table restaurant_location_events
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('theme', 45)->nullable();
            $table->longText('description')->nullable();
            $table->string('banner', 45)->nullable();
            $table->tinyInteger('one_day_event')->nullable();
            $table->tinyInteger('multi_day_event')->nullable();
            $table->date('begin_date')->nullable();
            $table->date('end_date')->nullable();
            $table->time('begin_time')->nullable();
            $table->time('end_time')->nullable();
            $table->unsignedInteger('restaurant_location_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["restaurant_location_id"], 'fk_restaurant_location_events_restaurant_locations1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_location_id', 'fk_restaurant_location_events_restaurant_locations1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
