<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantLocationHasUtilityTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_location_has_utility';

    /**
     * Run the migrations.
     * @table restaurant_location_has_utility
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('restaurant_location_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["restaurant_location_id"], 'fk_restaurant_locations_has_restaurant_location_utilities_r_idx1');

            $table->index(["id"], 'fk_restaurant_locations_has_restaurant_location_utilities_r_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_location_id', 'fk_restaurant_locations_has_restaurant_location_utilities_r_idx1')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('id', 'fk_restaurant_locations_has_restaurant_location_utilities_r_idx')
                ->references('id')->on('restaurant_location_utilities')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
