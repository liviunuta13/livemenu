<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorshipsRequestsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'sponsorships_requests';

    /**
     * Run the migrations.
     * @table sponsorships_requests
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('partner_id')->comment('Is the partner for request, is owner');
            $table->unsignedInteger('restaurant_id');
            $table->tinyInteger('accepted')->default(0);
            $table->timestamps();
			$table->softDeletes();

            $table->index(["partner_id"], 'fk_sponsorships_requests_partners1_idx');

            $table->index(["restaurant_id"], 'fk_sponsorships_requests_restaurants1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('partner_id', 'fk_sponsorships_requests_partners1_idx')
                ->references('id')->on('partners')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('restaurant_id', 'fk_sponsorships_requests_restaurants1_idx')
                ->references('id')->on('restaurant_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
