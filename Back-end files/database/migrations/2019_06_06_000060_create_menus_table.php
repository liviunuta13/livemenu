<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'menus';

    /**
     * Run the migrations.
     * @table menus
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->longText('description')->nullable();
            // $table->string('picture', 45)->nullable();
            $table->longText('picture')->nullable();
            $table->double('price')->nullable();
            $table->tinyInteger('specialty_of_the_house')->nullable();
            $table->tinyInteger('specific_area_food')->nullable();
            $table->integer('number_of_persons_that_can_eat')->nullable();
            $table->unsignedInteger('currency_id')->nullable()->comment('The currency');
            $table->string('cooking_time', 45)->nullable()->comment('Time for cooking (for food)');
            $table->tinyInteger('food')->nullable()->comment('Is food - true or is drink - false');
            $table->unsignedInteger('drink_producer_id')->nullable();
            // $table->string('drink_amount', 45)->nullable();
            $table->string('amount', 45)->nullable();
            $table->unsignedInteger('unit_id')->nullable()->comment('Drink currency unit');
            // $table->unsignedInteger('food_unit_id')->nullable()->comment('Food currency unit ');
            $table->unsignedInteger('menu_id')->nullable();
            $table->unsignedInteger('restaurant_location_id')->nullable();
            $table->string('recommended_for', 45)->nullable()->comment('Menu recommended for an event room - for restaurant location menu is null');
            $table->unsignedInteger('cat_id') -> nullable();
            $table->unsignedInteger('parent_id') -> nullable();
            $table->tinyInteger('confirmed') -> nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["restaurant_location_id"], 'fk_menu_restaurant_locations1_idx');

            $table->index(["unit_id"], 'fk_menu_units1_idx');

            $table->index(["drink_producer_id"], 'fk_menu_producers1_idx');

            // $table->index(["food_unit_id"], 'fk_menu_units2_idx');

            $table->index(["currency_id"], 'fk_food_types_currencies1_idx');

            $table->index(["menu_id"], 'fk_menu_menu1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('currency_id', 'fk_food_types_currencies1_idx')
                ->references('id')->on('currencies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('unit_id', 'fk_menu_units1_idx')
                ->references('id')->on('units')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('drink_producer_id', 'fk_menu_producers1_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            // $table->foreign('food_unit_id', 'fk_menu_units2_idx')
            //     ->references('id')->on('units')
            //     ->onDelete('no action')
            //     ->onUpdate('no action');

            $table->foreign('menu_id', 'fk_menu_menu1_idx')
                ->references('id')->on('menus')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('restaurant_location_id', 'fk_menu_restaurant_locations1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
