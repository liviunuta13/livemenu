<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalSkillsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'personal_skills';

    /**
     * Run the migrations.
     * @table personal_skills
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('matern_language', 45)->nullable();
            $table->unsignedInteger('profesionist_profile_id');
            $table->tinyInteger('managerial_skills')->nullable();
            $table->tinyInteger('digital_skills')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["profesionist_profile_id"], 'fk_personal_skills_profesionist_profiles1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('profesionist_profile_id', 'fk_personal_skills_profesionist_profiles1_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
