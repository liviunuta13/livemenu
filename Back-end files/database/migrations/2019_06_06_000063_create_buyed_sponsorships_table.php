<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyedSponsorshipsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'buyed_sponsorships';

    /**
     * Run the migrations.
     * @table buyed_sponsorships
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('restaurant_location_id')->nullable();
            $table->unsignedInteger('restaurant_profile_id')->nullable();
            $table->unsignedInteger('producer_profile_id')->nullable();
            $table->unsignedInteger('for_restaurant_location_id')->nullable()->comment('An restaurant location buys sponsorships for other restaurant location');
            $table->unsignedInteger('for_restaurant_profile_id')->nullable()->comment('An restaurant location buys sponsorships for restaurant profile');
            $table->unsignedInteger('for_producer_profile_id')->nullable()->comment('An restaurant location buys sponsorships for producer profile');
            $table->unsignedInteger('for_profesionist_profile_id')->nullable()->comment('An restaurant location buys sponsorships for profesionist profile');
            // $table->unsignedInteger('for_producer_profile_id')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["for_profesionist_profile_id"], 'fk_buyed_sponsorships_profesionist_profiles1_idx');

            $table->index(["for_producer_profile_id"], 'fk_buyed_sponsorships_producer_profiles3_idx');

            $table->index(["restaurant_location_id", "restaurant_profile_id"], 'fk_buyed_sponsorships_restaurant_locations1_idx');

            $table->index(["for_producer_profile_id"], 'fk_buyed_sponsorships_producer_profiles1_idx');

            $table->index(["for_restaurant_location_id", "for_restaurant_profile_id"], 'fk_buyed_sponsorships_restaurant_locations2_idx');

            $table->index(["producer_profile_id"], 'fk_buyed_sponsorships_producer_profiles2_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_location_id', 'fk_buyed_sponsorships_restaurant_locations1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('for_restaurant_location_id', 'fk_buyed_sponsorships_restaurant_locations2_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('for_producer_profile_id', 'fk_buyed_sponsorships_producer_profiles1_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('for_profesionist_profile_id', 'fk_buyed_sponsorships_profesionist_profiles1_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('producer_profile_id', 'fk_buyed_sponsorships_producer_profiles2_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('for_producer_profile_id', 'fk_buyed_sponsorships_producer_profiles3_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
