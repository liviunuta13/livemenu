<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantLocationHasFacilityTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_location_has_facility';

    /**
     * Run the migrations.
     * @table restaurant_location_has_facility
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('restaurant_location_id');
            $table->unsignedInteger('restaurant_location_facility_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["restaurant_location_facility_id"], 'fk_restaurant_locations_has_restaurant_location_facilities__idx');

            $table->index(["restaurant_location_id"], 'fk_restaurant_locations_has_restaurant_location_facilities__idx1');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_location_id', 'fk_restaurant_locations_has_restaurant_location_facilities__idx1')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('restaurant_location_facility_id', 'fk_restaurant_locations_has_restaurant_location_facilities__idx')
                ->references('id')->on('restaurant_location_facilities')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
