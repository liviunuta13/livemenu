<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesionistProfileHasForeignLanguageTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'profesionist_profile_has_foreign_language';

    /**
     * Run the migrations.
     * @table profesionist_profile_has_foreign_language
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('profesionist_profile_id');
            $table->unsignedInteger('foreign_language_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["profesionist_profile_id"], 'fk_profesionist_profiles_has_foreign_languages_profesionist_idx');

            $table->index(["foreign_language_id"], 'fk_profesionist_profiles_has_foreign_languages_foreign_lang_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('profesionist_profile_id', 'fk_profesionist_profiles_has_foreign_languages_profesionist_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('foreign_language_id', 'fk_profesionist_profiles_has_foreign_languages_foreign_lang_idx')
                ->references('id')->on('foreign_languages')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
