<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyMenuAvailabilitiesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'daily_menu_availabilities';

    /**
     * Run the migrations.
     * @table daily_menu_availabilities
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->longText('picture')->nullable();
            $table->date('begin_date')->nullable();
            $table->date('end_date')->nullable();
            $table->text('days_of_week');
            $table->time('breakfast_begin_time');
            $table->time('breakfast_end_time');
            $table->time('lunch_begin_time');
            $table->time('lunch_end_time');
            $table->time('dinner_begin_time');
            $table->time('dinner_end_time');
            $table->text('breakfast_menus_id');
            $table->text('lunch_menus_id');
            $table->text('dinner_menus_id');
            $table->unsignedInteger('restaurant_location_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["restaurant_location_id"], 'fk_daily_menu_availabilities_restaurant_locations1_idx');

            $table->unique(["id"], 'id_UNIQUE');

            $table->foreign('restaurant_location_id', 'fk_daily_menu_availabilities_restaurant_locations1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
