<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkRequestsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'work_requests';

    /**
     * Run the migrations.
     * @table work_requests
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('work_position', 100)->nullable();
            $table->unsignedInteger('location_id');
            $table->string('desired_salary', 45);
            $table->unsignedInteger('contract_type_id');
            $table->unsignedInteger('profesionist_profile_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["location_id"], 'fk_work_requests_locations1_idx');

            $table->index(["contract_type_id"], 'fk_work_requests_contract_types1_idx');

            $table->index(["profesionist_profile_id"], 'fk_work_requests_profesionist_profiles1_idx');


            $table->foreign('location_id', 'fk_work_requests_locations1_idx')
                ->references('id')->on('locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('contract_type_id', 'fk_work_requests_contract_types1_idx')
                ->references('id')->on('contract_types')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('profesionist_profile_id', 'fk_work_requests_profesionist_profiles1_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
