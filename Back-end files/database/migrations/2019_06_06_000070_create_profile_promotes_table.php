<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilePromotesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'profile_promotes';

    /**
     * Run the migrations.
     * @table profile_promotes
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('administrator_id');
            $table->unsignedInteger('product_id')->comment('If field is not null then the promote will be for the product');
            $table->unsignedInteger('event_id')->comment('If field is not null then the promote will be for the event');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["administrator_id"], 'fk_profile_promotes_administrators1_idx');

            $table->index(["event_id"], 'fk_profile_promotes_restaurant_location_events1_idx');

            $table->index(["product_id"], 'fk_profile_promotes_products1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('administrator_id', 'fk_profile_promotes_administrators1_idx')
                ->references('id')->on('administrators')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('product_id', 'fk_profile_promotes_products1_idx')
                ->references('id')->on('products')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('event_id', 'fk_profile_promotes_restaurant_location_events1_idx')
                ->references('id')->on('restaurant_location_events')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
