<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantLocationHasPostsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_location_has_posts';

    /**
     * Run the migrations.
     * @table restaurant_location_has_posts
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('restaurant_location_id');
            $table->unsignedInteger('post_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["post_id"], 'fk_restaurant_locations_has_posts_posts1_idx');

            $table->index(["restaurant_location_id"], 'fk_restaurant_locations_has_posts_restaurant_locations1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_location_id', 'fk_restaurant_locations_has_posts_restaurant_locations1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('post_id', 'fk_restaurant_locations_has_posts_posts1_idx')
                ->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
