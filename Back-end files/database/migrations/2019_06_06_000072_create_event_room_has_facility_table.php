<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRoomHasFacilityTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'event_room_has_facility';

    /**
     * Run the migrations.
     * @table event_room_has_facility
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('event_room_id');
            $table->unsignedInteger('facility_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["facility_id"], 'fk_event_rooms_has_event_room_facilities_event_room_facility_idx');

            $table->index(["event_room_id"], 'fk_event_rooms_has_event_room_facilities_event_rooms1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('event_room_id', 'fk_event_rooms_has_event_room_facilities_event_rooms1_idx')
                ->references('id')->on('event_rooms')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('facility_id', 'fk_event_rooms_has_event_room_facilities_event_room_facility_idx')
                ->references('id')->on('event_room_facilities')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
