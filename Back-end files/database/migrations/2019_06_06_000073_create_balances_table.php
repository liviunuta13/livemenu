<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalancesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'balances';

    /**
     * Run the migrations.
     * @table balances
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('from_id')->nullable();
            $table->unsignedInteger('to_id')->nullable();
            $table->unsignedInteger('amount')->nullable()->comment('+ / - transferred amount');
            $table->longText('description')->nullable();
            $table->string('type', 45)->nullable()->comment('The type can be: withdraw ');
            $table->unsignedInteger('currency_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["from_id"], 'fk_balances_users1_idx');

            $table->index(["to_id"], 'fk_balances_users2_idx');

            $table->index(["currency_id"], 'fk_balances_currencies1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('from_id', 'fk_balances_users1_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('to_id', 'fk_balances_users2_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('currency_id', 'fk_balances_currencies1_idx')
                ->references('id')->on('currencies')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
