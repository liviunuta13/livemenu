<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuHasIngredientTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'menu_has_ingredient';

    /**
     * Run the migrations.
     * @table menu_has_ingredient
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('menu_id');
            $table->unsignedInteger('ingredient_id');
            $table->unsignedInteger('amount')->nullable();
            $table->unsignedInteger('unit_id')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["ingredient_id"], 'fk_menu_has_menu_ingredients_menu_ingredients1_idx');

            $table->index(["menu_id"], 'fk_menu_has_menu_ingredients_menu1_idx');

            $table->index(["unit_id"], 'fk_menu_has_ingredients_units_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('menu_id', 'fk_menu_has_menu_ingredients_menu1_idx')
                ->references('id')->on('menus')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('ingredient_id', 'fk_menu_has_menu_ingredients_menu_ingredients1_idx')
                ->references('id')->on('ingredients')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('unit_id', 'fk_menu_has_ingredients_units_idx')
                ->references('id')->on('units')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
