<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostPicturesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'post_pictures';

    /**
     * Run the migrations.
     * @table post_pictures
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('picture')->nullable();
            $table->unsignedInteger('post_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["post_id"], 'fk_post_pictures_posts1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('post_id', 'fk_post_pictures_posts1_idx')
                ->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
