<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodPicturesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'food_pictures';

    /**
     * Run the migrations.
     * @table food_pictures
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('picture')->nullable();
            $table->unsignedInteger('menu_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["menu_id"], 'fk_food_pictures_food_types1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('menu_id', 'fk_food_pictures_food_types1_idx')
                ->references('id')->on('menus')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
