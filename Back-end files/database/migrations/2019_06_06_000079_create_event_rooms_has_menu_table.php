<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRoomsHasMenuTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'event_rooms_has_menu';

    /**
     * Run the migrations.
     * @table event_rooms_has_menu
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('event_room_id');
            $table->unsignedInteger('menu_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["menu_id"], 'fk_event_rooms_has_menu_menu1_idx');

            $table->index(["event_room_id"], 'fk_event_rooms_has_menu_event_rooms1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('event_room_id', 'fk_event_rooms_has_menu_event_rooms1_idx')
                ->references('id')->on('event_rooms')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('menu_id', 'fk_event_rooms_has_menu_menu1_idx')
                ->references('id')->on('menus')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
