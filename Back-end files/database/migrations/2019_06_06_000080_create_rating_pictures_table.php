<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingPicturesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'rating_pictures';

    /**
     * Run the migrations.
     * @table rating_pictures
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('picture');
            $table->unsignedInteger('rating_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["rating_id"], 'fk_rating_images_ratings1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('rating_id', 'fk_rating_images_ratings1_idx')
                ->references('id')->on('ratings')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
