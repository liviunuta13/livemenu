<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationAndTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_and_trainings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('begin_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('title_gain');
            $table->string('institution_name');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('profesionist_profile_id');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();

            $table->index(["country_id"], 'fk_professional_countries1_idx');

            $table->index(["city_id"], 'fk_professional_cities1_idx');

            $table->index(["profesionist_profile_id"], 'fk_professtional_profiles1_idx');


            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('country_id', 'fk_professional_countries1_idx')
                ->references('id')->on('counties')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('city_id', 'fk_professional_cities1_idx')
                ->references('id')->on('cities')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('profesionist_profile_id', 'fk_professtional_profiles1_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_and_trainings');
    }
}
