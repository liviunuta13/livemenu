<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoginTokensTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'user_login_tokens';

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        if (Schema::hasTable($this -> set_schema_table)) return;
        Schema::create($this -> set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table -> increments('id');
            $table -> unsignedInteger('user_id');
            $table -> foreign('user_id') -> references('id') -> on('users') -> onDelete('no action');
            $table -> string('token', 190) -> nullable();
            $table -> date('expire_date') -> nullable();
            $table -> double('latitude') -> nullable();
            $table -> double('longitude') -> nullable();
            $table -> string('ip', 20) -> nullable();
            $table -> text('user_agent') -> nullable();
            $table -> softDeletes();
            $table -> timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this -> set_schema_table);
    }
}
