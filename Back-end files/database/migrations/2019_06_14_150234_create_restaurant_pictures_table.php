<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_pictures', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('picture')->nullable();
            $table->unsignedInteger('restaurant_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["restaurant_id"], 'fk_restaurant_pictures_restaurant_profile1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_id', 'fk_restaurant_pictures_restaurant_profile1_idx')
                ->references('id')->on('restaurant_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_pictures');
    }
}
