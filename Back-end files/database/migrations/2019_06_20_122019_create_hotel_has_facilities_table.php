<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelHasFacilitiesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hotel_has_facility';

    /**
     * Run the migrations.
     * @table hotel_has_facility
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('hotel_id');
            $table->unsignedInteger('facility_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["facility_id"], 'fk_hotels_has_facilities_idx');

            $table->index(["hotel_id"], 'fk_hotels_has_facilities_idx1');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('hotel_id', 'fk_hotels_has_facilities_idx1')
                ->references('id')->on('hotels')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('facility_id', 'fk_hotels_has_facilities_idx')
                ->references('id')->on('hotel_facilities')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }

}
