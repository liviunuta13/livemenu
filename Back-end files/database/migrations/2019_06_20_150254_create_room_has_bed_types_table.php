<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomHasBedTypesTable extends Migration
{

    public $tableName = 'room_has_bed_types';

    /**
     * Run the migrations.
     * @table room_has_facility
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('room_id');
            $table->unsignedInteger('bed_type_id');
            $table->unsignedInteger('number')->default(1);
            $table->timestamps();
			$table->softDeletes();

            $table->index(["room_id"], 'fk_room_has_room_bed_type1_idx');

            $table->index(["bed_type_id"], 'fk_bed_type1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('room_id', 'fk_room_has_room_bed_type1_idx')
                ->references('id')->on('rooms')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('bed_type_id', 'fk_bed_type1_idx')
                ->references('id')->on('room_bed_types')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
