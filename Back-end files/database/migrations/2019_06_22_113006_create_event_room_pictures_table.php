<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRoomPicturesTable extends Migration
{
    public $tableName = 'event_room_pictures';

    /**
     * Run the migrations.
     * @table room_pictures
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('picture')->nullable();
            $table->unsignedInteger('event_room_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["event_room_id"], 'fk_event_room_pictures_rooms1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('event_room_id', 'fk_event_room_pictures_rooms1_idx')
                ->references('id')->on('event_rooms')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
