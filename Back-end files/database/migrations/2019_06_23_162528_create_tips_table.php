<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tips';

    /**
     * Run the migrations.
     * @table posts
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('tip_type_id');
            $table->unsignedInteger('number');
            $table->unsignedInteger('from_user_id')->nullable();
            $table->unsignedInteger('to_user_id')->nullable();
            $table->unsignedInteger('from_client_profile_id')->nullable();
            $table->unsignedInteger('to_client_profile_id')->nullable();
            $table->unsignedInteger('from_producer_profile_id')->nullable();
            $table->unsignedInteger('to_producer_profile_id')->nullable();
            $table->unsignedInteger('from_profesionist_profile_id')->nullable();
            $table->unsignedInteger('to_profesionist_profile_id')->nullable();
            $table->unsignedInteger('from_restaurant_profile_id')->nullable();
            $table->unsignedInteger('to_restaurant_profile_id')->nullable();
            $table->unsignedInteger('from_restaurant_location_id')->nullable();
            $table->unsignedInteger('to_restaurant_location_id')->nullable();
            $table->unsignedInteger('from_administrator_id')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["tip_type_id"], 'fk_tips_types_idx');

            $table->index(["from_user_id"], 'fk_posts_users2_idx');

            $table->index(["to_user_id"], 'fk_posts_users3_idx');

            $table->index(["from_producer_profile_id"], 'fk_tips_producer_profiles2_idx');

            $table->index(["to_producer_profile_id"], 'fk_tips_producer_profiles3_idx');

            $table->index(["from_restaurant_profile_id"], 'fk_tips_restaurant_profiles2_idx');

            $table->index(["to_restaurant_profile_id"], 'fk_tips_restaurant_profiles3_idx');

            $table->index(["from_profesionist_profile_id"], 'fk_tips_profesionist_profiles1_idx');

            $table->index(["to_profesionist_profile_id"], 'fk_tips_profesionist_profiles2_idx');

            $table->index(["from_client_profile_id"], 'fk_tips_client_profiles1_idx');

            $table->index(["to_client_profile_id"], 'fk_tips_client_profiles2_idx');

            $table->index(["from_restaurant_location_id"], 'fk_tips_restaurant_locations1_idx');

            $table->index(["to_restaurant_location_id"], 'fk_tips_restaurant_locations2_idx');

            $table->index(["from_administrator_id"], 'fk_tips_administrators1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('tip_type_id', 'fk_tips_types_idx')
                ->references('id')->on('tips_types')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('from_user_id', 'fk_posts_users2_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('to_user_id', 'fk_posts_users3_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('from_producer_profile_id', 'fk_tips_producer_profiles2_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('to_producer_profile_id', 'fk_tips_producer_profiles3_idx')
                ->references('id')->on('producer_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('from_restaurant_profile_id', 'fk_tips_restaurant_profiles2_idx')
                ->references('id')->on('restaurant_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('to_restaurant_profile_id', 'fk_tips_restaurant_profiles3_idx')
                ->references('id')->on('restaurant_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('from_restaurant_location_id', 'fk_tips_restaurant_locations1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('to_restaurant_location_id', 'fk_tips_restaurant_locations2_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('from_profesionist_profile_id', 'fk_tips_profesionist_profiles1_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('to_profesionist_profile_id', 'fk_tips_profesionist_profiles2_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('from_client_profile_id', 'fk_tips_client_profiles1_idx')
                ->references('id')->on('client_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('to_client_profile_id', 'fk_tips_client_profiles2_idx')
                ->references('id')->on('client_profiles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('from_administrator_id', 'fk_tips_administrators1_idx')
                ->references('id')->on('administrators')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
