<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducerLocationHasProductsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'producer_location_has_products';

    /**
     * Run the migrations.
     * @table producer_has_products
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('producer_location_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["product_id"], 'fk_producer_profiles_has_product1_idx');

            $table->index(["producer_location_id"], 'fk_producer_profiles_has_partners_producer_locations2_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('producer_location_id', 'fk_producer_profiles_has_partners_producer_locations2_idx')
                ->references('id')->on('producer_profile_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('product_id', 'fk_producer_profiles_has_product1_idx')
                ->references('id')->on('products')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
