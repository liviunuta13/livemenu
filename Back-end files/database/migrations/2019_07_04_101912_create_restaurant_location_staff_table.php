<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantLocationStaffTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_location_staff';

    /**
     * Run the migrations.
     * @table restaurant_staff
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('restaurant_location_id');
            $table->unsignedInteger('contract_type_id');
            $table->string('salary', 45)->nullable();
            $table->unsignedInteger('salary_currency_id');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable()->comment('If the contract is determined, it will enter and end date');
            $table->string('job', 45)->nullable()->comment('The name of the job');
            $table->unsignedInteger('profesionist_profile_id')->nullable()->comment('This field has value if the profesionist profile is associated with the staff ');
            $table->unsignedInteger('client_profile_id')->nullable()->comment('This field has value if the client profile is associated with the staff ');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["client_profile_id"], 'fk_restaurant_staff_client_profiles3_idx');

            $table->index(["profesionist_profile_id"], 'fk_restaurant_staff_profesionist_profiles3_idx');

            $table->index(["salary_currency_id"], 'fk_restaurant staff
restaurant_staff_currencies3_idx');

            $table->index(["restaurant_location_id"], 'fk_restaurant staff
restaurant_staff_restaurant_location1_idx');

            $table->index(["contract_type_id"], 'fk_restaurant staff
restaurant_staff_contract_types3_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('restaurant_location_id', 'fk_restaurant staff
restaurant_staff_restaurant_location1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('contract_type_id', 'fk_restaurant staff
restaurant_staff_contract_types3_idx')
                ->references('id')->on('contract_types')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('salary_currency_id', 'fk_restaurant staff
restaurant_staff_currencies3_idx')
                ->references('id')->on('currencies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('profesionist_profile_id', 'fk_restaurant_staff_profesionist_profiles3_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('client_profile_id', 'fk_restaurant_staff_client_profiles3_idx')
                ->references('id')->on('client_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
