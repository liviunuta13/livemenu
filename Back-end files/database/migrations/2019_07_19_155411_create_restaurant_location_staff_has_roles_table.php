<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantLocationStaffHasRolesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'restaurant_location_staff_has_roles';

    /**
     * Run the migrations.
     * @table producer_staff_has_roles
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('staff_id');
            $table->unsignedInteger('role_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["staff_id"], 'fk_restaurant_staff_has_roles_staff_idx');

            $table->index(["role_id"], 'fk_restaurant_staff_has_roles_role_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('staff_id', 'fk_restaurant_staff_has_roles_staff_idx')
                ->references('id')->on('restaurant_location_staff')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('role_id', 'fk_restaurant_staff_has_roles_role_idx')
                ->references('id')->on('restaurant_location_staff_roles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
