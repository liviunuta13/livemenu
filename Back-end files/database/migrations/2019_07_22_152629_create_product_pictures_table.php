<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPicturesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'product_pictures';

    /**
     * Run the migrations.
     * @table post_pictures
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('picture')->nullable();
            $table->unsignedInteger('product_id');
            $table->timestamps();
			$table->softDeletes();

            $table->index(["product_id"], 'fk_product_pictures_products1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('product_id', 'fk_product_pictures_products1_idx')
                ->references('id')->on('products')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
