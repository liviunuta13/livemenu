<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducerLocationPromotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producer_location_promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->longText('description')->nullable();
            $table->date('begin_date');
            $table->date('end_date');
            $table->time('begin_time');
            $table->time('end_time');
            $table->unsignedInteger('producer_location_id');
            $table->unsignedInteger('product_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index(["producer_location_id"], 'fk_producer_location_promotions_producer_locations1_idx');


            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('producer_location_id', 'fk_producer_location_promotions_producer_locations1_idx')
                ->references('id')->on('producer_profile_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producer_location_promotions');
    }
}
