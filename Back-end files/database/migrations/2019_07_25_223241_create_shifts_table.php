<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
           $table->increments('id');
            $table->unsignedInteger('restaurant_location_id');
            $table->string('day')->comment('Monday...');
            $table->time('openingTime');
            $table->time('closingTime')->nullable();

            $table->index(["restaurant_location_id"], 'fk_restaurant_shifts_restaurant_shifts_restaurant_location1_idx');

            $table->foreign('restaurant_location_id', 'fk_restaurant_shifts_restaurant_shifts_restaurant_location1_idx')
                ->references('id')->on('restaurant_locations')
                ->onDelete('cascade')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}
