<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionistEducationTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'professionist_education';

    /**
     * Run the migrations.
     * @table professtional_historic
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('profesionist_profile_id');
            $table->date('begin_date')->nullable();
            $table->tinyInteger('present')->nullable();
            $table->date('end_date')->nullable();
            $table->string('certificate_name', 190)->nullable();
            $table->string('institution_name', 45)->nullable();
            $table->longText('description')->nullable();
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('country_id')->nullable();
            $table->timestamps();
			$table->softDeletes();

            $table->index(["profesionist_profile_id"], 'fk_professional_education_profesionist_profiles1_idx');

            $table->index(["city_id"], 'fk_professional_education_cities1_idx');

            $table->index(["country_id"], 'fk_professional_education_countries1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('profesionist_profile_id', 'fk_professional_education_profesionist_profiles1_idx')
                ->references('id')->on('profesionist_profiles')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('city_id', 'fk_professional_education_cities1_idx')
                ->references('id')->on('cities')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('country_id', 'fk_professional_education_countries1_idx')
                ->references('id')->on('countries')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
