<?php

use App\RoomBedType;
use Illuminate\Database\Seeder;

class BedTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bed = new RoomBedType();

        $bed -> name = 'Simple bed';

        $bed -> number_of_people = 1;

        $bed -> save();


        $bed = new RoomBedType();

        $bed -> name = 'Double bed';

        $bed -> number_of_people = 2;

        $bed -> save();


        $bed = new RoomBedType();

        $bed -> name = 'Triple bed';

        $bed -> number_of_people = 3;

        $bed -> save();
    }
}
