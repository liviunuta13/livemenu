<?php

use Illuminate\Database\Seeder;

class ContractTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contract = new App\ContractType();

        $contract -> name = 'Definite';

        $contract -> save();


        $contract = new App\ContractType();

        $contract -> name = 'Indefinite';

        $contract -> save();


        $contract = new App\ContractType();

        $contract -> name = 'Seasonal';

        $contract -> save();
    }
}
