<?php

use Illuminate\Database\Seeder;
use App\County;
class CountiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counties = array(
            "Alba",
            "Arad",
            "Arges",
            "Bacau",
            "Bihor",
            "Bistrita-Nasaud",
            "Botosani",
            "Brasov",
            "Braila",
            "Buzau",
            "CarasSeverin",
            "Calaras",
            "Cluj",
            "Constanta",
            "Covasna",
            "Dambovita",
            "Dolj",
            "Galati",
            "Giurgiu",
            "Gorj",
            "Harghita",
            "Hunedoara",
            "Ialomita",
            "Iasi",
            "Ilfov",
            "Maramures",
            "Mehedinti",
            "Mures",
            "Neamt",
            "Olt",
            "Prahova",
            "Satu Mare",
            "Salaj",
            "Sibiu",
            "Suceava",
            "Teleorman",
            "Timis",
            "Tulcea",
            "Vaslui",
            "Valcea",
            "Vrancea");
        foreach ($counties as $county) {
            $county_new = new County();
            $county_new->name = $county;
            $county_new->country_id = 177;
            $county_new->save();
        }
    }
}
