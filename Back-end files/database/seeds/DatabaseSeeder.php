<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CountriesSeeder::class,
            CountiesSeeder::class,
            CitiesSeeder::class,
            CurrenciesSeeder::class,
            ContractTypes::class,
            Locations::class,
            BedTypesSeeder::class,
            TipsTypesSeeder::class,
            ProducerTypesSeeder::class,
            MusicTypesSeeder::class,
            RestaurantLocationFacilitiesSeeder::class,
            StaffRolesSeeder::class,
            UserSeeder::class,
        ]);
    }
}
