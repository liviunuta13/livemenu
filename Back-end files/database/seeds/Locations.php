<?php

use Illuminate\Database\Seeder;

class Locations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $location = new App\Location();

        $location -> name = 'Piața Gării, Iași, Romania';

        $location -> save();
    }
}
