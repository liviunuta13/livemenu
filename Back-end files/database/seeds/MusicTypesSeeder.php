<?php

use App\MusicType;
use Illuminate\Database\Seeder;

class MusicTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $music_type = new MusicType();

        $music_type -> name = 'Trap';

        $music_type -> save();

        $music_type = new MusicType();

        $music_type -> name = 'Rap';

        $music_type -> save();

        $music_type = new MusicType();

        $music_type -> name = 'EDM';

        $music_type -> save();
    }
}
