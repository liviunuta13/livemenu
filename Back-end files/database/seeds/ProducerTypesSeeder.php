<?php

use App\ProducerType;
use Illuminate\Database\Seeder;

class ProducerTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Small producers', 'Big producers', 'Retailers'];

        foreach ($types as $key => $value) {

            $producer_type = new ProducerType();

            $producer_type -> name = $value;

            $producer_type -> save();
        }
    }
}
