<?php

use Illuminate\Database\Seeder;
use App\RestaurantLocationFacility;

class RestaurantLocationFacilitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facility = new RestaurantLocationFacility();

        $facility -> name = 'TV';

        $facility -> description = 'TV on the wall';

        $facility -> save();


        $facility = new RestaurantLocationFacility();

        $facility -> name = 'Working desk';

        $facility -> description = 'Working desk';

        $facility -> save();


        $facility = new RestaurantLocationFacility();

        $facility -> name = 'Balcony';

        $facility -> description = 'Balcony';

        $facility -> save();
    }
}
