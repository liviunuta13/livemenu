<?php

use Illuminate\Database\Seeder;
use App\ProducerLocationStaffRole;
use App\RestaurantLocationStaffRole;

class StaffRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Producer lcoation staff roles

        $role = new RestaurantLocationStaffRole();

        $role -> name = 'Add/Modify menu';

        $role -> save();

        $role = new RestaurantLocationStaffRole();

        $role -> name = 'Add/Modify daily menu';

        $role -> save();

        $role = new RestaurantLocationStaffRole();

        $role -> name = 'Create posts';

        $role -> save();

        $role = new RestaurantLocationStaffRole();

        $role -> name = 'Add promotions';

        $role -> save();

        $role = new RestaurantLocationStaffRole();

        $role -> name = 'Add staff';

        $role -> save();

        $role = new RestaurantLocationStaffRole();

        $role -> name = 'Close contracts';

        $role -> save();


        // Restaurant location staff roles

        $role = new ProducerLocationStaffRole();

        $role -> name = 'Add/Modify products';

        $role -> save();

        $role = new ProducerLocationStaffRole();

        $role -> name = 'Create posts';

        $role -> save();

        $role = new ProducerLocationStaffRole();

        $role -> name = 'Add promotions';

        $role -> save();

        $role = new ProducerLocationStaffRole();

        $role -> name = 'Add staff';

        $role -> save();

        $role = new ProducerLocationStaffRole();

        $role -> name = 'Close contracts';

        $role -> save();
    }
}
