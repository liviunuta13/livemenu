<?php

use App\TipsType;
use Illuminate\Database\Seeder;

class TipsTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new TipsType();

        $type -> name = 'Silver';

        $type -> save();

        $type = new TipsType();

        $type -> name = 'Gold';

        $type -> save();

        $type = new TipsType();

        $type -> name = 'Platinum';

        $type -> save();
    }
}
