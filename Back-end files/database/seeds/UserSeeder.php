<?php

use App\User;
use App\Administrator;
use App\ProducerProfile;
use App\RestaurantProfile;
use App\ProfesionistProfile;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();

        $user -> name = 'Andrei Preda';

        $user -> email = 'andreipredafl@gmail.com';

        $user -> password = bcrypt('Dsoft12345');

        // $user -> secret_ingredients = 'Cimpoier';

        // $user -> favorite_food = 'Chicken';

        $user -> confirmed_email = true;

        $user -> save();


        $producer = new ProducerProfile();

        $producer -> name = 'Agricola';

        $producer -> description = 'Agricola';

        $producer -> fiscal_code = 'Js434525';

        $producer -> company_name = 'Agricola SRL';

        $producer -> administrator_name_surname = 'Name and Surname';

        $producer -> website = 'www.agricola.com';

        $producer -> email = 'andreipredafl@yahoo.com';

        $producer -> phone_number = '0740794880';

        $producer -> producer_type_id = '1';

        $producer -> save();


        $restaurant = new RestaurantProfile();

        $restaurant -> name = 'Mamamia Catering';

        $restaurant -> description =  'Mamamia Catering';

        $restaurant -> fiscal_code = '23124344';

        $restaurant -> company_name = 'Mamamia Catering SRL';

        $restaurant -> administrator_name = 'Mamamia administrator name';

        $restaurant -> website = 'www.mamamia.com';

        $restaurant -> company_email = 'contact@mamamia.com';

        $restaurant -> phone_number = '0740794880';

        $restaurant -> save();


        $proffesionist = new ProfesionistProfile();

        $proffesionist -> name = 'Andrei Preda Liber Profesionist';

        $proffesionist -> street_and_number = 'Street, number 113';

        $proffesionist -> postal_code = '104824';

        $proffesionist -> city_id = 1;

        $proffesionist -> phone_number = '0740794880';

        $proffesionist -> website = 'www.andreipreda.com';

        $proffesionist -> is_looking_for_job = 1;

        $proffesionist -> available_for_working = 1;

        $proffesionist -> save();


        $user -> restaurantProfile() -> associate($restaurant);

        $user -> producerProfile() -> associate($producer);

        $user -> profesionistProfile() -> associate($proffesionist);

        $user -> save();

        // Create administrator
        $user = new Administrator();

        $user -> name = 'Andrei Preda';

        $user -> email = 'andreipredafl@gmail.com';

        $user -> password = bcrypt('Dsoft12345');

        $user -> save();
    }
}
