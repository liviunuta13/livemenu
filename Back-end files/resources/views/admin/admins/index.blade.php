@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-12">
                @include('/admin/success')
                @include('/admin/errors')
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>{{ __('Administrators') }}</h4>
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addAdmin"><i class="fas fa-plus"></i> {{ __('Add administrator') }}</button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row float-right mb-3">
                            <button class="btn-danger">
                                <a href="{{ route('admins') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</a>
                            </button>
                            <form class="float-right" action="{{ route('search-admins') }}" method="post">
                                @csrf
                                <input type="text" class="ml-1 border" name="keyword" required>
                                <button type="submit" class="btn-success">{{ __('Search') }}</button>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($admins) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($admins as $key => $admin)
                                        <tr>
                                            <td>{{ $admin -> name }}</td>
                                            <td>{{ $admin -> email }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#editAdmin{{ $admin -> id }}">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    <a class="ml-2" data-toggle="modal" data-target="#deleteAdmin{{ $admin -> id }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/admins/modals/edit')
                                        @include('/admin/admins/modals/delete')
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="mt-4 float-right">
                                {{ $admins -> links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@include('/admin/admins/modals/add')
@endsection
