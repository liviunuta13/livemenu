<div class="modal fade" id="addAdmin">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-admin') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add administrator') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }}</label>
                        <input type="text" name="name" required class="form-control" placeholder="{{ __('Enter name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email address') }}</label>
                        <input type="email" name="email" required class="form-control" placeholder="{{ __('Enter email') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Password') }}</label>
                        <input type="password" name="password" required class="form-control" placeholder="{{ __('Password') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Password Confirmation') }}</label>
                        <input type="password" name="password_confirmation" required class="form-control"  placeholder="{{ __('Password Confirmatin') }}">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
