<div class="modal fade" id="editAdmin{{ $admin -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-admin', ['admin_id' => $admin -> id]) }}" method="post">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit administrator') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }}</label>
                        <input type="text" name="name" required class="form-control" value="{{ $admin -> name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email address') }}</label>
                        <input type="email" name="email" required class="form-control" value="{{ $admin -> email }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('New Password') }}</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>{{ __('New Password Confirmation') }}</label>
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
