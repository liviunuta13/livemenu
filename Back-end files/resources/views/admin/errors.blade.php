@if(Session::has('errors'))
    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
        @foreach (Session::get('errors') as $key => $errors)
            @foreach ($errors as $key => $message)
                {{ __($message) }}
            @endforeach
        @endforeach
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
