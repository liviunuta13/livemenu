<div class="modal fade" id="edit{{ $picture -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-picture', ['picture_id' => $picture -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit picture') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Event Room') }}</label><span class="text-danger">*</span>
                        <select class="form-control" name="event_room_id">
                            <option>Choose a event room</option>
                            @foreach (App\EventRoom::all() as $event_room)
                                <option @if($event_room->id == $picture->event_room_id) selected @endif
                                value="{{ $event_room -> id }}">{{ $event_room -> id }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">{{ __('Switch picture') }}</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="picture" class="custom-file-input"/>
                                <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
