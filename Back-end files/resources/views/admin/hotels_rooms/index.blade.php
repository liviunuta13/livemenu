@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Hotels rooms') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add room') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('rooms') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-rooms') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Hotel') }}</th>
                                        <th>{{ __('Surface') }}</th>
                                        <th>{{ __('Unit') }}</th>
                                        <th>{{ __('Seats') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($rooms) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($rooms as $key => $room)
                                        <tr>
                                            <td>{{ $room -> name }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($room -> hotel))
                                                        <form action="{{ route('search-hotels') }}" method="post">
                                                            @csrf
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $room-> hotel-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $room -> surface }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($room -> unit))
                                                        <form action="{{ route('search-units') }}" method="post">
                                                            @csrf
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $room-> unit-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $room ->seats}}</td>
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    <button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $restaurant -> id }}">{{ __('More') }}</button>
                                                    @include('/admin/restaurants/modals/more')
                                                </div>
                                            </td> --}}
                                            {{--<td>--}}
                                            {{--<div class="d-flex justify-content-center">--}}
                                            {{--<button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $job -> id }}">{{ __('More') }}</button>--}}
                                            {{--@include('/admin/restaurants_job_offers/modals/more')--}}
                                            {{--</div>--}}
                                            {{--</td>--}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $room -> id }}" title="Edit/view room">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/hotels_rooms/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $room -> id }}" title="{{ __('Delete room') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/hotels_rooms/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $rooms -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/hotels_rooms/modals/add')
@endsection
