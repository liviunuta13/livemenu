<div class="modal fade" id="edit{{ $room -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-room', ['room_id' => $room -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit room') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" value="{{ $room->name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Surface') }}</label><span class="text-danger">*</span>
                        <input type="text" name="surface" class="form-control" value="{{ $room->surface }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Unit') }}</label><span class="text-danger">*</span>
                        <select name="unit_id" class="form-control">
                            <option value="">Choose a unit</option>
                            @foreach(\App\Unit::all() as $unit)
                                <option @if($unit->id == $room->unit_id) selected @endif
                                value="{{$unit->id}}">{{$unit->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Hotel') }}</label><span class="text-danger">*</span>
                        <select name="hotel_id" class="form-control">
                            <option value="">Choose a hotel</option>
                            @foreach(\App\Hotel::all() as $hotel)
                                <option @if($hotel->id == $room->hotel_id) selected @endif
                                value="{{$hotel->id}}">{{$hotel->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Seats') }}</label>
                        <input type="number" name="seats" class="form-control"  value="{{ $room->seats }}" >
                    </div>
                    <div class="form-group">
                        <label>{{ __('Beds') }}</label>
                        <input type="number" name="beds_number" class="form-control"  value="{{ $room->beds_number }}" >
                    </div>
                    <div class="form-group">
                        <label>{{ __('Simple bed number') }}</label>
                        <input type="number" name="simple_bed_number" class="form-control" value="{{ $room->simple_bed_number }}"  >
                    </div>
                    <div class="form-group">
                        <label>{{ __('Double bed number') }}</label>
                        <input type="number" name="double_bed_number" class="form-control" value="{{ $room->double_bed_number }}"  >
                    </div>
                    <div class="form-group">
                        <label>{{ __('Triple bed number') }}</label>
                        <input type="number" name="triple_bed_number" class="form-control" value="{{ $room->triple_bed_number }}"  >
                    </div>
                    <div class="form-group">
                        <label>{{ __('Additional seats') }}</label>
                        <input type="number" name="additional_seats" class="form-control" value="{{ $room->additional_seats }}" >
                    </div>
                    <div class="form-group">
                        <label>{{ __('Facility:') }}</label>
                        @foreach(\App\RoomFacility::all() as $room_facility)
                            <input type="checkbox" name="facilities_id[]"  value="{{$room_facility->id}}">{{$room_facility->name}}
                        @endforeach
                    </div>
                </div>
                    <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                            <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                    </div>
            </form>
        </div>
        <!-- /.modal-content -->s
    </div>
    <!-- /.modal-dialog -->
</div>
