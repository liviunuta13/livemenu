<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-ingredient') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add ingredient') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" placeholder="{{__('Enter name')}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Price') }}</label><span class="text-danger">*</span>
                        <input type="text" name="price" class="form-control" placeholder="{{__('Enter the price')}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Amount') }}</label><span class="text-danger">*</span>
                        <input type="text" name="amount" class="form-control" placeholder="{{__('Enter the amount')}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Unit') }}</label><span class="text-danger">*</span>
                        <select name="unit_id" class="form-control">
                            <option value="">Choose a unit</option>
                            @foreach(\App\Unit::all() as $unit)
                                <option value="{{$unit->id}}">{{$unit->name}}</option >
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Currency') }}</label><span class="text-danger">*</span>
                        <select name="currency_id" class="form-control">
                            <option value="">Choose a currency</option>
                            @foreach(\App\Currency::all() as $currency)
                                <option value="{{$currency->id}}">{{$currency->name}}</option >
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Allergen:') }}</label>
                        @foreach(\App\IngredientAllergy::all() as $allergy)
                            <input type="checkbox" name="allergies_id[]"  value="{{$allergy->id}}">{{$allergy->name}}
                        @endforeach
                </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
