@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Ingredients Allergies') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add allergy') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('allergies') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-allergies') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($allergies) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($allergies as $key => $allergy)
                                        <tr>
                                            <td>{{ $allergy -> name }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $allergy -> id }}" title="Edit/view allergy">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/ingredients_allergies/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $allergy -> id }}" title="{{ __('Delete allergy') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/ingredients_allergies/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $allergies -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/ingredients_allergies/modals/add')
@endsection
