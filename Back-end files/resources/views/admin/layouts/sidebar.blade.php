<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link d-flex justify-content-center">
        <span class="brand-text font-weight-light">LiveMenu.Info</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/img/profile.png" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block"> {{auth()->user()->name!=null ? auth()->user()->name : "Administrator"}} </a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview">
                    <li class="nav-item">
                        <a href="{{ url('/admin/dashboard') }}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                {{ __('Dashboard') }}
                                <span class="right badge badge-danger">New</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-header">{{ __('PROFILES') }}</li>
                    <li class="nav-item">
                        <a href="{{ route('users') }}" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>{{ __('Users') }}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admins') }}" class="nav-link">
                            <i class="nav-icon fas fa-users-cog"></i>
                            <p>{{ __('Administrators') }}</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a  class="nav-link">
                            <i class="fas fa-hammer"></i>
                            <p>
                                {{ __('Professionists') }}
                                <i class="fa fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('professionists') }}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Profiles') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('educations') }}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Educations') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-utensils"></i>
                            <p>
                                {{ __('Restaurants') }}
                                <i class="fa fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('restaurants') }}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Profiles') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('locations') }}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Locations') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('specifics') }}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Specifics') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('jobs') }}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Job offers') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('units') }}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Units') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a  class="nav-link">
                            <i class="fas fa-hammer"></i>
                            <p>
                                {{ __('Producers') }}
                                <i class="fa fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('producers') }}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Profiles') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('producer_locations') }}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Locations') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Products') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('ingredients')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Ingredients') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('allergies')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Allergies') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a  class="nav-link">
                            <i class="fas fa-map-marker-alt"></i>
                            <p>
                                {{ __('Restaurants locations') }}
                                <i class="fa fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('staffs')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Staff') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('roles')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Staff roles') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('events')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Events') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('utilities')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Utilities') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('facilities')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Facilities') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Posts') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('promotions')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Promotions') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('programs')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Programs') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('programs_conditions')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Program condition') }}</p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="fas fa-utensils"></i>
                                    <p>
                                        {{ __('Hotels') }}
                                        <i class="fa fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('hotels') }}" class="nav-link">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>{{ __('Profiles') }}</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('hotels_facilities') }}" class="nav-link">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>{{ __('Facility') }}</p>
                                        </a>
                                    </li>
                                    <li class="nav-item has-treeview">
                                        <a href="#" class="nav-link">
                                            <i class="fas fa-bed"></i>
                                            <p>
                                                {{ __('Rooms') }}
                                                <i class="fa fa-angle-left right"></i>
                                            </p>
                                        </a>
                                        <ul class="nav nav-treeview">
                                            <li class="nav-item">
                                                <a href="{{ route('rooms') }}" class="nav-link">
                                                    <i class="fa fa-circle-o nav-icon"></i>
                                                    <p>{{ __('Profiles') }}</p>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('rooms_facilities') }}" class="nav-link">
                                                    <i class="fa fa-circle-o nav-icon"></i>
                                                    <p>{{ __('Facility') }}</p>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('room_events') }}" class="nav-link">
                                                    <i class="fa fa-circle-o nav-icon"></i>
                                                    <p>{{ __('Events') }}</p>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('events_facilities') }}" class="nav-link">
                                                    <i class="fa fa-circle-o nav-icon"></i>
                                                    <p>{{ __('Events Facility') }}</p>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('pictures') }}" class="nav-link">
                                                    <i class="fa fa-circle-o nav-icon"></i>
                                                    <p>{{ __('Events Pictures') }}</p>
                                                </a>
                                            </li>
                                      </ul>
                                  </li>
                            <li class="nav-item">
                                <a href="" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Menus') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>{{ __('Job offers') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/admin/dashboard') }}" class="nav-link" data-toggle="modal" data-target="#logout">
                            <i class="fas fa-sign-out-alt ml-1"></i>
                            <p>{{ __('Logout') }}</p>
                        </a>
                    </li>
                    @include('/admin/logout')
                </ul>
            </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
