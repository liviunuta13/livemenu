@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-12">
                @include('/admin/success')
                @include('/admin/errors')
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>{{ __('Producers profiles') }}</h4>
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add producer') }}</button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row float-right mb-3">
                            <button class="btn-danger">
                                <a href="{{ route('producers') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</a>
                            </button>
                            <form class="float-right" action="{{ route('search-producers') }}" method="post">
                                @csrf
                                <input type="text" class="ml-1 border" name="keyword" required>
                                <button type="submit" class="btn-success">{{ __('Search') }}</button>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Phone number') }}</th>
                                        <th>{{ __('Company name') }}</th>
                                        <th>{{ __('Logo') }}</th>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($producers) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($producers as $key => $producer)
                                        <tr>
                                            <td>{{ $producer -> name }}</td>
                                            <td>{{ $producer -> email }}</td>
                                            <td>{{ $producer -> phone_number }}</td>
                                            <td>{{ $producer -> company_name }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <img src="data:image/png;base64, {{ $producer -> logo }}" alt="Logo" class="img-responsive" style="width: 70px; height: auto;">
                                                </div>
                                            </td>
                                            <td>
                                                @if(isset($producer -> producerType))
                                                    {{ __($producer -> producerType -> name) }}
                                                @endif
                                            </td>
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($producer -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $producer -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $producer -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $producer -> id }}">{{ __('More') }}</button>
                                                    @include('/admin/producers/modals/more')
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $producer -> id }}" title="Edit/view restaurant">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/producers/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $producer -> id }}" title="{{ __('Delete restaurant') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                    @if($producer -> suspended == 1)
                                                        <a class="ml-2" href="{{ route('suspend-producer', ['producer_id' => $producer -> id]) }}">
                                                            <i class="fa fa-check text-success" data-toggle="modal" title="{{ __('Unsuspend producer') }}"></i>
                                                        </a>
                                                    @else
                                                        <a class="ml-2" href="{{ route('suspend-producer', ['producer_id' => $producer -> id]) }}">
                                                            <i class="fa fa-ban text-danger" data-toggle="modal" title="{{ __('Suspend producer') }}"></i>
                                                        </a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/producers/modals/delete')
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Phone number') }}</th>
                                        <th>{{ __('Company name') }}</th>
                                        <th>{{ __('Logo') }}</th>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="mt-4 float-right">
                                {{ $producers -> links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@include('/admin/producers/modals/add')
@endsection
