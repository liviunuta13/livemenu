<div class="modal fade" id="edit{{ $producer -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-producer', ['producer_id' => $producer -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit restaurant') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" value="{{ $producer -> name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Phone number') }} <span class="text-danger">*</span></label>
                        <input type="text" name="phone_number" required class="form-control" value="{{ $producer -> phone_number }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Company name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="company_name" required class="form-control" value="{{ $producer -> company_name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Fiscal code') }} <span class="text-danger">*</span></label>
                        <input type="text" name="fiscal_code" required class="form-control" value="{{ $producer -> fiscal_code }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Administrator name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="administrator_name" required class="form-control" value="{{ $producer -> administrator_name_surname }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Select producer type') }} <span class="text-danger">*</span></label>
                        <select class="form-control" name="producer_type_id">
                            @foreach ($producer_types as $key => $producer_type)
                                <option value="{{ $producer_type -> id }}">{{ $producer_type -> name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Company email') }} <span class="text-danger">*</span></label>
                        <input type="email" name="email" class="form-control" value="{{ $producer -> email }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Website') }}</label>
                        <input type="text" name="website" class="form-control" value="{{ $producer -> website }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">{{ __('Logo') }}</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="logo" class="custom-file-input">
                                <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <textarea name="description" rows="6" class="form-control">{{ $producer -> description }}</textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
