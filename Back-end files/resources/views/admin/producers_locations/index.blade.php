@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Producers locations') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add location') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('producer_locations') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-producer_locations') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Producer profile') }}</th>
                                        <th>{{ __('City ') }}</th>
                                        <th>{{ __('County') }}</th>
                                        <th>{{ __('Country') }}</th>
                                        <th>{{ __('Street') }}</th>
                                        <th>{{ __('Number') }}</th>
                                        <th>{{ __('Phone number') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Contact Person Name') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($producer_locations) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($producer_locations as $key => $producer_location)
                                        <tr>
                                            <td>{{ $producer_location -> name }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($producer_location -> producerProfile))
                                                        <form action="{{ route('search-producers') }}" method="post">
                                                            @csrf
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i>{{ $producer_location -> producerProfile-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $producer_location -> city->name }}</td>
                                            <td>{{ $producer_location -> county->name }}</td>
                                            <td>{{ $producer_location -> country->name }}</td>
                                            <td>{{ $producer_location -> street }}</td>
                                            <td>{{ $producer_location -> number }}</td>
                                            <td>{{ $producer_location -> phone_number }}</td>
                                            <td>{{ $producer_location -> email}}</td>
                                            <td>{{ $producer_location -> contact_person_name }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a class="ml-2" data-toggle="modal" data-target="#edit{{ $producer_location -> id }}" title="{{ __('Edit location') }}">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    <a data-toggle="modal" data-target="#delete{{ $producer_location -> id }}" title="Delete location">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                    @include('/admin/producers_locations/modals/edit')
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/producers_locations/modals/delete')
                                    @endforeach
                                    </tbody>

                                </table>
                                <div class="mt-4 float-right">
                                    {{ $producer_locations -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/producers_locations/modals/add')
@endsection


