<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-producer_location') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add location') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Producer Profile') }}</label> <span class="text-danger">*</span>
                        <select name="producer_profile_id" class="form-control">
                            <option value="">Choose a restaurant profile</option>
                            @foreach(\App\ProducerProfile::all() as $producer_profile)
                                <option value="{{$producer_profile->id}}">{{$producer_profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('City') }}</label><span class="text-danger">*</span>
                        <select name="city_id" class="form-control">
                            <option value="">--Please choose a city--</option>
                            @foreach(\App\City::all() as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('County') }}</label><span class="text-danger">*</span>
                        <select name="county_id" class="form-control">
                            <option value="">--Please choose a county--</option>
                            @foreach(\App\County::all() as $county)
                                <option value="{{$county->id}}">{{$county->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Country') }}</label><span class="text-danger">*</span>
                        <select name="country_id" class="form-control">
                            <option value="">--Please choose a country--</option>
                            @foreach(\App\Country::all() as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" placeholder="{{ __('Enter name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Street') }}</label><span class="text-danger">*</span>
                        <input type="text" name="street" class="form-control" placeholder="{{ __('Enter street') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Number ') }}</label><span class="text-danger">*</span>
                        <input type="number" name="number" class="form-control" placeholder="{{ __('Enter number of street') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Postal code') }}</label>
                        <input type="number" name="postal_code" class="form-control" placeholder="{{ __('Enter postal code') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Latitude') }}</label>
                        <input type="text" name="latitude" class="form-control" placeholder="{{ __('Enter latitude') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Longitude') }}</label>
                        <input type="text" name="longitude" class="form-control" placeholder="{{ __('Enter longitude') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Phone number') }}</label><span class="text-danger">*</span>
                        <input type="number" name="phone_number" class="form-control" placeholder="{{ __('Enter phone number') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Fixed phone number') }}</label>
                        <input type="number" name="fixed_phone_number" class="form-control" placeholder="{{ __('Enter fixed phone number') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email') }}</label><span class="text-danger">*</span>
                        <input type="email" name="email" class="form-control" placeholder="{{ __('Enter email') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Contact person name') }}</label><span class="text-danger">*</span>
                        <input type="text" name="contact_person_name" class="form-control" placeholder="{{ __('Enter contact person name') }}">
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label for="exampleInputFile">{{ __('Image') }}</label>--}}
                        {{--<div class="input-group">--}}
                            {{--<div class="custom-file">--}}
                                {{--<input type="file" name="picture[]" class="custom-file-input" multiple="true" >--}}
                                {{--<label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <textarea name="description" rows="6" class="form-control" placeholder="{{ __('Enter description') }}"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
