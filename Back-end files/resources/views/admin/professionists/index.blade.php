@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-12">
                @include('/admin/success')
                @include('/admin/errors')
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>{{ __('Professionists') }}</h4>
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add professionist') }}</button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row float-right mb-3">
                            <button class="btn-danger">
                                <a href="{{ route('professionists') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                            </button>
                            <form class="float-right" action="{{ route('search-professionists') }}" method="post">
                                @csrf
                                <input type="text" class="ml-1 border" name="keyword" required>
                                <button type="submit" class="btn-success">{{ __('Search') }}</button>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Phone Number') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Picture') }}</th>
                                        <th>{{ __('User\'s profile') }}</th>
                                        <th>{{ __('CV') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($professionists) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($professionists as $key => $professionist)
                                        <tr>
                                            <td>{{ $professionist -> name }}</td>
                                            <td>{{ $professionist -> email }}</td>
                                            <td>{{ $professionist -> phone_number }}</td>
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    <button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $professionist -> id }}">{{ __('More') }}</button>
                                                </div>
                                                @include('/admin/parts/professionist/more')
                                            </td> --}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <img src="data:image/png;base64, {{ $professionist -> picture }}" alt="Profile picture" class="img-responsive" style="width: 70px; height: auto;">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($professionist -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $professionist -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $professionist -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a href="{{ route('export-cv', ['professionist_id' => $professionist -> id]) }}">
                                                        <button type="button" class="btn-danger"><i class="fas fa-download"></i> {{ __('CV') }}</button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" title="{{ __('View/edit professionist') }}" data-target="#edit{{ $professionist -> id }}">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/professionists/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" title="{{ __('Delete professionist') }}" data-target="#delete{{ $professionist -> id }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                    @if($professionist -> suspended == 1)
                                                        <a class="ml-2" href="{{ route('suspend-professionist', ['professionist_id' => $professionist -> id]) }}">
                                                            <i class="fa fa-check text-success" data-toggle="modal" title="{{ __('Unsuspend professionist') }}"></i>
                                                        </a>
                                                    @else
                                                        <a class="ml-2" href="{{ route('suspend-professionist', ['professionist_id' => $professionist -> id]) }}">
                                                            <i class="fa fa-ban text-danger" data-toggle="modal" title="{{ __('Suspend professionist') }}"></i>
                                                        </a>
                                                    @endif
                                                </div>
                                            </td>
                                            @include('/admin/professionists/modals/delete')
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Phone Number') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Picture') }}</th>
                                        <th>{{ __('User\'s profile') }}</th>
                                        <th>{{ __('CV') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="mt-4 float-right">
                                {{ $professionists -> links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@include('/admin/professionists/modals/add')
@endsection
