<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-professionist') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add professionist') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }}</label>
                        <input type="text" name="name" required class="form-control" placeholder="{{ __('Enter name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email address') }}</label>
                        <input type="email" name="email" required class="form-control" placeholder="{{ __('Enter email') }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">{{ __('Profile picture') }}</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="picture" class="custom-file-input">
                                <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Select user') }}</label>
                        <select class="form-control" name="user_id">
                            <option></option>
                            @foreach ($users as $key => $user)
                                <option value="{{ $user -> id }}">{{ $user -> name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
