<div class="modal fade" id="edit{{ $professionist -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-professionist', ['professionist_id' => $professionist -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit professionist') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span> </label>
                        <input type="text" name="name" required class="form-control" value="{{ $professionist -> name }}"/>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email address') }} <span class="text-danger">*</span></label>
                        <input type="email" name="email" required class="form-control" value="{{ $professionist -> email }}"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">{{ __('Switch profile picture') }}</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="picture" class="custom-file-input"/>
                                <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
