<div class="modal fade" id="edit{{ $professionist -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-professionist', ['professionist_id' => $professionist -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit professionist') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personalInformations" role="tab" aria-controls="home" aria-selected="true">{{ __('Personal informations') }}</a>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#professionalExperience" role="tab" aria-controls="profile" aria-selected="false">{{ __('Professional experience') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#education" role="tab" aria-controls="contact" aria-selected="false">{{ __('Education') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#personalSkills" role="tab" aria-controls="contact" aria-selected="false">{{ __('Personal skills') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#other" role="tab" aria-controls="contact" aria-selected="false">{{ __('Other') }}</a>
                        </li> --}}
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="personalInformations" role="tabpanel" aria-labelledby="home-tab">
                            <div class="form-group mt-4">
                                <label>{{ __('Name') }} <span class="text-danger">*</span> </label>
                                <input type="text" name="name" required class="form-control" value="{{ $professionist -> name }}">
                            </div>
                            <div class="form-group">
                                <label>{{ __('Email address') }}</label>
                                <input type="email" name="email" required class="form-control" value="{{ $professionist -> email }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">{{ __('Switch profile picture') }}</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="picture" class="custom-file-input">
                                        <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label>{{ __('Street and number') }}</label>
                                <input type="text" name="street_and_number" required class="form-control" value="{{ $professionist -> street_and_number }}">
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>{{ __('Postal code') }}</label>
                                    <input type="email" name="postal_code" required class="form-control" value="{{ $professionist -> postal_code }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>{{ __('City') }}</label>
                                    <select class="form-control" name="city">
                                        <option value="{{ $professionist -> city -> id }}">{{ $professionist -> city -> name }}</option>
                                        @foreach ($cities as $key => $city)
                                            @if($city -> id != $professionist -> city -> id)
                                                <option value="{{ $city -> id }}">{{ $city -> name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>{{ __('County') }}</label>
                                    <select class="form-control" name="county">
                                        @if($professionist -> county)
                                            <option value="{{ $professionist -> county -> id }}">{{ $professionist -> county -> name }}</option>
                                            @foreach ($counties as $key => $county)
                                                @if($county -> id != $professionist -> county -> id)
                                                    <option value="{{ $county -> id }}">{{ $county -> name }}</option>
                                                @endif
                                            @endforeach
                                        @else
                                            @foreach ($counties as $key => $county)
                                                <option value="{{ $county -> id }}">{{ $county -> name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>{{ __('Country') }}</label>
                                    <select class="form-control" name="country">
                                        @if($professionist -> country)
                                            <option value="{{ $professionist -> country -> id }}">{{ $professionist -> country -> name }}</option>
                                            @foreach ($countries as $key => $country)
                                                @if($country -> id != $professionist -> country -> id)
                                                    <option value="{{ $country -> id }}">{{ $country -> name }}</option>
                                                @endif
                                            @endforeach
                                        @else
                                            @foreach ($countries as $key => $country)
                                                <option value="{{ $country -> id }}">{{ $country -> name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{ __('Phone number') }}</label>
                                <input type="text" name="phone_number" required class="form-control" value="{{ $professionist -> phone_number }}">
                            </div>
                            <div class="form-group">
                                <label>{{ __('Website') }}</label>
                                <input type="text" name="website" required class="form-control" value="{{ $professionist -> website }}">
                            </div> --}}
                        </div>
                        {{-- <div class="tab-pane fade" id="professionalExperience" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="form-group mt-4">
                                <label>{{ __('Experienta profesionala') }}</label>
                                <input type="text" name="professional_experience" required class="form-control" value="{{ $professionist -> professional_experience }}">
                            </div>
                            <div class="form-group mt-4">
                                <label>{{ __('Istoric profesional') }}</label>
                                @foreach ($professionist -> professionalHistory as $key => $history)
                                    <div class="card border">
                                        <div class="row ml-2 mr-2 mt-3">
                                            <div class="form-group col-md-6">
                                                <label>{{ __('De la') }}</label>
                                                <input type="date" name="history_begin_date[]" required class="form-control" value="{{ $history -> begin_date }}">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>{{ __('Pana la') }}</label>
                                                <input type="date" name="history_end_date[]" required class="form-control" value="{{ $history -> end_date }}">
                                                <label class="mt-2">{{ __('Present') }}</label>
                                                @if($history -> present == 1)
                                                    <input type="checkbox" name="history_present[]" checked>
                                                @else
                                                    <input type="checkbox" name="history_present[]">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group ml-2 mr-2">
                                            <label>{{ __('Working as') }}</label>
                                            <input type="text" name="history_working_as[]" required class="form-control" value="{{ $history -> working_as }}">
                                        </div>
                                        <div class="form-group ml-2 mr-2">
                                            <label>{{ __('Company name') }}</label>
                                            <input type="text" name="history_company_name[]" required class="form-control" value="{{ $history -> company_name }}">
                                        </div>
                                        <div class="row ml-2 mr-2">
                                            <div class="form-group col-md-6">
                                                <label>{{ __('City') }}</label>
                                                <select class="form-control" name="history_city[]">
                                                    <option value="{{ $history -> city_id }}">{{ $history -> city -> name }}</option>
                                                    @foreach ($cities as $key => $city)
                                                        @if($city -> id != $history -> city_id)
                                                            <option value="{{ $city -> id }}">{{ $city -> name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>{{ __('Country') }}</label>
                                                <select class="form-control" name="history_country[]">
                                                    @if($history -> country)
                                                        <option value="{{ $history -> country_id }}">{{ $history -> country -> name }}</option>
                                                        @foreach ($countries as $key => $country)
                                                            @if($country -> id != $history -> country_id)
                                                                <option value="{{ $country -> id }}">{{ $country -> name }}</option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        @foreach ($countries as $key => $country)
                                                            <option value="{{ $country -> id }}">{{ $country -> name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group ml-2 mr-2">
                                            <label>{{ __('Description') }}</label>
                                            <input type="text" name="history_description[]" required class="form-control" value="{{ $history -> description }}">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div> --}}
                        {{-- <div class="tab-pane fade" id="education" role="tabpanel" aria-labelledby="contact-tab">
                            <label>{{ __('Education') }}</label>
                            @foreach ($professionist -> education as $key => $education)
                                <div class="card border">
                                    <div class="row ml-2 mr-2 mt-3">
                                        <div class="form-group col-md-6">
                                            <label>{{ __('De la') }}</label>
                                            <input type="date" name="education_begin_date[]" required class="form-control" value="{{ $education -> begin_date }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Pana la') }}</label>
                                            <input type="date" name="education_end_date[]" required class="form-control" value="{{ $education -> end_date }}">
                                            <label class="mt-2">{{ __('Present') }}</label>
                                            @if($history -> present == 1)
                                                <input type="checkbox" name="education_present[]" checked>
                                            @else
                                                <input type="checkbox" name="education_present[]">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group ml-2 mr-2">
                                        <label>{{ __('Certification name') }}</label>
                                        <input type="text" name="education_certificate_name[]" required class="form-control" value="{{ $education -> certificate_name }}">
                                    </div>
                                    <div class="form-group ml-2 mr-2">
                                        <label>{{ __('Institution name') }}</label>
                                        <input type="text" name="education_institution_name[]" required class="form-control" value="{{ $education -> institution_name }}">
                                    </div>
                                    <div class="row ml-2 mr-2">
                                        <div class="form-group col-md-6">
                                            <label>{{ __('City') }}</label>
                                            <select class="form-control" name="education_city[]">
                                                <option value="{{ $history -> city_id }}">{{ $education -> city -> name }}</option>
                                                @foreach ($cities as $key => $city)
                                                    @if($city -> id != $education -> city_id)
                                                        <option value="{{ $city -> id }}">{{ $city -> name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Country') }}</label>
                                            <select class="form-control" name="education_country[]">
                                                @if($education -> country)
                                                    <option value="{{ $education -> country_id }}">{{ $education -> country -> name }}</option>
                                                    @foreach ($countries as $key => $country)
                                                        @if($country -> id != $education -> country_id)
                                                            <option value="{{ $country -> id }}">{{ $country -> name }}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    @foreach ($countries as $key => $country)
                                                        <option value="{{ $country -> id }}">{{ $country -> name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group ml-2 mr-2">
                                        <label>{{ __('Description') }}</label>
                                        <input type="text" name="education_description[]" required class="form-control" value="{{ $education -> description }}">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="personalSkills" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="form-group ml-2 mr-2">
                                <label>{{ __('Matern language') }}</label>
                                <input type="text" name="matern_language" required class="form-control" value="{{ $professionist -> matern_language }}">
                            </div>
                            <div class="form-group ml-2 mr-2">
                                <label>{{ __('Foreign languages') }}</label>
                                <input type="text" name="foreign_languages" required class="form-control" value="{{ $professionist -> foreign_languages }}">
                            </div>
                            <div class="form-group ml-2 mr-2">
                                <label>{{ __('Managerial skills') }}</label>
                                <input type="text" name="managerial_skills" required class="form-control" value="{{ $professionist -> managerial_skills }}">
                            </div>
                            <div class="form-group ml-2 mr-2">
                                <label>{{ __('Skills at working place ') }}</label>
                                <input type="text" name="skills_at_working_place" required class="form-control" value="{{ $professionist -> skills_at_working_place }}">
                            </div>
                            <div class="form-group ml-2 mr-2">
                                <label>{{ __('Digital skills') }}</label>
                                <input type="text" name="digital_skills" required class="form-control" value="{{ $professionist -> digital_skills }}">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="other" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="form-group ml-2 mr-2 mt-3">
                                <label>{{ __('Driving licence') }}</label>
                                <input type="text" name="driving_licence" required class="form-control" value="{{ $professionist -> driving_licence }}">
                            </div>
                            <div class="form-group ml-2 mr-2">
                                <label>{{ __('Other skills') }}</label>
                                <input type="text" name="other_skills" required class="form-control" value="{{ $professionist -> other_skills }}">
                            </div>
                            <div class="form-group ml-2 mr-2">
                                <label>{{ __('Personal description') }}</label>
                                <input type="text" name="personal_description" required class="form-control" value="{{ $professionist -> personal_description }}">
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        <div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
