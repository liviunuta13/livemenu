@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Professionists educations') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add education') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('educations') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-educations') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Preofessionist profile') }}</th>
                                        <th>{{ __('City') }}</th>
                                        <th>{{ __('Country') }}</th>
                                        <th>{{ __('Certificate name') }}</th>
                                        <th>{{ __('Institution name') }}</th>
                                        <th>{{ __('Begin date') }}</th>
                                        <th>{{ __('End date') }}</th>
{{--                                        <th>{{ __('Present') }}</th>--}}
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($educations) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($educations as $key => $education)
                                        <tr>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($education -> profesionistProfile))
                                                        <form action="{{ route('search-professionists') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $education -> profesionistProfile -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $education-> profesionistProfile-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $education -> city -> name}}</td>
                                            <td>{{ $education -> country->name }}</td>
                                            <td>{{ $education -> certificate_name}}</td>
                                            <td>{{ $education -> institution_name}}</td>
                                            <td>{{ $education -> begin_date}}</td>
                                            <td>{{ $education -> end_date}}</td>
                                            {{--<td>{{ $education -> present}}</td>--}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $education -> id }}" title="Edit/view education">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/professionists_educations/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $education -> id }}" title="{{ __('Delete education') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/professionists_educations/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $educations -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/professionists_educations/modals/add')
@endsection
