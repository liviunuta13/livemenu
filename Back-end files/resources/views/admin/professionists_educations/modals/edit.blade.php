<div class="modal fade" id="edit{{ $education -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-education', ['education_id' => $education -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add education ') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Professionist profile') }}</label><span class="text-danger">*</span>
                        <select name="profesionist_profile_id" class="form-control">
                            <option value="">Choose a professionist profile</option>
                            @foreach(\App\ProfesionistProfile::all() as $profile)
                                <option @if($profile->id == $education->profesionist_profile_id) selected @endif
                                value="{{$profile->id}}">{{$profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('City') }}</label><span class="text-danger">*</span>
                        <select name="city_id" class="form-control">
                            <option value="">Choose a city</option>
                            @foreach(\App\City::all() as $city)
                                <option @if($city->id == $education->city_id) selected @endif
                                value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __(' Country') }}</label><span class="text-danger">*</span>
                        <select name="country_id" class="form-control">
                            <option value="">Choose country</option>
                            @foreach(\App\Country::all() as $country)
                                <option @if($country->id == $education->country_id) selected @endif
                                value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Begin date ') }}</label><span class="text-danger">*</span>
                        <input type="date" name="begin_date" class="form-control" value="{{$education->begin_date}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('End date ') }}</label><span class="text-danger">*</span>
                        <input type="date" name="end_date" class="form-control" value="{{$education->end_date}}">
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label>{{ __('Present ') }}</label><span class="text-danger">*</span>--}}
                        {{--<input type="date" name="present" class="form-control" value="{{$education->present}}">--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <label>{{ __('Certificate name') }}</label><span class="text-danger">*</span>
                        <input type="text" name="certificate_name" class="form-control"value="{{$education->certificate_name}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Institution name') }}</label><span class="text-danger">*</span>
                        <input type="text" name="institution_name" class="form-control" value="{{$education->institution_name}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <textarea name="description" rows="6" class="form-control" >{{$education->description}}</textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
