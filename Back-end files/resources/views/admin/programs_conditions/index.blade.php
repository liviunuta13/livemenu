@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Restaurants programs condition') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add program condition') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('programs_conditions') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-programs_conditions') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __(' Name') }}</th>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Start_date') }}</th>
                                        <th>{{ __('End_date') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($program_conditions) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>

                                    @endif
                                    @foreach ($program_conditions as $key => $program_condition)
                                        <tr>
                                            <td>{{ $program_condition -> name }}</td>
                                            <td>{{ $program_condition -> type }}</td>
                                            <td>{{ $program_condition -> start_date }}</td>
                                            <td>{{ $program_condition -> end_date }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $program_condition ->id }}" title="Edit/view program">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/programs_conditions/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $program_condition ->id }}" title="{{ __('Delete program condition') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/programs_conditions/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $program_conditions -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/programs_conditions/modals/add')
@endsection


