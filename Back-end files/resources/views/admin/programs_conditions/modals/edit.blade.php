<div class="modal fade" id="edit{{ $program_condition -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-program_condition', ['program_condition_id' => $program_condition -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit program condition') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }}</label><span class="text-danger">*</span>
                        <input type="text" name="name" class="form-control"  value="{{$program_condition->name}}" >
                    </div>
                    <div class="form-group">
                        <label>{{ __('Type') }}</label><span class="text-danger">*</span>
                        <input type="text" name="type" class="form-control"  value="{{$program_condition->type}}" >
                    </div>
                    <div class="form-group">
                        <label>{{ __('Start date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="start_date" class="form-control"  value="{{$program_condition->start_date}}" >
                    </div>
                    <div class="form-group">
                        <label>{{ __('End date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="end_date" class="form-control"  value="{{$program_condition->end_date}}" >
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
