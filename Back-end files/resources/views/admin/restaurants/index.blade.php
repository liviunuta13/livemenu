@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-12">
                @include('/admin/success')
                @include('/admin/errors')
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>{{ __('Restaurants profiles') }}</h4>
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add restaurant') }}</button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row float-right mb-3">
                            <button class="btn-danger">
                                <a href="{{ route('restaurants') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                            </button>
                            <form class="float-right" action="{{ route('search-restaurants') }}" method="post">
                                @csrf
                                <input type="text" class="ml-1 border" name="keyword" required>
                                <button type="submit" class="btn-success">{{ __('Search') }}</button>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Phone number') }}</th>
                                        <th>{{ __('Company name') }}</th>
                                        <th>{{ __('Logo') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($restaurants) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($restaurants as $key => $restaurant)
                                        <tr>
                                            <td>{{ $restaurant -> name }}</td>
                                            <td>{{ $restaurant -> company_email }}</td>
                                            <td>{{ $restaurant -> phone_number }}</td>
                                            <td>{{ $restaurant -> company_name }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <img src="data:image/png;base64, {{ $restaurant -> logo }}" alt="Logo" class="img-responsive" style="width: 70px; height: auto;">
                                                </div>
                                            </td>
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    <button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $restaurant -> id }}">{{ __('More') }}</button>
                                                    @include('/admin/restaurants/modals/more')
                                                </div>
                                            </td> --}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $restaurant -> id }}">{{ __('More') }}</button>
                                                    @include('/admin/restaurants/modals/more')
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $restaurant -> id }}" title="Edit/view restaurant">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/restaurants/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $restaurant -> id }}" title="{{ __('Delete restaurant') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                    @if($restaurant -> suspended == 1)
                                                        <a class="ml-2" href="{{ route('suspend-restaurant', ['restaurant_id' => $restaurant -> id]) }}">
                                                            <i class="fa fa-check text-success" data-toggle="modal" title="{{ __('Unsuspend restaurant') }}"></i>
                                                        </a>
                                                    @else
                                                        <a class="ml-2" href="{{ route('suspend-restaurant', ['restaurant_id' => $restaurant -> id]) }}">
                                                            <i class="fa fa-ban text-danger" data-toggle="modal" title="{{ __('Suspend restaurant') }}"></i>
                                                        </a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/restaurants/modals/delete')
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Phone number') }}</th>
                                        <th>{{ __('Company name') }}</th>
                                        <th>{{ __('Logo') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="mt-4 float-right">
                                {{ $restaurants -> links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@include('/admin/restaurants/modals/add')
@endsection
