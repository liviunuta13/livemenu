<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-restaurant') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add restaurant') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" placeholder="{{ __('Enter name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Phone number') }} <span class="text-danger">*</span></label>
                        <input type="text" name="phone_number" required class="form-control" placeholder="{{ __('Enter phone number') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Company name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="company_name" required class="form-control" placeholder="{{ __('Enter company name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Fiscal code') }} <span class="text-danger">*</span></label>
                        <input type="number" name="fiscal_code" required class="form-control" placeholder="{{ __('Enter fiscal code') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Administrator name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="administrator_name" required class="form-control" placeholder="{{ __('Enter administrator name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Company email') }}</label>
                        <input type="email" name="company_email" class="form-control" placeholder="{{ __('Enter company email') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Website') }}</label>
                        <input type="text" name="website" class="form-control" placeholder="{{ __('Enter website') }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">{{ __('Logo') }}</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="logo" class="custom-file-input">
                                <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <textarea type="number" name="description" rows="6" class="form-control" placeholder="{{ __('Enter phone number') }}"></textarea>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
