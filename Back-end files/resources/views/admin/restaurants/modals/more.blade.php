<div class="modal fade" id="more{{ $restaurant -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ __('More details about the restaurant ') . $restaurant -> name }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <h5 class="info-box-text text-center text-muted">{{ __('Producer profile') }}</h5>
                <div class="d-flex justify-content-center">
                    @if(isset($restaurant -> user[0]) && isset($restaurant -> user[0] -> name) && isset($restaurant -> user[0] -> email))
                        <form action="{{ route('search-user') }}" method="post">
                            @csrf
                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                            <button class="alert-success" type="submit">
                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                            </button>
                        </form>
                    @endif
                </div>
                <hr>
                <div class="col-md-12 order-2 order-md-1 ml-1 mr-1 mt-4">
                    <h5 class="info-box-text text-center text-muted mb-4">{{ __('Restaurant\'s locations') }}</h5>
                    @if(count($restaurant -> restaurantLocations) > 0)
                        <div class="row">
                            @foreach ($restaurant -> restaurantLocations as $key => $location)
                                <div class="col-6 col-md-6">
                                    <a href="">
                                        <div class="info-box bg-light">
                                            <div class="info-box-content text-center">
                                                {{ __($location -> name) }}<br>
                                                <i class="fas fa-map-marker-alt"></i>
                                                <span>{{ $location -> city -> name }}</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="d-flex justify-content-center">---</div>
                    @endif
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
