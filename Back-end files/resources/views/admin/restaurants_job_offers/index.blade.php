@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                     @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Restaurants jobs') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add job') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('jobs') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-jobs') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Location') }}</th>
                                        <th>{{ __('Restaurant location') }}</th>
                                        <th>{{ __('Restaurant profile') }}</th>
                                        <th>{{ __('Type contract') }}</th>
                                        <th>{{ __('Salary') }}</th>
                                        <th>{{ __('Currency') }}</th>
                                        <th>{{ __('Producer') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($jobs) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($jobs as $key => $job)
                                        <tr>
                                            <td>{{ $job -> name }}</td>
                                            <td>{{ $job -> location->name }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($job -> restaurantLocation))
                                                        <form action="{{ route('search-location') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $job -> restaurantLocation -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $job-> restaurantLocation-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($job -> restaurantProfile))
                                                        <form action="{{ route('search-restaurants') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $job -> restaurantProfile -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $job-> restaurantProfile-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $job -> contractType -> name}}</td>
                                            <td>{{ $job -> salary }}</td>
                                            <td>{{ $job -> currency -> name}}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($job -> producerProfile))
                                                        <form action="{{ route('search-producers') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $job -> producerProfile -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $job-> producerProfile-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>

                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    <button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $restaurant -> id }}">{{ __('More') }}</button>
                                                    @include('/admin/restaurants/modals/more')
                                                </div>
                                            </td> --}}
                                            {{--<td>--}}
                                                {{--<div class="d-flex justify-content-center">--}}
                                                    {{--<button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $job -> id }}">{{ __('More') }}</button>--}}
                                                    {{--@include('/admin/restaurants_job_offers/modals/more')--}}
                                                {{--</div>--}}
                                            {{--</td>--}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $job -> id }}" title="Edit/view restaurant">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/restaurants_job_offers/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $job -> id }}" title="{{ __('Delete job') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/restaurants_job_offers/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        {{--<th>{{ __('Name') }}</th>--}}
                                        {{--<th>{{ __('Location') }}</th>--}}
                                        {{--<th>{{ __('Restaurant location') }}</th>--}}
                                        {{--<th>{{ __('Restaurant profile') }}</th>--}}
                                        {{--<th>{{ __('Type contract') }}</th>--}}
                                        {{--<th>{{ __('Currency') }}</th>--}}
                                        {{--<th>{{ __('Producer') }}</th>--}}
                                        {{--<th>{{ __('Salary') }}</th>--}}
                                         {{--<th>{{ __('Details') }}</th>--}}
                                        {{--<th>{{ __('Actions') }}</th>--}}
                                    </tr>
                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $jobs -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/restaurants_job_offers/modals/add')
@endsection
