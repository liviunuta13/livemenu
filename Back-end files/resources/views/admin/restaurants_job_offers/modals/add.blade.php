<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-job') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add job ') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Location') }}</label><span class="text-danger">*</span>
                        <select name="location_id" class="form-control">
                            <option value="">Choose a location</option>
                            @foreach(\App\Location::all() as $location)
                                <option value="{{$location->id}}">{{$location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Restaurant') }}</label><span class="text-danger">*</span>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Restaurant Profile') }}</label><span class="text-danger">*</span>
                        <select name="restaurant_profile_id" class="form-control">
                            <option value="">Choose restaurant profile </option>
                            @foreach(\App\RestaurantProfile::all() as $restaurant_profile)
                                <option value="{{$restaurant_profile->id}}">{{$restaurant_profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Contract') }}</label><span class="text-danger">*</span>
                        <select name="contract_type_id" class="form-control">
                            <option value="">Choose a type of contract</option>
                            @foreach(\App\ContractType::all() as $contract_type)
                                <option value="{{$contract_type->id}}">{{$contract_type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Currency') }}</label><span class="text-danger">*</span>
                        <select name="currency_id" class="form-control">
                            <option value="">Choose a currency</option>
                            @foreach(\App\Currency::all() as $currency)
                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Producer') }}</label><span class="text-danger">*</span>
                        <select name="producer_profile_id" class="form-control">
                            <option value="">Choose a producer profile</option>
                            @foreach(\App\ProducerProfile::all() as $producer_profile)
                                <option value="{{$producer_profile->id}}">{{$producer_profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Name ') }}</label><span class="text-danger">*</span>
                        <input type="text" name="name" class="form-control" placeholder="{{ __('Enter name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Latitude') }}</label>
                        <input type="text" name="latitude" class="form-control" placeholder="{{ __('Enter latitude') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Longitude') }}</label>
                        <input type="text" name="longitude" class="form-control" placeholder="{{ __('Enter longitude') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Salary') }}</label><span class="text-danger">*</span>
                        <input type="text" name="salary" class="form-control" placeholder="{{ __('Enter salary') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <textarea name="description" rows="6" class="form-control" placeholder="{{ __('Enter something') }}"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
