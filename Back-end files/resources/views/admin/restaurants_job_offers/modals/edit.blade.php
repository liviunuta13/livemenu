<div class="modal fade" id="edit{{ $job -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-job', ['job_id' => $job -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit job') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Location') }} <span class="text-danger">*</span></label>
                        <select name="location_id" class="form-control">
                            <option value="">--Please choose a location--</option>
                            @foreach(\App\Location::all() as $location)
                                <option @if($location->id == $job->location_id) selected @endif
                                value="{{$location->id}}">{{$location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Restaurant location') }} <span class="text-danger">*</span></label>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">---Please choose a restaurant --</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_locations)
                                <option @if($restaurant_locations->id == $job->restaurant_location_id) selected @endif
                                value="{{$restaurant_locations->id}}">{{$restaurant_locations->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Restaurant profile') }} <span class="text-danger">*</span></label>
                        <select name="restaurant_profile_id" class="form-control">
                            <option value="">--Please choose restaurant profile --</option>
                            @foreach(\App\RestaurantProfile::all() as $restaurant_profile)
                                <option @if($restaurant_profile->id == $job->restaurant_profile_id) selected @endif
                                value="{{$restaurant_profile->id}}">{{$restaurant_profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Type of contract') }} <span class="text-danger">*</span></label>
                        <select name="contract_type_id" class="form-control">
                            <option value="">---Please choose a type of contract --</option>
                            @foreach(\App\ContractType::all() as $contract_type)
                                <option @if($contract_type->id == $job->contract_type_id) selected @endif
                                value="{{$contract_type->id}}">{{$contract_type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Currency') }} <span class="text-danger">*</span></label>
                        <select name="currency_id" class="form-control">
                            <option value="">---Please choose a currency--</option>
                            @foreach(\App\Currency::all() as $currency)
                                <option @if($currency->id == $job->currency_id) selected @endif
                                value="{{$currency->id}}">{{$currency->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Producer profile') }}</label><span class="text-danger">*</span>
                        <select name="producer_profile_id" class="form-control">
                            <option value="">---Please choose a producer profile--</option>
                            @foreach(\App\ProducerProfile::all() as $producer_profile)
                                <option @if($producer_profile->id == $job->producer_profile_id) selected @endif
                                value="{{$producer_profile->id}}">{{$producer_profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Name ') }}</label><span class="text-danger">*</span>
                        <input type="text" name="name" class="form-control" value="{{ $job->name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Latitude') }}</label>
                        <input type="text" name="latitude" class="form-control" value="{{ $job->latitude }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Longitude') }}</label>
                        <input type="text" name="longitude" class="form-control" value="{{ $job->longitude }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Salary') }}</label><span class="text-danger">*</span>
                        <input type="number" name="salary" class="form-control" value="{{ $job->salary }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <textarea  name="description" rows="6" class="form-control" value="{{ __('Enter description') }}"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
