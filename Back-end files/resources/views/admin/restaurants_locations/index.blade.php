@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Restaurants locations') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add location') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('locations') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-location') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Restaurant profile') }}</th>
                                        <th>{{ __('County') }}</th>
                                        <th>{{ __('Street') }}</th>
                                        <th>{{ __('Number') }}</th>
                                        <th>{{ __('Phone number') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Michelin stars') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($restaurant_locations) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($restaurant_locations as $key => $restaurant_location)
                                        <tr>
                                            <td>{{ $restaurant_location -> name }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant_location -> restaurantProfile))
                                                        <form action="{{ route('search-restaurants') }}" method="post">
                                                            @csrf
{{--                                                            <input type="hidden" name="keyword" value="{{$restaurant_location -> restaurantProfile[0] -> email }}">--}}
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant_location -> restaurantProfile-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $restaurant_location -> county->name }}</td>
                                            <td>{{ $restaurant_location -> street }}</td>
                                            <td>{{ $restaurant_location -> number }}</td>
                                            <td>{{ $restaurant_location -> phone_number }}</td>
                                            <td>{{ $restaurant_location -> email}}</td>
                                            <td>{{ $restaurant_location -> michelin_stars }}</td>

                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            {{--<td>--}}
                                                {{--<div class="d-flex justify-content-center">--}}
                                                    {{--<button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $restaurant_location -> id }}">{{ __('More') }}</button>--}}
                                                    {{--@include('/admin/restaurants_locations/modals/more')--}}
                                                {{--</div>--}}
                                            {{--</td>--}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $restaurant_location -> id }}" title="Edit/view location">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/restaurants_locations/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $restaurant_location -> id }}" title="{{ __('Delete location') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/restaurants_locations/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $restaurant_locations -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/restaurants_locations/modals/add')
@endsection


