<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-location') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add location') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant Profile') }}</label> <span class="text-danger">*</span>
                        <select name="restaurant_profile_id" class="form-control">
                            <option value="">Choose a restaurant profile</option>
                            @foreach(\App\RestaurantProfile::all() as $restaurant_profile)
                                <option value="{{$restaurant_profile->id}}">{{$restaurant_profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('City') }}</label>
                        <select name="city_id" class="form-control">
                            <option value="">Choose a city</option>
                            @foreach(\App\City::all() as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('County') }}</label><span class="text-danger">*</span>
                        <select name="county_id" class="form-control">
                            <option value="">Choose a county</option>
                            @foreach(\App\County::all() as $county)
                                <option value="{{$county->id}}">{{$county->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Country') }}</label>
                        <select name="country_id" class="form-control">
                            <option value="">Choose a country</option>
                            @foreach(\App\Country::all() as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Music') }}</label>
                        <select name="music_type_id" class="form-control">
                            <option value="">Choose a music</option>
                            @foreach(\App\MusicType::all() as $music)
                                <option value="{{$music->id}}">{{$music->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" placeholder="{{ __('Enter name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Street') }}</label><span class="text-danger">*</span>
                        <input type="text" name="street" class="form-control" placeholder="{{ __('Enter street') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Number ') }}</label><span class="text-danger">*</span>
                        <input type="number" name="number" class="form-control" placeholder="{{ __('Enter number of street') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Postal code') }}</label>
                        <input type="number" name="postal_code" class="form-control" placeholder="{{ __('Enter postal code') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Latitude') }}</label>
                        <input type="text" name="latitude" class="form-control" placeholder="{{ __('Enter latitude') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Longitude') }}</label>
                        <input type="text" name="longitude" class="form-control" placeholder="{{ __('Enter longitude') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Phone number') }}</label><span class="text-danger">*</span>
                        <input type="number" name="phone_number" class="form-control" placeholder="{{ __('Enter phone number') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Facility:') }}</label>
                            @foreach(\App\RestaurantLocationFacility::all() as $restaurant_facility)
                            <input type="checkbox" name="facilities_id[]"  value="{{$restaurant_facility->id}}">{{$restaurant_facility->name}}
                            @endforeach
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email') }}</label><span class="text-danger">*</span>
                        <input type="email" name="email" class="form-control" placeholder="{{ __('Enter email') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Locality specific') }}</label>
                        <input type="text" name="locality_specific" class="form-control" placeholder="{{ __('Enter locality specific') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Number of tables') }}</label>
                        <input type="number" name="tables_number" class="form-control" placeholder="{{ __('Enter tables number') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Number of seats') }}</label>
                        <input type="number" name="number_of_seats" class="form-control" placeholder="{{ __('Enter number of seats') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Number of bathroom') }}</label>
                        <input type="number" name="bathrooms_number" class="form-control" placeholder="{{ __('Enter bathroom number') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Locality surface') }}</label>
                        <input type="number" name="locality_surface" class="form-control" placeholder="{{ __('Enter locality surface') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Live music') }}</label>
                        <input type="checkbox" name="live_music" value="1">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Catering') }}</label>
                        <input type="checkbox" name="catering" value="1">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Framing') }}</label>
                        <input type="text" name="framing" class="form-control" placeholder="{{ __('Enter framing') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Michelin stars') }}</label><span class="text-danger">*</span>
                        <input type="number" name="michelin_stars" class="form-control" placeholder="{{ __('Enter michelin stars') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Owns event room') }}</label>
                        <input type="checkbox" name="owns_event_room" value="1">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">{{ __('Image') }}</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="picture[]" class="custom-file-input" multiple="true" >
                                <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <textarea name="description" rows="6" class="form-control" placeholder="{{ __('Enter description') }}"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
