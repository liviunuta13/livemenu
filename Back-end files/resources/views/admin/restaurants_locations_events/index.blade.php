@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Restaurants events') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add event') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('events') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-events') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Restaurant Location') }}</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Theme') }}</th>
                                        <th>{{ __('Description') }}</th>
                                        <th>{{ __('Begin Date') }}</th>
                                        <th>{{ __('End Date') }}</th>
                                        <th>{{ __('Begin Time') }}</th>
                                        <th>{{ __('End Time') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($events) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                    @endif
                                    @foreach ($events as $key => $event)
                                        <tr>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($event -> restaurantLocation))
                                                        <form action="{{ route('search-location') }}" method="post">
                                                            @csrf
                                                            {{--<input type="hidden" name="keyword" value="{{$staff -> restaurantLocation -> email }}">--}}
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $event -> restaurantLocation-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $event -> name }}</td>
                                            <td>{{ $event -> theme }}</td>
                                            <td>{{ $event -> description }}</td>
                                            <td>{{ $event -> begin_date }}</td>
                                            <td>{{ $event -> end_date }}</td>
                                            <td>{{ $event -> begin_time }}</td>
                                            <td>{{ $event -> end_time }}</td>
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            {{--<td>--}}
                                            {{--<div class="d-flex justify-content-center">--}}
                                            {{--<button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $staff -> id }}">{{ __('More') }}</button>--}}
                                            {{--@include('/admin/restaurants_locations_staffs/modals/more')--}}
                                            {{--</div>--}}
                                            {{--</td>--}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $event ->id }}" title="Edit/view event">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/restaurants_locations_events/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $event ->id }}" title="{{ __('Delete event') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/restaurants_locations_events/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $events -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/restaurants_locations_events/modals/add')
@endsection


