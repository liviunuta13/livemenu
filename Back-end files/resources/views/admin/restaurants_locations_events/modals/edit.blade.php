<div class="modal fade" id="edit{{ $event -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-event', ['event_id' => $event -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit event') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant Location') }} <span class="text-danger">*</span></label>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">--Please choose a restaurant location--</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option @if($restaurant_location->id == $event->restaurant_location_id) selected @endif
                                value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" value="{{ $event->name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Theme') }} <span class="text-danger">*</span></label>
                        <input type="text" name="theme" required class="form-control" value="{{ $event->theme }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Banner') }}</label>
                        <input type="text" name="banner" class="form-control" value="{{ $event->banner }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('One Day Event') }}</label>
                        <input type="checkbox" name="one_day_event" value="1">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Multi Day Event') }}</label>
                        <input type="checkbox" name="multi_day_event" value="1" >
                    </div>
                    <div class="form-group">
                        <label>{{ __('Begin Date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="begin_date" class="form-control" value="{{$event->begin_date}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('End Date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="end_date" class="form-control" value="{{$event->end_date}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Begin Hour') }}</label><span class="text-danger">*</span>
                        <input type="time" name="begin_time" class="form-control" value="{{$event->begin_time}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('End Hour') }}</label><span class="text-danger">*</span>
                        <input type="time" name="end_time" class="form-control" value="{{$event->end_time}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label><span class="text-danger">*</span>
                        <textarea  name="description" class="form-control"  >{{$event->description}}</textarea>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
