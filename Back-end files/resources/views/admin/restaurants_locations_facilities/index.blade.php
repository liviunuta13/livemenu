@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Restaurants facilities') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add facility') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('facilities') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-facilities') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Icon') }}</th>
                                        <th>{{ __('Description') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($facilities) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($facilities as $facility)
                                        <tr>
                                            <td>{{ $facility -> name }}</td>
                                            <td><i class="{{$facility->icon}}"></i></td>
                                            <td>{{ $facility -> description }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $facility -> id }}" title="Edit/view facility">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/restaurants_locations_facilities/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $facility -> id }}" title="{{ __('Delete facility') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/restaurants_locations_facilities/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $facilities -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/restaurants_locations_facilities/modals/add')
@endsection


