@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Restaurants hotels') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add hotel') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('hotels') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-hotels') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Restaurant Location') }}</th>
                                        <th>{{ __('stars') }}</th>
                                        <th>{{ __('email') }}</th>
                                        <th>{{ __('phone_number') }}</th>
                                        <th>{{ __('capacity') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($hotels) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($hotels as $hotel)
                                        <tr>
                                            <td>{{ $hotel -> name }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($hotel -> restaurantLocation))
                                                        <form action="{{ route('search-location') }}" method="post">
                                                            @csrf
                                                            {{--<input type="hidden" name="keyword" value="{{$staff -> restaurantLocation -> email }}">--}}
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $hotel -> restaurantLocation-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $hotel -> stars }}</td>
                                            <td>{{ $hotel -> email }}</td>
                                            <td>{{ $hotel -> phone_number }}</td>
                                            <td>{{ $hotel -> capacity }}</td>
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            {{--<td>--}}
                                            {{--<div class="d-flex justify-content-center">--}}
                                            {{--<button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $specific -> id }}">{{ __('More') }}</button>--}}
                                            {{--@include('/auth/restaurants_specifics/modals/more')--}}
                                            {{--</div>--}}
                                            {{--</td>--}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $hotel -> id }}" title="Edit/view hotel">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/restaurants_locations_hotels/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $hotel -> id }}" title="{{ __('Delete hotel') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/restaurants_locations_hotels/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $hotels -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/restaurants_locations_hotels/modals/add')
@endsection


