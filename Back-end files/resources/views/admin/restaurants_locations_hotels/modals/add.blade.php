<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-hotel') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add hotel') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant Location') }}</label><span class="text-danger">*</span>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name"  class="form-control" placeholder="{{ __('Enter hotel name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Phone_number') }} <span class="text-danger">*</span></label>
                        <input type="number" name="phone_number"  class="form-control" placeholder="{{ __('Enter phone number') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Capacity') }} <span class="text-danger">*</span></label>
                        <input type="number" name="capacity"  class="form-control" placeholder="{{ __('Enter capacity') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Facility:') }}</label>
                        @foreach(\App\HotelFacility::all() as $hotel_facility)
                            <input type="checkbox" name="facilities_id[]"  value="{{$hotel_facility->id}}">{{$hotel_facility->name}}
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email') }}</label><span class="text-danger">*</span>
                        <input type="email" name="email" class="form-control" placeholder="{{ __('Enter email') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Stars') }}</label><span class="text-danger">*</span>
                        <input type="number" name="stars" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <textarea name="description" rows="6" class="form-control" placeholder="{{ __('Enter description') }}"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
