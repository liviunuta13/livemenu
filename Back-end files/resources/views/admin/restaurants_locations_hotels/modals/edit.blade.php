<div class="modal fade" id="edit{{ $hotel -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-hotel', ['hotel_id' => $hotel -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit hotel') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant Location') }} <span class="text-danger">*</span></label>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option @if($restaurant_location->id == $hotel->restaurant_location_id) selected @endif
                                value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name"  class="form-control" value="{{ $hotel->name}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Phone_number') }} <span class="text-danger">*</span></label>
                        <input type="number" name="phone_number"  class="form-control" value="{{ $hotel->phone_number }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Capacity') }} <span class="text-danger">*</span></label>
                        <input type="number" name="capacity"  class="form-control" value="{{ $hotel->capacity }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Facility:') }}</label>
                        @foreach(\App\HotelFacility::all() as $hotel_facility)
                            <input type="checkbox" name="facilities_id[]"  value="{{$hotel_facility->id}}">{{$hotel_facility->name}}
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email') }}</label><span class="text-danger">*</span>
                        <input type="email" name="email" class="form-control" value="{{ $hotel->email }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Stars') }}</label><span class="text-danger">*</span>
                        <input type="number" name="stars" class="form-control" value="{{$hotel->stars}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <textarea name="description" class="form-control" >{{$hotel->description}}</textarea>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
