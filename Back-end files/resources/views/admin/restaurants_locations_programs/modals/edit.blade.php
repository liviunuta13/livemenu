<div class="modal fade" id="edit{{ $program -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-program', ['program_id' => $program -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit program') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant Location') }} <span class="text-danger">*</span></label>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option @if($restaurant_location->id == $program->restaurant_location_id) selected @endif
                                value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Condition') }}</label><span class="text-danger">*</span>
                        <textarea  name="conditions" class="form-control"  >{{$program->condition}}</textarea>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
