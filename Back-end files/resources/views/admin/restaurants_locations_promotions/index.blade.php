@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Restaurants promotions') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add promotion') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('promotions') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-promotions') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Restaurant Location') }}</th>
{{--                                        <th>{{ __('Product') }}</th>--}}
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Description') }}</th>
                                        <th>{{ __('Begin Date') }}</th>
                                        <th>{{ __('End Date') }}</th>
                                        <th>{{ __('Begin Time') }}</th>
                                        <th>{{ __('End Time') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($promotions) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                    @endif
                                    @foreach ($promotions as $key => $promotion)
                                        <tr>

                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($promotion -> restaurantLocation))
                                                        <form action="{{ route('search-location') }}" method="post">
                                                            @csrf
                                                            {{--<input type="hidden" name="keyword" value="{{$staff -> restaurantLocation -> email }}">--}}
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $promotion -> restaurantLocation-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            {{--<td>--}}
                                                {{--<div class="d-flex justify-content-center">--}}
                                                    {{--@if(isset($promotion -> menu))--}}
                                                        {{--<form action="{{ route('search-menu') }}" method="post">--}}
                                                            {{--@csrf--}}
                                                            {{--<input type="hidden" name="keyword" value="{{$staff -> restaurantLocation -> email }}">--}}
                                                            {{--<button class="alert-success" type="submit">--}}
                                                                {{--<i class="fas fa-share"></i> {{ $promotion -> menu-> name }}--}}
                                                            {{--</button>--}}
                                                        {{--</form>--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}
                                            {{--</td>--}}
                                            <td>{{ $promotion -> name }}</td>
                                            <td>{{ $promotion -> description }}</td>
                                            <td>{{ $promotion -> begin_date }}</td>
                                            <td>{{ $promotion -> end_date }}</td>
                                            <td>{{ $promotion -> begin_time }}</td>
                                            <td>{{ $promotion -> end_time }}</td>
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            {{--<td>--}}
                                            {{--<div class="d-flex justify-content-center">--}}
                                            {{--<button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $staff -> id }}">{{ __('More') }}</button>--}}
                                            {{--@include('/admin/restaurants_locations_staffs/modals/more')--}}
                                            {{--</div>--}}
                                            {{--</td>--}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $promotion ->id }}" title="Edit/view promotion">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/restaurants_locations_promotions/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $promotion ->id }}" title="{{ __('Delete promotion') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/restaurants_locations_promotions/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $promotions -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/restaurants_locations_promotions/modals/add')
@endsection


