<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-promotion') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add promotion') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant Location') }}</label><span class="text-danger">*</span>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Product') }}</label>
                        <select name="product_id" class="form-control">
                            <option value="">Choose a product</option>
                            @foreach(\App\Menu::all() as $menu)
                                <option value="{{$menu->id}}">{{$menu->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Drinks Menu Category') }}</label>
                        <select name="drinks_menu_category_id" class="form-control">
                            <option value="">Choose a drink menu category</option>
                            @foreach(\App\Menu::all() as $menu)
                                <option value="{{$menu->id}}">{{$menu->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                    <label>{{ __('Food Menu Category') }}</label>
                        <select name="food_menu_category_id" class="form-control">
                            <option value="">Choose a food menu category</option>
                            @foreach(\App\Menu::all() as $menu)
                                <option value="{{$menu->id}}">{{$menu->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" placeholder="{{ __('Enter name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Begin Date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="begin_date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>{{ __('End Date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="end_date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Begin Hour') }}</label><span class="text-danger">*</span>
                        <input type="time" name="begin_time" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>{{ __('End Hour') }}</label><span class="text-danger">*</span>
                        <input type="time" name="end_time" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label><span class="text-danger">*</span>
                        <textarea name="description" rows="6" class="form-control" placeholder="{{ __('Enter description') }}"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
