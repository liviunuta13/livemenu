<div class="modal fade" id="edit{{ $promotion -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-promotion', ['promotion_id' => $promotion -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit promotion') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant Location') }} <span class="text-danger">*</span></label>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option @if($restaurant_location->id == $promotion->restaurant_location_id) selected @endif
                                value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Product') }}</label>
                        <select name="product_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\Product::all() as $product)
                                <option @if($product->id == $promotion->product_id) selected @endif
                                value="{{$product->id}}">{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Drinks Menu Category') }}</label>
                        <select name="drinks_menu_category_id" class="form-control">
                            <option value="">Choose a drink menu category</option>
                            @foreach(\App\Menu::all() as $menu)
                                <option @if($menu->id == $promotion->drinks_menu_category_id) selected @endif
                                value="{{$menu->id}}">{{$menu->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Food Menu Category') }}</label>
                        <select name="food_menu_category_id" class="form-control">
                            <option value="">Choose a food menu category</option>
                            @foreach(\App\Menu::all() as $menu)
                                <option  @if($menu->id == $promotion->food_menu_category_id) selected @endif
                                value="{{$menu->id}}">{{$menu->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" value="{{ $promotion->name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Begin Date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="begin_date" class="form-control" value="{{$promotion->begin_date}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('End Date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="end_date" class="form-control" value="{{$promotion->end_date}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Begin Hour') }}</label><span class="text-danger">*</span>
                        <input type="time" name="begin_time" class="form-control" value="{{$promotion->begin_time}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('End Hour') }}</label><span class="text-danger">*</span>
                        <input type="time" name="end_time" class="form-control" value="{{$promotion->end_time}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label><span class="text-danger">*</span>
                        <textarea  name="description" class="form-control"  >{{$promotion->description}}</textarea>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
