@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Restaurants staffs') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add staff') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('staffs') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-staffs') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Restaurant Location') }}</th>
                                        <th>{{ __('Job') }}</th>
                                        <th>{{ __('Contract') }}</th>
                                        <th>{{ __('Currency') }}</th>
                                        <th>{{ __('Salary') }}</th>
                                        <th>{{ __('Profesionist') }}</th>
                                        <th>{{ __('Start Date') }}</th>
                                        <th>{{ __('End Date') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($staffs) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($staffs as $key => $staff)
                                        <tr>

                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($staff -> location))
                                                        <form action="{{ route('search-location') }}" method="post">
                                                            @csrf
                                                            {{--<input type="hidden" name="keyword" value="{{$staff -> restaurantLocation -> email }}">--}}
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $staff -> location-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $staff -> job }}</td>
                                            <td>{{ $staff -> contractType->name }}</td>
                                            <td>{{ $staff -> currency->name }}</td>
                                            <td>{{ $staff -> salary }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($staff -> profesionist))
                                                        <form action="{{ route('search-professionists') }}" method="post">
                                                            @csrf
{{--                                                            <input type="hidden" name="keyword" value="{{$staff -> profesionist-> email }}">--}}
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $staff -> profesionist-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>{{ $staff -> start_date }}</td>
                                            <td>{{ $staff -> end_date }}</td>

                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            {{--<td>--}}
                                                {{--<div class="d-flex justify-content-center">--}}
                                                    {{--<button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $staff -> id }}">{{ __('More') }}</button>--}}
                                                    {{--@include('/admin/restaurants_locations_staffs/modals/more')--}}
                                                {{--</div>--}}
                                            {{--</td>--}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $staff ->id }}" title="Edit/view staff">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/restaurants_locations_staffs/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $staff ->id }}" title="{{ __('Delete staff') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/restaurants_locations_staffs/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $staffs -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/restaurants_locations_staffs/modals/add')
@endsection


