<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-staff') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add staff') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant Location') }}</label><span class="text-danger">*</span>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Contract') }}</label><span class="text-danger">*</span>
                        <select name="contract_type_id" class="form-control">
                            <option value="">Choose a contract</option>
                            @foreach(\App\ContractType::all() as $contract)
                                <option value="{{$contract->id}}">{{$contract->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Currency') }}</label><span class="text-danger">*</span>
                        <select name="salary_currency_id" class="form-control">
                            <option value="">Choose a currency</option>
                            @foreach(\App\Currency::all() as $currency)
                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Profesionist ') }}</label><span class="text-danger">*</span>
                        <select name="profesionist_profile_id" class="form-control">
                            <option value="">Choose a profesionist</option>
                            @foreach(\App\ProfesionistProfile::all() as $profesionist)
                                <option value="{{$profesionist->id}}">{{$profesionist->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Job') }} <span class="text-danger">*</span></label>
                        <input type="text" name="job_name" required class="form-control" placeholder="{{ __('Enter job name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Role:') }}</label>
                        @foreach(\App\RestaurantLocationStaffRole::all() as $role)<br>
                            <input type="checkbox" name="roles_id[]"  value="{{$role->id}}">{{$role->name}}
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label>{{ __('Salary') }} <span class="text-danger">*</span></label>
                        <input type="text" name="salary" required class="form-control" placeholder="{{ __('Enter salary') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Start Date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="start_date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>{{ __('End date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="end_date" class="form-control">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
