<div class="modal fade" id="edit{{ $staff -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-staff', ['staff_id' => $staff -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit staff') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant Location') }} <span class="text-danger">*</span></label>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">--Please choose a restaurant location--</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option @if($restaurant_location->id == $staff->restaurant_location_id) selected @endif
                                value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Contract') }}</label><span class="text-danger">*</span>
                        <select name="contract_type_id" class="form-control">
                            <option value="">--Please choose a contract--</option>
                            @foreach(\App\ContractType::all() as $contract)
                                <option @if($contract->id == $staff->contract_type_id) selected @endif
                                value="{{$contract->id}}">{{$contract->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Currency') }}</label><span class="text-danger">*</span>
                        <select name="salary_currency_id" class="form-control">
                            <option value="">--Please choose a contract--</option>
                            @foreach(\App\Currency::all() as $currency)
                                <option @if($currency->id == $staff->salary_currency_id) selected @endif
                                value="{{$currency->id}}">{{$currency->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Profesionist ') }}</label><span class="text-danger">*</span>
                        <select name="profesionist_profile_id" class="form-control">
                            <option value="">Choose a profesionist</option>
                            @foreach(\App\ProfesionistProfile::all() as $profesionist)
                                <option @if($profesionist->id == $staff->profesionist_profile_id) selected @endif
                                value="{{$profesionist->id}}">{{$profesionist->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Job') }} <span class="text-danger">*</span></label>
                        <input type="text" name="job_name" required class="form-control" value="{{ $staff->job }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Role:') }}</label>
                        @foreach(\App\RestaurantLocationStaffRole::all() as $roles)<br>
                            <input type="checkbox" name="roles_id[]"  value="{{$roles->id}}">{{$roles->name}}
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label>{{ __('Salary') }} <span class="text-danger">*</span></label>
                        <input type="text" name="salary" required class="form-control" value="{{ $staff->salary }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Start Date') }}</label><span class="text-danger">*</span>
                        <input type="date" name="start_date" class="form-control" value="{{ $staff->start_date }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __(' End date ') }}</label><span class="text-danger">*</span>
                        <input type="date" name="end_date" class="form-control" value="{{ $staff->end_date  }}">
                    </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
