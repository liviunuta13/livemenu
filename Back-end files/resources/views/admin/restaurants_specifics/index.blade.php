@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                {{--@if(Session::has('success'))--}}
                                    {{--<div class="alert alert-success alert-dismissible fade show" role="alert">--}}
                                        {{--<strong>Success!</strong> {{Session::get('success')}}--}}
                                        {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
                                            {{--<span aria-hidden="true">&times;</span>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                                <h4>{{ __('Restaurants specifics') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add specific') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('specifics') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-specifics') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($specifics) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($specifics as $key => $specific)
                                        <tr>
                                            <td>{{ $specific -> name }}</td>
                                            {{-- <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($restaurant -> user[0]))
                                                        <form action="{{ route('search-user') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $restaurant -> user[0] -> email }}">
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $restaurant -> user[0] -> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td> --}}
                                            {{--<td>--}}
                                                {{--<div class="d-flex justify-content-center">--}}
                                                    {{--<button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $specific -> id }}">{{ __('More') }}</button>--}}
                                                    {{--@include('/auth/restaurants_specifics/modals/more')--}}
                                                {{--</div>--}}
                                            {{--</td>--}}
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $specific -> id }}" title="Edit/view specific">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/restaurants_specifics/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $specific -> id }}" title="{{ __('Delete specific') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/restaurants_specifics/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    {{--<tr>--}}
                                        {{--<th>{{ __('Name') }}</th>--}}
                                        {{-- <th>{{ __('Details') }}</th> --}}
                                        {{--<th>{{ __('Actions') }}</th>--}}
                                    {{--</tr>--}}
                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $specifics -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/restaurants_specifics/modals/add')
@endsection


