@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @include('/admin/success')
                    @include('/admin/errors')
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>{{ __('Rooms events') }}</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"><i class="fas fa-plus"></i> {{ __('Add event room') }}</button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row float-right mb-3">
                                <button class="btn-danger">
                                    <a href="{{ route('room_events') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</span></a>
                                </button>
                                <form class="float-right" action="{{ route('search-room_events') }}" method="post">
                                    @csrf
                                    <input type="text" class="ml-1 border" name="keyword" required>
                                    <button type="submit" class="btn-success">{{ __('Search') }}</button>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Restaurant location') }}</th>
                                        <th>{{ __('Restaurant profile') }}</th>
                                        <th>{{ __('Rooms number') }}</th>
                                        <th>{{ __('Tables number') }}</th>
                                        <th>{{ __('Persons number') }}</th>
                                        <th>{{ __('Surface') }}</th>
                                        <th>{{ __('Surface unit') }}</th>
                                        <th>{{ __('Description') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($event_rooms) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($event_rooms as $event_room)
                                        <tr>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($event_room -> restaurantLocation))
                                                        <form action="{{ route('search-location') }}" method="post">
                                                            @csrf
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $event_room -> restaurantLocation-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($event_room -> restaurantProfile))
                                                        <form action="{{ route('search-restaurants') }}" method="post">
                                                            @csrf
                                                            <button class="alert-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $event_room -> restaurantProfile-> name }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
{{--                                            <td>{{ $event_room->restaurant_location_profile_id }}</td>--}}
                                            <td>{{ $event_room->rooms_number }}</td>
                                            <td>{{ $event_room->tables_number }}</td>
                                            <td>{{ $event_room->persons_number }}</td>
                                            <td>{{ $event_room->surface }}</td>
                                            <td>{{ $event_room->unit->name }}</td>
                                            <td>{{ $event_room->descripton }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" data-target="#edit{{ $event_room -> id }}" title="Edit/view room event">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/rooms_events/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" data-target="#delete{{ $event_room -> id }}" title="{{ __('Delete room event') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/rooms_events/modals/delete')
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                                <div class="mt-4 float-right">
                                    {{ $event_rooms -> links() }}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
    @include('/admin/rooms_events/modals/add')
@endsection


