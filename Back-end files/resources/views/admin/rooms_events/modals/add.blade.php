<div class="modal fade" id="add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-room_event') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add room event') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant location') }}</label> <span class="text-danger">*</span>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Restaurant profile') }}</label> <span class="text-danger">*</span>
                        <select name="restaurant_location_profile_id" class="form-control">
                            <option value="">Choose a restaurant profile</option>
                            @foreach(\App\RestaurantProfile::all() as $restaurant_profile)
                                <option value="{{$restaurant_profile->id}}">{{$restaurant_profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Surface unit') }}</label> <span class="text-danger">*</span>
                        <select name="surface_unit_id" class="form-control">
                            <option value="">Choose a surface unit</option>
                            @foreach(\App\Unit::all() as $unit)
                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Room number') }}</label><span class="text-danger">*</span>
                        <input type="number" name="rooms_number" class="form-control"  placeholder="{{ __('Enter room number')}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Table number') }}</label><span class="text-danger">*</span>
                        <input type="number" name="tables_number" class="form-control"  placeholder="{{ __('Enter table number')}}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Person number') }}</label><span class="text-danger">*</span>
                        <input type="number" name="persons_number" class="form-control"  placeholder="{{ __('Enter person number') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Surface') }}</label><span class="text-danger">*</span>
                        <input type="text" name="surface" class="form-control"  placeholder="{{ __('Enter surface') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Disponible room number') }}</label>
                        <input type="number" name="disponible_rooms_number" class="form-control"  placeholder="{{ __('Enter disponible rooms number') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Event room number') }}</label>
                        <input type="checkbox" name="owns_event_room" value="1">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Facility:') }}</label>
                        @foreach(\App\EventRoomFacility::all() as $room_facility)
                            <input type="checkbox" name="facilities_id[]"  value="{{$room_facility->id}}">{{$room_facility->name}}
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label><span class="text-danger">*</span>
                        <textarea name="descripton" class="form-control" rows="6" placeholder="{{ __('Enter description') }}"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
