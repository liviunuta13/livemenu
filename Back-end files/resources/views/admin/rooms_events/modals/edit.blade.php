<div class="modal fade" id="edit{{ $event_room -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-room_event', ['room_event_id' => $event_room -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit room event') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Restaurant location') }}</label> <span class="text-danger">*</span>
                        <select name="restaurant_location_id" class="form-control">
                            <option value="">Choose a restaurant location</option>
                            @foreach(\App\RestaurantLocation::all() as $restaurant_location)
                                <option @if($restaurant_location->id == $event_room->restaurant_location_id) selected @endif
                                value="{{$restaurant_location->id}}">{{$restaurant_location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Restaurant profile') }}</label> <span class="text-danger">*</span>
                        <select name="restaurant_location_profile_id" class="form-control">
                            <option value="">Choose a restaurant profile</option>
                            @foreach(\App\RestaurantProfile::all() as $restaurant_profile)
                                <option @if($restaurant_profile->id == $event_room->restaurant_location_profile_id) selected @endif
                                value="{{$restaurant_profile->id}}">{{$restaurant_profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Surface unit') }}</label> <span class="text-danger">*</span>
                        <select name="surface_unit_id" class="form-control">
                            <option value="">Choose a surface unit</option>
                            @foreach(\App\Unit::all() as $unit)
                                <option @if($unit-> id == $event_room -> unit_id) selected @endif
                                value="{{$unit->id}}">{{$unit->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Room number') }}</label><span class="text-danger">*</span>
                        <input type="number" name="rooms_number" class="form-control"  value="{{ $event_room->rooms_number }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Table number') }}</label><span class="text-danger">*</span>
                        <input type="number" name="tables_number" class="form-control"  value="{{$event_room->tables_number }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Person number') }}</label><span class="text-danger">*</span>
                        <input type="number" name="persons_number" class="form-control"  value="{{ $event_room->persons_number }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Surface') }}</label><span class="text-danger">*</span>
                        <input type="text" name="surface" class="form-control"  value="{{ $event_room->surface }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Disponible room number') }}</label>
                        <input type="number" name="disponible_rooms_number" class="form-control"  value="{{ $event_room->disponible_rooms_number }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Event room number') }}</label>
                        <input type="checkbox" name="owns_event_room" value="1">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Facility:') }}</label>
                        @foreach(\App\EventRoomFacility::all() as $room_facility)
                            <input type="checkbox" name="facilities_id[]"  value="{{$room_facility->id}}">{{$room_facility->name}}
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label><span class="text-danger">*</span>
                        <textarea name="descripton" class="form-control" rows="6" >{{$event_room->description }}</textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
