@if(Session::has('success'))
    @foreach (Session::get('success') as $key => $success)
        <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
            {{ $success }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endforeach
@endif
