@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-12">
                @include('/admin/success')
                @include('/admin/errors')
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>{{ __('Users') }}</h4>
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUser"><i class="fas fa-plus"></i> {{ __('Add user') }}</button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row float-right mb-3">
                            <button class="btn-danger">
                                <a href="{{ route('users') }}"><i class="fas fa-retweet text-white"></i><span class="ml-1 mr-2 text-white">{{ __('Reset') }}</a>
                            </button>
                            <form class="float-right" action="{{ route('search-user') }}" method="post">
                                @csrf
                                <input type="text" class="ml-1 border" name="keyword" required>
                                <button type="submit" class="btn-success">{{ __('Search') }}</button>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Nickname') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Picture') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($users) == 0)
                                        <tr>
                                            <td>{{ __('No results..') }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @foreach ($users as $key => $user)
                                        <tr>
                                            <td>{{ $user -> name }}</td>
                                            <td>{{ $user -> nickname }}</td>
                                            <td>
                                                {{ $user -> email }}
                                                @if($user -> confirmed_email)
                                                    <badge class="badge badge-success ml-1">{{ __('Confirmed') }}</badge>
                                                @else
                                                    <badge class="badge badge-danger ml-1">{{ __('Not confirmed') }}</badge>
                                                @endif
                                             </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <img src="data:image/png;base64, {{ $user -> profile_picture }}" alt="Profile picture" class="img-responsive" style="width: 70px; height: auto;">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <button type="button" class="btn-info" data-toggle="modal" data-target="#more{{ $user -> id }}">{{ __('More') }}</button>
                                                    @include('/admin/users/modals/more')
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <a data-toggle="modal" title="{{ __('Edit user') }}"data-target="#editUser{{ $user -> id }}">
                                                        <i class="fa fa-edit text-primary"></i>
                                                    </a>
                                                    @include('/admin/users/modals/edit')
                                                    <a class="ml-2" data-toggle="modal" title="{{ __('Delete user') }}" data-target="#deleteUser{{ $user -> id }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('/admin/users/modals/delete')
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Nickname') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Picture') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="mt-4 float-right">
                                {{ $users -> links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@include('/admin/users/modals/add')
@endsection
