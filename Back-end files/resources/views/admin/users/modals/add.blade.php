<div class="modal fade" id="addUser">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('add-user') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add user') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" placeholder="{{ __('Enter name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Nickname') }}</label>
                        <input type="text" name="nickname" class="form-control" placeholder="{{ __('Enter nickname') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email address') }} <span class="text-danger">*</span></label>
                        <input type="email" name="email" required class="form-control" placeholder="{{ __('Enter email') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Password') }} <span class="text-danger">*</span></label>
                        <input type="password" name="password" required class="form-control" placeholder="{{ __('Password') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Password Confirmation') }}</label>
                        <input type="password" name="password_confirmation" required class="form-control"  placeholder="{{ __('Password Confirmatin') }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">{{ __('Profile picture') }}</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="profile_picture" class="custom-file-input">
                                <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>{{ __('Favorite food') }} ("," {{ __('as separator') }})</label>
                        <input type="text" class="form-control" name="favorite_food" placeholder="Pizza, sushi">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Secret Ingredients') }} ("," {{ __('as separator') }})</label>
                        <input type="text" class="form-control" name="secret_ingredients" placeholder="Salt, cimpoier">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Restaurant Specifics') }} ("," {{ __('as separator') }})</label>
                        <input type="text" class="form-control" name="restaurant_specifics" placeholder="Italiano, americano">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Music Types') }} ("," {{ __('as separator') }})</label>
                        <input type="text" class="form-control" name="music_types" placeholder="Classic, trap">
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
