<div class="modal fade" id="editUser{{ $user -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('edit-user', ['user_id' => $user -> id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit user') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Name') }} <span class="text-danger">*</span></label>
                        <input type="text" name="name" required class="form-control" value="{{ $user -> name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Nickname') }}</label>
                        <input type="text" name="nickname" class="form-control" value="{{ $user -> nickname }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email address') }} <span class="text-danger">*</span></label>
                        <input type="email" name="email" required class="form-control" value="{{ $user -> email }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('New Password') }} <span class="text-danger">*</span></label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>{{ __('New Password Confirmation') }} <span class="text-danger">*</span></label>
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">{{ __('Switch Profile picture') }}</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="profile_picture" class="custom-file-input">
                                <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>{{ __('Favorite food') }} ("," {{ __('as separator') }})</label>
                        <input type="text" class="form-control" name="favorite_food" value="{{ $user -> favorite_food }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Secret Ingredients') }} ("," {{ __('as separator') }})</label>
                        <input type="text" class="form-control" name="secret_ingredients" value="{{ $user -> secret_ingredients }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Restaurant Specifics') }} ("," {{ __('as separator') }})</label>
                        <input type="text" class="form-control" name="restaurant_specifics" value="{{ $user -> restaurant_specifics }}">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Music Types') }} ("," {{ __('as separator') }})</label>
                        <input type="text" class="form-control" name="music_types" value="{{ $user -> music_types }}">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
