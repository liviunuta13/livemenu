<div class="modal fade" id="more{{ $user -> id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ __('More details about the user ') . $user -> name }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="w-100 order-2 order-md-1 ml-2 mr-2">
                        <div class="row">
                            <div class="col-12 col-md-12">
                                <div class="info-box bg-light border border-success">
                                    <div class="info-box-content">
                                        <h5 class="info-box-text text-center text-muted">{{ __('Professionist profile') }}</h5>
                                        <div class="d-flex justify-content-center">
                                            @if($user -> profesionistProfile)
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($user -> profesionistProfile -> email))
                                                        <form action="{{ route('search-professionists') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $user -> profesionistProfile -> email }}">
                                                            <button class="btn-success" type="submit">
                                                                <i class="fas fa-share"></i> {{ $user -> profesionistProfile['name'] }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            @else
                                                ---
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div class="info-box bg-light border border-danger">
                                    <div class="info-box-content">
                                        <span class="info-box-text text-center text-muted">{{ __('Restaurant profile') }}</span>
                                        <div class="d-flex justify-content-center">
                                            @if($user -> restaurantProfile)
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($user -> restaurantProfile -> company_email))
                                                        <form action="{{ route('search-restaurants') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $user -> restaurantProfile -> company_email }}">
                                                            <button class="btn-danger" type="submit">
                                                                <i class="fas fa-share"></i> {{ $user -> restaurantProfile['name'] }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            @else
                                                ---
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div class="info-box bg-light border border-primary">
                                    <div class="info-box-content">
                                        <span class="info-box-text text-center text-muted text-justify">{{ __('Producer profile') }}</span>
                                        <div class="d-flex justify-content-center">
                                            @if($user -> producerProfile)
                                                <div class="d-flex justify-content-center">
                                                    @if(isset($user -> producerProfile -> email))
                                                        <form action="{{ route('search-producers') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="keyword" value="{{ $user -> producerProfile -> email }}">
                                                            <button class="btn-primary" type="submit">
                                                                <i class="fas fa-share"></i> {{ $user -> producerProfile['name'] }}
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            @else
                                                ---
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12 col-md-12">
                                <div class="info-box bg-light border">
                                    <div class="info-box-content">
                                        <span class="info-box-text text-center text-muted text-justify">{{ __('Favorite food') }}</span>
                                        <div class="d-flex justify-content-center">
                                            @if($user -> favorite_food)
                                                {{ $user -> favorite_food }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div class="info-box bg-light border">
                                    <div class="info-box-content">
                                        <span class="info-box-text text-center text-muted text-justify">{{ __('Secret Ingredients') }}</span>
                                        <div class="d-flex justify-content-center">
                                            @if($user -> favorite_food)
                                                {{ $user -> secret_ingredients }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div class="info-box bg-light border">
                                    <div class="info-box-content">
                                        <span class="info-box-text text-center text-muted text-justify">{{ __('Restaurant Specifics') }}</span>
                                        <div class="d-flex justify-content-center">
                                            @if($user -> favorite_food)
                                                {{ $user -> restaurant_specifics }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-12">
                                <div class="info-box bg-light border">
                                    <div class="info-box-content">
                                        <span class="info-box-text text-center text-muted text-justify">{{ __('Music Types') }}</span>
                                        <div class="d-flex justify-content-center">
                                            @if($user -> favorite_food)
                                                {{ $user -> music_types }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
