<div>
	<h3>LiveMenu.Info</h3>
    <h4>To confirm the registration, please click
        @if($new_email !== null)
			<a href="{{ env('APP_URL') }}/api/user/email/confirm/{{$user_id}}/{{$new_email}}/{{$confirmed_token}}">
	            here!
	        </a>
		@else
			<a href="{{ env('APP_URL') }}/api/user/edit/email/confirm/{{$user_id}}/{{$confirmed_token}}">
	            here!
	        </a>
		@endif
    </h4>
</div>
