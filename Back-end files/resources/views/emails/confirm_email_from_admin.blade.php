<div>
	<h3>LiveMenu.Info</h3>
    <h4>To confirm the account added by admin, please click
		<a href="{{ env('APP_URL') }}/api/user/edit/email/confirm/{{$user_id}}/{{$confirmed_token}}">
			here!
		</a>
    </h4>
</div>
