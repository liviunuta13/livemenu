<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Curriculum Vitae</title>
    </head>

    <body>

        <div id="image">
            <img src="{{ base_path('/public/pictures/logo.png') }}" alt="" style="height: 55px; width: 100px;">
        </div>
        <h2 class="align-center">Curriculum Vitae</h2>

        <h4 class="red-text mt-0">Informatii personale</h4>
        <div class="right">
            <h3>Nume: {{ $data -> name }}</h3>
            <h4>Email: {{ $data -> email }}</h4>
            <h4>Locatie: {{ $data -> street_and_number }}, Cod postal: {{ $data -> postal_code }}, @if(isset($data -> city -> name)) Oras: {{ $data -> city -> name }} @endif, @if(isset( $data -> county -> name)) Judet: {{ $data -> county -> name }} @endif,
                @if(isset($data -> country -> name)) Tara: {{ $data -> country -> name }} @endif, Telefon: {{ $data -> phone_number }}, Website: {{ $data -> website }}</h4>
        </div>

        <h4 class="red-text mt-0">Experienta personala</h4>
        <div class="right">
            <h4 class="">Functiile pentru care sunt dispus sa lucrez:  {{ $data -> professional_experience }}</h4>
            @foreach ($data -> professionalHistory as $key => $hist)
                <h4 class="red-text mt">Istoric profesional</h4>
                <div class="history-element">
                    <h4>De la: {{ $hist -> begin_date }} pana la: {{ $hist -> end_date }}</h4>
                    @if($hist -> present == 0)
                        <h4>Present: nu</h4>
                    @else
                        <h4>Present: da</h4>
                    @endif
                    <h4>Post ocupat: {{ $hist -> working_as }}</h4>
                    <h4>Nume firma: {{ $hist -> company_name }}</h4>
                    @if(isset($hist -> city -> name))
                        <h4>Oras: {{ $hist -> city -> name }}</h4>
                    @endif
                    @if(isset($hist -> country -> name))
                        <h4>Tara: {{ $hist -> country -> name }}</h4>
                    @endif
                    <h4>Desciere: {{ $hist -> description }}</h4>
                </div>
            @endforeach
        </div>

        <h4 class="red-text mt-0">Educatie si formare</h4>
        <div class="right">
            <h4>De la: {{ $data -> begin_date }} pana la: {{ $data -> end_date }}</h4>
            @if($data -> present == 0)
                <h4>Present: nu</h4>
            @else
                <h4>Present: da</h4>
            @endif
            <h4>Titlul certificatului: {{ $data -> education_certification_name }}</h4>
            <h4>Denumirea institutiei: {{ $data -> education_institution_name }}</h4>
            @if(isset($data -> city -> name))
                <h4>Oras: {{ $data -> city -> name }}</h4>
            @endif
            @if(isset($data -> country -> name))
                <h4>Tara: {{ $data -> country -> name }}</h4>
            @endif
            <h4>Desciere: {{ $data -> description }}</h4>
        </div>

        <h4 class="red-text mt-0">Competente personale</h4>
        <div class="right">
            @if(isset($data -> matern_language -> name))
                <h4>Limba materna: {{ $data -> matern_language -> name }}</h4>
            @endif
            <h4>Limbi straine:</h4>
            @if(count($data -> profesionistProfileHasForeignLanguages))
                @foreach ($data -> profesionistProfileHasForeignLanguages as $key => $value)
                    @if(isset($value -> foreignLanguage -> name))
                        <h4>{{ $value -> foreignLanguage -> name }}</h4>
                    @endif
                @endforeach
            @endif
            <h4>Competente manageriale: {{ $data -> managerial_skills }}</h4>
            <h4>Competente la locul de munca: {{ $data -> skills_at_working_place }}</h4>
            <h4>Competente digitale: {{ $data -> digital_skills }}</h4>
        </div>

        <h4 class="red-text mt-0">Informatii suplimentare</h4>
        <div class="right">
            <h4>Permis de conducere, categoria: {{ $data -> driving_licence }}</h4>
            <h4>Alte competente: {{ $data -> other_skills }}</h4>
            <h4>Descriere personala: {{ $data -> personal_description }}</h4>
        </div>
    </body>

    <style media="screen">
        #image {
            /* display:inline-block; */
            /* margin-right:10px; */
            /* width: 25%; */
            /* background-color:red; */
        }
        .right {
            display:inline-block;
            width: 70%;
            /* background-color:red; */
            height: auto;
            margin-left: 25%;
        }
        .align-center {
            text-align: center;
        }
        .left {
            display:inline-block;
            margin-right:10px;
            width: 20%;
            /* background-color:red; */
            height: auto;
            /* margin-top: 10%; */
        }
        .red-text {
            color: #ED3137;
        }
        .mt-0 {
            margin-top: 0px;
        }
        .history-element {
            margin-left: 50px;
        }
        .mt {
            margin-top: 50px;
        }
    </style>
</html>
