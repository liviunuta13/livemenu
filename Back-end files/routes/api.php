<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: http://localhost:80');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'UserLoginController@login');

Route::post('/register', 'UserRegisterController@register');

Route::get('/user/edit/email/confirm/{user_id}/{confirmed_token}', 'UserRegisterController@confirmEmail');

Route::get('/user/email/confirm/{user_id}/{new_email}/{confirmed_token}', 'UserProfileController@confirmEditEmail');

Route::middleware(['auth-user']) -> group(function() {

    Route::get('/logout', 'UserLoginController@logout');

    Route::get('/user/read/{id}', 'UserProfileController@read');

    Route::post('/user/edit', 'UserProfileController@save');

    Route::post('/user/delete', 'UserProfileController@delete');

    Route::post('/user/rating/add', 'UserRatingController@add');


    Route::get('/user/music_types', 'UserController@getMusicTypes');


    Route::post('/user/follow', 'UserFollowsController@follow');

    Route::post('/user/unfollow', 'UserFollowsController@unfollow');


    Route::post('/user/professionist/add', 'UserProfileController@addProfessionistProfile');

    Route::get('/professionist', 'ProfessionistProfileController@read');

    Route::get('/professionist/{professionist_id}/read', 'ProfessionistProfileController@readProfessionist');

    Route::get('/professionists', 'ProfessionistProfileController@readProfessionists');

    Route::post('/professionist/edit', 'ProfessionistProfileController@save');

    Route::get('/professionist/delete', 'ProfessionistProfileController@delete');

    Route::get('/professionist/working_status/{value}/update', 'ProfessionistProfileController@workingStatus');

    Route::get('/professionist/available_for_working/{value}/update', 'ProfessionistProfileController@availableForWorking');

    Route::post('/professionist/post/add', 'ProfessionistPostsController@add');

    Route::post('/professionist/post/{post_id}/edit', 'ProfessionistPostsController@edit');

    Route::get('/professionist/post/{post_id}/delete', 'ProfessionistPostsController@delte');

    Route::post('/professionist/work_request/add', 'ProfessionistWorkRequestsController@add');

    Route::post('/professionist/work_request/{work_request_id}/edit', 'ProfessionistWorkRequestsController@update');

    Route::get('/professionist/work_request/{work_request_id}/delete', 'ProfessionistWorkRequestsController@delete');

    Route::post('/professionist/rating/add', 'RatingsController@addRatingFromProfessionist');



    Route::get('/professionist/export_cv', 'ProfessionistProfileController@exportCV');



    Route::get('/restaurants', 'RestaurantsController@read');

    Route::get('/producers', 'ProducersController@read');


    Route::post('/user/restaurant/add', 'UserProfileController@addRestarantProfile');

    Route::post('/user/producer/add', 'UserProfileController@addProducerProfile');

    Route::post('/user/client/add', 'UserProfileController@addClientProfile');

    Route::get('/restaurant/{restaurant_id}/read', 'RestaurantProfileController@readRestaurant');

    Route::get('/user/tips/types', 'UserController@getTipsTypes');

    Route::post('/restaurant/edit', 'RestaurantProfileController@save');

    Route::get('/restaurant/delete', 'RestaurantProfileController@delete');

    Route::post('/restaurant/location/add', 'RestaurantLocationsController@add');

    Route::post('/restaurant/post/add', 'RestaurantPostsController@add');

    Route::post('/restaurant/post/{post_id}/edit', 'RestaurantPostsController@save');

    Route::get('/restaurant/post/{post_id}/delete', 'RestaurantPostsController@delete');

    Route::get('/restaurant/post/{post_id}/picture/{picture_id}/delete', 'RestaurantPostsController@deletePicture');

    Route::get('/restaurant/picture/{picture_id}/delete/', 'RestaurantProfileController@deletePicture');

    Route::post('/restaurant/edit/location/{location_id}', 'RestaurantLocationsController@save');

    Route::get('/restaurant/locations/facilities', 'RestaurantLocationsController@getFacilities');

    Route::get('/restaurant/location/{location_id}/delete', 'RestaurantLocationsController@delete');

    Route::get('/restaurant/location/{location_id}/picture/{picture_id}/delete', 'RestaurantLocationsController@deletePicture');

    Route::get('/restaurant/location/{location_id}/read', 'RestaurantLocationsController@readLocation');

    Route::post('/restaurant/location/{location_id}/menu/add', 'RestaurantLocationMenuController@add');

    // Route::get('/restaurant/location/{location_id}/menu', 'RestaurantLocationMenuController@readMenu');

    Route::post('/restaurant/location/{location_id}/menu/{menu_id}/daily/add', 'RestaurantLocationMenuController@dailyMenuAdd');

    Route::get('/restaurant/location/{location_id}/daily_menu/{menu_id}/delete', 'RestaurantLocationMenuController@dailyMenuDelete');

    Route::post('/restaurant/location/{location_id}/promotion/add', 'RestaurantLocationPromotionsController@add');

    Route::post('/restaurant/location/{location_id}/promotion/{promotion_id}/edit', 'RestaurantLocationPromotionsController@save');

    Route::get('/restautant/location/{location_id}/promotion/{promotion_id}/delete', 'RestaurantLocationPromotionsController@delete');

    Route::get('/restaurant/location/{location_id}/promotions', 'RestaurantLocationPromotionsController@getPromotions');

    Route::post('/restaurant/location/{location_id}/event/add', 'RestaurantLocationEventsController@add');

    Route::post('/restaurant/location/{location_id}/event/{event_id}/edit', 'RestaurantLocationEventsController@save');

    Route::get('/restaurant/location/{location_id}/event/{event_id}/delete', 'RestaurantLocationEventsController@delete');

    Route::get('/restaurant/location/{location_id}/events', 'RestaurantLocationEventsController@getEvents');

    Route::post('/restaurant/location/{location_id}/job/add', 'RestaurantLocationJobsController@add');

    Route::post('/restaurant/location/{location_id}/job/{job_id}/edit', 'RestaurantLocationJobsController@save');

    Route::get('/restaurant/location/{location_id}/job/{job_id}/delete', 'RestaurantLocationJobsController@delete');

    Route::get('/restaurant/location/{location_id}/jobs', 'RestaurantLocationJobsController@getJobs');

    Route::get('/hotel/facilities', 'HotelController@getFacilities');

    Route::post('/restaurant/location/{location_id}/hotel/add', 'RestaurantLocationHotelsController@add');

    Route::post('/restaurant/location/{location_id}/hotel/{hotel_id}/edit', 'RestaurantLocationHotelsController@save');

    Route::get('/restaurant/location/{location_id}/hotel/{hotel_id}/delete', 'RestaurantLocationHotelsController@delete');

    Route::get('/restaurant/location/{location_id}/hotel/{hotel_id}/picture/{picture_id}/delete', 'RestaurantLocationHotelsController@deletePicture');

    Route::get('/hotel/room/facilities', 'HotelRoomsController@getFacilities');

    Route::get('/hotel/{hotel_id}/room/{room_id}/read', 'HotelRoomsController@getRooms');

    Route::middleware(['restaurant-has-hotel']) -> group(function() {

        Route::post('/hotel/{hotel_id}/rooms/add', 'HotelRoomsController@add');

        Route::post('/hotel/{hotel_id}/room/{room_id}/edit', 'HotelRoomsController@save');

        Route::get('/hotel/{hotel_id}/room/{room_id}/delete', 'HotelRoomsController@delete');

        Route::get('/hotel/{hotel_id}/room/{room_id}/picture/{picture_id}/delete', 'HotelRoomsController@deletePicture');
    });

    Route::post('/restaurant/location/{location_id}/eventroom/add', 'RestaurantLocationsEventRoomsController@add');

    Route::post('/restaurant/location/{location_id}/eventroom/{eventroom_id}/edit', 'RestaurantLocationsEventRoomsController@save');

    Route::get('/restaurant/location/{location_id}/eventroom/{eventroom_id}/delete', 'RestaurantLocationsEventRoomsController@delete');

    Route::get('/restaurant/location/{location_id}/eventroom/{eventroom_id}/delete', 'RestaurantLocationsEventRoomsController@delete');

    Route::post('/restaurant/location/{location_id}/post/add', 'RestaurantLocationsPostsController@add');

    Route::post('/restaurant/location/{location_id}/post/{post_id}/edit', 'RestaurantLocationsPostsController@save');

    Route::get('/restaurant/location/{location_id}/post/{post_id}/delete', 'RestaurantLocationsPostsController@delete');

    Route::get('/restaurant/location/{location_id}/post/{post_id}/picture/{picture_id}/delete', 'RestaurantLocationsPostsController@deletePicture');



    Route::post('/restaurant/location/{location_id}/rating/add', 'RatingsController@addRatingFromRestaurantLocation');



    Route::post('/restaurant/{restaurant_id}/sponsorship/partner/{partner_id}/send', 'RestaurantsController@sendSponsorship');

    Route::post('/restaurant/location/{location_id}/tips/give', 'TipsController@restaurantLocationGive');


    Route::post('/restaurant/location/{location_id}/staff/add', 'RestaurantLocationStaffController@add');

    Route::post('/restaurant/location/{location_id}/staff/{staff_id}/edit', 'RestaurantLocationStaffController@save');

    Route::get('/restaurant/location/{location_id}/staff/{staff_id}/delete', 'RestaurantLocationStaffController@delete');


    Route::get('/hotels', 'HotelController@readHotels');



    Route::post('/producer/edit', 'ProducerProfileController@save');

    Route::get('/producer/delete', 'ProducerProfileController@delete');

    Route::get('/producer/picture/{picture_id}/delete', 'ProducerProfileController@deletePicture');

    Route::get('/producer/{producer_id}/read', 'ProducerProfileController@readProducer');

    Route::post('/producer/location/add', 'ProducerLocationsController@add');

    Route::post('/producer/location/{location_id}/edit', 'ProducerLocationsController@save');

    Route::get('/producer/location/{location_id}/delete', 'ProducerLocationsController@delete');

    Route::get('/producer/location/{location_id}/picture/{picture_id}/delete', 'ProducerLocationsController@deletePicture');

    Route::get('/producer/location/{location_id}/read', 'ProducerLocationsController@readLocation');

    // .............
    Route::post('/producer/location/{location_id}/product/add', 'ProducerProductsController@add');

    Route::post('/producer/location/{location_id}/product/delete', 'ProducerProductsController@delete');
    // .............

    Route::post('/producer/promotion/add', 'PrducerPromotionsController@add');

    Route::post('/producer/promotion/{promotion_id}/edit', 'PrducerPromotionsController@save');

    Route::get('/producer/promotion/{promotion_id}/delete', 'PrducerPromotionsController@delete');

    Route::get('/producer/promotions', 'PrducerPromotionsController@getPromotions');

    Route::post('/producer/job_offer/add', 'ProducerJobOffersController@add');

    Route::post('/producer/job_offer/{job_offer_id}/edit', 'ProducerJobOffersController@save');

    Route::get('/producer/job_offer/{job_offer_id}/delete', 'ProducerJobOffersController@delete');

    Route::get('/producer/job_offers', 'ProducerJobOffersController@getJobOffers');

    Route::post('/producer/location/{location_id}/staff/add', 'ProducerLocationStaffController@add');

    Route::post('/producer/location/staff/{staff_id}/edit', 'ProducerLocationStaffController@save');

    Route::get('/producer/location/staff/{staff_id}/delete', 'ProducerLocationStaffController@delete');


    Route::get('/staff/roles', 'StaffRolesController@getStaffRoles');


    Route::get('/producer/staff/{staff_id}/associate/role/{role_id}', 'ProducerLocationStaffController@associateRole');

    Route::get('/producer/staff/{staff_id}/dissociate/role/{role_id}', 'ProducerLocationStaffController@dissociateRole');



    Route::post('/producer/partner/add', 'ProducerPartnersController@add');

    Route::post('/producer/location/{location_id}/rating/add', 'RatingsController@addRatingFromProducerLocation');

    Route::post('/producer/post/add', 'ProducerPostsController@add');

    Route::post('/producer/post/{post_id}/edit', 'ProducerPostsController@save');

    Route::get('/producer/post/{post_id}/delete', 'ProducerPostsController@delete');

    Route::post('/producer/affiliation_request/add', 'ProducerAffiliationRequestsController@add');

    Route::post('/producer/affiliation_request/{affiliation_request_id}/edit', 'ProducerAffiliationRequestsController@save');

    Route::get('/producer/affiliation_request/{affiliation_request_id}/delete', 'ProducerAffiliationRequestsController@delete');

    Route::get('/producer/affiliation_request/{affiliation_request_id}/submit', 'ProducersAffiliateController@affiliateToProducer');

    Route::get('/producer/affiliation_request/{affiliation_request_id}/confirm/{confirm_value}', 'ProducersAffiliateController@confirmAffiliatonRequest');

    // Producer's staff routes by roles

    Route::get('/producer/location/{location_id}/staff/{staff_id}/promotion/add', 'ProducerLocationsStaffPromotionsController@add');

    Route::get('/languages', 'UserController@languages');

    Route::get('/cities', 'UserController@cities');

    Route::get('/counties', 'UserController@counties');

    Route::get('/countries', 'UserController@countries');
});


include(base_path()."/routes/dymka-api.php");
