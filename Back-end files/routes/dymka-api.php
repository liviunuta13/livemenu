<?php

Route::middleware(['auth-user'])->group(function () {

    Route::post('/professionist/history/add', 'ProfessionalHistoricController@add');
    Route::post('/professionist/history/read/{id}', 'ProfessionalHistoricController@read');
    Route::post('/professionist/history/edit/{id}', 'ProfessionalHistoricController@edit');
    Route::post('/professionist/history/delete/{id}', 'ProfessionalHistoricController@delete');

    Route::post('/professionist/personalSkill/add', 'PersonalSkillsController@add');
    Route::post('/professionist/personalSkill/read/{id}', 'PersonalSkillsController@read');
    Route::post('/professionist/personalSkill/edit/{id}', 'PersonalSkillsController@edit');
    Route::post('/professionist/personalSkill/delete/{id}', 'PersonalSkillsController@delete');

    Route::post('/professionist/educationAndTraining/add', 'EducationAndTrainingController@add');
    Route::post('/professionist/educationAndTraining/read/{id}', 'EducationAndTrainingController@read');
    Route::post('/professionist/educationAndTraining/edit/{id}', 'EducationAndTrainingController@edit');
    Route::post('/professionist/educationAndTraining/delete/{id}', 'EducationAndTrainingController@delete');


    Route::post('/restaurant/location/{location_id}/staff/{staff_id}/post/add', 'RestaurantLocationStaffPostsController@add');
    Route::post('/restaurant/location/{location_id}/staff/{staff_id}/promotion/add', 'RestaurantLocationStaffPromotionsController@add');
    Route::post('/restaurant/location/{location_id}/staff/{staff_id}/staff/add', 'RestaurantLocationAddStaffController@add');
    Route::post('/restaurant/location/{location_id}/staff/{staff_id}/staff/delete/{staff_delete_id}', 'RestaurantLocationStaffCloseContractsController@delete');

    Route::post('/producer/location/{location_id}/staff/{staff_id}/post/add', 'ProducerLocationStaffPostsController@add');
    Route::post('/producer/location/{location_id}/staff/{staff_id}/promotion/add', 'ProducerLocationStaffPromotionsController@add');
    Route::post('/producer/location/{location_id}/staff/{staff_id}/staff/add', 'ProducerLocationAddStaffController@add');
    Route::post('/producer/location/{location_id}/staff/{staff_id}/staff/delete/{staff_delete_id}', 'ProducerLocationStaffCloseContractsController@delete');

    Route::post('/test/location/{location_id}','ShiftController@restaurantOpen');
    Route::post('/restaurant/location/{location_id}/add','ShiftController@addShifts');
    Route::post('/restaurant/location/{location_id}/edit/{id}','ShiftController@editShifts');
});


