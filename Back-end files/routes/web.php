<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts/app');

});

// Authentication Routes...
Route::get('/admin/login', 'Auth\LoginController@showLoginForm') -> name('login');
Route::post('/admin/login', 'Auth\LoginController@login');
Route::post('/admin/logout', 'Auth\LoginController@logout') -> name('logout');

// Registration Routes...
Route::get('/admin/register', 'Auth\RegisterController@showRegistrationForm') -> name('register');
Route::post('/admin/register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('/admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('/admin/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('/admin/password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index') -> name('home');

Route::middleware(['auth']) -> group(function () {

    Route::group(['prefix' => 'admin'], function () {

        Route::get('/dashboard', 'Admin\DashboardController@index');


        Route::get('/users', 'Admin\UsersController@index')->name('users');

        Route::post('/users/search', 'Admin\UsersController@search')->name('search-user');

        Route::post('/user/add', 'Admin\UsersController@add')->name('add-user');

        Route::post('/user/{user_id}/edit', 'Admin\UsersController@edit')->name('edit-user');

        Route::get('/user/{user_id}/delete', 'Admin\UsersController@delete')->name('delete-user');


        Route::get('/admins', 'Admin\AdminsController@index')->name('admins');

        Route::post('/admins/search', 'Admin\AdminsController@search')->name('search-admins');

        Route::post('/admin/add', 'Admin\AdminsController@add')->name('add-admin');

        Route::post('/admin/{admin_id}/edit', 'Admin\AdminsController@edit')->name('edit-admin');

        Route::get('/admin/{admin_id}/delete', 'Admin\AdminsController@delete')->name('delete-admin');


        Route::get('/professionists', 'Admin\ProfessionistsController@index')->name('professionists');

        Route::post('/professionists/search', 'Admin\ProfessionistsController@search')->name('search-professionists');

        Route::post('/professionist/add', 'Admin\ProfessionistsController@add')->name('add-professionist');

        Route::post('/professionist/{professionist_id}/edit', 'Admin\ProfessionistsController@edit')->name('edit-professionist');

        Route::get('/professionist/{professionist_id}/delete', 'Admin\ProfessionistsController@delete')->name('delete-professionist');

        Route::get('/professionist/{professionist_id}/suspend', 'Admin\ProfessionistsController@suspend')->name('suspend-professionist');

        Route::get('/professionist/{professionist_id}/export_cv', 'Admin\ProfessionistsController@exportCV')->name('export-cv');


        Route::get('/restaurants', 'Admin\RestaurantsController@index')->name('restaurants');

        Route::post('/restaurants/search', 'Admin\RestaurantsController@search')->name('search-restaurants');

        Route::post('/restaurant/add', 'Admin\RestaurantsController@add')->name('add-restaurant');

        Route::post('/restaurant/{restaurant_id}/edit', 'Admin\RestaurantsController@edit')->name('edit-restaurant');

        Route::get('/restaurant/{restaurant_id}/delete', 'Admin\RestaurantsController@delete')->name('delete-restaurant');

        Route::get('/restaurant/{restaurant_id}/suspend', 'Admin\RestaurantsController@suspend')->name('suspend-restaurant');


        Route::get('/producers', 'Admin\ProducersController@index')->name('producers');

        Route::post('/producers/search', 'Admin\ProducersController@search')->name('search-producers');

        Route::post('/producer/add', 'Admin\ProducersController@add')->name('add-producer');

        Route::post('/producer/{producer_id}/edit', 'Admin\ProducersController@edit')->name('edit-producer');

        Route::get('/producer/{producer_id}/delete', 'Admin\ProducersController@delete')->name('delete-producer');

        Route::get('/producer/{producer_id}/suspend', 'Admin\ProducersController@suspend')->name('suspend-producer');


        //Manu

        Route::get('/restaurants/locations', 'Admin\RestaurantLocationsController@index')->name('locations');

        Route::post('/restaurants/locations/search', 'Admin\RestaurantLocationsController@search')->name('search-location');

        Route::post('/restaurant/location/add', 'Admin\RestaurantLocationsController@add')->name('add-location');

        Route::post('/restaurant/location/{location_id}/edit', 'Admin\RestaurantLocationsController@save')->name('edit-location');

        Route::get('/restaurant/location/{location_id}/delete', 'Admin\RestaurantLocationsController@delete')->name('delete-location');


        Route::get('/restaurants/jobs', 'Admin\RestaurantLocationsJobsController@index')->name('jobs');

        Route::post('/restaurants/jobs/search', 'Admin\RestaurantLocationsJobsController@search')->name('search-jobs');

        Route::post('/restaurant/job/add', 'Admin\RestaurantLocationsJobsController@add')->name('add-job');

        Route::post('/restaurant/job/{job_id}/edit', 'Admin\RestaurantLocationsJobsController@save')->name('edit-job');

        Route::get('/restaurant/job/{job_id}/delete', 'Admin\RestaurantLocationsJobsController@delete')->name('delete-job');


        Route::get('/restaurants/specifics', 'Admin\RestaurantSpecificsController@index')->name('specifics');

        Route::post('/restaurants/specifics/search', 'Admin\RestaurantSpecificsController@search')->name('search-specifics');

        Route::post('/restaurant/specific/add', 'Admin\RestaurantSpecificsController@add')->name('add-specific');

        Route::post('/restaurant/specific/{specific_id}/edit', 'Admin\RestaurantSpecificsController@edit')->name('edit-specific');

        Route::get('/restaurant/specific/{specific_id}/delete', 'Admin\RestaurantSpecificsController@delete')->name('delete-specific');


        Route::get('/restaurants/staffs', 'Admin\RestaurantLocationStaffsController@index')->name('staffs');

        Route::post('/restaurants/staffs/search', 'Admin\RestaurantLocationStaffsController@search')->name('search-staffs');

        Route::post('/restaurant/staff/add', 'Admin\RestaurantLocationStaffsController@add')->name('add-staff');

        Route::post('/restaurant/staff/{staff_id}/edit', 'Admin\RestaurantLocationStaffsController@save')->name('edit-staff');

        Route::get('/restaurant/staff/{staff_id}/delete', 'Admin\RestaurantLocationStaffsController@delete')->name('delete-staff');


        Route::get('/restaurants/events', 'Admin\RestaurantLocationEventsController@index')->name('events');

        Route::post('/restaurants/events/search', 'Admin\RestaurantLocationEventsController@search')->name('search-events');

        Route::post('/restaurant/event/add', 'Admin\RestaurantLocationEventsController@add')->name('add-event');

        Route::post('/restaurant/event/{event_id}/edit', 'Admin\RestaurantLocationEventsController@save')->name('edit-event');

        Route::get('/restaurant/event/{event_id}/delete', 'Admin\RestaurantLocationEventsController@delete')->name('delete-event');


        Route::get('/restaurants/facilities', 'Admin\RestaurantLocationFacilitiesController@index')->name('facilities');

        Route::post('/restaurants/facilities/search', 'Admin\RestaurantLocationFacilitiesController@search')->name('search-facilities');

        Route::post('/restaurant/facility/add', 'Admin\RestaurantLocationFacilitiesController@add')->name('add-facility');

        Route::post('/restaurant/facility/{facility_id}/edit', 'Admin\RestaurantLocationFacilitiesController@save')->name('edit-facility');

        Route::get('/restaurant/facility/{facility_id}/delete', 'Admin\RestaurantLocationFacilitiesController@delete')->name('delete-facility');


        Route::get('/restaurants/promotions', 'Admin\RestaurantLocationPromotionsController@index')->name('promotions');

        Route::post('/restaurants/promotions/search', 'Admin\RestaurantLocationPromotionsController@search')->name('search-promotions');

        Route::post('/restaurant/promotion/add', 'Admin\RestaurantLocationPromotionsController@add')->name('add-promotion');

        Route::post('/restaurant/promotion/{promotion_id}/edit', 'Admin\RestaurantLocationPromotionsController@save')->name('edit-promotion');

        Route::get('/restaurant/promotion/{promotion_id}/delete', 'Admin\RestaurantLocationPromotionsController@delete')->name('delete-promotion');


        Route::get('/restaurants/hotels', 'Admin\RestaurantLocationHotelsController@index')->name('hotels');

        Route::post('/restaurants/hotels/search', 'Admin\RestaurantLocationHotelsController@search')->name('search-hotels');

        Route::post('/restaurant/hotel/add', 'Admin\RestaurantLocationHotelsController@add')->name('add-hotel');

        Route::post('/restaurant/hotel/{hotel_id}/edit', 'Admin\RestaurantLocationHotelsController@save')->name('edit-hotel');

        Route::get('/restaurant/hotel/{hotel_id}/delete', 'Admin\RestaurantLocationHotelsController@delete')->name('delete-hotel');


        Route::get('/restaurants/hotel_facilities', 'Admin\HotelFacilitiesController@index')->name('hotels_facilities');

        Route::post('/restaurants/hotels_facilities/search', 'Admin\HotelFacilitiesController@search')->name('search-hotels_facilities');

        Route::post('/restaurant/hotel_facility/add', 'Admin\HotelFacilitiesController@add')->name('add-hotel_facility');

        Route::post('/restaurant/hotel_facility/{facility_id}/edit', 'Admin\HotelFacilitiesController@save')->name('edit-hotel_facility');

        Route::get('/restaurant/hotel_facility/{facility_id}/delete', 'Admin\HotelFacilitiesController@delete')->name('delete-hotel_facility');


        Route::get('/restaurants/programs', 'Admin\RestaurantLocationProgramsController@index')->name('programs');

        Route::post('/restaurants/programs/search', 'Admin\RestaurantLocationProgramsController@search')->name('search-programs');

        Route::post('/restaurant/program/add', 'Admin\RestaurantLocationProgramsController@add')->name('add-program');

        Route::post('/restaurant/program/{program_id}/edit', 'Admin\RestaurantLocationProgramsController@save')->name('edit-program');

        Route::get('/restaurant/program/{program_id}/delete', 'Admin\RestaurantLocationProgramsController@delete')->name('delete-program');


        Route::get('/restaurants/roles', 'Admin\RestaurantLocationStaffRolesController@index')->name('roles');

        Route::post('/restaurants/roles/search', 'Admin\RestaurantLocationStaffRolesController@search')->name('search-roles');

        Route::post('/restaurant/role/add', 'Admin\RestaurantLocationStaffRolesController@add')->name('add-role');

        Route::post('/restaurant/role/{role_id}/edit', 'Admin\RestaurantLocationStaffRolesController@save')->name('edit-role');

        Route::get('/restaurant/role/{role_id}/delete', 'Admin\RestaurantLocationStaffRolesController@delete')->name('delete-role');


        Route::get('/restaurants/producer_locations', 'Admin\ProducerLocationsController@index')->name('producer_locations');

        Route::post('/restaurants/producer_locations/search', 'Admin\ProducerLocationsController@search')->name('search-producer_locations');

        Route::post('/restaurant/producer_location/add', 'Admin\ProducerLocationsController@add')->name('add-producer_location');

        Route::post('/restaurant/producer_location/{producer_location_id}/edit', 'Admin\ProducerLocationsController@save')->name('edit-producer_location');

        Route::get('/restaurant/producer_location/{producer_location_id}/delete', 'Admin\ProducerLocationsController@delete')->name('delete-producer_location');


        Route::get('/restaurants/programs_conditions', 'Admin\ProgramConditionsController@index')->name('programs_conditions');

        Route::post('/restaurants/programs_conditions/search', 'Admin\ProgramConditionsController@search')->name('search-programs_conditions');

        Route::post('/restaurant/program_condition/add', 'Admin\ProgramConditionsController@add')->name('add-program_condition');

        Route::post('/restaurant/program_condition/{program_condition_id}/edit', 'Admin\ProgramConditionsController@save')->name('edit-program_condition');

        Route::get('/restaurant/program_condition/{program_condition_id}/delete', 'Admin\ProgramConditionsController@delete')->name('delete-program_condition');


        Route::get('/restaurants/rooms', 'Admin\HotelRoomsController@index')->name('rooms');

        Route::post('/restaurants/rooms/search', 'Admin\HotelRoomsController@search')->name('search-rooms');

        Route::post('/restaurant/room/add', 'Admin\HotelRoomsController@add')->name('add-room');

        Route::post('/restaurant/room/{room_id}/edit', 'Admin\HotelRoomsController@save')->name('edit-room');

        Route::get('/restaurant/room/{room_id}/delete', 'Admin\HotelRoomsController@delete')->name('delete-room');


        Route::get('/restaurants/rooms_facilities', 'Admin\RoomFacilitiesController@index')->name('rooms_facilities');

        Route::post('/restaurants/rooms_facilities/search', 'Admin\RoomFacilitiesController@search')->name('search-rooms_facilities');

        Route::post('/restaurant/room_facility/add', 'Admin\RoomFacilitiesController@add')->name('add-room_facility');

        Route::post('/restaurant/room_facility/{room_facility_id}/edit', 'Admin\RoomFacilitiesController@save')->name('edit-room_facility');

        Route::get('/restaurant/room_facility/{room_facility_id}/delete', 'Admin\RoomFacilitiesController@delete')->name('delete-room_facility');


        Route::get('/restaurants/units', 'Admin\UnitsController@index')->name('units');

        Route::post('/restaurants/units/search', 'Admin\UnitsController@search')->name('search-units');

        Route::post('/restaurant/unit/add', 'Admin\UnitsController@add')->name('add-unit');

        Route::post('/restaurant/unit/{unit_id}/edit', 'Admin\UnitsController@save')->name('edit-unit');

        Route::get('/restaurant/unit/{unit_id}/delete', 'Admin\UnitsController@delete')->name('delete-unit');


        Route::get('/restaurants/utilities', 'Admin\RestaurantUtilitiesController@index')->name('utilities');

        Route::post('/restaurants/utilities/search', 'Admin\RestaurantUtilitiesController@search')->name('search-utilities');

        Route::post('/restaurant/utility/add', 'Admin\RestaurantUtilitiesController@add')->name('add-utility');

        Route::post('/restaurant/utility/{utility_id}/edit', 'Admin\RestaurantUtilitiesController@save')->name('edit-utility');

        Route::get('/restaurant/utility/{utility_id}/delete', 'Admin\RestaurantUtilitiesController@delete')->name('delete-utility');


        Route::get('/restaurants/ingredients', 'Admin\IngredientsController@index')->name('ingredients');

        Route::post('/restaurants/ingredients/search', 'Admin\IngredientsController@search')->name('search-ingredients');

        Route::post('/restaurant/ingredient/add', 'Admin\IngredientsController@add')->name('add-ingredient');

        Route::post('/restaurant/ingredient/{ingredient_id}/edit', 'Admin\IngredientsController@save')->name('edit-ingredient');

        Route::get('/restaurant/ingredient/{ingredient_id}/delete', 'Admin\IngredientsController@delete')->name('delete-ingredient');


        Route::get('/restaurants/room_events', 'Admin\EventRoomsController@index')->name('room_events');

        Route::post('/restaurants/room_events/search', 'Admin\EventRoomsController@search')->name('search-room_events');

        Route::post('/restaurant/room_event/add', 'Admin\EventRoomsController@add')->name('add-room_event');

        Route::post('/restaurant/room_event/{room_event_id}/edit', 'Admin\EventRoomsController@save')->name('edit-room_event');

        Route::get('/restaurant/room_event/{room_event_id}/delete', 'Admin\EventRoomsController@delete')->name('delete-room_event');


        Route::get('/restaurants/events_facilities', 'Admin\EventRoomFacilitiesController@index')->name('events_facilities');

        Route::post('/restaurants/events_facilities/search', 'Admin\EventRoomFacilitiesController@search')->name('search-events_facilities');

        Route::post('/restaurant/event_facility/add', 'Admin\EventRoomFacilitiesController@add')->name('add-event_facility');

        Route::post('/restaurant/event_facility/{event_facility_id}/edit', 'Admin\EventRoomFacilitiesController@save')->name('edit-event_facility');

        Route::get('/restaurant/event_facility/{event_facility_id}/delete', 'Admin\EventRoomFacilitiesController@delete')->name('delete-event_facility');


        Route::get('/restaurants/pictures', 'Admin\EventRoomPicturesController@index')->name('pictures');

        Route::post('/restaurants/pictures/search', 'Admin\EventRoomPicturesController@search')->name('search-pictures');

        Route::post('/restaurant/picture/add', 'Admin\EventRoomPicturesController@add')->name('add-picture');

        Route::post('/restaurant/picture/{picture_id}/edit', 'Admin\EventRoomPicturesController@save')->name('edit-picture');

        Route::get('/restaurant/picture/{pictures_id}/delete', 'Admin\EventRoomPicturesController@delete')->name('delete-picture');


        Route::get('/restaurants/allergies', 'Admin\IngredientAllergiesController@index')->name('allergies');

        Route::post('/restaurants/allergies/search', 'Admin\IngredientAllergiesController@search')->name('search-allergies');

        Route::post('/restaurant/allergy/add', 'Admin\IngredientAllergiesController@add')->name('add-allergy');

        Route::post('/restaurant/allergy/{allergy_id}/edit', 'Admin\IngredientAllergiesController@save')->name('edit-allergy');

        Route::get('/restaurant/allergy/{allergy_id}/delete', 'Admin\IngredientAllergiesController@delete')->name('delete-allergy');


        Route::get('/restaurants/educations', 'Admin\ProfessionistEducationsController@index')->name('educations');

        Route::post('/restaurants/educations/search', 'Admin\ProfessionistEducationsController@search')->name('search-educations');

        Route::post('/restaurant/education/add', 'Admin\ProfessionistEducationsController@add')->name('add-education');

        Route::post('/restaurant/education/{education_id}/edit', 'Admin\ProfessionistEducationsController@edit')->name('edit-education');

        Route::get('/restaurant/education/{education_id}/delete', 'Admin\ProfessionistEducationsController@delete')->name('delete-education');

    });
});

