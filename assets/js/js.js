var link_request = "http://api1.livemenu.info";



$(document).ready(function () {
    /*Functie pentru scroll lateral restaurant*/
    var infinite_scroll_r = $('.infinite_scroll_restaurant');
    var infinite_scroll_children_r = infinite_scroll_r[0].childElementCount;
    var new_width_r = infinite_scroll_children_r * $('.col-md-3').width();
    infinite_scroll_r[0].style.width = new_width_r + 'px';
});

$(document).ready(function () {
    /*Functie pentru scroll lateral*/
    var infinite_scroll = $('.infinite_scroll');
    var infinite_scroll_children = infinite_scroll[0].childElementCount;
    var new_width = infinite_scroll_children * 165;
    infinite_scroll[0].style.width = new_width + 'px';
});

$(document).ready(function () {
    /*Functie pentru scroll lateral profesionist*/
    var infinite_scroll_p = $('.infinite_scroll_profesionist');
    var infinite_scroll_children_p = infinite_scroll_p[0].childElementCount;
    var new_width_p = infinite_scroll_children_p * 165;
    infinite_scroll_p[0].style.width = new_width_p + 'px';
});

$(document).ready(function () {
    /*Functie pentru asezarea tip grid a postarilor*/
    $('.grid').masonry({
        itemSelector: '.grid-item'
    });
});

$(document).ready(function(){
    /*Functie pentru dimensiunea google maps listare*/
    var mapa = $('#mapa_afisare')[0];
    var width = $('#latime_mapa_afisare').width();
    mapa.style.width = width + 'px';
});

$(document).ready(function () {
    /*Functie pentru dimensiunea google maps*/
    var mapa = $('#search_map')[0];
    var width = $('.map_width').width();
    mapa.style.width = width + 'px';
});

function showAbsoluteButtons() {
    var edit = $('.icon_hovered_edit');
    var deleteb = $('.icon_hovered_delete');
    edit.css('left', '20px');
    deleteb.css('right', '20px');
}

function hideAbsoluteButtons() {
    var edit = $('.icon_hovered_edit');
    var deleteb = $('.icon_hovered_delete');
    edit.css('left', '-30px');
    deleteb.css('right', '-30px');
}

function triggerInputFile() {
    var input = $('#upload_photo_profile');
    input.click();
}


function addProfileGut(sect){
    var text = $(sect).parent('div').find('.text_added_profile_ed').html();
    var salveaza = $(sect).parent('div').find('.btn_save');
    var input = $(sect).parent('div').find('.preluare_informatii');
    var p_tag = $(sect).parent('div').find('.text_added_profile_ed');
    var buton = $(sect);

    salveaza.css('display', 'block');
    p_tag.css('display', 'none');
    input[0].style.display="block";
    buton.css('display', 'none');
    input[0].value = text;
}

function saveProfileGut(sect){
    var text = $(sect).parent('div').find('.preluare_informatii').val();
    var editeaza = $(sect).parent('div').find('.btn_edit');
    var input = $(sect).parent('div').find('.preluare_informatii');
    var p_tag = $(sect).parent('div').find('.text_added_profile_ed');
    var buton = $(sect);

    editeaza.css('display', 'block');
    buton.css('display', 'none');
    input.css('display', 'none');
    if(text == ""){
        if(buton[0].id == "save_fav_food"){
            text = "Nu ai adaugat inca niciun meniu";
        }else if(buton[0].id == "save_sec_ing"){
            text = "Nu ai adaugat inca niciun ingredient";
        }else if(buton[0].id == "save_fav_rest"){
            text = "Nu ai adaugat inca niciun specific";
        }else if(buton[0].id == "save_fac_mus"){
            text = "Nu ai adaugat inca niciun tip de muzica";
        }
    }
    p_tag[0].innerHTML = text;
    p_tag.css('display', 'block');
}



function addAlternative(inputt) {
    var input = $(inputt).parent('div').find('.preluare_informatii').val();
    var clearing = $(inputt).parent('div').find('.preluare_informatii')[0];
    var div = $(inputt).parent('div').find('.append_alternatives_here');


    div.append(
        "<div class='alternative'>" + input + " <i class='fas fa-times' onclick='deleteAlternative(this)'></i></div>"
    );
    clearing.value = "";
}
function deleteAlternative(deleted) {
    var to_delete = $(deleted).parent('div');
    to_delete.remove();
}



function showUserPosts() {
    var this_button = $('#buton1show');
    var buton_activ = $('.buton_filtrare_postari_activ');

   if(this_button.hasClass('buton_filtrare_postari_inactiv')){
        buton_activ.removeClass('buton_filtrare_postari_activ').addClass('buton_filtrare_postari_inactiv');
        this_button.removeClass('buton_filtrare_postari_inactiv').addClass('buton_filtrare_postari_activ');
    }
}

function showOtherUserPosts(){
    var this_button = $('#buton2show');
    var buton_activ = $('.buton_filtrare_postari_activ');

    if(this_button.hasClass('buton_filtrare_postari_inactiv')){
        buton_activ.removeClass('buton_filtrare_postari_activ').addClass('buton_filtrare_postari_inactiv');
        this_button.removeClass('buton_filtrare_postari_inactiv').addClass('buton_filtrare_postari_activ');
    }
}



//Functie de inchidere a sidebarului de editare profil
function closeEditareProfil(){
    var div = $('.editare_profil_sidebar');
    var bg = $('.black_bg_edit_aside');
    div.css('display', 'none');
    bg.css('display', 'none');
}



//Functie de deschidere a sidebarului de editare profil
function openEditareProfil(){
    var div = $('.editare_profil_sidebar');
    var bg = $('.black_bg_edit_aside');
    div.css('display', 'block');
    bg.css('display', 'block');
}




function adaugaIstoricProfesional(){
    var adaugare = $('#maiMulteIstoricuriProfesionale');
    var de_adaugat =
        '<hr/><div class="row">\
                <div class="col-md-6">\
                    <p class="label_input_modify_profile">De la<span style="color:#ed3237">*</span>:</p>\
                    <input type="date" name="date_start" class="input_editare_profil" placeholder="LL/ZZ/AAAA"/>\
                </div>\
                <div class="col-md-6">\
                    <p class="label_input_modify_profile">Pana la<span style="color:#ed3237">*</span>:</p>\
                    <input type="date" name="date_final" class="input_editare_profil" placeholder="LL/ZZ/AAAA"/>\
                    <label class="label_input_checkbox">\
                            <input type="checkbox" name="present_check">\
                            <span class="checkmark_checkbox"></span>\
                    </label>\
                    <p class="checkbox_text_black">Prezent</p>\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-12">\
                    <p class="label_input_modify_profile">Postul ocupat<span style="color:#ed3237">*</span></p>\
                    <input type="text" name="postul_ocupat" class="input_editare_profil" placeholder="Bucatar" />\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-12">\
                    <p class="label_input_modify_profile">Nume firma<span style="color:#ed3237">*</span></p>\
                    <input type="text" name="nume_firma" class="input_editare_profil" placeholder="SC.EXAMPLE.SRL" />\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-6">\
                    <p class="label_input_modify_profile">Oras<span style="color:#ed3237">*</span></p>\
                    <input type="text" name="oras_istoric_pr" class="input_editare_profil" placeholder="Pitesti"/>\
                </div>\
                <div class="col-md-6">\
                    <p class="label_input_modify_profile">Tara<span style="color:#ed3237">*</span></p>\
                    <input type="text" name="tara_istoric_pr" class="input_editare_profil" placeholder="Romania"/>\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-12">\
                    <p class="label_input_modify_profile">Descriere</p>\
                    <textarea class="textarea_editare_profil" name="descriere_istoric" placeholder="LOREM IPSUM DOLOR SIT AMEN, ETC..." />\
                </div>\
            </div>'
        ;
    adaugare.append(de_adaugat);
}




//Functie de schimbare a taburilor de editare profil din sidebar
//Fiecare tab este deja existent, doar le afisam/ascundem
function schimbareContentEditare(buttonClicked){
    var button = $(buttonClicked);
    var icon = button.children('i');
    var id = button[0].id;
    var lastActiveDiv = $('.lastEditActive');


    if(button.hasClass('button_fast_nav_editer_inactive')){
        var butonActiv = $('.button_fast_nav_editer_active');
        var butonActivIcon = butonActiv.children('i');
        butonActiv.removeClass('button_fast_nav_editer_active').addClass('button_fast_nav_editer_inactive');
        button.removeClass('button_fast_nav_editer_inactive').addClass('button_fast_nav_editer_active');
        icon.removeClass('fa-plus').addClass('fa-minus');
        butonActivIcon.removeClass('fa-minus').addClass('fa-plus');

        if(id == 'editare1'){
            lastActiveDiv.removeClass('lastEditActive');
            $('#afisareEditare1').addClass('lastEditActive');
        }
        else if(id == 'editare2'){
            lastActiveDiv.removeClass('lastEditActive');
            $('#afisareEditare2').addClass('lastEditActive');
        }
        else if(id == 'editare3'){
            lastActiveDiv.removeClass('lastEditActive');
            $('#afisareEditare3').addClass('lastEditActive');
        }
        else if(id == 'editare4'){
            lastActiveDiv.removeClass('lastEditActive');
            $('#afisareEditare4').addClass('lastEditActive');
        }
        else if(id == 'editare5'){
            lastActiveDiv.removeClass('lastEditActive');
            $('#afisareEditare5').addClass('lastEditActive');
        }
    }
}





//Functie de inchidere a navigatorului pe mobil
function closeNavigator(){
    var navigator = $('.base_navigator');
    navigator.css('margin-left', '-100%');
}




//Functie de deschidere a navigatorului pe mobil
function openNavigator(){
    var navigator = $('.base_navigator');
    navigator.css('margin-left', '0');
}




//Functie de deschidere a procesului de adaugare imagine
function triggerInputFileRestaurant() {
    var input = $('#upload_photo_restaurant');
    input.click();
}




function openSidebarNavigare(){
    var navigator = $('.navigator_sidebar');
    navigator.css('left', '0');
}

function closeSidebarNavigare(){
    var navigator = $('.navigator_sidebar');
    navigator.css('left', '-500px');
}

function extendMenu(){
    if($('.icon_change_menu_sidebar').hasClass('fa-caret-down')){
        $('.hided_elements').css({'height':'auto', 'margin-top':'10px'});
        $('.icon_change_menu_sidebar').removeClass('fa-caret-down').addClass('fa-caret-up');
    }else{
        $('.hided_elements').css({'height':'0', 'margin-top':'0'});
        $('.icon_change_menu_sidebar').removeClass('fa-caret-up').addClass('fa-caret-down');
    }
}


/*
function adaugaIntervalProgram(){
    var div = $('#add_program_serv');
    var de_adaugat =
        '<div class="box_program">\
                    <div class="row">\
                        <div class="col-md-12">\
                            <span class="label_program">De</span>\
                            <select class="select_program">\
                                <option selected> Luni </option>\
                                <option> Marti </option>\
                                <option> Miercuri </option>\
                                <option> Joi </option>\
                                <option> Vineri </option>\
                                <option> Sambata </option>\
                                <option> Duminica </option>\
                            </select>\
                            <span class="label_program">Pana</span>\
                            <select class="select_program">\
                                <option selected> Luni </option>\
                                <option> Marti </option>\
                                <option> Miercuri </option>\
                                <option> Joi </option>\
                                <option> Vineri </option>\
                                <option> Sambata </option>\
                                <option> Duminica </option>\
                            </select>\
                            <div class="interval_orar_program">\
                                <span class="label_program">De la</span>\
                                <input type="number" class="interval_orar" min="00" max="24" placeholder="00"/>\
                                <span class="points_between">:</span>\
                                <input type="number" class="interval_orar" min="00" max="24" placeholder="00"/>\
                                <span class="label_program"> - Pana la</span>\
                                <input type="number" class="interval_orar" min="00" max="24" placeholder="00"/>\
                                <span class="points_between">:</span>\
                                <input type="number" class="interval_orar" min="00" max="24" placeholder="00"/>\
                                <Br> <Br>\
                                <label class="label_input_checkbox">\
                                    <input type="checkbox">\
                                    <span class="checkmark_checkbox"></span>\
                                </label>\
                                <p class="checkbox_text_black">Inchis</p>\
                            </div>\
                        </div>\
                    </div>\
                </div>'
    ;
    div.append(de_adaugat);
}
*/
/*
function adaugaZiProgram(){
    var div = $('#add_program_serv');
    var de_adaugat =
        '<div class="box_program">\
                    <div class="row">\
                        <div class="col-md-12">\
                            <span class="label_program">Ziua</span>\
                            <select class="select_program">\
                                <option selected> Luni </option>\
                                <option> Marti </option>\
                                <option> Miercuri </option>\
                                <option> Joi </option>\
                                <option> Vineri </option>\
                                <option> Sambata </option>\
                                <option> Duminica </option>\
                            </select>\
                            <div class="interval_orar_program">\
                                <span class="label_program">De la</span>\
                                <input type="number" class="interval_orar" min="00" max="24" placeholder="00"/>\
                                <span class="points_between">:</span>\
                                <input type="number" class="interval_orar" min="00" max="24" placeholder="00"/>\
                                <span class="label_program"> - Pana la</span>\
                                <input type="number" class="interval_orar" min="00" max="24" placeholder="00"/>\
                                <span class="points_between">:</span>\
                                <input type="number" class="interval_orar" min="00" max="24" placeholder="00"/>\
                                <Br> <Br>\
                                <label class="label_input_checkbox">\
                                    <input type="checkbox">\
                                    <span class="checkmark_checkbox"></span>\
                                </label>\
                                <p class="checkbox_text_black">Inchis</p>\
                            </div>\
                        </div>\
                    </div>\
                </div>'
    ;
    div.append(de_adaugat);
}
*/

function openAdaugareCamera(){
    var div = $('.adaugare_camere_sidebar');
    var bg = $('.black_bg_edit_aside2');
    div.css('display', 'block');
    bg.css('display', 'block');
}
function CloseAdaugareCamera(){
    var div = $('.adaugare_camere_sidebar');
    var bg = $('.black_bg_edit_aside2');
    div.css('display', 'none');
    bg.css('display', 'none');
}
function openInfoModalTable(el) {
    var div = $(el).parent('td').children('div');
    if(div.hasClass('showed')){
        div.css('display', 'none');
        div.removeClass('showed');
    }else{
        div.css('display', 'block');
        div.addClass('showed');
    }
}

function showThisProfile(but){
    var butt = $(but);
    var last_active = butt.parent('div').find('.active_but_prof_sh');

    last_active.removeClass('active_but_prof_sh');
    butt.addClass('active_but_prof_sh');
}

$(document).ready(function () {
    /*Functie pentru dimensiunea google maps*/
    var mapa = $('.map_loc')[0];
    var width = $('#mapa_loc').width();
    mapa.style.width = width - 12 + 'px';
});

function showMoreDetailsMenu(ev){
    var div = $(ev).parent('div').parent('div').children('.div_more_info');
    div.removeClass('undisplayed').addClass('displayed');
}
function closeMoreDetailsMenu(ev){
    var div = $(ev).parent('div').parent('div').parent('div');
    div.removeClass('displayed').addClass('undisplayed');
}


function openCloseLogins(bt){
    var buton = $(bt);
    var divs = $('.hide_cards').find('.col-md-3');

    if(buton.hasClass('fa-angle-down')){
        divs.css('max-height', '900px');
        buton.addClass('fa-angle-up').removeClass('fa-angle-down');
    }else if(buton.hasClass('fa-angle-up')){
        divs.css('max-height', '0px');
        buton.addClass('fa-angle-down').removeClass('fa-angle-up');
    }
}













var dateStart=[], dateFinal=[], datePrezent=[], postOcupat=[], numeFirma=[], orasFinal=[], orasFinalNr=[], taraFinal=[], taraFinalNr=[], descriereFinal=[];

function stergeIstoric(id, el){
    var array_id = id - 1;
    var div = $(el).parent('div');
    var adaugare = $('#maiMulteIstoricuriProfesionale');
    var pres;

    dateStart.splice(array_id, 1);
    dateFinal.splice(array_id, 1);
    datePrezent.splice(array_id, 1);
    postOcupat.splice(array_id, 1);
    numeFirma.splice(array_id, 1);
    orasFinal.splice(array_id, 1);
    orasFinalNr.splice(array_id, 1);
    taraFinal.splice(array_id, 1);
    taraFinalNr.splice(array_id, 1);
    descriereFinal.splice(array_id, 1);
    console.log(dateStart + ', '+ dateFinal + ', '+ postOcupat + ', '+numeFirma+', '+ orasFinal+', '+taraFinal+', '+descriereFinal+', '+ orasFinalNr+', '+taraFinalNr+', '+datePrezent);

    adaugare[0].innerHTML = "";

    for(var i=0; i<dateStart.length; i++){
        var d = i+1;
        if(datePrezent[i] == 0){
            pres = dateFinal[i];
        }else{
            pres = "Prezent";
        }
        var de_adaugat =
            '<div class="istoric_profesional" id="ist_'+d+'">\
                <p class="postul_ocupat">Postul ocupat: <b>' + postOcupat[i] + '</b></p>\
                <p class="postul_ocupat">Nume angajator: <b>' + numeFirma[i] + '</b></p>\
                <p class="postul_ocupat">De la: <b>' + dateStart[i] + '</b> - Pana la: <b>' + pres + '</b></p>\
                <p class="postul_ocupat">Oras: <b>' + orasFinal[i] + '</b> &ensp; Tara: <b>' + taraFinal[i] + '</b></p>\
                <p class="postul_ocupat_descriere">' + descriereFinal[i] + '</p>\
                <button class="delete_ist" onclick="stergeIstoric('+d+', this)">Sterge istoric</button>\
            </div>';
        adaugare.append(de_adaugat);
    }
}

function saveHistory(info) {
    var datestart_actual, datefinal_actual, dateprezent_actual, postocupat_actual, numefirma_actual, oras_actual_nr, oras_actual, tara_actual, tara_actual_nr, descriere_actual;
    var adaugare = $('#maiMulteIstoricuriProfesionale');
    var pres;
    var oras = document.getElementById('oras_istoric_pr'), tara = document.getElementById('tara_istoric_pr');

    datestart_actual = $('input[name=date_start]').val();
    if ($('input[name=present_check]')[0].checked == true) {
        dateprezent_actual = 1;
        datefinal_actual = "1753-01-01";
    } else {
        datefinal_actual = $('input[name=date_final]').val();
        dateprezent_actual = 0;
    }
    postocupat_actual = $('input[name=postul_ocupat]').val();
    numefirma_actual = $('input[name=nume_firma]').val();
    oras_actual = oras.options[oras.selectedIndex].text;
    oras_actual_nr = oras.value;
    tara_actual = tara.options[tara.selectedIndex].text;
    tara_actual_nr = tara.value;
    descriere_actual = $('textarea[name=descriere_istoric]').val();

    if ((datestart_actual != null && datestart_actual != '') && (datefinal_actual != null && datefinal_actual != '') && (postocupat_actual != null && postocupat_actual != '') && (numefirma_actual != null && numefirma_actual != '') && (oras_actual != null && oras_actual != '') && (tara_actual != null && tara_actual != '') && (descriere_actual != null && descriere_actual != '')) {
        dateStart.push(datestart_actual);
        dateFinal.push(datefinal_actual);
        datePrezent.push(dateprezent_actual);
        postOcupat.push(postocupat_actual);
        numeFirma.push(numefirma_actual);
        orasFinal.push(oras_actual);
        orasFinalNr.push(oras_actual_nr);
        taraFinal.push(tara_actual);
        taraFinalNr.push(tara_actual_nr);
        descriereFinal.push(descriere_actual);
        var array_length = postOcupat.length;

        if(dateprezent_actual == 0){
            pres = datefinal_actual;
        }else{
            pres = "Prezent";
        }

        var de_adaugat =
            '<div class="istoric_profesional" id="ist_'+array_length+'">\
                <p class="postul_ocupat">Postul ocupat: <b>' + postocupat_actual + '</b></p>\
                <p class="postul_ocupat">Nume angajator: <b>' + numefirma_actual + '</b></p>\
                <p class="postul_ocupat">De la: <b>' + datestart_actual + '</b> - Pana la: <b>' + pres + '</b></p>\
                <p class="postul_ocupat">Oras: <b>' + oras_actual + '</b> &ensp; Tara: <b>' + tara_actual + '</b></p>\
                <p class="postul_ocupat_descriere">' + descriere_actual + '</p>\
                <button class="delete_ist" onclick="stergeIstoric('+array_length+', this)">Sterge istoric</button>\
            </div>';
        adaugare.append(de_adaugat);

        $('input[name=date_start]')[0].value = '';
        $('input[name=date_final]')[0].value = '';
        $('input[name=present_check]').prop('checked', false);
        $('input[name=postul_ocupat]')[0].value = '';
        $('input[name=nume_firma]')[0].value = '';
        oras.value = 1;
        tara.value = 1;
        $('textarea[name=descriere_istoric]')[0].value = "";

        console.log(dateStart + ' | '+ dateFinal + ' | '+ postOcupat + ' | '+numeFirma+' | '+ orasFinal+' | '+taraFinal+' | '+descriereFinal+' | '+datePrezent);
    }
}




//In liniile de cod ce urmeaza mai jos sunt functiile de salvare si creare a vectorilor de elemente pentru istoricurile educative

var dateStartEd=[], dateFinalEd=[], datePrezentEd=[], titlulCertificatului=[], numeInstitutie=[], orasFinalEd=[], orasFinalNrEd=[], taraFinalEd=[], taraFinalNrEd=[], descriereFinalEd=[];

function stergeIstoricEducational(id, el){
    var array_id = id - 1;
    var div = $(el).parent('div');
    var adaugare = $('#maiMulteIstoricuriEducative');
    var pres;

    dateStartEd.splice(array_id, 1);
    dateFinalEd.splice(array_id, 1);
    datePrezentEd.splice(array_id, 1);
    titlulCertificatului.splice(array_id, 1);
    numeInstitutie.splice(array_id, 1);
    orasFinalEd.splice(array_id, 1);
    orasFinalNrEd.splice(array_id, 1);
    taraFinalEd.splice(array_id, 1);
    taraFinalNrEd.splice(array_id, 1);
    descriereFinalEd.splice(array_id, 1);
    console.log(dateStartEd + ', '+ dateFinalEd + ', '+ titlulCertificatului + ', '+numeInstitutie+', '+ orasFinalEd+', '+taraFinalEd+', '+descriereFinalEd+', '+datePrezentEd+', '+ orasFinalNrEd+', '+taraFinalNrEd);

    adaugare[0].innerHTML = "";

    for(var i=0; i<dateStartEd.length; i++){
        var d = i+1;
        if(datePrezentEd[i] == 0){
            pres = dateFinalEd[i];
        }else{
            pres = "Prezent";
        }
        var de_adaugat =
            '<div class="istoric_profesional" id="edc_'+d+'">\
                <p class="postul_ocupat">Titlul certificatului: <b>' + titlulCertificatului[i] + '</b></p>\
                <p class="postul_ocupat">Institutie emitatoare: <b>' + numeInstitutie[i] + '</b></p>\
                <p class="postul_ocupat">De la: <b>' + dateStartEd[i] + '</b> - Pana la: <b>' + pres + '</b></p>\
                <p class="postul_ocupat">Oras: <b>' + orasFinalEd[i] + '</b> &ensp; Tara: <b>' + taraFinalEd[i] + '</b></p>\
                <p class="postul_ocupat_descriere">' + descriereFinalEd[i] + '</p>\
                <button class="delete_ist" onclick="stergeIstoricEducational('+d+', this)">Sterge istoric</button>\
            </div>';
        adaugare.append(de_adaugat);
    }
}

function saveHistoryEducation(info) {
    var datestart_actual, datefinal_actual, dateprezent_actual, titlul_certificatului, numeinstitutie_actual, oras_actual_nr, oras_actual, tara_actual, tara_actual_nr, descriere_actual;
    var adaugare = $('#maiMulteIstoricuriEducative');
    var pres;
    var oras = document.getElementById('orasPrimireEducatie'), tara = document.getElementById('taraPrimireEducatie');

    datestart_actual = $('input[name=startDateEducatie]').val();
    if ($('input[name=prezentDateEducatie]')[0].checked == true) {
        dateprezent_actual = 1;
        datefinal_actual = "1753-01-01";
    } else {
        datefinal_actual = $('input[name=finalDateEducatie]').val();
        dateprezent_actual = 0;
    }
    titlul_certificatului = $('input[name=certificateTitle]').val();
    numeinstitutie_actual = $('input[name=institutionName]').val();
    oras_actual = oras.options[oras.selectedIndex].text;
    oras_actual_nr = oras.value;
    tara_actual = tara.options[tara.selectedIndex].text;
    tara_actual_nr = tara.value;
    descriere_actual = $('textarea[name=descriereEducatie]').val();

    if ((datestart_actual != null && datestart_actual != '') && (datefinal_actual != null && datefinal_actual != '') && (titlul_certificatului != null && titlul_certificatului != '') && (numeinstitutie_actual != null && numeinstitutie_actual != '') && (oras_actual != null && oras_actual != '') && (tara_actual != null && tara_actual != '') && (descriere_actual != null && descriere_actual != '')) {
        dateStartEd.push(datestart_actual);
        dateFinalEd.push(datefinal_actual);
        datePrezentEd.push(dateprezent_actual);
        titlulCertificatului.push(titlul_certificatului);
        numeInstitutie.push(numeinstitutie_actual);
        orasFinalEd.push(oras_actual);
        orasFinalNrEd.push(oras_actual_nr);
        taraFinalEd.push(tara_actual);
        taraFinalNrEd.push(tara_actual_nr);
        descriereFinalEd.push(descriere_actual);
        var array_length = titlulCertificatului.length;

        if(dateprezent_actual == 0){
            pres = datefinal_actual;
        }else{
            pres = "Prezent";
        }

        var de_adaugat =
            '<div class="istoric_profesional" id="edc_'+array_length+'">\
                <p class="postul_ocupat">Titlul certificatului: <b>' + titlul_certificatului + '</b></p>\
                <p class="postul_ocupat">Institutie emitatoare: <b>' + numeinstitutie_actual + '</b></p>\
                <p class="postul_ocupat">De la: <b>' + datestart_actual + '</b> - Pana la: <b>' + pres + '</b></p>\
                <p class="postul_ocupat">Oras: <b>' + oras_actual + '</b> &ensp; Tara: <b>' + tara_actual + '</b></p>\
                <p class="postul_ocupat_descriere">' + descriere_actual + '</p>\
                <button class="delete_ist" onclick="stergeIstoricEducational('+array_length+', this)">Sterge istoric</button>\
            </div>';
        adaugare.append(de_adaugat);

        $('input[name=startDateEducatie]')[0].value = '';
        $('input[name=finalDateEducatie]')[0].value = '';
        $('input[name=prezentDateEducatie]').prop('checked', false);
        $('input[name=certificateTitle]')[0].value = '';
        $('input[name=institutionName]')[0].value = '';
        oras.value = 1;
        tara.value = 1;
        $('textarea[name=descriereEducatie]')[0].value = "";

        console.log(dateStartEd + ' | '+ dateFinalEd + ' | '+ titlulCertificatului + ' | '+numeInstitutie+' | '+ orasFinalEd+' | '+taraFinalEd+' | '+descriereFinalEd+' | '+datePrezentEd+' | '+orasFinalNrEd+' | '+taraFinalNrEd);
    }
}

var contract_type_cerere;
function changeContractValue(ed){
    if($(ed)[0].checked == true){
        contract_type_cerere = $(ed)[0].value;
        console.log(contract_type_cerere);
    }
}


function deleteCerere(elem){
    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');
    $.get(link_request+'/api/professionist/work_request/'+ elem +'/delete?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
        function(response){
            location.reload();
        });
}



//Functie de afisare a butonului de adaugare imagine pentru o postare
function showImageAreaPost(){
    var div = $('.adaugare_imagini_post');
    if(div[0].classList.contains('unshowed')){
        div.css('display', 'block');
        div.removeClass('unshowed').addClass('showed');
    }else{
        div.css('display', 'none');
        div.removeClass('showed').addClass('unshowed');
    }
}



//Functie de afisare a campului de a da check-in pentru o postare
function showCheckInAreaPost(){
    var div = $('.display_not_check_in');
    if(div[0].classList.contains('unshowed')){
        div.css('display', 'block');
        div.removeClass('unshowed').addClass('showed');
    }else{
        div.css('display', 'none');
        div.removeClass('showed').addClass('unshowed');
    }
}




//Functie de deschidere a procesului de selectare imagine
function triggerInputFilePostProf() {
    var input = $('#upload_photo_post');
    input.click();
}




//Functie de stergere a unei imagini incarcate pentru o postare
//Initializarea unui vector in care sa fie stocate imaginile pentru a putea fi trimise
var vector_imagini_postare = [];
function deleteImagePostPending(id){

    //Stergerea imaginii din vector
    vector_imagini_postare.splice(id, 1);
    $('.images_added')[0].innerHTML = '';

    //Re-generarea imaginilor pentru postare dupa stergere in front-end
    for(var i=0; i<vector_imagini_postare.length; i++){
        var de_adaugat =
            "<div style='position:relative'>"+
            "<i class='far fa-times-circle delete_uploaded_image_restaurant' onclick='deleteImagePostPending("+ i +")'></i>"+
            "<img src='"+ vector_imagini_postare[i] +"' class='img_uploaded_restaurant' />"+
            "</div>";
        $('.images_added').append(de_adaugat);
    }
}




//Functie pentru deschiderea modalului unei postari la restaurant/locatie restaurant
function openModalPostare(chld){
    //Preluarea variabilelor din local storage
    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');
    var restaurant_profile_id_storage = localStorage.getItem('restaurant_profile_id');
    var id = $(chld).parent('div').parent('div')[0].id;
    var post_id = id.substring(7);

    //Verificare daca este vorba despre o locatie a unui restaurant sau despre un restaurant ca entitate
    var restaurant_location_id_storage;
    if(localStorage.getItem('restaurant_location_id') != null){
        restaurant_location_id_storage = localStorage.getItem('restaurant_location_id');
    }else{
        restaurant_location_id_storage = null;
    }

    //Utilizarea rutei corespunzatoare unei locatii de restaurant sau unui restaurant ca entitate
    if(localStorage.getItem('restaurant_location_id') != null){
        $.get(link_request+'/api/restaurant/location/'+ restaurant_location_id_storage +'/read?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response) {
                var raspuns = JSON.parse(response);
                var link_access = raspuns.data.restaurant_location;
                var carousel = $('.carouselInnerPostare');
                var div_img = $('.single_image_modal_div')[0];
                for(var i = 0; i<link_access.posts_from.length; i++){
                    if(link_access.posts_from[i].id == post_id){
                        //Verificare daca este vorba de mai multe imagini (pentru a crea un carusel) sau doar de o imagine
                        if(link_access.posts_from[i].post_pictures.length > 1){
                            $('.carousel_verification').css('display', 'inline-block');
                            $('.normal_image_verification').css('display', 'none');
                            var de_adaugat;

                            de_adaugat =
                                "<div class='carousel-item carouseItemPostare active'>"+
                                "<div class='outer_image_modal'>"+
                                "<div class='middle_image_modal'>"+
                                "<div class='inner_image_modal'>"+
                                "<img src='"+ link_access.posts_from[i].post_pictures[0].picture +"' class='big_image_modal' />"+
                                "</div>"+
                                "</div>"+
                                "</div>"+
                                "</div>";
                            carousel.append(de_adaugat);

                            for(var j=1; j<link_access.posts_from[i].post_pictures.length; j++){
                                de_adaugat =
                                    "<div class='carousel-item carouseItemPostare'>"+
                                    "<div class='outer_image_modal'>"+
                                    "<div class='middle_image_modal'>"+
                                    "<div class='inner_image_modal'>"+
                                    "<img src='"+ link_access.posts_from[i].post_pictures[j].picture +"' class='big_image_modal' />"+
                                    "</div>"+
                                    "</div>"+
                                    "</div>"+
                                    "</div>";
                                carousel.append(de_adaugat);
                            }
                        }else{
                            $('.carousel_verification').css('display', 'none');
                            $('.normal_image_verification').css('display', 'inline-block');
                            div_img.innerHTML = "<img src='"+ link_access.posts_from[i].post_pictures[0].picture +"' class='big_image_modal' />";
                        }
                    }
                }
            });
    }else{
        $.get(link_request+'/api/restaurant/'+ restaurant_profile_id_storage +'/read?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response) {
                var raspuns = JSON.parse(response);
                var link_access = raspuns.data;
                var carousel = $('.carouselInnerPostare');
                var div_img = $('.single_image_modal_div')[0];
                for(var i = 0; i<link_access.posts_from.length; i++){
                    if(link_access.posts_from[i].id == post_id){
                        //Verificare daca este vorba de mai multe imagini (pentru a crea un carusel) sau doar de o imagine
                        if(link_access.posts_from[i].post_pictures.length > 1){
                            $('.carousel_verification').css('display', 'inline-block');
                            $('.normal_image_verification').css('display', 'none');
                            var de_adaugat;

                            de_adaugat =
                                "<div class='carousel-item carouseItemPostare active'>"+
                                "<div class='outer_image_modal'>"+
                                "<div class='middle_image_modal'>"+
                                "<div class='inner_image_modal'>"+
                                "<img src='"+ link_access.posts_from[i].post_pictures[0].picture +"' class='big_image_modal' />"+
                                "</div>"+
                                "</div>"+
                                "</div>"+
                                "</div>";
                            carousel.append(de_adaugat);

                            for(var j=1; j<link_access.posts_from[i].post_pictures.length; j++){
                                de_adaugat =
                                    "<div class='carousel-item carouseItemPostare'>"+
                                    "<div class='outer_image_modal'>"+
                                    "<div class='middle_image_modal'>"+
                                    "<div class='inner_image_modal'>"+
                                    "<img src='"+ link_access.posts_from[i].post_pictures[j].picture +"' class='big_image_modal' />"+
                                    "</div>"+
                                    "</div>"+
                                    "</div>"+
                                    "</div>";
                                carousel.append(de_adaugat);
                            }
                        }else{
                            $('.carousel_verification').css('display', 'none');
                            $('.normal_image_verification').css('display', 'inline-block');
                            div_img.innerHTML = "<img src='"+ link_access.posts_from[i].post_pictures[0].picture +"' class='big_image_modal' />";
                        }
                    }
                }
            });
    }


    var descriere = $(chld).parent('div').children('div').find('.descriere_post')[0].innerHTML;
    $('#descriere_postare_modal')[0].innerHTML = descriere;
    $('#modalPostare').modal();
}




//Functie de stergere a unei postari la restaurant/locatie restaurant
function deletePost(post){

    //Preluare variabile din local storage pentru rute
    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');

    //Verificare daca este vorba de o locatie de restaurant sau de un restaurant ca si entitate
    var restaurant_location_id_storage;
    if(localStorage.getItem('restaurant_location_id') != null){
        restaurant_location_id_storage = localStorage.getItem('restaurant_location_id');
    }else{
        restaurant_location_id_storage = null;
    }

    //Stabilirea id-ului postarii ce urmeaza a fi sterse
    var id = $(post).parent('li').parent('ul').parent('div').parent('div').parent('header').parent('div').parent('div').parent('div')[0].id;
    var post_id = id.substring(7);

    //Utilizarea rutei corespunzatoare pentru stergerea postarii de:
    //1-Locatie restaurant
    //2-Restaurant ca si entitate
    if(restaurant_location_id_storage!=null){
        $.get(link_request+'/api/restaurant/location/'+ restaurant_location_id_storage +'/post/'+ post_id +'/delete?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response){
                location.reload();
            });
    }else{
        $.get(link_request+'/api/restaurant/post/'+ post_id +'/delete?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response){
                location.reload();
            });
    }
}




//Functie de trigger pentru inchidere a modalului de editare a unei postari
//Aceasta functie curata modalul de editare de elementele adaugate si nesalvate pentru a putea edita o alta postare si a nu ocupa spatiu inutil
$(document).ready(function () {
    $('#modalPostare').on('hidden.bs.modal', function () {
        var img_div = $('#modalPostare').find('.normal_image_verification').children('div').children('div').children('div');
        var car = $('#modalPostare').find('.carousel_verification').children('.carouselInnerPostare');
        if($('#modalPostare').find('.normal_image_verification')[0].style.display != 'none'){
            var img = img_div.children('img');
            img_div[0].removeChild(img[0]);
            $('#modalPostare').find('.normal_image_verification').css('display', 'none');
        }else if($('#modalPostare').find('.carousel_verification')[0].style.display != 'none'){
            while (car.children('div')[0]) {
                var first_child = car.children('div');
                car[0].removeChild(first_child[0]);
            }
            $('#modalPostare').find('.carousel_verification').css('display', 'none');
        }
    });
});





//Functie de stergere a unei imagini a unei postari din modul de editare
function deleteImagePostEditing(id){
    //Preluarea variabilelor pentru rute din local storage
    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');

    //Verificare daca este vorba de un restaurant ca entitate sau de o locatie de restaurant
    var restaurant_location_id_storage;
    if(localStorage.getItem('restaurant_location_id') != null){
        restaurant_location_id_storage = localStorage.getItem('restaurant_location_id');
    }else{
        restaurant_location_id_storage = null;
    }

    //Stergerea imaginii din vectorul de imagini puse spre editare
    //Stergerea vizuala a imaginii din front-end
    var id_image = vector_imagini_postare_editare[id][1];
    vector_imagini_postare_editare.splice(id, 1);
    $('#photos_edit_post')[0].innerHTML = '';
    for(var i=0; i<vector_imagini_postare_editare.length; i++){
        var de_adaugat =
            "<div class='col-md-6' style='position:relative'>"+
            "<i class='far fa-times-circle delete_uploaded_image_restaurant' onclick='deleteImagePostEditing("+ i +")'></i>"+
        "<img src='"+ vector_imagini_postare_editare[i][0] +"' class='img_uploaded_restaurant' />"+
        "</div>";
        $('#photos_edit_post').append(de_adaugat);
    }


    //Utilizarea rutei corespunzatoare pentru stergerea postarii de:
    //1-Locatie restaurant
    //2-Restaurant ca entitate
    if(restaurant_location_id_storage!=null){
        $.get(link_request+'/api/restaurant/location/'+restaurant_location_id_storage+'/post/'+post_id_global+'/picture/'+id_image+'/delete' +'?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response){
                var raspuns = JSON.parse(response);
                // console.log(raspuns);
            });
    }else{
        $.get(link_request+'/api/restaurant/post/'+post_id_global+'/picture/'+id_image+'/delete' +'?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response){
                var raspuns = JSON.parse(response);
                // console.log(raspuns);
            });
    }
}






//Functie de editare a unei postari de la restaurant/locatie restaurant
//Initializarea unui vector pentru stocarea imaginilor din postare pentru a putea fi sterse
//Initializarea unei variabile globale de id postare pentru a putea fi salvate editarile in index
var vector_imagini_postare_editare = [], post_id_global;
function editPost(chld){

    //Setare a variabilelor din local storage pentru rute
    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');
    var restaurant_profile_id_storage = localStorage.getItem('restaurant_profile_id');

    //Verificare daca este vorba de o locatie de restaurant sau de un restaurant ca entitate
    var restaurant_location_id_storage;
    if(localStorage.getItem('restaurant_location_id') != null){
        restaurant_location_id_storage = localStorage.getItem('restaurant_location_id');
    }else{
        restaurant_location_id_storage = null;
    }

    //Preluarile pentru id-ul postarii, descrierea actuala a postarii, afisarea modalului si initializarea vectorului si a spatiului de generare imagini
    var id = $(chld).parent('li').parent('ul').parent('div').parent('div').parent('header').parent('div').parent('div').parent('div')[0].id;
    var post_id = id.substring(7);
    post_id_global = post_id;
    vector_imagini_postare_editare = [];
    var descriere = $(chld).parent('li').parent('ul').parent('div').parent('div').parent('header').parent('div').find('.descriere_post')[0].innerHTML;
    $('.edit_post_textarea').val(descriere);
    $('#modalEditarePostare').modal();
    $('#photos_edit_post')[0].innerHTML = '';

    //utilizarea rutei corespunzatoare in functie de restaurant/locatie restaurant
    //1-locatie restaurant
    //2-restaurant ca entitate
    if(restaurant_location_id_storage !=null){
        $.get(link_request+'/api/restaurant/location/'+ restaurant_location_id_storage +'/read?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response) {
                var raspuns = JSON.parse(response);
                var link_access = raspuns.data.restaurant_location;

                //generearea imaginilor puse spre editare in modal
                for(var i = 0; i<link_access.posts_from.length; i++){
                    if(link_access.posts_from[i].id == post_id){
                        for(var j=0; j<link_access.posts_from[i].post_pictures.length; j++){
                            vector_imagini_postare_editare.push([link_access.posts_from[i].post_pictures[j].picture, link_access.posts_from[i].post_pictures[j].id]);
                            var de_adaugat =
                                "<div class='col-md-6' style='position:relative'>"+
                                "<i class='far fa-times-circle delete_uploaded_image_restaurant' onclick='deleteImagePostEditing("+ j +")'></i>"+
                                "<img src='"+ link_access.posts_from[i].post_pictures[j].picture +"' class='img_uploaded_restaurant' />"+
                                "</div>";
                            $('#photos_edit_post').append(de_adaugat);
                        }
                    }
                }
            });
    }else{
        $.get(link_request+'/api/restaurant/'+ restaurant_profile_id_storage +'/read?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response) {
                var raspuns = JSON.parse(response);
                var link_access = raspuns.data;

                //generearea imaginilor puse spre editare in modal
                for(var i = 0; i<link_access.posts_from.length; i++){
                    if(link_access.posts_from[i].id == post_id){
                        for(var j=0; j<link_access.posts_from[i].post_pictures.length; j++){
                            vector_imagini_postare_editare.push([link_access.posts_from[i].post_pictures[j].picture, link_access.posts_from[i].post_pictures[j].id]);
                            var de_adaugat =
                                "<div class='col-md-6' style='position:relative'>"+
                                "<i class='far fa-times-circle delete_uploaded_image_restaurant' onclick='deleteImagePostEditing("+ j +")'></i>"+
                                "<img src='"+ link_access.posts_from[i].post_pictures[j].picture +"' class='img_uploaded_restaurant' />"+
                                "</div>";
                            $('#photos_edit_post').append(de_adaugat);
                        }
                    }
                }
            });
    }
}






var latitudine, longitudine;
function initMap() {
    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');
    var restaurant_location_id_storage = localStorage.getItem('restaurant_location_id');


    $.get(link_request+'/api/restaurant/location/'+ restaurant_location_id_storage +'/read?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
    function(response){
        var raspuns = JSON.parse(response);
        var link_access_rest_info = raspuns.data.restaurant_location;

        if((link_access_rest_info.latitude != null) && (link_access_rest_info.longitude != null)){
            var latt = parseFloat(link_access_rest_info.latitude);
            var lngg = parseFloat(link_access_rest_info.longitude);
        }else{
            var latt = -25.363;
            var lngg = 131.044;
        }

        var location = {lat: latt, lng: lngg};
        var map = new google.maps.Map(
            document.getElementById('map_editare'), {zoom: 12, center: location});
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function (evt) {
            latitudine= evt.latLng.lat();
            longitudine =evt.latLng.lng();
        });

        map.setCenter(marker.position);
        marker.setMap(map);
    });
}

//Functie de adaugare a facilitatilor locatiei de restaurant
var vector_facilitati = [];
function chooseFacility(id){
    var select_option = $(id)[0].options[$(id)[0].selectedIndex].value;
    var id_option = 0;
    var nume_facilitate = $(id)[0].options[$(id)[0].selectedIndex].text;
    var div = $(id).parent('div').children('.append_alternatives_here');

    if(select_option != 'default'){
        id_option = select_option.substring(8);
        $(id)[0].options[$(id)[0].selectedIndex].disabled = true;
        id_option = parseInt(id_option);
        vector_facilitati.push(id_option);

        div.append(
            "<div class='alternative' id='id_facilitate_selectata"+id_option+"'>" + nume_facilitate + " <i class='fas fa-times' onclick='deleteSelectedFacility(this)'></i></div>"
        );
    }
}
//Functie de stergere a facilitatii locatiei de restaurant
function deleteSelectedFacility(deleted) {

    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');
    var restaurant_location_id_storage = localStorage.getItem('restaurant_location_id');

    var to_delete = $(deleted).parent('div');
    var id = parseInt(to_delete[0].id.substring(19));
    for(var i=0; i<vector_facilitati.length; i++){
        if(vector_facilitati[i]==id){
            vector_facilitati.splice(i, 1);
            $('#select_facilitate')[0].options[id].disabled = false;
        }
    }
    to_delete.remove();

    $.get(link_request+'/api/restaurant/location/'+restaurant_location_id_storage+'/facility/'+id+'/delete' +'?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
        function(response){
            var raspuns = JSON.parse(response);
            console.log(raspuns);
        });
}

//Functie de adaugare a tipurilor de muzica locatiei de restaurant
var vector_tipuri_muzica = [];
function chooseMusicType(id){
    var select_option = $(id)[0].options[$(id)[0].selectedIndex].value;
    var id_option = 0;
    var nume_muzica = $(id)[0].options[$(id)[0].selectedIndex].text;
    var div = $(id).parent('div').children('.append_alternatives_here');

    if(select_option != 'default'){
        id_option = select_option.substring(5);
        $(id)[0].options[$(id)[0].selectedIndex].disabled = true;
        id_option = parseInt(id_option);
        vector_tipuri_muzica.push(id_option);

        div.append(
            "<div class='alternative' id='id_muzica_selectata"+id_option+"'>" + nume_muzica + " <i class='fas fa-times' onclick='deleteSelectedMusicType(this)'></i></div>"
        );
    }
    console.log(vector_tipuri_muzica);
}
//Functie de stergere a tipurilor de muzica ale locatiei de restaurant
function deleteSelectedMusicType(deleted) {
    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');
    var restaurant_location_id_storage = localStorage.getItem('restaurant_location_id');

    var to_delete = $(deleted).parent('div');
    var id = parseInt(to_delete[0].id.substring(19));
    for(var i=0; i<vector_tipuri_muzica.length; i++){
        if(vector_tipuri_muzica[i]==id){
            vector_tipuri_muzica.splice(i, 1);
            $('#select_tip_muzica')[0].options[id].disabled = false;
        }
    }
    to_delete.remove();

    $.get(link_request+'/api/restaurant/location/'+restaurant_location_id_storage+'/music_type/'+id+'/delete' +'?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
        function(response){
            var raspuns = JSON.parse(response);
            console.log(raspuns);
        });

    console.log(vector_tipuri_muzica);
}




//Functie pentru stergere imagini care NU SUNT PRELUATE DIN DATABASE, aceste imagini sunt incarcate in sesiunea actuala
//Initiliazarea unui vector pentru a stoca imaginile incarcate actual
var vector_imagini_locatie = [];
function deleteImageLocatieRestaurant(id){

    //Stergerea imaginii respective din vector
    vector_imagini_locatie.splice(id, 1);
    $('#image_locatie')[0].innerHTML = '';

    //Re-afisarea imaginilor PRELUATE din database in front-end
    for(var i=0; i<vectorImaginiRestaurantPreluate.length; i++){
        var de_adaugat =
            "<div class='col-md-4'>"+
            "<i class='far fa-times-circle delete_uploaded_image_restaurant' onclick='deleteImageLocatieRestaurantPreluata("+ i +")'></i>"+
            "<img src='"+ vectorImaginiRestaurantPreluate[i][0] +"' class='img_uploaded_restaurant' />"+
            "</div>";
        $('#image_locatie').append(de_adaugat);
    }

    //Re-afisarea imaginilor incarcate actual in sesiune in front-end dupa stergere
    for(var i=0; i<vector_imagini_locatie.length; i++){
        var de_adaugat =
            "<div class='col-md-4'>"+
            "<i class='far fa-times-circle delete_uploaded_image_restaurant' onclick='deleteImageLocatieRestaurant("+ i +")'></i>"+
            "<img src='"+ vector_imagini_locatie[i] +"' class='img_uploaded_restaurant' />"+
            "</div>";
        $('#image_locatie').append(de_adaugat);
    }
}




//Functie de stergere a imaginilor unui restaurant/locatie restaurant PRELUATE DIN DATABASE
//Initializarea unui vector pentru a stoca imaginile preluate din database
var vectorImaginiRestaurantPreluate = [];
function deleteImageLocatieRestaurantPreluata(id){

    //Preluarea variabilelor din localstorage pentru a fi utilizate la rute
    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');

    //Verificare daca este vorba despre un restaurant ca si entitate sau despre o locatie de restaurant
    var restaurant_location_id_storage;
    if(localStorage.getItem('restaurant_location_id') != null){
        restaurant_location_id_storage = localStorage.getItem('restaurant_location_id');
    }else{
        restaurant_location_id_storage = null;
    }


    //Stergerea imaginii din vectorul de imagini
    var id_image = vectorImaginiRestaurantPreluate[id][1];
    vectorImaginiRestaurantPreluate.splice(id, 1);

    $('#image_locatie')[0].innerHTML = '';

    //Re-afisarea imaginilor in front-end dupa stergere
    for(var i=0; i<vectorImaginiRestaurantPreluate.length; i++){
        var de_adaugat =
            "<div class='col-md-4'>"+
            "<i class='far fa-times-circle delete_uploaded_image_restaurant' onclick='deleteImageLocatieRestaurantPreluata("+ i +")'></i>"+
            "<img src='"+ vectorImaginiRestaurantPreluate[i][0] +"' class='img_uploaded_restaurant' />"+
            "</div>";
        $('#image_locatie').append(de_adaugat);
    }

    //Re-afisarea imaginilor in front-end, dar afisam imaginile adaugate noi, nu cele preluate din db
    //ATENTIE: se foloseste o alta functie de stergere, cea pentru imagini care nu sunt preluate din db
    for(var i=0; i<vector_imagini_locatie.length; i++){
        var de_adaugat =
            "<div class='col-md-4'>"+
            "<i class='far fa-times-circle delete_uploaded_image_restaurant' onclick='deleteImageLocatieRestaurant("+ i +")'></i>"+
            "<img src='"+ vector_imagini_locatie[i] +"' class='img_uploaded_restaurant' />"+
            "</div>";
        $('#image_locatie').append(de_adaugat);
    }

    //Utilizarea rutei corespunzatoare pentru stergere a unei imagini PRELUATE de:
    //1-Locatie restaurant
    //2-Restaurant ca si entitate
    if(restaurant_location_id_storage != null){
        $.get(link_request+'/api/restaurant/location/'+restaurant_location_id_storage+'/picture/'+id_image+'/delete' +'?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response){
                var raspuns = JSON.parse(response);
                //console.log(raspuns);
            });
    }else{
        $.get(link_request+'/api/restaurant/picture/'+id_image+'/delete' +'?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
            function(response){
                var raspuns = JSON.parse(response);
                //console.log(raspuns);
            });
    }
}




//Functie de redirectionare a utilizatorului din pagina de administrare a restaurantului in pagina de administrare a locatiei de restaurant, setand variabila id a locatiei corespunzatoare in local storage
function goToLocation(ev){
    var id_locatie = $(ev)[0].id.substring(7);
    localStorage.setItem('restaurant_location_id', id_locatie);
    window.location.href = "../restaurant/index_locatie.html";
}



//Functie pentru stergerea unui program de la locatie restaurant
function deleteProgram(id_get){
    //Preluarea variabilelor din localstorage pentru a fi utilizate la rute
    var id_storage = localStorage.getItem('id');
    var user_id_storage = localStorage.getItem('user_id');
    var token_storage = localStorage.getItem('token');
    var user_agent_storage = localStorage.getItem('user_agent');

    //Verificare daca este vorba despre un restaurant ca si entitate sau despre o locatie de restaurant
    var restaurant_location_id_storage;
    if(localStorage.getItem('restaurant_location_id') != null){
        restaurant_location_id_storage = localStorage.getItem('restaurant_location_id');
    }else{
        restaurant_location_id_storage = null;
    }

    //preluarea id-ului
    var id = parseInt($(id_get).parent('div')[0].id.substring(15));

    //stergerea din fe
    $(id_get).parent('div').remove();

    //request delete
    $.get(link_request+'/api/restaurant/location/'+ restaurant_location_id_storage +'/program/'+id+'/delete?user_id='+user_id_storage+"&user_agent="+user_agent_storage+"&token="+token_storage,
        function(response){

        });
}





