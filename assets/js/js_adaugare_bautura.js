var drink =
    {
        "name": null,
        "categories": [],
        "pictures": []
    };
var valute = ['RON', 'EUR', 'USD'];
var gramaje = ['g', 'kg', 'ml', 'l'];

var id_categorie_bautura = 0, id_subcategorie_bautura = 0, id_bautura = 0;



//Functie de schimbare afisare intre mancare si bautura
function showAdaugaBautura(){
    var sectiune_mancare = $('#adauga_mancare');
    var sectiune_bautura = $('#adauga_bautura');
    var buton_activ = $('.buton_activ_meniu');
    var buton_inactiv = $('.buton_inactiv_meniu');

    sectiune_mancare.css('display', 'none');
    sectiune_bautura.css('display', 'block');
    buton_inactiv.addClass('buton_activ_meniu').removeClass('buton_inactiv_meniu');
    buton_activ.addClass('buton_inactiv_meniu').removeClass('buton_activ_meniu');
}


//Functii pentru categorii
function adaugaCategorieBautura(here){
    var aici_se_adauga = $('.div_pentru_categorii_bautura');

    id_categorie_bautura = parseInt($(here).parent('div').find('.div_pentru_categorii_bautura')[0].childElementCount);

    aici_se_adauga.append(
        '<div class="parinte_categorie_adaugata" id="categorie_bautura'+id_categorie_bautura+'">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Bere" /> \
                        <p class="save_add" onclick="saveCategorieBautura(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>\
                        <button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );
}

function deleteCategorieBautura(val){
    var to_delete = $(val).parent('div').parent('div');
    var id = parseInt(to_delete[0].id.substring(17));
    drink.categories.splice(id, 1);

    $('.div_pentru_categorii_bautura')[0].innerHTML = '';

    for(var i=0; i<drink.categories.length; i++){
        var aici_se_adauga = $('.div_pentru_categorii_bautura');
        var type;

        id_categorie_bautura = parseInt($('.div_pentru_categorii_bautura')[0].childElementCount);

        if(drink.categories[i].categories.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';
        }else if(drink.categories[i].components.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>';
        }else{
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button><button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>';
        }

        aici_se_adauga.append(
            '<div class="parinte_categorie_adaugata" id="categorie_bautura'+id_categorie_bautura+'">\
                    <div class="categorie_adaugata">\
                        <span>'+ drink.categories[i].name +'</span>\
                        <i class="far fa-trash-alt" onclick="deleteCategorieBautura(this)"></i>\
                        <i class="fas fa-pencil-alt" onclick="editCategorieBautura(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">'+type+'\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                    </div>'
        );

        if(drink.categories[i].components.length > 0){
            var for_bautura = $("#categorie_bautura"+id_categorie_bautura).find('.subcategorii_si_meniuri_adaugate');

            id_bautura = parseInt(for_bautura[0].childElementCount);

            for(var x=0; x<drink.categories[i].components.length; x++){
                for_bautura.append(
                    '<div class="elemente_subcategorie" id="bautura_adaugata_categorie_subcategorie'+id_bautura+'">\
                            <div class="categorie_adaugata">\
                                   <span class="nume_meniu">'+drink.categories[i].components[x].name+'</span> (<span class="gramaj_meniu">'+drink.categories[i].components[x].amount +'ml</span>) - <span class="pret_meniu">'+drink.categories[i].components[x].price+'</span> <span class="valuta_meniu">' +valute[drink.categories[i].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteDishBautura(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualBautura(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                        </div>'
                );
            }
        }else{
            var for_subcategorii = $("#categorie_bautura"+id_categorie_bautura).find('.subcategorii_si_meniuri_adaugate');
            for(var j=0; j<drink.categories[i].categories.length; j++){

                id_subcategorie_bautura = parseInt(for_subcategorii[0].childElementCount);

                for_subcategorii.append(
                    '<div class="elemente_subcategorie" id="subcategorie_bautura'+id_subcategorie_bautura+'">\
                    <div class="categorie_adaugata">\
                       <span>'+ drink.categories[i].categories[j].name +'</span>\
                       <i class="far fa-trash-alt" onclick="deleteSubCategorieBautura(this)"></i>\
                       <i class="fas fa-pencil-alt" onclick="editSubCategorieBautura(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                </div>'
                );

                var for_bautura = for_subcategorii.find("#subcategorie_bautura"+id_subcategorie_bautura).find('.subcategorii_si_meniuri_adaugate');

                for(var x=0; x<drink.categories[i].categories[j].components.length; x++){

                    id_bautura = parseInt(for_bautura[0].childElementCount);

                    for_bautura.append(
                        '<div class="elemente_subcategorie" id="bautura_adaugata_categorie_subcategorie'+id_bautura+'">\
                            <div class="categorie_adaugata">\
                                   <span class="nume_meniu">'+drink.categories[i].categories[j].components[x].name+'</span> (<span class="gramaj_meniu">'+drink.categories[i].categories[j].components[x].amount +'ml</span>) - <span class="pret_meniu">'+drink.categories[i].categories[j].components[x].price+'</span> <span class="valuta_meniu">' +valute[drink.categories[i].categories[j].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteDishBautura(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualBautura(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                        </div>'
                    );
                }
            }
        }
    }

    console.log(drink);
}

function saveCategorieBautura(val){
    var element_parent = $(val).parent('div');
    var element_id = parseInt($(val).parent('div').parent('div')[0].id.substring(17));
    var valoare = element_parent.children('input').val();

    if(valoare == '' || valoare == ' '){
        element_parent.parent('div').remove();
    }else{
        element_parent[0].innerHTML =
            '<span>'+ valoare +'</span>\
        <i class="far fa-trash-alt" onclick="deleteCategorieBautura(this)"></i>\
        <i class="fas fa-pencil-alt" onclick="editCategorieBautura(this)"></i>';

        drink.categories[element_id] =
            {
                "name" : valoare,
                categories : [],
                components : []
            };
        console.log(drink);
        id_bautura = 0;
    }
}

function editCategorieBautura(element){
    var element_parent = $(element).parent('div');
    var element_value = element_parent.children('span').html();
    console.log(element_parent);

    element_parent[0].innerHTML =
        '<input type="text" class="input_interior_div" placeholder="Ex: Bere" value="'+
        element_value
        +'"/>'+
        '<p class="save_add" onclick="saveCategorieBautura(this)">SAVE</p>';
}


//Functii pentru subcategorii
function adaugaSubcategorieBautura(cat){
    var change = $(cat).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');

    id_categorie_bautura = parseInt($(cat).parent('div').parent('div')[0].id.substring(17));
    id_subcategorie_bautura = parseInt($(cat).parent('div').parent('div').find('.subcategorii_si_meniuri_adaugate')[0].childElementCount);

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie" id="subcategorie_bautura'+id_subcategorie_bautura+'">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Bere bruna" /> \
                        <p class="save_add" onclick="saveSubCategorieBautura(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );
}

function saveSubCategorieBautura(val){
    var element_parent = $(val).parent('div');
    //var element_id = parseInt($(val).parent('div').parent('div')[0].id.substring(17));
    var valoare = element_parent.children('input').val();

    if(valoare == '' || valoare == ' '){
        element_parent.parent('div').remove();
    }else{
        element_parent[0].innerHTML =
            '<span>'+ valoare +'</span>\
        <i class="far fa-trash-alt" onclick="deleteSubCategorieBautura(this)"></i>\
        <i class="fas fa-pencil-alt" onclick="editSubCategorieBautura(this)"></i>';

        drink.categories[id_categorie_bautura].categories[id_subcategorie_bautura] = {
            "name" : valoare,
            categories : [],
            components : []
        };

        console.log(drink);

    }
}

function deleteSubCategorieBautura(val){
    var to_delete = $(val).parent('div').parent('div');
    var id = parseInt(to_delete[0].id.substring(20));
    var id_categorie = parseInt(to_delete.parent('div').parent('div')[0].id.substring(17));
    drink.categories[id_categorie].categories.splice(id, 1);
    //console.log(food);

    $('.div_pentru_categorii_bautura')[0].innerHTML = '';

    for(var i=0; i<drink.categories.length; i++){
        var aici_se_adauga = $('.div_pentru_categorii_bautura');
        var type;

        id_categorie_bautura = parseInt($('.div_pentru_categorii_bautura')[0].childElementCount);

        if(drink.categories[i].categories.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';
        }else if(drink.categories[i].components.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>';
        }else{
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button><button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>';
        }

        aici_se_adauga.append(
            '<div class="parinte_categorie_adaugata" id="categorie_bautura'+id_categorie_bautura+'">\
                    <div class="categorie_adaugata">\
                        <span>'+ drink.categories[i].name +'</span>\
                        <i class="far fa-trash-alt" onclick="deleteCategorieBautura(this)"></i>\
                        <i class="fas fa-pencil-alt" onclick="editCategorieBautura(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">'+type+'\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                    </div>'
        );

        if(drink.categories[i].components.length > 0){
            var for_bautura = $("#categorie_bautura"+id_categorie_bautura).find('.subcategorii_si_meniuri_adaugate');

            id_bautura = parseInt(for_bautura[0].childElementCount);

            for(var x=0; x<drink.categories[i].components.length; x++){
                for_bautura.append(
                    '<div class="elemente_subcategorie" id="bautura_adaugata_categorie_subcategorie'+id_bautura+'">\
                            <div class="categorie_adaugata">\
                                   <span class="nume_meniu">'+drink.categories[i].components[x].name+'</span> (<span class="gramaj_meniu">'+drink.categories[i].components[x].amount +'ml</span>) - <span class="pret_meniu">'+drink.categories[i].components[x].price+'</span> <span class="valuta_meniu">' +valute[drink.categories[i].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteDishBautura(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualBautura(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                        </div>'
                );
            }
        }else{
            var for_subcategorii = $("#categorie_bautura"+id_categorie_bautura).find('.subcategorii_si_meniuri_adaugate');
            for(var j=0; j<drink.categories[i].categories.length; j++){

                id_subcategorie_bautura = parseInt(for_subcategorii[0].childElementCount);

                for_subcategorii.append(
                    '<div class="elemente_subcategorie" id="subcategorie_bautura'+id_subcategorie_bautura+'">\
                    <div class="categorie_adaugata">\
                       <span>'+ drink.categories[i].categories[j].name +'</span>\
                       <i class="far fa-trash-alt" onclick="deleteSubCategorieBautura(this)"></i>\
                       <i class="fas fa-pencil-alt" onclick="editSubCategorieBautura(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                </div>'
                );

                var for_bautura = for_subcategorii.find("#subcategorie_bautura"+id_subcategorie_bautura).find('.subcategorii_si_meniuri_adaugate');

                for(var x=0; x<drink.categories[i].categories[j].components.length; x++){

                    id_bautura = parseInt(for_bautura[0].childElementCount);

                    for_bautura.append(
                        '<div class="elemente_subcategorie" id="bautura_adaugata_categorie_subcategorie'+id_bautura+'">\
                            <div class="categorie_adaugata">\
                                   <span class="nume_meniu">'+drink.categories[i].categories[j].components[x].name+'</span> (<span class="gramaj_meniu">'+drink.categories[i].categories[j].components[x].amount +'ml</span>) - <span class="pret_meniu">'+drink.categories[i].categories[j].components[x].price+'</span> <span class="valuta_meniu">' +valute[drink.categories[i].categories[j].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteDishBautura(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualBautura(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                        </div>'
                    );
                }
            }
        }
    }

    console.log(drink);
}

function editSubCategorieBautura(element){
    var element_parent = $(element).parent('div');
    var element_value = element_parent.children('span').html();

    element_parent[0].innerHTML =
        '<input type="text" class="input_interior_div" placeholder="Ex: Bere bruna" value="'+
        element_value
        +'"/>'+
        '<p class="save_add" onclick="saveSubCategorieBautura(this)">SAVE</p>';
}




//Functii pentru bauturi
function adaugaBautura(meniu){
    var change = $(meniu).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');

    id_categorie_bautura = parseInt($(meniu).parent('div').parent('div')[0].id.substring(17));
    id_bautura = parseInt($(meniu).parent('div').parent('div').find('.subcategorii_si_meniuri_adaugate')[0].childElementCount);

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie" id="bautura_adaugata_categorie_subcategorie'+id_bautura+'">\
                            <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE BAUTURA</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume bautura:</p>\
                                            <input type="text" class="input_name_creare_meniu" placeholder="EX: Ciuc"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-4 col-5">\
                                            <p class="label_input_creare_meniu">Pret:</p>\
                                            <input type="number" class="input_pret_add_men" />\
                                        </div>\
                                        <div class="col-md-8 col-7">\
                                            <p class="label_input_creare_meniu">Unitate monetara:</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option value="1" selected>RON</option>\
                                                <option value="2">EUR</option>\
                                                <option value="3">USD</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="nume_ingredient_add_men"><i class="fas fa-caret-right"></i> Cantitate</p>\
                                            <input type="number" class="gramaj_ingredient_add_men" placeholder="500"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option value="1">g</option>\
                                                <option value="2">kg</option>\
                                                <option value="3" selected>ml</option>\
                                                <option value="4">l</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-3 col-4">\
                                            <p class="label_input_creare_meniu">Producator:</p>\
                                        </div>\
                                        <div class="col-md-9 col-8">\
                                            <select class="input_adaugare_ingrediente_add_men" id="producer_id_drink">\
                                                <option value="1" selected>Producator 1</option>\
                                                <option value="2">Producator 2</option>\
                                                <option value="3">Producator 3</option>\
                                                <option value="4">Producator 4</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualBautura(this)">ADAUGA</button>\
                                        </div>\
                                    </div>\
                            </div>\
                        </div>'
    );
    id_bautura++;
}

function saveActualBautura(meniu){
    var parent = $(meniu).parent('div').parent('div').parent('div');
    var big_parent = parent.parent('div').parent('div');

    var nume = parent.find('.input_name_creare_meniu').val();
    var pret = parent.find('.input_pret_add_men').val();
    var valuta = parent.find('.select_unit_monetara_add_men').val();
    var valuta_id = parent.find('.select_unit_monetara_add_men')[0].value;
    var quantity = parent.find('.gramaj_ingredient_add_men').val();
    var quantity_id = parent.find('.select_um_gramaj_ing_add_men')[0].value;
    var producer_id = parent.find('#producer_id_drink')[0].value;
    var id_categorie, id_subcategorie;

    var id_bautura = parseInt(parent.parent('div').parent('div')[0].id.substring(39));

    if(parent.parent('div').parent('div').parent('div').parent('div')[0].id.substring(0, 17)=="categorie_bautura"){
        id_categorie = parseInt(parent.parent('div').parent('div').parent('div').parent('div')[0].id.substring(17));
    }else{
        id_subcategorie = parseInt(parent.parent('div').parent('div').parent('div').parent('div')[0].id.substring(20));
        id_categorie = parseInt(parent.parent('div').parent('div').parent('div').parent('div').parent('div').parent('div')[0].id.substring(17));
    }


    if(nume == '' || pret == '' || nume == ' ' || pret == ' '){
        big_parent.remove();
    }else{

        big_parent[0].innerHTML =
            '<div class="categorie_adaugata">\
                                <span class="nume_meniu">'+nume+'</span> (<span class="cantitate_meniu">'+quantity+'ml</span>) - <span class="pret_meniu">'+pret+'</span> <span class="valuta_meniu">' +valute[valuta-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteDishBautura(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualBautura(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" />\
                                <i class="fas fa-info-circle"></i>\
                            </div>';

        if(id_subcategorie != null){
            drink.categories[id_categorie].categories[id_subcategorie].components[id_bautura] =
                {
                    "name": nume,
                    "price": pret,
                    "currency_id": valuta_id,
                    "amount": quantity,
                    "unit_id": quantity_id,
                    "drink_producer_id": producer_id
                };
        }else{
            drink.categories[id_categorie].components[id_bautura] =
                {
                    "name": nume,
                    "price": pret,
                    "currency_id": valuta_id,
                    "amount": quantity,
                    "unit_id": quantity_id,
                    "drink_producer_id": producer_id
                };
        }

        console.log(drink);
    }
}

function editActualBautura(meniu){

    var to_edit = $(meniu).parent('div').parent('div');
    var id = parseInt(to_edit[0].id.substring(39));
    var id_sub_categorie, id_categorie, nume, pret, monetary_unit, quantity, quantity_id, producer_id, ruta;

    if(to_edit.parent('div').parent('div')[0].id.substring(0, 17) == 'categorie_bautura'){
        id_categorie = parseInt(to_edit.parent('div').parent('div')[0].id.substring(17));
        ruta = drink.categories[id_categorie].components[id];
    }else if (to_edit.parent('div').parent('div')[0].id.substring(0, 20) == 'subcategorie_bautura'){
        id_sub_categorie = parseInt(to_edit.parent('div').parent('div')[0].id.substring(20));
        id_categorie = parseInt(to_edit.parent('div').parent('div').parent('div').parent('div')[0].id.substring(17));
        ruta = drink.categories[id_categorie].categories[id_sub_categorie].components[id];
    }

    nume = ruta.name;
    pret = ruta.price;
    monetary_unit = ruta.currency_id;
    quantity = ruta.amount;
    quantity_id = ruta.unit_id;
    producer_id = ruta.drink_producer_id;

    to_edit[0].innerHTML=
        '<div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE BAUTURA</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume bautura:</p>\
                                            <input type="text" class="input_name_creare_meniu" value="'+nume+'" placeholder="EX: Ciuc"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-4 col-5">\
                                            <p class="label_input_creare_meniu">Pret:</p>\
                                            <input type="number" value="'+pret+'" class="input_pret_add_men" />\
                                        </div>\
                                        <div class="col-md-8 col-7">\
                                            <p class="label_input_creare_meniu">Unitate monetara:</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option value="1">RON</option>\
                                                <option value="2">EUR</option>\
                                                <option value="3">USD</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="nume_ingredient_add_men"><i class="fas fa-caret-right"></i> Cantitate</p>\
                                            <input type="number" value="'+quantity+'" class="gramaj_ingredient_add_men" placeholder="500"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option value="1">g</option>\
                                                <option value="2">kg</option>\
                                                <option value="3">ml</option>\
                                                <option value="4">l</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-3 col-4">\
                                            <p class="label_input_creare_meniu">Producator:</p>\
                                        </div>\
                                        <div class="col-md-9 col-8">\
                                            <select class="input_adaugare_ingrediente_add_men" id="producer_id_drink">\
                                                <option value="1">Producator 1</option>\
                                                <option value="2">Producator 2</option>\
                                                <option value="3">Producator 3</option>\
                                                <option value="4">Producator 4</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualBautura(this)">SALVEAZA</button>\
                                        </div>\
                                    </div>\
                            </div>';

    to_edit.find('.select_unit_monetara_add_men')[0].options[monetary_unit-1].selected = true;
    to_edit.find('.select_um_gramaj_ing_add_men')[0].options[quantity_id-1].selected = true;
    to_edit.find('#producer_id_drink')[0].options[producer_id-1].selected = true;
}

function deleteDishBautura(val){
    var to_delete = $(val).parent('div').parent('div');
    var id = parseInt(to_delete[0].id.substring(39));
    var id_sub_categorie, id_categorie;
    if(to_delete.parent('div').parent('div')[0].id.substring(0, 17) === 'categorie_bautura'){
        id_categorie = parseInt(to_delete.parent('div').parent('div')[0].id.substring(17));
        drink.categories[id_categorie].components.splice(id, 1);
    }else if(to_delete.parent('div').parent('div')[0].id.substring(0, 20) === 'subcategorie_bautura'){
        id_sub_categorie = parseInt(to_delete.parent('div').parent('div')[0].id.substring(20));
        id_categorie = parseInt(to_delete.parent('div').parent('div').parent('div').parent('div')[0].id.substring(17));
        drink.categories[id_categorie].categories[id_sub_categorie].components.splice(id, 1);
    }

    //console.log(food);

    $('.div_pentru_categorii_bautura')[0].innerHTML = '';

    for(var i=0; i<drink.categories.length; i++){
        var aici_se_adauga = $('.div_pentru_categorii_bautura');
        var type;

        id_categorie_bautura = parseInt($('.div_pentru_categorii_bautura')[0].childElementCount);

        if(drink.categories[i].categories.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';
        }else if(drink.categories[i].components.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>';
        }else{
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button><button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>';
        }

        aici_se_adauga.append(
            '<div class="parinte_categorie_adaugata" id="categorie_bautura'+id_categorie_bautura+'">\
                    <div class="categorie_adaugata">\
                        <span>'+ drink.categories[i].name +'</span>\
                        <i class="far fa-trash-alt" onclick="deleteCategorieBautura(this)"></i>\
                        <i class="fas fa-pencil-alt" onclick="editCategorieBautura(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">'+type+'\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                    </div>'
        );

        if(drink.categories[i].components.length > 0){
            var for_bautura = $("#categorie_bautura"+id_categorie_bautura).find('.subcategorii_si_meniuri_adaugate');

            id_bautura = parseInt(for_bautura[0].childElementCount);

            for(var x=0; x<drink.categories[i].components.length; x++){
                for_bautura.append(
                    '<div class="elemente_subcategorie" id="bautura_adaugata_categorie_subcategorie'+id_bautura+'">\
                            <div class="categorie_adaugata">\
                                   <span class="nume_meniu">'+drink.categories[i].components[x].name+'</span> (<span class="gramaj_meniu">'+drink.categories[i].components[x].amount +'ml</span>) - <span class="pret_meniu">'+drink.categories[i].components[x].price+'</span> <span class="valuta_meniu">' +valute[drink.categories[i].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteDishBautura(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualBautura(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                        </div>'
                );
            }
        }else{
            var for_subcategorii = $("#categorie_bautura"+id_categorie_bautura).find('.subcategorii_si_meniuri_adaugate');
            for(var j=0; j<drink.categories[i].categories.length; j++){

                id_subcategorie_bautura = parseInt(for_subcategorii[0].childElementCount);

                for_subcategorii.append(
                    '<div class="elemente_subcategorie" id="subcategorie_bautura'+id_subcategorie_bautura+'">\
                    <div class="categorie_adaugata">\
                       <span>'+ drink.categories[i].categories[j].name +'</span>\
                       <i class="far fa-trash-alt" onclick="deleteSubCategorieBautura(this)"></i>\
                       <i class="fas fa-pencil-alt" onclick="editSubCategorieBautura(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                </div>'
                );

                var for_bautura = for_subcategorii.find("#subcategorie_bautura"+id_subcategorie_bautura).find('.subcategorii_si_meniuri_adaugate');

                for(var x=0; x<drink.categories[i].categories[j].components.length; x++){

                    id_bautura = parseInt(for_bautura[0].childElementCount);

                    for_bautura.append(
                        '<div class="elemente_subcategorie" id="bautura_adaugata_categorie_subcategorie'+id_bautura+'">\
                            <div class="categorie_adaugata">\
                                   <span class="nume_meniu">'+drink.categories[i].categories[j].components[x].name+'</span> (<span class="gramaj_meniu">'+drink.categories[i].categories[j].components[x].amount +'ml</span>) - <span class="pret_meniu">'+drink.categories[i].categories[j].components[x].price+'</span> <span class="valuta_meniu">' +valute[drink.categories[i].categories[j].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteDishBautura(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualBautura(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                        </div>'
                    );
                }
            }
        }
    }

    console.log(drink);
}



