function showAdaugaBautura(){
    var sectiune_mancare = $('#adauga_mancare');
    var sectiune_bautura = $('#adauga_bautura');
    var buton_activ = $('.buton_activ_meniu');
    var buton_inactiv = $('.buton_inactiv_meniu');

    sectiune_mancare.css('display', 'none');
    sectiune_bautura.css('display', 'block');
    buton_inactiv.addClass('buton_activ_meniu').removeClass('buton_inactiv_meniu');
    buton_activ.addClass('buton_inactiv_meniu').removeClass('buton_activ_meniu');
}


function adaugaCategorieBautura(here){
    var aici_se_adauga = $(here).parent('div');
    aici_se_adauga.append(
        '<div class="parinte_categorie_adaugata">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Bere" /> \
                        <p class="save_add" onclick="saveCategorieBautura(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>\
                        <button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );
}

function deleteCategorieBautura(val){
    var to_delete = $(val).parent('div').parent('div');
    to_delete.remove();
}

function saveCategorieBautura(val){
    var element_parent = $(val).parent('div');
    var valoare = element_parent.children('input').val();

    if(valoare == '' || valoare == ' '){
        element_parent.parent('div').remove();
    }else{
        element_parent[0].innerHTML =
            '<span>'+ valoare +'</span>\
        <i class="far fa-trash-alt" onclick="deleteCategorieBautura(this)"></i>\
        <i class="fas fa-pencil-alt" onclick="editCategorieBautura(this)"></i>';
    }
}

function editCategorieBautura(element){
    var element_parent = $(element).parent('div');
    var element_value = element_parent.children('span').html();
    console.log(element_parent);

    element_parent[0].innerHTML =
        '<input type="text" class="input_interior_div" placeholder="Ex: Bere" value="'+
        element_value
        +'"/>'+
        '<p class="save_add" onclick="saveCategorieBautura(this)">SAVE</p>';
}

function adaugaSubcategorieBautura(cat){
    var change = $(cat).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Bere bruna" /> \
                        <p class="save_add" onclick="saveCategorieBautura(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaSubcategorieBautura(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>\
                        <button class="adauga_subcategorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );

}

function adaugaBautura(meniu){
    var change = $(meniu).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaBautura(this)">ADAUGA BAUTURA <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie">\
                            <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE BAUTURA</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume bautura:</p>\
                                            <input type="text" class="input_name_creare_meniu" placeholder="EX: Ciuc"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="nume_ingredient_add_men"><i class="fas fa-caret-right"></i> Cantitate</p>\
                                            <input type="number" class="gramaj_ingredient_add_men" placeholder="500"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option>g</option>\
                                                <option>kg</option>\
                                                <option selected>ml</option>\
                                                <option>l</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-3 col-4">\
                                            <p class="label_input_creare_meniu">Producator:</p>\
                                        </div>\
                                        <div class="col-md-9 col-8">\
                                            <select class="input_adaugare_ingrediente_add_men">\
                                                <option selected>Producator 1</option>\
                                                <option>Producator 2</option>\
                                                <option>Producator 3</option>\
                                                <option>Producator 4</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualBautura(this)">ADAUGA</button>\
                                        </div>\
                                    </div>\
                            </div>\
                        </div>'
    );
}

function saveActualBautura(meniu){
    var parent = $(meniu).parent('div').parent('div').parent('div');
    var big_parent = parent.parent('div').parent('div');
    var nume = parent.find('.input_name_creare_meniu').val();
    var cantitate = parent.find('.gramaj_ingredient_add_men').val();

    if(nume == '' || nume == ' '){
        big_parent.remove();
    }else{
        big_parent[0].innerHTML =
            '<div class="categorie_adaugata">\
                                <span class="nume_meniu">'+nume+'</span> (<span class="cantitate_meniu">'+ cantitate +'</span>ml)\
                                <i class="far fa-trash-alt" onclick="deleteCategorieBautura(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualBautura(this)"></i>\
                            </div>';
    }
}


function editActualBautura(meniu){
    var parent = $(meniu).parent('div');
    var big_parent = parent.parent('div');
    var nume = parent.find('.nume_meniu').text();
    var cantitate = parent.find('.cantitate_meniu').text();

    big_parent[0].innerHTML=
        '<div class="elemente_subcategorie">\
                            <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE BAUTURA</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume bautura:</p>\
                                            <input type="text" class="input_name_creare_meniu" placeholder="EX: Ciuc" value="'+ nume +'"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="nume_ingredient_add_men"><i class="fas fa-caret-right"></i> Cantitate</p>\
                                            <input type="number" class="gramaj_ingredient_add_men" placeholder="500" value="'+ cantitate +'"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option>g</option>\
                                                <option>kg</option>\
                                                <option selected>ml</option>\
                                                <option>l</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-3 col-4">\
                                            <p class="label_input_creare_meniu">Producator:</p>\
                                        </div>\
                                        <div class="col-md-9 col-8">\
                                            <select class="input_adaugare_ingrediente_add_men">\
                                                <option selected>Producator 1</option>\
                                                <option>Producator 2</option>\
                                                <option>Producator 3</option>\
                                                <option>Producator 4</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualBautura(this)">SALVEAZA</button>\
                                        </div>\
                                    </div>\
                            </div>\
                        </div>';
}
