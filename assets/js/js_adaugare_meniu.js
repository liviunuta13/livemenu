var meniu = {};
var food =
    {
        "name": null,
        "categories": [],
        "pictures": []
    };
var ingredients = [], pictures_dish = [];
var id_categorie_mancare = 0, id_subcategorie_mancare = 0, id_mancare = 0;

var valute = ['RON', 'EUR', 'USD'];
var ingrediente_preluate = ['', 'Ingredient 1', 'Ingredient 2', 'Ingredient 3', 'Ingredient 4', 'Ingredient 5'];

//Functie de schimbare afisare intre mancare si bautura
function showAdaugaMancare(){
    var sectiune_mancare = $('#adauga_mancare');
    var sectiune_bautura = $('#adauga_bautura');
    var buton_activ = $('.buton_activ_meniu');
    var buton_inactiv = $('.buton_inactiv_meniu');

    sectiune_mancare.css('display', 'block');
    sectiune_bautura.css('display', 'none');
    buton_inactiv.addClass('buton_activ_meniu').removeClass('buton_inactiv_meniu');
    buton_activ.addClass('buton_inactiv_meniu').removeClass('buton_activ_meniu');
}


//Functii pentru categorii
function adaugaCategorieMeniu(here){
    var aici_se_adauga = $('.div_pentru_categorii');

    id_categorie_mancare = parseInt($(here).parent('div').find('.div_pentru_categorii')[0].childElementCount);

    aici_se_adauga.append(
        '<div class="parinte_categorie_adaugata" id="categorie_mancare'+id_categorie_mancare+'">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Ciorbe" />\
                        <p class="save_add" onclick="saveCategorieMeniu(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>\
                        <button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );
}

function deleteCategorieMeniu(val){
    var to_delete = $(val).parent('div').parent('div');
    var id = parseInt(to_delete[0].id.substring(17));
    food.categories.splice(id, 1);

    $('.div_pentru_categorii')[0].innerHTML = '';

    for(var i=0; i<food.categories.length; i++){
        var aici_se_adauga = $('.div_pentru_categorii');
        var type;

        id_categorie_mancare = parseInt($('.div_adaugare_categorie').parent('div').find('.div_pentru_categorii')[0].childElementCount);

        if(food.categories[i].categories.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';
        }else if(food.categories[i].components.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>';
        }else{
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button><button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>';
        }

        aici_se_adauga.append(
            '<div class="parinte_categorie_adaugata" id="categorie_mancare'+id_categorie_mancare+'">\
                    <div class="categorie_adaugata">\
                        <span>'+ food.categories[i].name +'</span>\
                        <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                        <i class="fas fa-pencil-alt" onclick="editCategorieMeniu(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">'+type+'\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                    </div>'
        );

        if(food.categories[i].components.length > 0){
            var for_meniuri = $("#categorie_mancare"+id_categorie_mancare).find('.subcategorii_si_meniuri_adaugate');

            id_mancare = parseInt(for_meniuri[0].childElementCount);

            for(var x=0; x<food.categories[i].components.length; x++){

                var gramaj_total = 0;

                for(var g=0; g<food.categories[i].components[x].ingredients.length; g++){
                    var amount = food.categories[i].components[x].ingredients[g].amount;
                    gramaj_total = gramaj_total + parseInt(amount);
                }

                for_meniuri.append(
                    '<div class="elemente_subcategorie" id="meniu_adaugat_categorie_subcategorie'+id_mancare+'">\
                            <div class="categorie_adaugata">\
                               <span class="nume_meniu">'+food.categories[i].components[x].name+'</span> (<span class="gramaj_meniu">'+gramaj_total+'g</span>) - <span class="pret_meniu">'+food.categories[i].components[x].price+'</span> <span class="valuta_meniu">' +valute[food.categories[i].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualMeniu(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                    </div>'
                );
            }
        }else{
            var for_subcategorii = $("#categorie_mancare"+id_categorie_mancare).find('.subcategorii_si_meniuri_adaugate');
            for(var j=0; j<food.categories[i].categories.length; j++){

                id_subcategorie_mancare = parseInt(for_subcategorii[0].childElementCount);

                for_subcategorii.append(
                    '<div class="elemente_subcategorie" id="subcategorie_mancare'+id_subcategorie_mancare+'">\
                    <div class="categorie_adaugata">\
                       <span>'+ food.categories[i].categories[j].name +'</span>\
                       <i class="far fa-trash-alt" onclick="deleteSubCategorieMeniu(this)"></i>\
                       <i class="fas fa-pencil-alt" onclick="editSubCategorieMeniu(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                </div>'
                );

                var for_meniuri = for_subcategorii.find("#subcategorie_mancare"+id_subcategorie_mancare).find('.subcategorii_si_meniuri_adaugate');

                for(var x=0; x<food.categories[i].categories[j].components.length; x++){

                    id_mancare = parseInt(for_meniuri[0].childElementCount);

                    var gramaj_total = 0;

                    for(var g=0; g<food.categories[i].categories[j].components[x].ingredients.length; g++){
                        var amount = food.categories[i].categories[j].components[x].ingredients[g].amount;
                        gramaj_total = gramaj_total + parseInt(amount);
                    }

                    for_meniuri.append(
                        '<div class="elemente_subcategorie" id="meniu_adaugat_categorie_subcategorie'+id_mancare+'">\
                            <div class="categorie_adaugata">\
                                   <span class="nume_meniu">'+food.categories[i].categories[j].components[x].name+'</span> (<span class="gramaj_meniu">'+gramaj_total+'g</span>) - <span class="pret_meniu">'+food.categories[i].categories[j].components[x].price+'</span> <span class="valuta_meniu">' +valute[food.categories[i].categories[j].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualMeniu(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                        </div>'
                    );
                }
            }
        }
    }

    console.log(food);
}

function saveCategorieMeniu(val){
    var element_parent = $(val).parent('div');
    var element_id = parseInt($(val).parent('div').parent('div')[0].id.substring(17));
    var valoare = element_parent.children('input').val();

    if(valoare == '' || valoare == ' '){
        element_parent.parent('div').remove();
    }else{
        element_parent[0].innerHTML =
            '<span>'+ valoare +'</span>\
        <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
        <i class="fas fa-pencil-alt" onclick="editCategorieMeniu(this)"></i>';


        food.categories[element_id] =
            {
                "name" : valoare,
                categories : [],
                components : []
            };
        console.log(food);
        id_mancare = 0;
    }
}

function editCategorieMeniu(element){
    var element_parent = $(element).parent('div');
    var element_value = element_parent.children('span').html();

    element_parent[0].innerHTML =
        '<input type="text" class="input_interior_div" placeholder="Ex: Ciorbe" value="'+
        element_value
        +'"/>'+
        '<p class="save_add" onclick="saveCategorieMeniu(this)">SAVE</p>';
}


//Functii pentru subcategorii
function adaugaSubcategorieMeniu(cat){
    var change = $(cat).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');

    id_categorie_mancare = parseInt($(cat).parent('div').parent('div')[0].id.substring(17));
    id_subcategorie_mancare = parseInt($(cat).parent('div').parent('div').find('.subcategorii_si_meniuri_adaugate')[0].childElementCount);

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie" id="subcategorie_mancare'+id_subcategorie_mancare+'">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Ciorbe de porc" /> \
                        <p class="save_add" onclick="saveSubCategorieMeniu(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );
}

function saveSubCategorieMeniu(val){
    var element_parent = $(val).parent('div');
    //var element_id = parseInt($(val).parent('div').parent('div')[0].id.substring(17));
    var valoare = element_parent.children('input').val();

    if(valoare == '' || valoare == ' '){
        element_parent.parent('div').remove();
    }else{
        element_parent[0].innerHTML =
            '<span>'+ valoare +'</span>\
        <i class="far fa-trash-alt" onclick="deleteSubCategorieMeniu(this)"></i>\
        <i class="fas fa-pencil-alt" onclick="editSubCategorieMeniu(this)"></i>';

        food.categories[id_categorie_mancare].categories[id_subcategorie_mancare] = {
            "name" : valoare,
            categories : [],
            components : []
        };

        console.log(food);

    }
}

function deleteSubCategorieMeniu(val){
    var to_delete = $(val).parent('div').parent('div');
    var id = parseInt(to_delete[0].id.substring(20));
    var id_categorie = parseInt(to_delete.parent('div').parent('div')[0].id.substring(17));
    food.categories[id_categorie].categories.splice(id, 1);
    //console.log(food);

    $('.div_pentru_categorii')[0].innerHTML = '';

    for(var i=0; i<food.categories.length; i++){
        var aici_se_adauga = $('.div_pentru_categorii');
        var type;

        id_categorie_mancare = parseInt($('.div_adaugare_categorie').parent('div').find('.div_pentru_categorii')[0].childElementCount);

        if(food.categories[i].categories.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';
        }else if(food.categories[i].components.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>';
        }else{
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button><button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>';
        }

        aici_se_adauga.append(
            '<div class="parinte_categorie_adaugata" id="categorie_mancare'+id_categorie_mancare+'">\
                    <div class="categorie_adaugata">\
                        <span>'+ food.categories[i].name +'</span>\
                        <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                        <i class="fas fa-pencil-alt" onclick="editCategorieMeniu(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">'+type+'\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                    </div>'
        );

        if(food.categories[i].components.length > 0){
            var for_meniuri = $("#categorie_mancare"+id_categorie_mancare).find('.subcategorii_si_meniuri_adaugate');

            id_mancare = parseInt(for_meniuri[0].childElementCount);

            for(var x=0; x<food.categories[i].components.length; x++){

                var gramaj_total = 0;

                for(var g=0; g<food.categories[i].components[x].ingredients.length; g++){
                    var amount = food.categories[i].components[x].ingredients[g].amount;
                    gramaj_total = gramaj_total + parseInt(amount);
                }

                for_meniuri.append(
                    '<div class="elemente_subcategorie" id="meniu_adaugat_categorie_subcategorie'+id_mancare+'">\
                            <div class="categorie_adaugata">\
                               <span class="nume_meniu">'+food.categories[i].components[x].name+'</span> (<span class="gramaj_meniu">'+gramaj_total+'g</span>) - <span class="pret_meniu">'+food.categories[i].components[x].price+'</span> <span class="valuta_meniu">' +valute[food.categories[i].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualMeniu(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                    </div>'
                );
            }
        }else{
            var for_subcategorii = $("#categorie_mancare"+id_categorie_mancare).find('.subcategorii_si_meniuri_adaugate');
            for(var j=0; j<food.categories[i].categories.length; j++){

                id_subcategorie_mancare = parseInt(for_subcategorii[0].childElementCount);

                for_subcategorii.append(
                    '<div class="elemente_subcategorie" id="subcategorie_mancare'+id_subcategorie_mancare+'">\
                    <div class="categorie_adaugata">\
                       <span>'+ food.categories[i].categories[j].name +'</span>\
                       <i class="far fa-trash-alt" onclick="deleteSubCategorieMeniu(this)"></i>\
                       <i class="fas fa-pencil-alt" onclick="editSubCategorieMeniu(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                </div>'
                );

                var for_meniuri = for_subcategorii.find("#subcategorie_mancare"+id_subcategorie_mancare).find('.subcategorii_si_meniuri_adaugate');

                for(var x=0; x<food.categories[i].categories[j].components.length; x++){

                    id_mancare = parseInt(for_meniuri[0].childElementCount);

                    var gramaj_total = 0;

                    for(var g=0; g<food.categories[i].categories[j].components[x].ingredients.length; g++){
                        var amount = food.categories[i].categories[j].components[x].ingredients[g].amount;
                        gramaj_total = gramaj_total + parseInt(amount);
                    }

                    for_meniuri.append(
                        '<div class="elemente_subcategorie" id="meniu_adaugat_categorie_subcategorie'+id_mancare+'">\
                            <div class="categorie_adaugata">\
                                   <span class="nume_meniu">'+food.categories[i].categories[j].components[x].name+'</span> (<span class="gramaj_meniu">'+gramaj_total+'g</span>) - <span class="pret_meniu">'+food.categories[i].categories[j].components[x].price+'</span> <span class="valuta_meniu">' +valute[food.categories[i].categories[j].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualMeniu(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                        </div>'
                    );
                }
            }
        }
    }

    console.log(food);
}

function editSubCategorieMeniu(element){
    var element_parent = $(element).parent('div');
    var element_value = element_parent.children('span').html();

    element_parent[0].innerHTML =
        '<input type="text" class="input_interior_div" placeholder="Ex: Ciorbe" value="'+
        element_value
        +'"/>'+
        '<p class="save_add" onclick="saveSubCategorieMeniu(this)">SAVE</p>';
}


//Functii pentru meniuri
function adaugaMeniu(meniu){
    var change = $(meniu).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');
    var id_imagine_dish = 0;
    pictures_dish = [];

    id_categorie_mancare = parseInt($(meniu).parent('div').parent('div')[0].id.substring(17));
    id_mancare = parseInt($(meniu).parent('div').parent('div').find('.subcategorii_si_meniuri_adaugate')[0].childElementCount);

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie" id="meniu_adaugat_categorie_subcategorie'+id_mancare+'">\
                            <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE MENIU</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume meniu:</p>\
                                            <input type="text" class="input_name_creare_meniu" placeholder="EX: Ciorba de sfecla"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-4 col-5">\
                                            <p class="label_input_creare_meniu">Pret:</p>\
                                            <input type="number" class="input_pret_add_men" />\
                                        </div>\
                                        <div class="col-md-8 col-7">\
                                            <p class="label_input_creare_meniu">Unitate monetara:</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option selected value="1">RON</option>\
                                                <option value="2">EUR</option>\
                                                <option value="3">USD</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-3 col-3">\
                                            <p class="label_input_creare_meniu">Ingrediente:</p>\
                                        </div>\
                                        <div class="col-md-9 col-9">\
                                            <select class="input_adaugare_ingrediente_add_men" onchange="selectIngredient(this)">\
                                                 <option value="default" disabled selected="selected">Selecteaza un ingredient</option>\
                                                 <option value="1">Ingredient 1</option>\
                                                 <option value="2">Ingredient 2</option>\
                                                 <option value="3">Ingredient 3</option>\
                                                 <option value="4">Ingredient 4</option>\
                                                 <option value="5">Ingredient 5</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row append_ingredients_here">\
                                    </div>\
                                    <div class="row append_dish_images">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Imagini:</p>\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <button class="add_photo_add_men" onclick="triggerInputFileAddMen()"><i class="fas fa-plus"></i></button>\
                                            <input type="file" hidden id="upload_dish_photo"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Scurt istoric al meniului:</p>\
                                            <textarea class="descriere_meniu_add_men" id="descriere_meniu"></textarea>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Cate persoane pot manca?</p>\
                                            <input type="number" class="input_nr_persoane_add_men" id="no_of_persons" placeholder="20"/>\
                                        </div>\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Timp de gatire:</p>\
                                            <input type="text" class="input_durata_add_men" id="timp_gatire" placeholder="20 min"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox" id="specialitatea_casei">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Specialitatea casei</p>\
                                        </div>\
                                        <div class="col-md-7">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox" id="specific_area_food">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Mancare caracteristica zonei</p>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualMeniu(this)">ADAUGA</button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>'
    );

    //Functie de adaugare a unei imagini pentru dish
    $('#upload_dish_photo').on('change', function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                pictures_dish.push(e.target.result);

                var de_adaugat =
                    "<div class='col-md-3 col-3'>\
                       <i class='far fa-times-circle delete_uploaded_image_menu' onclick='deleteDishImage("+id_imagine_dish+")'></i>\
                       <img src='"+e.target.result+"' class='img_uploaded_menu' />\
                     </div>";
                $('.append_dish_images').append(de_adaugat);
                id_imagine_dish++;

            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    id_mancare++;
}

function saveActualMeniu(meniu){
    var parent = $(meniu).parent('div').parent('div').parent('div');
    var big_parent = parent.parent('div').parent('div');

    var nume = parent.find('.input_name_creare_meniu').val();
    var pret = parent.find('.input_pret_add_men').val();
    var valuta = parent.find('.select_unit_monetara_add_men').val();
    var valuta_id = parent.find('.select_unit_monetara_add_men')[0].value;
    var descriere = parent.find('#descriere_meniu').val();
    var nr_persoane = parent.find('#no_of_persons').val();
    var timp_gatire = String(parent.find('#timp_gatire').val());
    var specialitate, specific_area_food, id_categorie, id_subcategorie, gramaj_total = 0;
    if(parent.find('#specialitatea_casei')[0].checked){
        specialitate = 1;
    }else{
        specialitate = 0;
    }
    if(parent.find('#specific_area_food')[0].checked){
        specific_area_food = 1;
    }else{
        specific_area_food = 0;
    }

    var id_meniu = parseInt(parent.parent('div').parent('div')[0].id.substring(36));

    if(parent.parent('div').parent('div').parent('div').parent('div')[0].id.substring(0, 17)=="categorie_mancare"){
        id_categorie = parseInt(parent.parent('div').parent('div').parent('div').parent('div')[0].id.substring(17));
    }else{
        id_subcategorie = parseInt(parent.parent('div').parent('div').parent('div').parent('div')[0].id.substring(20));
        id_categorie = parseInt(parent.parent('div').parent('div').parent('div').parent('div').parent('div').parent('div')[0].id.substring(17));
    }


    if(nume == '' || pret == '' || nume == ' ' || pret == ' '){
        big_parent.remove();
    }else{

        var obj_ing = {
            "ingredient_id": null,
            "amount": null,
            "unit_id": null,
            "allergen": null
        };
        var max_childs = $('.append_ingredients_here')[0].childNodes.length - 1;
        for(var i=1; i<=max_childs; i++){
            var ingredient_id = parseInt($('.append_ingredients_here')[0].childNodes[i].id.substring(16));
            var amount = $('.append_ingredients_here')[0].childNodes[i].children[1].value;
            var unit_id = $('.append_ingredients_here')[0].childNodes[i].children[2].value;
            var allergen;

            gramaj_total = gramaj_total + parseInt(amount);

            if($('.append_ingredients_here')[0].childNodes[i].children[3].children[0].children[0].checked == true){
                allergen = 1;
            }else{
                allergen = 0;
            }

            obj_ing = {
                "ingredient_id": ingredient_id,
                "amount": amount,
                "unit_id": unit_id,
                "allergen": allergen
            };

            ingredients.push(obj_ing);
        }


        big_parent[0].innerHTML =
            '<div class="categorie_adaugata">\
                                <span class="nume_meniu">'+nume+'</span> (<span class="gramaj_meniu">'+gramaj_total+'g</span>) - <span class="pret_meniu">'+pret+'</span> <span class="valuta_meniu">' +valute[valuta-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteDishMeniu(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualMeniu(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>';

        if(id_subcategorie != null){
            food.categories[id_categorie].categories[id_subcategorie].components[id_meniu] =
                {
                    "name": nume,
                    "description": descriere,
                    "pictures": pictures_dish,
                    "price": pret,
                    "currency_id": valuta_id,
                    "number_of_persons_that_can_eat": nr_persoane,
                    "cooking_time": timp_gatire,
                    "specialty_of_the_house": specialitate,
                    "specific_area_food": specific_area_food,
                    "ingredients": ingredients
                };
        }else{
            food.categories[id_categorie].components[id_meniu] =
                {
                    "name": nume,
                    "description": descriere,
                    "pictures": pictures_dish,
                    "price": pret,
                    "currency_id": valuta_id,
                    "number_of_persons_that_can_eat": nr_persoane,
                    "cooking_time": timp_gatire,
                    "specialty_of_the_house": specialitate,
                    "specific_area_food": specific_area_food,
                    "ingredients": ingredients
                };
        }
        ingredients = [];

        console.log(food);
    }
}

function editActualMeniu(meniu){

    var to_edit = $(meniu).parent('div').parent('div');
    var id = parseInt(to_edit[0].id.substring(36));
    var id_sub_categorie, id_categorie, nume, pret, monetary_unit, ingredients = [], descriere, no_of_persons, no_of_minutes, specialty, characteristic_food, ruta;

    if(to_edit.parent('div').parent('div')[0].id.substring(0, 17) == 'categorie_mancare'){
        id_categorie = parseInt(to_edit.parent('div').parent('div')[0].id.substring(17));
        ruta = food.categories[id_categorie].components[id];
    }else{
        id_sub_categorie = parseInt(to_edit.parent('div').parent('div')[0].id.substring(20));
        id_categorie = parseInt(to_edit.parent('div').parent('div').parent('div').parent('div')[0].id.substring(17));
        ruta = food.categories[id_categorie].categories[id_sub_categorie].components[id];
    }

    nume = ruta.name;
    pret = ruta.price;
    monetary_unit = parseInt(ruta.currency_id);
    /*

    */
    descriere = ruta.description;
    no_of_persons = ruta.number_of_persons_that_can_eat;
    no_of_minutes = ruta.cooking_time;
    specialty = ruta.specialty_of_the_house;
    if(specialty === 1){
        specialty = 'checked';
    }else if(specialty === 0){
        specialty = 'unchecked';
    }
    characteristic_food = ruta.specific_area_food;
    if(characteristic_food === 1){
        characteristic_food = 'checked';
    }else if(characteristic_food === 0){
        characteristic_food = 'unchecked';
    }


    to_edit[0].innerHTML=
        ' <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE MENIU</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume meniu:</p>\
                                            <input type="text" class="input_name_creare_meniu" placeholder="EX: Ciorba de sfecla" value="'+ nume +'"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-4 col-5">\
                                            <p class="label_input_creare_meniu">Pret:</p>\
                                            <input type="number" class="input_pret_add_men" value="'+ pret +'"/>\
                                        </div>\
                                        <div class="col-md-8 col-7">\
                                            <p class="label_input_creare_meniu">Unitate monetara:</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option value="1" >RON</option>\
                                                <option value="2" >EUR</option>\
                                                <option value="3" >USD</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-3 col-3">\
                                            <p class="label_input_creare_meniu">Ingrediente:</p>\
                                        </div>\
                                        <div class="col-md-9 col-9">\
                                            <select class="input_adaugare_ingrediente_add_men" onchange="selectIngredient(this)">\
                                                 <option value="default" disabled selected="selected">Selecteaza un ingredient</option>\
                                                 <option value="1">Ingredient 1</option>\
                                                 <option value="2">Ingredient 2</option>\
                                                 <option value="3">Ingredient 3</option>\
                                                 <option value="4">Ingredient 4</option>\
                                                 <option value="5">Ingredient 5</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row append_ingredients_here">\
                                    </div>\
                                    <div class="row append_dish_images">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Imagini:</p>\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <button class="add_photo_add_men" onclick="triggerInputFileAddMen()"><i class="fas fa-plus"></i></button>\
                                            <input type="file" hidden id="upload_dish_photo"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Scurt istoric al meniului:</p>\
                                            <textarea class="descriere_meniu_add_men" id="descriere_meniu">'+descriere+'</textarea>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Cate persoane pot manca?</p>\
                                            <input type="number" class="input_nr_persoane_add_men" id="no_of_persons" placeholder="20" value="'+no_of_persons+'"/>\
                                        </div>\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Timp de gatire:</p>\
                                            <input type="text" class="input_durata_add_men" id="timp_gatire" placeholder="20 min" value="'+no_of_minutes+'"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox" id="specialitatea_casei" '+specialty+'>\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Specialitatea casei</p>\
                                        </div>\
                                        <div class="col-md-7">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox" id="specific_area_food" '+characteristic_food+'>\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Mancare caracteristica zonei</p>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualMeniu(this)">MODIFICA</button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>';

    pictures_dish = [];
    for(var i=0; i<ruta.pictures.length; i++){
        pictures_dish.push(ruta.pictures[i]);

        var de_adaugat =
            "<div class='col-md-3 col-3'>\
               <i class='far fa-times-circle delete_uploaded_image_menu' onclick='deleteDishImage("+i+")'></i>\
               <img src='"+ruta.pictures[i]+"' class='img_uploaded_menu' />\
             </div>";
        $('.append_dish_images').append(de_adaugat);
    }

    to_edit.find('.select_unit_monetara_add_men')[0].options[monetary_unit-1].selected = true;

    var div = to_edit.find('.append_ingredients_here');
    for(var i = 0; i<ruta.ingredients.length; i++){
        var cantitate = ruta.ingredients[i].amount;
        var id_ing = ruta.ingredients[i].ingredient_id;
        var id_masura_ing = parseInt(ruta.ingredients[i].unit_id);
        var select_masura = to_edit.find('#ingredient_value'+id_ing).find('.select_um_gramaj_ing_add_men');
        var alergen = ruta.ingredients[i].allergen;

        var nume_ing = ingrediente_preluate[id_ing];
        to_edit.find('.input_adaugare_ingrediente_add_men')[0].options[id_ing].disabled = true;


        div.append(
            '<div class="col-md-12" id="ingredient_value'+id_ing+'">\
             <p class="nume_ingredient_add_men"><i class="fas fa-caret-right"></i> '+nume_ing+'</p>\
             <input type="number" class="gramaj_ingredient_add_men" placeholder="100" value="'+cantitate+'"/>\
             <select class="select_um_gramaj_ing_add_men">\
                 <option value="1" >g</option>\
                 <option value="2" >kg</option>\
                 <option value="3" >ml</option>\
                 <option value="4" >l</option>\
             </select>\
             <div class="div_inliner">\
                 <label class="label_input_checkbox">\
                     <input type="checkbox">\
                     <span class="checkmark_checkbox"></span>\
                 </label>\
                 <p class="nume_ingredient_add_men">Alergen</p>\
             </div>\
             <i class="far fa-trash-alt trash_ingredient_add_men" onclick="deleteIngredient(this)"></i>\
        </div>'
        );

        if(ruta.ingredients[i].allergen == 1){
            $('#ingredient_value'+id_ing).find('.label_input_checkbox').children('input')[0].checked = true;
        }else{
            $('#ingredient_value'+id_ing).find('.label_input_checkbox').children('input')[0].checked = false;
        }

        $('#ingredient_value'+id_ing).find('.select_um_gramaj_ing_add_men')[0].options[id_masura_ing-1].selected = true;
    }

    $('#upload_dish_photo').on('change', function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                pictures_dish.push(e.target.result);

                var de_adaugat =
                    "<div class='col-md-3 col-3'>\
                       <i class='far fa-times-circle delete_uploaded_image_menu' onclick='deleteDishImage("+ruta.pictures.length+")'></i>\
                       <img src='"+e.target.result+"' class='img_uploaded_menu' />\
                     </div>";
                $('.append_dish_images').append(de_adaugat);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
}

function deleteDishMeniu(val){
    var to_delete = $(val).parent('div').parent('div');
    var id = parseInt(to_delete[0].id.substring(36));
    var id_sub_categorie, id_categorie;

    console.log(to_delete.parent('div').parent('div')[0].id.substring(0, 17));
    if(to_delete.parent('div').parent('div')[0].id.substring(0, 17) === 'categorie_mancare'){
        id_categorie = parseInt(to_delete.parent('div').parent('div')[0].id.substring(17));
        food.categories[id_categorie].components.splice(id, 1);
    }else if(to_delete.parent('div').parent('div')[0].id.substring(0, 20) === 'subcategorie_mancare'){
        id_sub_categorie = parseInt(to_delete.parent('div').parent('div')[0].id.substring(20));
        id_categorie = parseInt(to_delete.parent('div').parent('div').parent('div').parent('div')[0].id.substring(17));
        food.categories[id_categorie].categories[id_sub_categorie].components.splice(id, 1);
    }

    //console.log(food);

    $('.div_pentru_categorii')[0].innerHTML = '';

    for(var i=0; i<food.categories.length; i++){
        var aici_se_adauga = $('.div_pentru_categorii');
        var type;

        id_categorie_mancare = parseInt($('.div_adaugare_categorie').parent('div').find('.div_pentru_categorii')[0].childElementCount);

        if(food.categories[i].categories.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';
        }else if(food.categories[i].components.length > 0){
            type = '<button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>';
        }else{
            type = '<button class="adauga_subcategorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button><button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>';
        }

        aici_se_adauga.append(
            '<div class="parinte_categorie_adaugata" id="categorie_mancare'+id_categorie_mancare+'">\
                    <div class="categorie_adaugata">\
                        <span>'+ food.categories[i].name +'</span>\
                        <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                        <i class="fas fa-pencil-alt" onclick="editCategorieMeniu(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">'+type+'\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                    </div>'
        );

        if(food.categories[i].components.length > 0){
            var for_meniuri = $("#categorie_mancare"+id_categorie_mancare).find('.subcategorii_si_meniuri_adaugate');

            id_mancare = parseInt(for_meniuri[0].childElementCount);

            for(var x=0; x<food.categories[i].components.length; x++){

                var gramaj_total = 0;

                for(var g=0; g<food.categories[i].components[x].ingredients.length; g++){
                    var amount = food.categories[i].components[x].ingredients[g].amount;
                    gramaj_total = gramaj_total + parseInt(amount);
                }

                for_meniuri.append(
                    '<div class="elemente_subcategorie" id="meniu_adaugat_categorie_subcategorie'+id_mancare+'">\
                            <div class="categorie_adaugata">\
                               <span class="nume_meniu">'+food.categories[i].components[x].name+'</span> (<span class="gramaj_meniu">'+gramaj_total+'g</span>) - <span class="pret_meniu">'+food.categories[i].components[x].price+'</span> <span class="valuta_meniu">' +valute[food.categories[i].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualMeniu(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                    </div>'
                );
            }
        }else{
            var for_subcategorii = $("#categorie_mancare"+id_categorie_mancare).find('.subcategorii_si_meniuri_adaugate');
            for(var j=0; j<food.categories[i].categories.length; j++){

                id_subcategorie_mancare = parseInt(for_subcategorii[0].childElementCount);

                for_subcategorii.append(
                    '<div class="elemente_subcategorie" id="subcategorie_mancare'+id_subcategorie_mancare+'">\
                    <div class="categorie_adaugata">\
                       <span>'+ food.categories[i].categories[j].name +'</span>\
                       <i class="far fa-trash-alt" onclick="deleteSubCategorieMeniu(this)"></i>\
                       <i class="fas fa-pencil-alt" onclick="editSubCategorieMeniu(this)"></i>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
                </div>'
                );

                var for_meniuri = for_subcategorii.find("#subcategorie_mancare"+id_subcategorie_mancare).find('.subcategorii_si_meniuri_adaugate');

                for(var x=0; x<food.categories[i].categories[j].components.length; x++){

                    id_mancare = parseInt(for_meniuri[0].childElementCount);

                    var gramaj_total = 0;

                    for(var g=0; g<food.categories[i].categories[j].components[x].ingredients.length; g++){
                        var amount = food.categories[i].categories[j].components[x].ingredients[g].amount;
                        gramaj_total = gramaj_total + parseInt(amount);
                    }

                    for_meniuri.append(
                        '<div class="elemente_subcategorie" id="meniu_adaugat_categorie_subcategorie'+id_mancare+'">\
                            <div class="categorie_adaugata">\
                                   <span class="nume_meniu">'+food.categories[i].categories[j].components[x].name+'</span> (<span class="gramaj_meniu">'+gramaj_total+'g</span>) - <span class="pret_meniu">'+food.categories[i].categories[j].components[x].price+'</span> <span class="valuta_meniu">' +valute[food.categories[i].categories[j].components[x].currency_id-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualMeniu(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>\
                        </div>'
                    );
                }
            }
        }
    }

    console.log(food);
}


function triggerInputFileAddMen(){
    var input = $('#upload_dish_photo');
    input.click();
}

//Functie de stergere a unei imagini a dishului de adaugat
function deleteDishImage(id){
    //Stergerea imaginii respective din vector
    pictures_dish.splice(id, 1);
    $('.append_dish_images')[0].innerHTML =
        '<div class="col-md-12">\
             <p class="label_input_creare_meniu">Imagini:</p>\
         </div>\
         <div class="col-md-3 col-3">\
             <button class="add_photo_add_men" onclick="triggerInputFileAddMen()"><i class="fas fa-plus"></i></button>\
             <input type="file" hidden id="upload_dish_photo"/>\
         </div>';

    //Re-afisarea imaginilor incarcate actual in sesiune in front-end dupa stergere
    for(var i=0; i<pictures_dish.length; i++){
        var de_adaugat =
            "<div class='col-md-3 col-3'>\
               <i class='far fa-times-circle delete_uploaded_image_menu' onclick='deleteDishImage("+i+")'></i>\
               <img src='"+pictures_dish[i]+"' class='img_uploaded_menu' />\
             </div>";
        $('.append_dish_images').append(de_adaugat);
    }
}

//Functie de selectare a unui ingredient din lista de ingrediente
function selectIngredient(inp){
    var value = $(inp)[0].options[$(inp)[0].selectedIndex].value;
    var text = $(inp)[0].options[$(inp)[0].selectedIndex].text;
    var div = $('.append_ingredients_here');

    $(inp)[0].options[$(inp)[0].selectedIndex].disabled = true;
    div.append(
        '<div class="col-md-12" id="ingredient_value'+value+'">\
             <p class="nume_ingredient_add_men"><i class="fas fa-caret-right"></i> '+ text +'</p>\
             <input type="number" class="gramaj_ingredient_add_men" placeholder="100"/>\
             <select class="select_um_gramaj_ing_add_men">\
                 <option value="1" selected >g</option>\
                 <option value="2" >kg</option>\
                 <option value="3" >ml</option>\
                 <option value="4" >l</option>\
             </select>\
             <div class="div_inliner">\
                 <label class="label_input_checkbox">\
                     <input type="checkbox">\
                     <span class="checkmark_checkbox"></span>\
                 </label>\
                 <p class="nume_ingredient_add_men">Alergen</p>\
             </div>\
             <i class="far fa-trash-alt trash_ingredient_add_men" onclick="deleteIngredient(this)"></i>\
        </div>'
    );
}

//Functie de stergere a unui ingredient din adaugarea meniului
function deleteIngredient(ing){
    var ing_id_nr = parseInt($(ing).parent('div')[0].id.substring(16));
    var ing_id = $(ing).parent('div')[0].id;

    $('.input_adaugare_ingrediente_add_men')[0].options[ing_id_nr].disabled = false;

    $(ing).parent('div').remove();
}


//Functii pentru promotii
function addPromotion(meniu){
    var parent = $(meniu).parent('div');
    var big_parent = parent.parent('div');
    var nume = parent.find('.nume_meniu').text();
    var pret = parent.find('.pret_meniu').text();

    big_parent[0].innerHTML=
        ' <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE PROMOTIE</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Perioada promotiei:</p>\
                                        </div>\
                                        <div class="col-md-2 col-6">\
                                            <p class="label_input_creare_meniu">De la:</p>\
                                        </div>\
                                        <div class="col-md-4 col-6">\
                                            <input type="text" class="input_clasic_add_men datepicker" placeholder="LL/ZZ/AAAA" data-provide="datepicker"/>\
                                        </div>\
                                        <div class="col-md-2 col-6">\
                                            <p class="label_input_creare_meniu">Pana la:</p>\
                                        </div>\
                                        <div class="col-md-4 col-6">\
                                            <input type="text" class="input_clasic_add_men datepicker" placeholder="LL/ZZ/AAAA" data-provide="datepicker"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Text al promotiei:</p>\
                                            <textarea class="descriere_meniu_add_men"></textarea>\
                                        </div>\
                                    </div>\
                                    <hr />\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume meniu: <b class="nume_meniu_promotie"> '+ nume +' </b></p>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-4 col-5">\
                                            <p class="label_input_creare_meniu">Pret:</p>\
                                            <input type="number" class="input_pret_add_men" value="'+ pret +'"/>\
                                        </div>\
                                        <div class="col-md-8 col-7">\
                                            <p class="label_input_creare_meniu">Unitate monetara:</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option>RON</option>\
                                                <option>EUR</option>\
                                                <option>USD</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="nume_ingredient_add_men"><i class="fas fa-caret-right"></i> Salam de Dedulesti</p>\
                                            <input type="number" class="gramaj_ingredient_add_men" placeholder="100"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option selected >g</option>\
                                                <option>kg</option>\
                                                <option>ml</option>\
                                                <option>l</option>\
                                            </select>\
                                            <div class="div_inliner">\
                                                <label class="label_input_checkbox">\
                                                    <input type="checkbox">\
                                                    <span class="checkmark_checkbox"></span>\
                                                </label>\
                                                <p class="nume_ingredient_add_men">Alergen</p>\
                                            </div>\
                                            <i class="far fa-trash-alt trash_ingredient_add_men"></i>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Imagini:</p>\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <button class="add_photo_add_men" onclick="triggerInputFileAddMen()"><i class="fas fa-plus"></i></button>\
                                            <input type="file" hidden id="upload_menu_photo"/>\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Cate persoane pot manca?</p>\
                                            <input type="number" class="input_nr_persoane_add_men" placeholder="20"/>\
                                        </div>\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Timp de gatire:</p>\
                                            <input type="text" class="input_durata_add_men" placeholder="20 min"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="savePromotion(this)">ADAUGA</button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>';
}

function savePromotion(meniu){
    var parent = $(meniu).parent('div').parent('div').parent('div');
    var big_parent = parent.parent('div').parent('div');
    var nume = parent.find('.nume_meniu_promotie').text();
    var pret = parent.find('.input_pret_add_men').val();
    var valuta = parent.find('.select_unit_monetara_add_men').val();

    big_parent[0].innerHTML =
        '<div class="categorie_adaugata">\
                            <span class="nume_meniu">'+nume+'</span> (<span class="gramaj_meniu">'+gramaj_total+'g</span>) - <span class="pret_meniu">'+pret+'</span> <span class="valuta_meniu">' +valute[valuta-1]+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualMeniu(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>';

}