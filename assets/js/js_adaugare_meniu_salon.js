function showAdaugaMancare(){
    var sectiune_mancare = $('#adauga_mancare');
    var sectiune_bautura = $('#adauga_bautura');
    var buton_activ = $('.buton_activ_meniu');
    var buton_inactiv = $('.buton_inactiv_meniu');

    sectiune_mancare.css('display', 'block');
    sectiune_bautura.css('display', 'none');
    buton_inactiv.addClass('buton_activ_meniu').removeClass('buton_inactiv_meniu');
    buton_activ.addClass('buton_inactiv_meniu').removeClass('buton_activ_meniu');
}

function adaugaCategorieMeniu(here){
    var aici_se_adauga = $(here).parent('div');
    aici_se_adauga.append(
        '<div class="parinte_categorie_adaugata">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Ciorbe" /> \
                        <p class="save_add" onclick="saveCategorieMeniu(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>\
                        <button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );
}

function deleteCategorieMeniu(val){
    var to_delete = $(val).parent('div').parent('div');
    to_delete.remove();
}

function saveCategorieMeniu(val){
    var element_parent = $(val).parent('div');
    var valoare = element_parent.children('input').val();

    if(valoare == '' || valoare == ' '){
        element_parent.parent('div').remove();
    }else{
        element_parent[0].innerHTML =
            '<span>'+ valoare +'</span>\
        <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
        <i class="fas fa-pencil-alt" onclick="editCategorieMeniu(this)"></i>';
    }
}

function editCategorieMeniu(element){
    var element_parent = $(element).parent('div');
    var element_value = element_parent.children('span').html();
    console.log(element_parent);

    element_parent[0].innerHTML =
        '<input type="text" class="input_interior_div" placeholder="Ex: Ciorbe" value="'+
        element_value
        +'"/>'+
        '<p class="save_add" onclick="saveCategorieMeniu(this)">SAVE</p>';
}

function adaugaSubcategorieMeniu(cat){
    var change = $(cat).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Ciorbe de porc" /> \
                        <p class="save_add" onclick="saveCategorieMeniu(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaSubcategorieMeniu(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>\
                        <button class="adauga_subcategorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );

}

function adaugaMeniu(meniu){
    var change = $(meniu).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaMeniu(this)">ADAUGA MENIU <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie">\
                            <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE MENIU</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume meniu:</p>\
                                            <input type="text" class="input_name_creare_meniu" placeholder="EX: Ciorba de sfecla"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-3 col-3">\
                                            <p class="label_input_creare_meniu">Ingrediente:</p>\
                                        </div>\
                                        <div class="col-md-9 col-9">\
                                            <input type="text" class="input_adaugare_ingrediente_add_men" placeholder="Cauta un ingredient, ex: marar"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="nume_ingredient_add_men"><i class="fas fa-caret-right"></i> Salam de Dedulesti</p>\
                                            <input type="number" class="gramaj_ingredient_add_men" placeholder="100"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option selected >g</option>\
                                                <option>kg</option>\
                                                <option>ml</option>\
                                                <option>l</option>\
                                            </select>\
                                            <div class="div_inliner">\
                                                <label class="label_input_checkbox">\
                                                    <input type="checkbox">\
                                                    <span class="checkmark_checkbox"></span>\
                                                </label>\
                                                <p class="nume_ingredient_add_men">Alergen</p>\
                                            </div>\
                                            <i class="far fa-trash-alt trash_ingredient_add_men"></i>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Imagini:</p>\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <button class="add_photo_add_men" onclick="triggerInputFileAddMen()"><i class="fas fa-plus"></i></button>\
                                            <input type="file" hidden id="upload_menu_photo"/>\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Scurt istoric al meniului:</p>\
                                            <textarea class="descriere_meniu_add_men"></textarea>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Cate persoane pot manca?</p>\
                                            <input type="number" class="input_nr_persoane_add_men" placeholder="20"/>\
                                        </div>\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Timp de gatire:</p>\
                                            <input type="text" class="input_durata_add_men" placeholder="20 min"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Specialitatea casei</p>\
                                        </div>\
                                        <div class="col-md-7">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Mancare caracteristica zonei</p>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualMeniu(this)">ADAUGA</button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>'
    );
}

function saveActualMeniu(meniu){
    var parent = $(meniu).parent('div').parent('div').parent('div');
    var big_parent = parent.parent('div').parent('div');
    var nume = parent.find('.input_name_creare_meniu').val();
    var valuta = parent.find('.select_unit_monetara_add_men').val();

    if(nume == '' || nume == ' '){
        big_parent.remove();
    }else{
        big_parent[0].innerHTML =
            '<div class="categorie_adaugata">\
                                <span class="nume_meniu">'+nume+'</span> (<span class="gramaj_meniu">200g</span>)\
                                <i class="far fa-trash-alt" onclick="deleteCategorieMeniu(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualMeniu(this)"></i>\
                            </div>';
    }
}


function editActualMeniu(meniu){
    var parent = $(meniu).parent('div');
    var big_parent = parent.parent('div');
    var nume = parent.find('.nume_meniu').text();

    big_parent[0].innerHTML=
        ' <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE MENIU</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume meniu:</p>\
                                            <input type="text" class="input_name_creare_meniu" placeholder="EX: Ciorba de sfecla" value="'+ nume +'"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-3 col-3">\
                                            <p class="label_input_creare_meniu">Ingrediente:</p>\
                                        </div>\
                                        <div class="col-md-9 col-9">\
                                            <input type="text" class="input_adaugare_ingrediente_add_men" placeholder="Cauta un ingredient, ex: marar"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="nume_ingredient_add_men"><i class="fas fa-caret-right"></i> Salam de Dedulesti</p>\
                                            <input type="number" class="gramaj_ingredient_add_men" placeholder="100"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option selected >g</option>\
                                                <option>kg</option>\
                                                <option>ml</option>\
                                                <option>l</option>\
                                            </select>\
                                            <div class="div_inliner">\
                                                <label class="label_input_checkbox">\
                                                    <input type="checkbox">\
                                                    <span class="checkmark_checkbox"></span>\
                                                </label>\
                                                <p class="nume_ingredient_add_men">Alergen</p>\
                                            </div>\
                                            <i class="far fa-trash-alt trash_ingredient_add_men"></i>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Imagini:</p>\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <button class="add_photo_add_men" onclick="triggerInputFileAddMen()"><i class="fas fa-plus"></i></button>\
                                            <input type="file" hidden id="upload_menu_photo"/>\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                        <div class="col-md-3 col-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Scurt istoric al meniului:</p>\
                                            <textarea class="descriere_meniu_add_men"></textarea>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Cate persoane pot manca?</p>\
                                            <input type="number" class="input_nr_persoane_add_men" placeholder="20"/>\
                                        </div>\
                                        <div class="col-md-6">\
                                            <p class="label_input_creare_meniu">Timp de gatire:</p>\
                                            <input type="text" class="input_durata_add_men" placeholder="20 min"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Specialitatea casei</p>\
                                        </div>\
                                        <div class="col-md-7">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Mancare caracteristica zonei</p>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualMeniu(this)">ADAUGA</button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>';
}
