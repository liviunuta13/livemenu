function adaugaCategorieProdus(here){
    var aici_se_adauga = $(here).parent('div');
    aici_se_adauga.append(
        '<div class="parinte_categorie_adaugata">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Vinuri" /> \
                        <p class="save_add" onclick="saveCategorieProdus(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaSubcategorieProdus(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>\
                        <button class="adauga_subcategorie_button" onclick="adaugaProdus(this)">ADAUGA PRODUS <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );
}

function deleteCategorieProdus(val){
    var to_delete = $(val).parent('div').parent('div');
    to_delete.remove();
}

function saveCategorieProdus(val){
    var element_parent = $(val).parent('div');
    var valoare = element_parent.children('input').val();

    if(valoare == '' || valoare == ' '){
        element_parent.parent('div').remove();
    }else{
        element_parent[0].innerHTML =
            '<span>'+ valoare +'</span>\
        <i class="far fa-trash-alt" onclick="deleteCategorieProdus(this)"></i>\
        <i class="fas fa-pencil-alt" onclick="editCategorieProdus(this)"></i>';
    }
}

function editCategorieProdus(element){
    var element_parent = $(element).parent('div');
    var element_value = element_parent.children('span').html();
    console.log(element_parent);

    element_parent[0].innerHTML =
        '<input type="text" class="input_interior_div" placeholder="Ex: Vinuri" value="'+
        element_value
        +'"/>'+
        '<p class="save_add" onclick="saveCategorieProdus(this)">SAVE</p>';
}

function adaugaSubcategorieProdus(cat){
    var change = $(cat).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaSubcategorieProdus(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie">\
                    <div class="categorie_adaugata">\
                        <input type="text" class="input_interior_div" placeholder="Ex: Vin Demisec" /> \
                        <p class="save_add" onclick="saveCategorieProdus(this)">SAVE</p>\
                    </div>\
                    <div class="adaugare_subcategorie_si_meniu">\
                        <button class="adauga_subcategorie_button" onclick="adaugaSubcategorieProdus(this)">ADAUGA SUBCATEGORIE <i class="far fa-plus-square"></i></button>\
                        <button class="adauga_subcategorie_button" onclick="adaugaProdus(this)">ADAUGA PRODUS <i class="far fa-plus-square"></i></button>\
                    </div>\
                    <div class="subcategorii_si_meniuri_adaugate">\
                    </div>\
        </div>'
    );

}

function adaugaProdus(meniu){
    var change = $(meniu).parent('div');
    var adaugare = change.parent('div').children('.subcategorii_si_meniuri_adaugate');

    change[0].innerHTML = '<button class="adauga_categorie_button" onclick="adaugaProdus(this)">ADAUGA PRODUS <i class="far fa-plus-square"></i></button>';

    adaugare.append(
        '<div class="elemente_subcategorie">\
                            <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE PRODUS</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume produs:</p>\
                                            <input type="text" class="input_name_creare_meniu" placeholder="EX: Busuioaca de Bohotin"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-4">\
                                            <p class="label_input_creare_meniu">Pret:</p>\
                                            <input type="number" class="input_pret_add_men" />\
                                        </div>\
                                        <div class="col-md-8">\
                                            <p class="label_input_creare_meniu">Unitate monetara:</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option selected>RON</option>\
                                                <option>EUR</option>\
                                                <option>USD</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-7">\
                                            <p class="label_input_creare_meniu">Cantitate:</p>\
                                            <input type="number" class="gramaj_ingredient_add_men" placeholder="100"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option selected>g</option>\
                                                <option>kg</option>\
                                                <option>ml</option>\
                                                <option>l</option>\
                                            </select>\
                                        </div>\
                                        <div class="col-md-5">\
                                            <p class="label_input_creare_meniu">Pret per</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option selected>Unitate</option>\
                                                <option>Cantitate</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Imagini:</p>\
                                        </div>\
                                        <div class="col-md-3">\
                                            <button class="add_photo_add_men" onclick="triggerInputFileAddMen()"><i class="fas fa-plus"></i></button>\
                                            <input type="file" hidden id="upload_menu_photo"/>\
                                        </div>\
                                        <div class="col-md-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                        <div class="col-md-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Scurt istoric al produsului:</p>\
                                            <textarea class="descriere_meniu_add_men"></textarea>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Produs special</p>\
                                        </div>\
                                        <div class="col-md-7">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Produs caracteristica zonei</p>\
                                        </div>\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Produs unicat</p>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualProdus(this)">ADAUGA</button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>'
    );
}

function saveActualProdus(meniu){
    var parent = $(meniu).parent('div').parent('div').parent('div');
    var big_parent = parent.parent('div').parent('div');
    var nume = parent.find('.input_name_creare_meniu').val();
    var pret = parent.find('.input_pret_add_men').val();
    var valuta = parent.find('.select_unit_monetara_add_men').val();

    if(nume == '' || pret == '' || nume == ' ' || pret == ' '){
        big_parent.remove();
    }else{
        big_parent[0].innerHTML =
            '<div class="categorie_adaugata">\
                                <span class="nume_meniu">'+nume+'</span> (<span class="gramaj_meniu">200g</span>) - <span class="pret_meniu">'+pret+'</span> <span class="valuta_meniu">' +valuta+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteCategorieProdus(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualProdus(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>';
    }
}


function editActualProdus(meniu){
    var parent = $(meniu).parent('div');
    var big_parent = parent.parent('div');
    var nume = parent.find('.nume_meniu').text();
    var pret = parent.find('.pret_meniu').text();

    big_parent[0].innerHTML=
        '<div class="elemente_subcategorie">\
                            <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE/EDITARE PRODUS</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume produs:</p>\
                                            <input type="text" class="input_name_creare_meniu" placeholder="EX: Busuioaca de Bohotin" value="'+ nume +'"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-4">\
                                            <p class="label_input_creare_meniu">Pret:</p>\
                                            <input type="number" class="input_pret_add_men" value="'+ pret +'"/>\
                                        </div>\
                                        <div class="col-md-8">\
                                            <p class="label_input_creare_meniu">Unitate monetara:</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option selected>RON</option>\
                                                <option>EUR</option>\
                                                <option>USD</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-7">\
                                            <p class="label_input_creare_meniu">Cantitate:</p>\
                                            <input type="number" class="gramaj_ingredient_add_men" placeholder="100"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option selected>g</option>\
                                                <option>kg</option>\
                                                <option>ml</option>\
                                                <option>l</option>\
                                            </select>\
                                        </div>\
                                        <div class="col-md-5">\
                                            <p class="label_input_creare_meniu">Pret per</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option selected>Unitate</option>\
                                                <option>Cantitate</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Imagini:</p>\
                                        </div>\
                                        <div class="col-md-3">\
                                            <button class="add_photo_add_men" onclick="triggerInputFileAddMen()"><i class="fas fa-plus"></i></button>\
                                            <input type="file" hidden id="upload_menu_photo"/>\
                                        </div>\
                                        <div class="col-md-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                        <div class="col-md-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Scurt istoric al produsului:</p>\
                                            <textarea class="descriere_meniu_add_men"></textarea>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Produs special</p>\
                                        </div>\
                                        <div class="col-md-7">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Produs caracteristica zonei</p>\
                                        </div>\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Produs unicat</p>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="saveActualProdus(this)">ADAUGA</button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>';
}

function addPromotion(meniu){
    var parent = $(meniu).parent('div');
    var big_parent = parent.parent('div');
    var nume = parent.find('.nume_meniu').text();
    var pret = parent.find('.pret_meniu').text();

    big_parent[0].innerHTML=
        ' <div class="creare_meniu_div">\
                                <div class="header_creare_meniu">\
                                    <p class="title_header_add_men">ADAUGARE PROMOTIE</p>\
                                </div>\
                                <div class="body_creare_meniu">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Perioada promotiei:</p>\
                                        </div>\
                                        <div class="col-md-2">\
                                            <p class="label_input_creare_meniu">De la:</p>\
                                        </div>\
                                        <div class="col-md-4">\
                                            <input type="text" class="input_clasic_add_men datepicker" placeholder="LL/ZZ/AAAA" data-provide="datepicker"/>\
                                        </div>\
                                        <div class="col-md-2">\
                                            <p class="label_input_creare_meniu">Pana la:</p>\
                                        </div>\
                                        <div class="col-md-4">\
                                            <input type="text" class="input_clasic_add_men datepicker" placeholder="LL/ZZ/AAAA" data-provide="datepicker"/>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Text al promotiei:</p>\
                                            <textarea class="descriere_meniu_add_men"></textarea>\
                                        </div>\
                                    </div>\
                                    <hr />\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Nume meniu: <b class="nume_meniu_promotie"> '+ nume +' </b></p>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-4">\
                                            <p class="label_input_creare_meniu">Pret:</p>\
                                            <input type="number" class="input_pret_add_men" value="'+ pret +'"/>\
                                        </div>\
                                        <div class="col-md-8">\
                                            <p class="label_input_creare_meniu">Unitate monetara:</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option>RON</option>\
                                                <option>EUR</option>\
                                                <option>USD</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-7">\
                                            <p class="label_input_creare_meniu">Cantitate:</p>\
                                            <input type="number" class="gramaj_ingredient_add_men" placeholder="100"/>\
                                            <select class="select_um_gramaj_ing_add_men">\
                                                <option selected>g</option>\
                                                <option>kg</option>\
                                                <option>ml</option>\
                                                <option>l</option>\
                                            </select>\
                                        </div>\
                                        <div class="col-md-5">\
                                            <p class="label_input_creare_meniu">Pret per</p>\
                                            <select class="select_unit_monetara_add_men">\
                                                <option selected>Unitate</option>\
                                                <option>Cantitate</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <p class="label_input_creare_meniu">Imagini:</p>\
                                        </div>\
                                        <div class="col-md-3">\
                                            <button class="add_photo_add_men" onclick="triggerInputFileAddMen()"><i class="fas fa-plus"></i></button>\
                                            <input type="file" hidden id="upload_menu_photo"/>\
                                        </div>\
                                        <div class="col-md-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                        <div class="col-md-3">\
                                            <i class="far fa-times-circle delete_uploaded_image_menu"></i>\
                                            <img src="../assets/images/pozares1.jpg" class="img_uploaded_menu" />\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Produs special</p>\
                                        </div>\
                                        <div class="col-md-7">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Produs caracteristica zonei</p>\
                                        </div>\
                                        <div class="col-md-5">\
                                            <label class="label_input_checkbox">\
                                                <input type="checkbox">\
                                                <span class="checkmark_checkbox"></span>\
                                            </label>\
                                            <p class="nume_ingredient_add_men">Produs unicat</p>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <button class="button_add_men" onclick="savePromotion(this)">ADAUGA</button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>';
}

function savePromotion(meniu){
    var parent = $(meniu).parent('div').parent('div').parent('div');
    var big_parent = parent.parent('div').parent('div');
    var nume = parent.find('.nume_meniu_promotie').text();
    var pret = parent.find('.input_pret_add_men').val();
    var valuta = parent.find('.select_unit_monetara_add_men').val();

    big_parent[0].innerHTML =
        '<div class="categorie_adaugata">\
                            <span class="nume_meniu">'+nume+'</span> (<span class="gramaj_meniu">200g</span>) - <span class="pret_meniu">'+pret+'</span> <span class="valuta_meniu">' +valuta+'</span>\
                                <i class="far fa-trash-alt" onclick="deleteCategorieProdus(this)"></i>\
                                <i class="fas fa-pencil-alt" onclick="editActualProdus(this)"></i>\
                                <img src="../assets/images/percent.png" class="img_iconita_add_men" onclick="addPromotion(this)"/>\
                                <i class="fas fa-info-circle"></i>\
                            </div>';

}
