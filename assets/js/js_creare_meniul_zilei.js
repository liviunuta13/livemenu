function showSubelementeMZ(div){
    var sageata = $(div);
    var parent = $(div).parent('div').find('.subelemente_meniul_zilei');
    if(sageata.hasClass('fa-angle-down')){
        sageata.removeClass('fa-angle-down').addClass('fa-angle-up');
        parent.css('display', 'block');
    }else{
        sageata.removeClass('fa-angle-up').addClass('fa-angle-down');
        parent.css('display', 'none');
    }
}
function adaugaMeniuLaMZ(element){
    var date = $(element).parent('div').children('span').text();
    var generare = $('#appendMeniurileZilei');
    generare.append(
        '<div class="categorie_adaugata_mn">\
                <span>'+ date +'</span>\
                <i class="fas fa-pencil-alt icon_edit_mn"></i>\
                <i class="far fa-trash-alt" onclick="deleteCategorieMeniulZilei(this)"></i>\
                <select class="select_time_day">\
                    <option selected>Mic dejun</option>\
                    <option>Pranz</option>\
                    <option>Cina</option>\
                </select>\
            </div>'
    );
}
function deleteCategorieMeniulZilei(val){
    var to_delete = $(val).parent('div');
    to_delete.remove();
}
